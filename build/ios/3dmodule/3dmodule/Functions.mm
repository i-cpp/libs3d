#include "Functions.h"
#include <vector>

void log_func( const char *msg )
{
    NSLog(@"%@",[[NSString alloc] initWithCString:msg encoding:NSUTF8StringEncoding]);
}

void *open_func( const char *filename )
{
    NSString *name = [[NSString alloc] initWithCString:filename encoding:NSUTF8StringEncoding];
    NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:name]]; 
	return (__bridge void*)handle;
}

bool read_func( void* io, void *data, unsigned int size )
{
    if ( !io ) return false;
    NSFileHandle *handle = (__bridge NSFileHandle *)io;
    NSData *readData = [handle readDataOfLength:size];
    memcpy(data, [readData bytes], readData.length);
    return readData.length > 0;
}
				
void close_func( void* io )
{
    NSFileHandle *handle = (__bridge NSFileHandle *)io;
    [handle closeFile];
}

unsigned int get_time()
{
    static NSDate *startTime=[NSDate date];
    return (unsigned int)(-[startTime timeIntervalSinceNow]*1000.0);
}

unsigned char *load_rgba( const char *filename, int &width, int &height )
{
	NSString *name = [[NSString alloc] initWithCString:filename encoding:NSUTF8StringEncoding];
    UIImage *image = [UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:name]];
	
	width = image.size.width;
    height = image.size.height;
    CGImageRef spriteImage = image.CGImage;
    
    unsigned char *spriteData = (unsigned char *) calloc(width * height * 4, sizeof(unsigned char));
    CGContextRef spriteContext = CGBitmapContextCreate(spriteData, width, height, 8, 
			width * 4, CGImageGetColorSpace(spriteImage), kCGImageAlphaPremultipliedLast);
    CGContextDrawImage(spriteContext, CGRectMake(0.0, 0.0, (CGFloat)width, (CGFloat)height), spriteImage);
    
    CGContextRelease(spriteContext);
    
	return spriteData;
}

unsigned int load_texture_from_data( unsigned char *data, int w, int h, int bpp, bool linearFilter, bool genMipMaps )
{
	GLenum format = 0;
	if ( bpp == 1 ) format = GL_LUMINANCE;
	else if ( bpp == 3 ) format = GL_RGB;
	else if ( bpp == 4 ) format = GL_RGBA;
	unsigned int Id;
	glGenTextures( 1, &Id );
	glBindTexture( GL_TEXTURE_2D, Id );
	glTexImage2D( GL_TEXTURE_2D, 0, bpp == 4 ? GL_RGBA : GL_RGB, w, h, 0, 
		bpp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data );
	if ( !genMipMaps )
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, linearFilter ? GL_LINEAR : GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, linearFilter ? GL_LINEAR : GL_NEAREST );
	}
	else
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, linearFilter ? GL_LINEAR : GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, linearFilter ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_NEAREST );
		glGenerateMipmap( GL_TEXTURE_2D );
	}
	return Id;
}

unsigned int g_numTextures = 0;
struct TEXTURE {
    unsigned int id;
    char *name;
};

std::vector<TEXTURE *> g_Textures(0);

unsigned int load_texture( const char *filename, struct S3D_Image *img, bool gen_texture )
{
    if ( !img )
	{
		for ( int i = 0; i < g_numTextures; i++ )
		{
			if ( !strcmp( g_Textures[i]->name, filename ) )
			{
				return g_Textures[i]->id;
			}
		}
	}
	unsigned int texture = 0;
	int bpp = 4;
	int width, height;
	unsigned char *pixels = load_rgba( filename, width, height );
	if ( pixels )
	{
		if ( gen_texture )
		{
			texture = load_texture_from_data( pixels, width, height, bpp, true, true );
            TEXTURE *tex = new TEXTURE;
			tex->id = texture;
			tex->name = strdup( filename );
            
            std::vector<TEXTURE *>::iterator iter;
            iter = g_Textures.end();
            g_Textures.insert(iter, tex);
			g_numTextures++;
		}
		if ( img )
		{
			img->data = (unsigned char*)malloc( width * height * bpp );
			memcpy( img->data, pixels, width * height * bpp );
			img->width = width;
			img->height = height;
			img->bpp = bpp;
		}
		free( pixels );
	}
	return texture;
}


void free_textures()
{
    for ( int i = 0; i < g_numTextures; i++ )
	{
		if ( g_Textures[i] )
		{
			if ( glIsTexture( g_Textures[i]->id ) )
				glDeleteTextures( 1, &g_Textures[i]->id );
			if ( g_Textures[i]->name )
				free( g_Textures[i]->name );
		}
	}
    g_Textures.clear();
    g_numTextures = 0;
}
