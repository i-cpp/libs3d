#import "ViewController.h"

#import "Functions.h"

@interface ViewController () {
    CS3DVec4f viewport;
    std::vector<CS3DModel *> group;
    struct Touch {
        Touch()
        {
            x = y = sx = sy = 0;
            state = 0;
        }
        Touch(int _x, int _y, int _state )
        {
            x = sx = _x;
            y = sy = _y;
            state = _state;
        }
        int x, y;
        int sx, sy;
        int state;
    };
    std::vector< Touch > Touches;
    float lastTimeStamp;
    float startScale;
    float scale;
    bool initS3D;
    CS3DMat16f initLightMat;
}

@property (strong, nonatomic) EAGLContext *context;


@end

@implementation ViewController

@synthesize context = _context;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];

    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    view.drawableMultisample = GLKViewDrawableMultisample4X;
    
    [self setupGL];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self resize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc. that aren't in use.
    
    /*if ( [self.view window] == nil )
    {
        [self tearDownGL];
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
        [self.view removeFromSuperview];
        self.view = nil;
    }*/
}

- (void)setView:(UIView *)view
{
    if ([self isViewLoaded] && view==nil) {
        [self tearDownGL];
    }
    super.view = view;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self performSelector:@selector(resize) withObject:nil afterDelay:0.25];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self resize];
}

- (void)resize
{
    viewport = CS3DVec4f( self.view.bounds.origin.x, self.view.bounds.origin.y, 
                         self.view.bounds.size.width, self.view.bounds.size.height );
    glViewport( viewport[0], viewport[1], viewport[2], viewport[3] );
    s3dSetViewport( viewport[0], viewport[1], viewport[2], viewport[3] );
    s3dPerspective( S3D_FOVY, S3D_NEAR, S3D_FAR );
}

- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    
    // init openGL
    glClearColor( 1, 1, 1, 1 );
    glEnable( GL_DEPTH_TEST );
    
    glEnable ( GL_BLEND );
	glBlendFunc ( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    
//    [self resize];
	    
    initS3D = false;
    lastTimeStamp = 0;
    startScale = 1.0;
    scale = 1.0;
}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
    
    s3dFree();
    for ( size_t i = 0; i < group.size(); i++ )
    {
        delete group[i];
    }
    group.clear();
    initS3D = false;
}

-(void)handlePinch:(UIPinchGestureRecognizer*)sender
{
    scale = min( max( startScale + [sender scale] - 1, 0.1 ), 5 );
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        startScale = scale;
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSSet *allTouches = [event allTouches];
    int touchId = 0;
    for (UITouch *touch in allTouches) 
    { 
        CGPoint pt = [touch locationInView:touch.view];
        if ( Touches.size() > touchId )
        {
            Touches[touchId].x = pt.x;
            Touches[touchId].y = pt.y;
        }
        else Touches.push_back(Touch(pt.x, pt.y, 0));
        touchId++;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSSet *allTouches = [event allTouches];
    int touchId = 0;
    for (UITouch *touch in allTouches) 
    { 
        CGPoint pt = [touch locationInView:touch.view];
        Touch t(pt.x, pt.y, 1);
        if ( Touches.size() > touchId )
        {
            Touches[touchId] = t;
        }
        else Touches.push_back(t);
        touchId++;
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSSet *allTouches = [event allTouches];
    int touchId = 0;
    for (UITouch *touch in allTouches) 
    { 
        CGPoint pt = [touch locationInView:touch.view];
        if ( Touches.size() > touchId )
        {
            Touches[touchId].x = pt.x;
            Touches[touchId].y = pt.y;
            Touches[touchId].state = 3;
        }
        else Touches.push_back(Touch(pt.x, pt.y, 3));
        touchId++;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSSet *allTouches = [event allTouches];
    int touchId = 0;
    for (UITouch *touch in allTouches) 
    { 
        CGPoint pt = [touch locationInView:touch.view];
        if ( Touches.size() > touchId )
        {
            Touches[touchId].x = pt.x;
            Touches[touchId].y = pt.y;
            Touches[touchId].state = 3;
        }
        else Touches.push_back(Touch(pt.x, pt.y, 3));
        touchId++;
    }
}

- (void)updateCamera
{
    static float dx = 0;
    static float dy = 0;
    bool animated = false;
    for ( size_t i = 0; i < Touches.size(); i++ )
    {
        Touch t = Touches[i];
        if ( t.state )
        {
            dx += ( t.x - t.sx ) / 100.f;
            dy += ( t.y - t.sy ) / 100.f;
            animated = false;
        }
    }
    if ( animated )
    {
        if ( !lastTimeStamp ) lastTimeStamp = s3dCurTime();
        float dt = ( s3dCurTime() - lastTimeStamp ) * 0.001f;
        dx += dt * 10;
    }
    while ( dx > 360 ) dx -= 360;
    while ( dy > 360 ) dy -= 360;
    lastTimeStamp = s3dCurTime();
    
    CS3DMat16f tMat;
    tMat.scale(CS3DVec3f(scale));
    tMat.rotate( dy, S3D_AXIS_X );
    tMat.rotate( dx, S3D_AXIS_Y );
    tMat.inverse();
    tMat.translate( CS3DVec3f(0,100,200) );
    s3dCamera->SetPosition(tMat.translation());
    s3dCamera->SetTarget(CS3DVec3f(0,10,0));
    s3dCamera->Update();
}

- (void)update
{
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    int defFBO;
    glGetIntegerv( GL_FRAMEBUFFER_BINDING, &defFBO );
    if ( !initS3D )
    {        
        initS3D = true;
        // init S3D
        s3dSetIOFunctions( open_func, read_func, close_func, log_func, 0 );
        if ( s3dInit() )
        {
            s3dSetLoadTextureFunc( load_texture );
            s3dSetLoadTextureFunc( load_texture_from_data );
            s3dSetTimeFunc( get_time );
#if ( S3D_USE_LIGTHING )
            s3dLight->LoadLight( "models/light.cfg" );
#endif
            s3dCamera->Reset(S3D_TARGET_CAMERA);
#if ( S3D_USE_SHADOWS )
            if ( s3dShadow )
            {
                //s3dShadow->fovi = 60.f;
                //s3dShadow->soft = false;
            }
#endif
        }
        
        UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
        [self.view addGestureRecognizer:pinchGesture];
        
        const int num_models = 7;
        const char *modelNames[num_models] = {
            "butterfly8/plane.cfg",
            "butterfly8/body.cfg", "butterfly8/head.cfg", 
            "butterfly8/legs_r.cfg", "butterfly8/legs_l.cfg", 
            "butterfly8/wings_r.cfg", "butterfly8/wings_l.cfg"
        };
        
        for ( int i = 0; i < num_models; i++ )
        {
            CS3DModel *model = new CS3DModel;
            char filename[0x100];
            sprintf( filename, "models/%s", modelNames[i] );
            model->Load( filename );
            model->SetPosition( model->GetPivot() );
            group.push_back( model );
        }
        
        [self updateCamera];
        
        // static shadow:
#if ( S3D_USE_SHADOWS )
        if ( s3dShadow )
        {
            s3dShadow->Draw( (CS3DTransformedObject**)&group[0], group.size() );
            [view bindDrawable];
        }
#endif
        initLightMat = s3dCamera->Get() * s3dLight->GetLight(0)->mat;
    }
    else
        [self updateCamera];
    
    CS3DMat16f invCameraMat = s3dCamera->Get();
    invCameraMat.inverse();
    s3dLight->GetLight(0)->mat = invCameraMat * initLightMat;
    
    int i = group.size();
    while ( i-- > 0 && group[i] )
    {
        group[i]->Draw();
    }
    
    for ( size_t i = 0; i < Touches.size(); i++ )
    {
        if ( Touches[i].state == 1 )
            Touches[i].state = 2;
        else if ( Touches[i].state == 3 )
        {
            Touches.erase( Touches.begin() + i );
        }
    }
    //NSLog(@"fps=%i", [self framesPerSecond]);
    /*
    int er = S3D_OpenGLOp::GetError();
    if ( er != 0 )
    {
        NSLog(@"er=%i", er);
        return;
    }//*/
	glFinish();
}

@end
