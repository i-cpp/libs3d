#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#import <S3D.h>

void log_func( const char *msg );
void *open_func( const char *filename );
bool read_func( void* io, void *data, unsigned int size );
void close_func( void* io );
unsigned int get_time();
unsigned int load_texture( const char *filename, struct S3D_Image *img, bool gen_texture );
unsigned int load_texture_from_data( unsigned char *data, int w, int h, int bpp, bool linearFilter, bool genMipMaps );
void free_textures();

#endif