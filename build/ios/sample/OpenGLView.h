#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

extern bool g_Active;

@interface OpenGLView : UIView{   
    CAEAGLLayer* _eaglLayer;
    EAGLContext* _context;
	
    GLuint _colorRenderBuffer;
	GLuint _depthRenderBuffer;
	GLuint framebuffer;
	
	CFTimeInterval previousTimestamp;
	float Fps;
	float FrameCounter;
}

@end
