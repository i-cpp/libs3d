#import <UIKit/UIKit.h>
#import "SPAppDelegate.h"

int main(int argc, char *argv[])
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    int res =  UIApplicationMain(argc, argv, nil, NSStringFromClass([SPAppDelegate class]));
    [pool release];
    return res;
    
}
