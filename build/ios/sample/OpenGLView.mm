#import "OpenGLView.h"

#include <Functions.h>

@implementation OpenGLView

CS3DModel *Model = 0;
void DrawModels();

bool g_Active = false;

+ (Class)layerClass {
    return [CAEAGLLayer class];
}

- (void)setupLayer {
    _eaglLayer = (CAEAGLLayer*) self.layer;
    _eaglLayer.opaque = YES;
}

- (void)setupContext {   
    EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;
    _context = [[EAGLContext alloc] initWithAPI:api];
    if (!_context) {
        NSLog(@"Failed to initialize OpenGLES 2.0 context");
        exit(1);
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        NSLog(@"Failed to set current OpenGL context");
        exit(1);
    }
    
	// init openGL
    glClearColor( 0, 0, 0, 1.0 );
	g_FrameRect = CS3DVec4f( self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height );
    glViewport( g_FrameRect[0], g_FrameRect[1], g_FrameRect[2], g_FrameRect[3] );
    glEnable ( GL_BLEND );
	glBlendFunc ( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
    
	// init S3D
	s3dSetIOFunctions( open_func, read_func, close_func, log_func, 0 );
	if ( s3dInit() )
	{
		s3dSetLoadTextureFunc( load_texture );
		s3dSetLoadTextureFunc( load_texture_from_data );
		s3dSetTimeFunc( get_time );
#if ( S3D_USE_LIGTHING )
		s3dLight->LoadLight( "models/light.cfg" );
#endif
        
		s3dCamera->Reset( S3D_TARGET_CAMERA );
		s3dCamera->SetPosition( S3D_AXIS_Y, 500 );
		s3dCamera->SetPosition( S3D_AXIS_Z, 900 );
		s3dCamera->Update();
	}
		
	g_TouchesCoords.x = self.frame.size.width / 2;
	g_TouchesCoords.y = self.frame.size.height / 2;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	CGPoint pt = [[touches anyObject] locationInView:self];
	g_TouchesCoords = POINT( pt.x, pt.y );
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	CGPoint pt = [[touches anyObject] locationInView:self];
	g_TouchesCoords = POINT( pt.x, pt.y );
	g_ButtonState = ie_DOWN;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	CGPoint pt = [[touches anyObject] locationInView:self];
	g_TouchesCoords = POINT( pt.x, pt.y );
	g_ButtonState = ie_UP;
}

-(void) beginFrame
{
	Model = new CS3DModel;
	Model.Load( "models/cheb/cheb.cfg" );
	
	[self calcFPS];
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
}

-(void) calcFPS
{
	CFTimeInterval t = CFAbsoluteTimeGetCurrent() - previousTimestamp;
	if ( t >= 1.0f )
	{
		previousTimestamp = CFAbsoluteTimeGetCurrent();
		Fps = FrameCounter * t;
		FrameCounter = 0;
	}
	else FrameCounter++;
}

- (void)draw
{
	if ( g_Active )
	{
		[self beginFrame];
		
		DrawModels();
		
		[self endFrame];
	}
}

-(void) endFrame
{
	glFinish();
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}

//*
void DrawModels()
{
	if ( Model )
		Model->Draw();
}
//*/
- (void)setupColorRenderBuffer {
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);        
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_eaglLayer];    
}

- (void)setupDepthRenderBuffer {
    glGenRenderbuffers(1, &_depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, self.frame.size.width, self.frame.size.height);
	glEnable(GL_DEPTH_TEST);
}

- (void)setupFrameBuffer {
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
                              GL_RENDERBUFFER, _colorRenderBuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 
                              GL_RENDERBUFFER, _depthRenderBuffer);
    
}

- (id)initWithFrame:(CGRect)frame
{
	FrameCounter = 0;
	previousTimestamp = 0;
    self = [super initWithFrame:frame];
    if (self) {        
        [self setupLayer];        
        [self setupContext];             
        [self setupDepthRenderBuffer]; 
        [self setupColorRenderBuffer];     
        [self setupFrameBuffer];

		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if ( status != GL_FRAMEBUFFER_COMPLETE )
		{
			NSLog(@"Failed to make compile framebuffer object %x", status);
		}
		
        [NSTimer scheduledTimerWithTimeInterval:0.001 
                                         target:self
                                       selector:@selector(draw)
                                       userInfo:nil
                                        repeats:YES];
    }
    return self;
}

// Replace dealloc method with this
- (void)dealloc
{
	s3dFree();
	
	// free textures!
	
    [_context release];
    _context = nil;
    [super dealloc];
}

@end
