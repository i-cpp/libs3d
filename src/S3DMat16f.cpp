#include <S3D.h>

CS3DMat16f::CS3DMat16f()
{
	identity();
}

float &CS3DMat16f::operator[]( int index )
{
    return m[index];
}

float CS3DMat16f::operator[]( int index ) const
{
    return m[index];
}

void CS3DMat16f::setRow( int row, float *v, int rank )
{
	memcpy( &m[ row * 4 ], v, sizeof( float ) * rank );
}

float *CS3DMat16f::getRow( int row )
{
	return &m[ row * 4 ];
}

void CS3DMat16f::set( const float matrix[16] )
{
	for ( int i = 0; i < 16; i++ )
		m[i] = matrix[i];
}

void CS3DMat16f::identity()
{
    m[ 0 ] = 1; m[ 4 ] = 0; m[  8 ] = 0; m[ 12 ] = 0;
    m[ 1 ] = 0; m[ 5 ] = 1; m[  9 ] = 0; m[ 13 ] = 0;
    m[ 2 ] = 0; m[ 6 ] = 0; m[ 10 ] = 1; m[ 14 ] = 0;
    m[ 3 ] = 0; m[ 7 ] = 0; m[ 11 ] = 0; m[ 15 ] = 1;
}

void CS3DMat16f::translate( const CS3DVec3f &v )
{
	m[12] += ( m[0] * v.x + m[4] * v.y + m[ 8] * v.z );
    m[13] += ( m[1] * v.x + m[5] * v.y + m[ 9] * v.z );
    m[14] += ( m[2] * v.x + m[6] * v.y + m[10] * v.z );
    m[15] += ( m[3] * v.x + m[7] * v.y + m[11] * v.z );
}

void CS3DMat16f::rotate( float angle, S3D_ENUM axis )
{
	CS3DMat16f rotMat;
	float sinAngle, cosAngle;
	sinAngle = sinf( angle * S3D_PI / 180.0f );
	cosAngle = cosf( angle * S3D_PI / 180.0f );
	if ( axis == S3D_AXIS_X )
	{
		rotMat[5] = cosAngle;
		rotMat[6] = sinAngle;
		rotMat[9] = -sinAngle;
		rotMat[10] = cosAngle;
	}
	else if ( axis == S3D_AXIS_Y )
	{
		rotMat[0] = cosAngle;
		rotMat[2] = -sinAngle;
		rotMat[8] = sinAngle;
		rotMat[10] = cosAngle;
	}
	else if ( axis == S3D_AXIS_Z )
	{
		rotMat[0] = cosAngle;
		rotMat[1] = sinAngle;
		rotMat[4] = -sinAngle;
		rotMat[5] = cosAngle;
	}
	*this = rotMat * *this;
}

void CS3DMat16f::scale( const CS3DVec3f &v )
{
	m[0] *= v.x;   m[4] *= v.y;   m[8]  *= v.z;
	m[1] *= v.x;   m[5] *= v.y;   m[9]  *= v.z;
	m[2] *= v.x;   m[6] *= v.y;   m[10] *= v.z;
	m[3] *= v.x;   m[7] *= v.y;   m[11] *= v.z;
}

CS3DVec3f CS3DMat16f::multVecWithoutTranslate( const CS3DVec3f in )
{
	CS3DVec3f out;
	out[0] = m[0] * in[0] + m[4] * in[1] + m[ 8] * in[2];
	out[1] = m[1] * in[0] + m[5] * in[1] + m[ 9] * in[2];
	out[2] = m[2] * in[0] + m[6] * in[1] + m[10] * in[2];
	return out;
}

void CS3DMat16f::transpose()
{
	float temp[16];
	temp[ 0] = m[0]; temp[ 1] = m[4]; temp[ 2] = m[ 8]; temp[ 3] = m[12];
	temp[ 4] = m[1]; temp[ 5] = m[5]; temp[ 6] = m[ 9]; temp[ 7] = m[13];
	temp[ 8] = m[2]; temp[ 9] = m[6]; temp[10] = m[10]; temp[11] = m[14];
	temp[12] = m[3]; temp[13] = m[7]; temp[14] = m[11]; temp[15] = m[15];
	set( temp );
}

CS3DMat16f CS3DMat16f::operator*( const CS3DMat16f &other ) const
{
	CS3DMat16f out;
    for ( int i = 0; i < 4; i++ )
	{
		for ( int j = 0; j < 4; j++ )
		{
			out[i * 4 + j] = 
			m[i * 4 + 0] * other[0 * 4 + j] +
			m[i * 4 + 1] * other[1 * 4 + j] +
			m[i * 4 + 2] * other[2 * 4 + j] +
			m[i * 4 + 3] * other[3 * 4 + j];
		}
    }
	return out;
}

CS3DVec3f CS3DMat16f::operator*( const CS3DVec3f &v ) const
{
	CS3DVec3f out;
	out[0] = m[0] * v[0] + m[4] * v[1] + m[ 8] * v[2] + m[12];
	out[1] = m[1] * v[0] + m[5] * v[1] + m[ 9] * v[2] + m[13];
	out[2] = m[2] * v[0] + m[6] * v[1] + m[10] * v[2] + m[14];
	return out;
}

CS3DVec4f CS3DMat16f::operator*( const CS3DVec4f &v ) const
{
	CS3DVec4f out;
    for ( int i = 0; i < 4; i++ )
	{
		out[i] = v[0] * m[ 0 * 4 + i ] +
				 v[1] * m[ 1 * 4 + i ] +
				 v[2] * m[ 2 * 4 + i ] +
				 v[3] * m[ 3 * 4 + i ];
    }
	return out;
}

S3D_BoundingBox CS3DMat16f::operator*( const S3D_BoundingBox &bb ) const
{
	S3D_BoundingBox out;
	const CS3DMat16f &mat = *this;
	out.min = mat * bb.min;
	out.max = mat * bb.max;
	return out;
}

void CS3DMat16f::simpleInverse()
{
	CS3DMat16f invMat = *this;
	invMat.transpose();
	invMat[3] = 0; invMat[7] = 0; invMat[11] = 0;
	CS3DVec3f t = -invMat.multVecWithoutTranslate( CS3DVec3f( m[12], m[13], m[14] ) );
	memcpy( &invMat[12], &t, sizeof( CS3DVec3f ) );
	*this = invMat;
}

void CS3DMat16f::getNormalMat( float matrix[9] )
{
	float det = m[0] * ( m[5] * m[10] - m[9] * m[6] ) -
			    m[1] * ( m[4] * m[10] - m[6] * m[8] ) +
				m[2] * ( m[4] * m[ 9] - m[5] * m[8] );
	float invdet = 1.0f / det;
	
	matrix[0] = ( m[5] * m[10] - m[9] * m[6] ) *  invdet;
	matrix[3] = ( m[1] * m[10] - m[2] * m[9] ) * -invdet;
	matrix[6] = ( m[1] * m[ 6] - m[2] * m[5] ) *  invdet;
	
	matrix[1] = ( m[4] * m[10] - m[6] * m[8] ) * -invdet;
	matrix[4] = ( m[0] * m[10] - m[2] * m[8] ) *  invdet;
	matrix[7] = ( m[0] * m[ 6] - m[4] * m[2] ) * -invdet;
	
	matrix[2] = ( m[4] * m[9] - m[8] * m[5] ) *  invdet;
	matrix[5] = ( m[0] * m[9] - m[8] * m[1] ) * -invdet;
	matrix[8] = ( m[0] * m[5] - m[4] * m[1] ) *  invdet;
}

bool CS3DMat16f::inverse()
{
    float inv[16], det;

    inv[0] =   m[5]*m[10]*m[15] - m[5]*m[11]*m[14] - m[9]*m[6]*m[15]
             + m[9]*m[7]*m[14] + m[13]*m[6]*m[11] - m[13]*m[7]*m[10];
    inv[4] =  -m[4]*m[10]*m[15] + m[4]*m[11]*m[14] + m[8]*m[6]*m[15]
             - m[8]*m[7]*m[14] - m[12]*m[6]*m[11] + m[12]*m[7]*m[10];
    inv[8] =   m[4]*m[9]*m[15] - m[4]*m[11]*m[13] - m[8]*m[5]*m[15]
             + m[8]*m[7]*m[13] + m[12]*m[5]*m[11] - m[12]*m[7]*m[9];
    inv[12] = -m[4]*m[9]*m[14] + m[4]*m[10]*m[13] + m[8]*m[5]*m[14]
             - m[8]*m[6]*m[13] - m[12]*m[5]*m[10] + m[12]*m[6]*m[9];
    inv[1] =  -m[1]*m[10]*m[15] + m[1]*m[11]*m[14] + m[9]*m[2]*m[15]
             - m[9]*m[3]*m[14] - m[13]*m[2]*m[11] + m[13]*m[3]*m[10];
    inv[5] =   m[0]*m[10]*m[15] - m[0]*m[11]*m[14] - m[8]*m[2]*m[15]
             + m[8]*m[3]*m[14] + m[12]*m[2]*m[11] - m[12]*m[3]*m[10];
    inv[9] =  -m[0]*m[9]*m[15] + m[0]*m[11]*m[13] + m[8]*m[1]*m[15]
             - m[8]*m[3]*m[13] - m[12]*m[1]*m[11] + m[12]*m[3]*m[9];
    inv[13] =  m[0]*m[9]*m[14] - m[0]*m[10]*m[13] - m[8]*m[1]*m[14]
             + m[8]*m[2]*m[13] + m[12]*m[1]*m[10] - m[12]*m[2]*m[9];
    inv[2] =   m[1]*m[6]*m[15] - m[1]*m[7]*m[14] - m[5]*m[2]*m[15]
             + m[5]*m[3]*m[14] + m[13]*m[2]*m[7] - m[13]*m[3]*m[6];
    inv[6] =  -m[0]*m[6]*m[15] + m[0]*m[7]*m[14] + m[4]*m[2]*m[15]
             - m[4]*m[3]*m[14] - m[12]*m[2]*m[7] + m[12]*m[3]*m[6];
    inv[10] =  m[0]*m[5]*m[15] - m[0]*m[7]*m[13] - m[4]*m[1]*m[15]
             + m[4]*m[3]*m[13] + m[12]*m[1]*m[7] - m[12]*m[3]*m[5];
    inv[14] = -m[0]*m[5]*m[14] + m[0]*m[6]*m[13] + m[4]*m[1]*m[14]
             - m[4]*m[2]*m[13] - m[12]*m[1]*m[6] + m[12]*m[2]*m[5];
    inv[3] =  -m[1]*m[6]*m[11] + m[1]*m[7]*m[10] + m[5]*m[2]*m[11]
             - m[5]*m[3]*m[10] - m[9]*m[2]*m[7] + m[9]*m[3]*m[6];
    inv[7] =   m[0]*m[6]*m[11] - m[0]*m[7]*m[10] - m[4]*m[2]*m[11]
             + m[4]*m[3]*m[10] + m[8]*m[2]*m[7] - m[8]*m[3]*m[6];
    inv[11] = -m[0]*m[5]*m[11] + m[0]*m[7]*m[9] + m[4]*m[1]*m[11]
             - m[4]*m[3]*m[9] - m[8]*m[1]*m[7] + m[8]*m[3]*m[5];
    inv[15] =  m[0]*m[5]*m[10] - m[0]*m[6]*m[9] - m[4]*m[1]*m[10]
             + m[4]*m[2]*m[9] + m[8]*m[1]*m[6] - m[8]*m[2]*m[5];

    det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];
    if ( det == 0 ) return false;
    det = 1.0f / det;
    for ( int i = 0; i < 16; i++ )
        m[i] = inv[i] * det;
    return true;
}

void CS3DMat16f::lookAt( CS3DVec3f eye, CS3DVec3f target, CS3DVec3f up )
{
	CS3DVec3f forward = target - eye;
	forward.normalize();

	CS3DVec3f side = forward ^ up;
	side.normalize();

	up = side ^ forward;

	identity();
	m[0] = side[0];
	m[4] = side[1];
	m[8] = side[2];

	m[1] = up[0];
	m[5] = up[1];
	m[9] = up[2];

	m[2] = -forward[0];
	m[6] = -forward[1];
	m[10] = -forward[2];

	translate( -eye );
}

void CS3DMat16f::frustum( float left, float right, float bottom, float top, float nearZ, float farZ )
{
	float deltaX = right - left;
	float deltaY = top - bottom;
	float deltaZ = farZ - nearZ;

	m[0] = 2.0f * nearZ / deltaX;
	m[1] = m[2] = m[3] = 0.0f;

	m[5] = 2.0f * nearZ / deltaY;
	m[4] = m[6] = m[7] = 0.0f;

	m[8] = (right + left) / deltaX;
	m[9] = (top + bottom) / deltaY;
	m[10] = -(nearZ + farZ) / deltaZ;
	m[11] = -1.0f;

	m[14] = -2.0f * nearZ * farZ / deltaZ;
	m[12] = m[13] = m[15] = 0.0f;
}

void CS3DMat16f::ortho( float left, float right, float bottom, float top, float nearZ, float farZ )
{
	identity();

    float deltaX = right - left;
    float deltaY = top - bottom;
    float deltaZ = farZ - nearZ;

    m[12] = -(right + left) / deltaX;
    m[13] = -(top + bottom) / deltaY;
    m[14] = -(farZ + nearZ) / deltaZ;

    m[0] =   2.0f / deltaX;
    m[5] =   2.0f / deltaY;
    m[10] = -2.0f / deltaZ;
}

CS3DVec3f CS3DMat16f::eulerXYZ()
{
	float angle_x, angle_y, angle_z;
	float ftrx, ftry;

	angle_y = -asinf( m[2] );        
	float C =  cosf( angle_y );

	if ( C * 8192.0f > S3D_EPS )             
	{
		ftrx =  m[10] / C;           
		ftry = -m[6] / C;
		angle_x = atan2f( ftry, ftrx );

		ftrx =  m[0] / C;           
		ftry = -m[1] / C;
		angle_z = atan2f( ftry, ftrx );
	}
	else                                
	{
		angle_x = 0;                     
		ftrx = m[5];                
		ftry = m[4];
		angle_z = atan2f( ftry, ftrx );
	}
	return CS3DVec3f( -angle_x, angle_y, -angle_z ) / S3D_PI * 180;
}

CS3DVec3f CS3DMat16f::translation()
{
    return CS3DVec3f( m[12], m[13], m[14] );
}