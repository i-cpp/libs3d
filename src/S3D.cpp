#include <S3D.h>
#include <S3DPlatform.h>

#if ( S3D_OGLES_2_0 )
#	define glClearDepth glClearDepthf
#else
	PFNGLACTIVETEXTUREPROC glActiveTexture = 0;
	PFNGLGENERATEMIPMAPPROC glGenerateMipmap = 0;
#endif

unsigned int (*s3dLoadTexture)( const char *filename, S3D_Image *img, bool gen_texture ) = 0;
unsigned int (*s3dLoadTextureFromData)( unsigned char *data, 
	int width, int height, int bpp, bool linearFilter, bool genMipMaps ) = 0;
void* (*s3dGetProcAddress)( const char *name ) = 0;

CS3DShader *s3dSimpleShader = 0;
CS3DShader *s3dColoredMeshShader = 0;
CS3DShader *s3dSpriteShader = 0;
CS3DShader *s3dFurShader = 0;
S3D_Shaders *s3dLoadedShaders = 0;
CS3DBuffer *s3dBuffer = 0;
CS3DCamera *s3dCamera = 0;
CS3DIOController *s3dIOController = 0;
#if ( S3D_USE_LIGTHING )
CS3DLight *s3dLight = 0;
#endif
int s3dViewport[4];
CS3DMat16f s3dProjection;
unsigned int s3dLookupMap = 0, 
			 s3dBlankTexture = 0, 
			 s3dBlankNormalTexture = 0,
			 s3dBlankCubeMap = 0;

struct S3D_BufferState {
	S3D_Color color;
	bool withDepth;
};
S3D_BufferState s3dLastBufferState;

bool s3dInitialized = false;
S3D_TexMemChunks *s3dTexMemAlloted = 0;

struct S3D_Info {
	const char *vendor;
	const char *renderer;
	const char *version;
	const char *extensions;
};
S3D_Info s3dInfo;

bool s3dInit()
{
	if ( !s3dInitialized )
	{
		s3dTexMemAlloted = new S3D_TexMemChunks;
		s3dTexMemAlloted->clear();

		if ( !s3dIOController )
			s3dIOController = new CS3DIOController;
		s3dIOController->DeleteFile( S3D_LOG_FILE );

#if ( !S3D_OGLES_2_0 )
		if ( !s3dGetProcAddress )
		{
			s3dIOController->PrintLog( "Function s3dGetProcAddress does not exist.\n" );
			delete s3dIOController;
			s3dIOController = 0;
			return false;
		}
#endif

		char logMsg[0x1000] = { 0 };
		s3dInfo.vendor = (const char*)glGetString( GL_VENDOR );
		s3dInfo.renderer = (const char*)glGetString( GL_RENDERER );
		s3dInfo.version = (const char*)glGetString( GL_VERSION );
		s3dInfo.extensions = (const char*)glGetString( GL_EXTENSIONS );
		strcpy( logMsg, "Vendor: " ); strcat( logMsg, s3dInfo.vendor );
		strcat( logMsg, "\nRenderer: " ); strcat( logMsg, s3dInfo.renderer );
		strcat( logMsg, "\nVersion: " ); strcat( logMsg, s3dInfo.version );
		strcat( logMsg, "\n\nExtensions:\n" );
		s3dIOController->PrintLog( logMsg );
		s3dIOController->PrintLog( s3dInfo.extensions );
		s3dIOController->PrintLog( "\n\n" );

#if ( !S3D_OGLES_2_0 )
		S3D_GET_FUNC( PFNGLACTIVETEXTUREPROC, glActiveTexture, "glActiveTexture" )
		if ( !glGenerateMipmap )
		{
			if ( S3D_OpenGLOp::ExtensionSupported( "GL_ARB_framebuffer_object" ) )
				S3D_GET_FUNC( PFNGLGENERATEMIPMAPPROC, glGenerateMipmap, "glGenerateMipmap" )
			else if ( S3D_OpenGLOp::ExtensionSupported( "GL_EXT_framebuffer_object" ) )
				S3D_GET_FUNC( PFNGLGENERATEMIPMAPPROC, glGenerateMipmap, "glGenerateMipmapEXT" )
		}
#endif
		unsigned char pixel[4] = {255, 255, 255, 255};
		S3D_OpenGLOp::CreateTexture( s3dBlankTexture, pixel, 1, 1, 4 );
		pixel[0] = pixel[1] = 127;
		S3D_OpenGLOp::CreateTexture( s3dBlankNormalTexture, pixel, 1, 1, 3, false );
        S3D_OpenGLOp::DisableTexture();
#if ( S3D_USE_CUBEMAPS )
		s3dBlankCubeMap = S3D_OpenGLOp::CreateCubeMap( 0 );
#endif

		s3dCamera = new CS3DCamera;

#if ( S3D_USE_LIGTHING )
 		s3dLight = new CS3DLight();
#endif
#if ( S3D_USE_BUFFERS )
		s3dBuffer = new CS3DBuffer;
#endif

		int openGLVer = ( s3dInfo.version[0] - 48 ) * 100 + ( s3dInfo.version[2] - 48 ) * 10 + ( s3dInfo.version[4] - 48 );
		if ( S3D_USE_SHADERS )
		{
			if ( S3D_OGLES_2_0 || openGLVer >= 200 )
			{
				s3dLoadedShaders = new S3D_Shaders;
				s3dSimpleShader = new CS3DShader;
				if ( !s3dSimpleShader->Load( S3D_VERTEX_SHADER_SIMPLE, S3D_FRAGMENT_SHADER_SIMPLE ) )
				{
					s3dIOController->PrintLog( "Can not create s3dSimpleShader.\n" );
					delete s3dSimpleShader;
					s3dSimpleShader = 0;
				}
				s3dColoredMeshShader = new CS3DShader;
				if ( !s3dColoredMeshShader->Load( S3D_VERTEX_SHADER_COLORED_MESH, S3D_FRAGMENT_SHADER_COLORED_LAMBERT ) )
				{
					s3dIOController->PrintLog( "Can not create s3dColoredMeshShader.\n" );
					delete s3dColoredMeshShader;
					s3dColoredMeshShader = 0;
				}
				s3dSpriteShader = new CS3DShader;
				if ( !s3dSpriteShader->Load( S3D_VERTEX_SHADER_SPRITE, S3D_FRAGMENT_SHADER_SPRITE ) )
				{
					s3dIOController->PrintLog( "Can not create s3dSpriteShader.\n" );
					delete s3dSpriteShader;
					s3dSpriteShader = 0;
				}
				s3dFurShader = new CS3DShader;
				if ( !s3dFurShader->Load( S3D_VERTEX_SHADER_FUR, S3D_FRAGMENT_SHADER_FUR ) )
				{
					s3dIOController->PrintLog( "Can not create fur shader.\n" );
					delete s3dFurShader;
					s3dFurShader = 0;
				}
				s3dGaussianBlur = new S3D_GaussianBlur;
				if ( !s3dGaussianBlur->Create( 6 ) )
				{
					s3dIOController->PrintLog( "Can not create gauss blur.\n" );
					delete s3dGaussianBlur;
					s3dGaussianBlur = 0;
				}
#if ( S3D_USE_SHADOWS )
				s3dShadow = new S3D_ShadowMap;
				if ( !s3dShadow->Create() )
				{
					s3dIOController->PrintLog( "Can not create shadow map.\n" );
					delete s3dShadow;
					s3dShadow = 0;
				}
#endif
				if ( !s3dLookupMap )
				{
					int width  = 128;
					int	height = 128;
					float *buf = new float[ width * height ];
					float *ptr = buf;
					float invW = 1.0f / (float)width, invH = 1.0f / (float)height;
					for ( float dotNL = 0.0; dotNL < 1.0; dotNL += invW )
					{
						for ( float dotNV = 0.0; dotNV < 1.0; dotNV += invH )
						{
							float acosNL = acosf( dotNL );
							float acosNV = acosf( dotNV );
							float val = sinf( max( acosNL, acosNV ) ) * tanf( min( acosNL, acosNV ) * 0.95f );
							*ptr++ = val;
						}
					}
					
					S3D_OpenGLOp::CreateTexture( s3dLookupMap, buf, width, height, 1, false, 1 );
					S3D_OpenGLOp::Clamp( true, true );
                    S3D_OpenGLOp::DisableTexture();
					delete [] buf;
				}
			}
			else s3dIOController->PrintLog( "Can not find version shaders 2.0 or higher.\n" );
		}
#if ( S3D_USE_BUFFERS )
		if ( !s3dLoadedShaders )
		{
			delete s3dBuffer;
			s3dBuffer = 0;
		}
#endif

		glGetIntegerv( GL_VIEWPORT, s3dViewport );
		s3dInitialized = true;
	}
	return s3dInitialized;
}

void s3dFree()
{
	if ( s3dGaussianBlur )
	{
		delete s3dGaussianBlur;
		s3dGaussianBlur = 0;
	}
#if ( S3D_USE_SHADOWS )
	if ( s3dShadow )
	{
		delete s3dShadow;
		s3dShadow = 0;
	}
#endif

	if ( s3dColoredMeshShader )
	{
		delete s3dColoredMeshShader;
		s3dColoredMeshShader = 0;
	}
	if ( s3dSimpleShader )
	{
		delete s3dSimpleShader;
		s3dSimpleShader = 0;
	}
	if ( s3dSpriteShader )
	{
		delete s3dSpriteShader;
		s3dSpriteShader = 0;
	}
	if ( s3dFurShader )
	{
		delete s3dFurShader;
		s3dFurShader = 0;
	}
	if ( s3dLoadedShaders )
	{
		for ( unsigned int i = 0; i < s3dLoadedShaders->size(); i++ )
			delete (*s3dLoadedShaders)[i];
		delete s3dLoadedShaders;
		s3dLoadedShaders = 0;
	}
	if ( s3dBlankTexture )
	{
		S3D_OpenGLOp::DeleteTextures( 1, &s3dBlankTexture );
		s3dBlankTexture = 0;
	}
	if ( s3dBlankNormalTexture )
	{
		S3D_OpenGLOp::DeleteTextures( 1, &s3dBlankNormalTexture );
		s3dBlankNormalTexture = 0;
	}
	if ( s3dBlankCubeMap )
	{
		S3D_OpenGLOp::DeleteTextures( 1, &s3dBlankCubeMap );
		s3dBlankCubeMap = 0;
	}
	if ( s3dLookupMap )
	{
		S3D_OpenGLOp::DeleteTextures( 1, &s3dLookupMap );
		s3dLookupMap = 0;
	}
	if ( s3dBuffer )
	{
		delete s3dBuffer;
		s3dBuffer = 0;
	}
	if ( s3dCamera )
	{
		delete s3dCamera;
		s3dCamera = 0;
	}
#if ( S3D_USE_LIGTHING )
	if ( s3dLight )
	{
		delete s3dLight;
		s3dLight = 0;
	}
#endif
	if ( s3dIOController )
	{
		delete s3dIOController;
		s3dIOController = 0;
	}
	if ( s3dTexMemAlloted )
	{
		delete s3dTexMemAlloted;
		s3dTexMemAlloted = 0;
	}
	s3dInitialized = false;
}

void s3dSetIOFunctions( void *(*open_func) ( const char *filename ),
						bool  (*read_func) ( void* io, void *data, unsigned int size ),
						void  (*close_func)( void* io ),
						void  (*log_func) ( const char *msg ),
						void  (*delete_func) ( const char *filename ) )
{
	if ( !s3dIOController ) s3dIOController = new CS3DIOController;
	s3dIOController->SetIOFunctions( open_func, read_func, close_func, log_func, delete_func );
}

void s3dSetLoadTextureFunc( unsigned int (*func)( const char *, S3D_Image *, bool ) )
{
	s3dLoadTexture = func;
}

void s3dSetLoadTextureFunc( unsigned int (*func)( unsigned char *, int, int, int, bool, bool ) )
{
	s3dLoadTextureFromData = func;
}

void s3dSetGetProcAddressFunc( void* (*func)( const char * ) )
{
	s3dGetProcAddress = func;
}

const char *s3dGetInfo( S3D_ENUM id )
{
	return (const char*)*(&s3dInfo.vendor + (id - S3D_VENDOR));
}

unsigned int s3dGetTexMemAlloted()
{
	if ( s3dTexMemAlloted )
	{
		unsigned int total_size = 0;
		unsigned int count_of_alloted = (unsigned int)s3dTexMemAlloted->size();
		for ( unsigned int i = 0; i < count_of_alloted; i++ )
		{
			total_size += (*s3dTexMemAlloted)[i].size;
		}
		return total_size;
	}
	return 0;
}

void s3dPerspective( float fovy, float zNear, float zFar )
{
	float aspect = (float)s3dViewport[2] / (float)s3dViewport[3];
	float xmin, xmax, ymin, ymax;
	ymax = zNear * tanf( ( fovy * S3D_PI ) / 360.0f );
	ymin = -ymax;
	xmin = ymin * aspect;
	xmax = ymax * aspect;
	s3dProjection.frustum( xmin, xmax, ymin, ymax, zNear, zFar );
}

void s3dOrtho( float zNear, float zFar )
{
	s3dProjection.ortho( (float)s3dViewport[0], (float)s3dViewport[2], 
		(float)s3dViewport[1], (float)s3dViewport[3], zNear, zFar );
}

bool s3dProject( CS3DVec3f *obj, CS3DVec3f *win, const float modelMat[16] )
{
	CS3DVec4f in, out;

	in[0] = (*obj)[0];
	in[1] = (*obj)[1];
	in[2] = (*obj)[2];
	in[3] = 1.0f;

	CS3DMat16f mat;
	mat.set( modelMat );
	out = mat * in;

	in = s3dProjection * out;

	if ( in[3] == 0.0f ) return false;

	in[0] /= in[3];
	in[1] /= in[3];
	in[2] /= in[3];

	in[0] = in[0] * 0.5f + 0.5f;
	in[1] = in[1] * 0.5f + 0.5f;
	in[2] = in[2] * 0.5f + 0.5f;

	in[0] = in[0] * s3dViewport[2] + s3dViewport[0];
	in[1] = in[1] * s3dViewport[3] + s3dViewport[1];

	(*win)[0] = in[0];
	(*win)[1] = ((float)s3dViewport[3] - in[1]) - 1.0f;
	(*win)[2] = in[2];

	return true;
}

bool s3dUnProject( CS3DVec3f *win, CS3DVec3f *obj, const float invmvpMat[16] )
{
	CS3DVec4f in, out;

	CS3DMat16f mat;
	mat.set( invmvpMat );

	in[0] = (*win)[0];
	in[1] = ((float)s3dViewport[3] - (*win)[1]) - 1.0f;
	in[2] = (*win)[2];
	in[3] = 1.0f;

	in[0] = ( in[0] - s3dViewport[0] ) / s3dViewport[2];
	in[1] = ( in[1] - s3dViewport[1] ) / s3dViewport[3];

	in[0] = in[0] * 2 - 1;
	in[1] = in[1] * 2 - 1;
	in[2] = in[2] * 2 - 1;

	out = mat * in;

	if ( out[3] == 0.0f ) return false;

	out[0] /= out[3];
	out[1] /= out[3];
	out[2] /= out[3];

	*obj = out.xyz();
	return true;
}

CS3DVec3f s3dUnProjectGround( int srcX, int srcY )
{
	CS3DVec3f src, near_value, far_value;
	CS3DMat16f invMat;
	invMat = s3dCamera->Get() * s3dProjection;
	if ( !invMat.inverse() ) return CS3DVec3f();

	src = CS3DVec3f( (float)srcX, (float)srcY, -1.0f );
	s3dUnProject( &src, &near_value, (float*)&invMat );

	src = CS3DVec3f( (float)srcX, (float)srcY, 1.0f );
	s3dUnProject( &src, &far_value, (float*)&invMat );

	float tY = ( s3dCamera->GroundLevel() - near_value[1] ) / ( far_value[1] - near_value[1] );
	return S3D_MIX( near_value, far_value, tY );
}

void s3dSetViewport( int x, int y, int width, int height )
{
	s3dViewport[0] = x;
	s3dViewport[1] = y;
	s3dViewport[2] = width;
	s3dViewport[3] = height;
}

void s3dSetLookupMap( int activeID )
{
	S3D_OpenGLOp::EnableTexture( s3dLookupMap, activeID );
}

namespace S3D_OpenGLOp {
#if ( !S3D_OGLES_2_0 )
	void BindMesh( S3D_Body *body, bool useNormals, bool useTexCoords )
	{
		if ( !body ) return;
		if ( useTexCoords )
		{
			glEnableClientState( GL_TEXTURE_COORD_ARRAY );
			glTexCoordPointer( 2, GL_FLOAT, sizeof( S3D_Body ), &body->tcoord );
		}
#if ( S3D_USE_LIGTHING )
		if ( useNormals )
		{
			glEnableClientState( GL_NORMAL_ARRAY );
			glNormalPointer( GL_FLOAT, sizeof( S3D_Body ), &body->normal );
		}
#endif
		glEnableClientState( GL_VERTEX_ARRAY );
		glVertexPointer( 3, GL_FLOAT, sizeof( S3D_Body ), &body->vertex );
	}

	void UnbindMesh()
	{
		glDisableClientState( GL_TEXTURE_COORD_ARRAY );
		glDisableClientState( GL_NORMAL_ARRAY );
		glDisableClientState( GL_VERTEX_ARRAY );
	}

	void SetMaterial( unsigned int face, S3D_Material *mat )
	{
		if ( !mat ) return;
		glMaterialfv( face, GL_EMISSION, (float*)&mat->emission );
		glMaterialfv( face, GL_AMBIENT, (float*)&mat->ambient );
		glMaterialfv( face, GL_DIFFUSE, (float*)&mat->diffuse );
		glMaterialfv( face, GL_SPECULAR, (float*)&mat->specular );
		glMaterialf ( face, GL_SHININESS, mat->shininess );
	}

	void EnableLighting( S3D_Light *LightSources )
	{
		if ( !LightSources ) return;
		glEnable( GL_LIGHTING );
		glEnable( GL_NORMALIZE );
		for ( unsigned i = 0; i < S3D_MAX_LIGHTS; i++ )
		{
			if ( LightSources[ i ].enabled )
			{
				GLenum curLight = GL_LIGHT0 + i;
				glEnable( curLight );
				glLightfv( curLight, GL_AMBIENT, (float*)&LightSources[ i ].ambient );
				glLightfv( curLight, GL_DIFFUSE, (float*)&LightSources[ i ].diffuse );
				glLightfv( curLight, GL_SPECULAR, (float*)&LightSources[ i ].specular );
				glLightfv( curLight, GL_POSITION, (float*)&LightSources[ i ].pos );
			}
		}
	}

	void DisableLighting( S3D_Light *LightSources )
	{
		if ( !LightSources ) return;
		for ( unsigned i = 0; i < S3D_MAX_LIGHTS; i++ )
		{
			if ( LightSources[ i ].enabled )
				glDisable( GL_LIGHT0 + i );
		}
		glDisable( GL_NORMALIZE );
		glDisable( GL_LIGHTING );
	}

	void SetMatrices( const float ModelViewMat[16] )
	{
		glMatrixMode( GL_PROJECTION );
		glLoadMatrixf( (float*)&s3dProjection );
		glMatrixMode( GL_MODELVIEW );
		glLoadMatrixf( (float*)&s3dCamera->Get() );
#if ( S3D_USE_LIGTHING )
		s3dLight->EnableLighting();
#endif
		glLoadMatrixf( ModelViewMat );
	}
#endif
	
	void DrawPrimitives( unsigned int mode, int first, int count )
	{
		glDrawArrays( mode, first, count );
	}

	void DrawPrimitives( unsigned int mode, const S3D_Index *IDs, int count )
	{
		GLenum type = GL_UNSIGNED_BYTE;
		if ( sizeof( S3D_Index ) > 1 ) type += sizeof( S3D_Index );
		glDrawElements( mode, count, type, IDs );
	}

	void Clamp( bool s, bool t )
	{
		if ( s ) glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
		else glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
		if ( t ) glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
		else glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
		/*
		if ( s ) glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER );
		if ( t ) glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER );
		float clr [] = { 1, 1, 1, 1 };
		glTexParameterfv( GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, clr );
		*/
	}

	void DeleteTextures( int count, const unsigned int *ids )
	{
		if ( s3dTexMemAlloted )
		{
			for ( int j = 0; j < count; j++ )
			{
				unsigned int texId = ids[j];
				unsigned int count_of_alloted = (unsigned int)s3dTexMemAlloted->size();
				for ( unsigned int i = 0; i < count_of_alloted; i++ )
				{
					if ( texId == (*s3dTexMemAlloted)[i].id )
					{
						s3dTexMemAlloted->erase( s3dTexMemAlloted->begin() + i );
						break;
					}
				}
			}
		}
		glDeleteTextures( count, ids );
	}

	void DisableTexture( int activeID, bool cubeMap )
	{
		if ( activeID != -1 )
		{
			glActiveTexture( GL_TEXTURE0 + activeID );
		}
#if ( S3D_USE_CUBEMAPS )
		GLenum target = cubeMap ? GL_TEXTURE_CUBE_MAP : GL_TEXTURE_2D;
#else
		GLenum target = GL_TEXTURE_2D;
#endif
		glBindTexture( target, 0 );
#if ( !S3D_OGLES_2_0 )
		glDisable( target );
#endif
	}

	bool EnableTexture( unsigned int texID, int activeID, bool cubeMap )
	{
		if ( glIsTexture( texID ) )
		{
#if ( S3D_USE_CUBEMAPS )
			GLenum target = cubeMap ? GL_TEXTURE_CUBE_MAP : GL_TEXTURE_2D;
#else
			GLenum target = GL_TEXTURE_2D;
#endif
#if ( !S3D_OGLES_2_0 )
			glEnable( target );
#endif
			if ( activeID != -1 )
			{
				glActiveTexture( GL_TEXTURE0 + activeID );
			}
			glBindTexture( target, texID );
			return true;
		}
		return false;
	}

	void CreateTexture( unsigned int &texID, void *data, int w, int h, int bpp, bool linear, S3D_ENUM type, bool genMipMaps )
	{
#if ( !S3D_OGLES_2_0 )
		glEnable( GL_TEXTURE_2D );
#endif
		glGenTextures( 1, &texID );
		glBindTexture( GL_TEXTURE_2D, texID );
		
        if ( !genMipMaps
#if ( !S3D_OGLES_2_0 )
		  || !glGenerateMipmap
#endif
			)
        {
            glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, linear ? GL_LINEAR : GL_NEAREST );
            glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, linear ? GL_LINEAR : GL_NEAREST );
        }

        else
        {
            glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, linear ? GL_LINEAR : GL_NEAREST );
            glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, linear ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_NEAREST );
            glGenerateMipmap( GL_TEXTURE_2D );
        }
        
		GLenum dataType = GL_UNSIGNED_BYTE;
		GLenum format = GL_RGB, intFormat = GL_RGB;
		switch ( bpp )
		{
			case 1: format = intFormat = GL_LUMINANCE; break;
			case 2: format = intFormat = GL_LUMINANCE_ALPHA; break;
			case 3: format = intFormat = GL_RGB; break;
			case 4: format = intFormat = GL_RGBA; break;
#ifdef GL_DEPTH_COMPONENT
			case -1:
				bpp = 4;
                format = GL_DEPTH_COMPONENT;
                intFormat = GL_DEPTH_COMPONENT;
                dataType = GL_UNSIGNED_INT;
            break;
#endif
#ifdef GL_RGB32F
			case -2:
				bpp = 12;
                format = GL_RGB;
                intFormat = GL_RGB32F;
				dataType = GL_FLOAT;
            break;
#else
#   if ( S3D_OGLES_2_0 )
#       ifdef GL_HALF_FLOAT_OES
			case -2:
				bpp = 6;
                format = GL_RGB;
                intFormat = GL_RGB;
                dataType = GL_HALF_FLOAT_OES;
            break;
#       else
            case -2:
				bpp = 3;
				format = intFormat = GL_RGB;
			break;
#       endif
#   else
            case -2:
				bpp = 3;
				format = intFormat = GL_RGB;
			break;
#   endif
#endif
		}
        switch ( type )
        {
            case 1: dataType = GL_FLOAT; break;
            case 2: dataType = GL_UNSIGNED_INT; break;
            case 3: dataType = GL_UNSIGNED_SHORT; break;
        }
		glTexImage2D( GL_TEXTURE_2D, 0, intFormat, w, h, 0, format, dataType, data );
		s3dTexMemAlloted->push_back( S3D_VideoMemChunk( texID, w * h * bpp ) );
	}

	unsigned CreateCubeMap( const char *format )
	{
		unsigned int texID = 0;
#if ( S3D_USE_CUBEMAPS )
		const char *side[6] = {
			"_left", "_right", "_top", "_bottom", "_back", "_front"
		};
		GLenum target = GL_TEXTURE_CUBE_MAP;
		GLenum fmt = GL_RGB;
#if ( !S3D_OGLES_2_0 )
		glEnable( target );
#endif
		glGenTextures( 1, &texID );
		glPixelStorei ( GL_UNPACK_ALIGNMENT, 1 );
		glBindTexture( target, texID );
		glTexParameteri( target, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( target, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glTexParameteri( target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
		glTexParameteri( target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
#ifdef GL_TEXTURE_WRAP_R
		glTexParameteri( target, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );
#endif
		int size = 0;
		if ( format )
		{
			for ( int i = 0; i < 6; i++ )
			{
				S3D_Image img;
				char filename[0x100];
				strcpy( filename, format );
				strcat( filename, side[i] );
				strcat( filename, S3D_TEXTURE_FILE_FORMAT );
				s3dLoadTexture( filename, &img, false );
				switch ( img.bpp )
				{
					case 1: fmt = GL_LUMINANCE; break;
					case 2: fmt = GL_LUMINANCE_ALPHA; break;
					case 3: fmt = GL_RGB; break;
					case 4: fmt = GL_RGBA; break;
				}
				glTexImage2D( GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, fmt,
					img.width, img.height, 0, fmt, GL_UNSIGNED_BYTE, img.data );
				if ( img.data ) free( img.data );
				size += img.width * img.height * img.bpp;
			}
		}
		else
		{
			unsigned char pixel[3] = { 255, 255, 255 };
			for ( int i = 0; i < 6; i++ )
			{
				glTexImage2D( GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, 
					1, 1, 0, GL_RGB, GL_UNSIGNED_BYTE, pixel );
			}
			size += 3;
		}
		if ( s3dTexMemAlloted )
			s3dTexMemAlloted->push_back( S3D_VideoMemChunk( texID, size ) );
		DisableTexture( -1, true );
#endif
		return texID;
	}

	unsigned GetBlankTexture( S3D_ENUM type )
	{
		if ( type == 1 ) return s3dBlankCubeMap;
		else if ( type == 2 ) return s3dBlankNormalTexture;
		return s3dBlankTexture;
	}

	bool ExtensionSupported( const char *extension )
	{
		unsigned char *Where, *Terminator;
		const unsigned char *start = (const unsigned char *)s3dInfo.extensions;
		while( 1 )
		{
			Where = (unsigned char *)strstr( (const char *) start, extension );
			if ( !Where ) break;
			Terminator = Where + strlen( extension );
			if ( ( Where == start || *( Where - 1 ) == ' ' )
			  && ( *Terminator == ' ' || *Terminator == '\0' ) ) return true;
			start = Terminator;
		}
		char Err[0x100] = { 0 };
		strcpy( Err, "Can not find OpenGL extension: " );
		strcat( Err, extension ); strcat( Err, ".\n" );
		s3dIOController->PrintLog( Err );
		return false;
	}

	void EnableDepthMask()
	{
		glDepthMask( 1 );
	}

	void DisableDepthMask()
	{
		glDepthMask( 0 );
	}

	void DisableBackFace()
	{
		glEnable( GL_CULL_FACE );
		glCullFace( GL_BACK );
	}

	void EnableBackFace()
	{
		glDisable( GL_CULL_FACE );
	}
	
	void ClearFrameBuffer()
	{
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glEnable( GL_DEPTH_TEST );
	}

	void EnableShadowBuffer( int size )
	{
		// fill buffer state
		s3dLastBufferState.withDepth = glIsEnabled( GL_DEPTH_TEST ) == GL_TRUE;
		glGetFloatv( GL_COLOR_CLEAR_VALUE, (float*)&s3dLastBufferState.color );

		if ( !s3dLastBufferState.withDepth ) glEnable( GL_DEPTH_TEST );
		glViewport( 0, 0, size, size );
		glClearColor( 1, 1, 1, 1 );
#if ( S3D_SHADOW_ALGORITHM != S3D_SOFT_SHADOW_VSM )
		glClear( GL_DEPTH_BUFFER_BIT );
		glColorMask( 0, 0, 0, 0 );
#else
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
#endif
	}

	void DisableShadowBuffer()
	{
		if ( !s3dLastBufferState.withDepth ) glDisable( GL_DEPTH_TEST );
		glClearColor( 
			s3dLastBufferState.color.r, 
			s3dLastBufferState.color.g, 
			s3dLastBufferState.color.b, 
			s3dLastBufferState.color.a 
		);
#if ( S3D_SHADOW_ALGORITHM != S3D_SOFT_SHADOW_VSM )
		glColorMask( 1, 1, 1, 1 );
#endif
		glViewport( s3dViewport[0], s3dViewport[1], s3dViewport[2], s3dViewport[3] );
	}
    
    int GetError()
    {
        return glGetError();
    }

};