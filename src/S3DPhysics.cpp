#include <S3D.h>

#if ( S3D_USE_PHYSICS )

#include <btBulletDynamicsCommon.h>
#include <LinearMath/btAabbUtil2.h>
#include <BulletDynamics/Character/btKinematicCharacterController.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>

btDefaultCollisionConfiguration* collisionConfiguration = 0;
btCollisionDispatcher* dispatcher = 0;
btAxisSweep3* overlappingPairCache = 0;
btOverlappingPairCallback *ghostPairCallback = 0;
btSequentialImpulseConstraintSolver* solver = 0;
btDiscreteDynamicsWorld* dynamicsWorld = 0;
unsigned lastTimeStamp = 0;

enum Control {
	LEFT, RIGHT, FORWARD, BACKWARD, JUMP, NUM_CONROLS
};
struct S3D_Player {
	S3D_Player() 
		: controller(0)
	{
		memset( control, 0, sizeof( S3D_ENUM ) * NUM_CONROLS );
	}
	btKinematicCharacterController *controller;
	S3D_ENUM control[NUM_CONROLS];
	float walkVelocity;
	float rotationStep;
};
S3D_Player s3dPlayer;

btVector3 btuGetShapeAabbSize( btCollisionShape *shape )
{
	btVector3 min, max, size;
	btTransform tIdentity;
	tIdentity.setIdentity();
	shape->getAabb( tIdentity, min, max );
	size = max - min;
	return size;
}

btRigidBody *btuAddRigidBody( btScalar mass, btCollisionShape *shape, const btTransform &defaultTransform )
{
	//rigidbody is dynamic if and only if mass is non zero, otherwise static
	bool isDynamic = mass != 0.f;

	btVector3 localInertia( 0, 0, 0 );
	if ( isDynamic )
	{
		shape->calculateLocalInertia( mass, localInertia );
	}

	//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
	btDefaultMotionState* myMotionState = new btDefaultMotionState( defaultTransform );
	btRigidBody::btRigidBodyConstructionInfo rbInfo( mass, myMotionState, shape, localInertia );
	btRigidBody* body = new btRigidBody(rbInfo);

	if ( isDynamic )
	{
		btVector3 half = btuGetShapeAabbSize( shape ) * .5f;
		btScalar motionThreshold = min( min( half.getX(), half.getY() ), half.getZ() );

		body->setCcdMotionThreshold( motionThreshold );
		body->setCcdSweptSphereRadius( motionThreshold * 0.9f );

		body->forceActivationState( DISABLE_DEACTIVATION );
		body->setSleepingThresholds( 0.0f, 0.0f );
		body->setLinearFactor( btVector3( 1, 1, 1 ) );
		body->setAngularFactor( 1.f );
	}

	//add the body to the dynamics world
	dynamicsWorld->addRigidBody( body );
	return body;
}

void s3dSetAsPlayer( S3D_PhysicalObject &obj, float capsuleRadius, float capsuleHeight, float stepHeight, float jumpSpeed, float walkVelocity, float rotationStep )
{
	btTransform transform;
	transform.setFromOpenGLMatrix( (const btScalar*)&obj.mat );

	btPairCachingGhostObject *ghostObject;

	s3dPlayer.walkVelocity = walkVelocity;
	s3dPlayer.rotationStep = rotationStep;
	if ( !s3dPlayer.controller )
	{
		ghostObject = new btPairCachingGhostObject();
		ghostObject->setActivationState( DISABLE_DEACTIVATION );
		ghostObject->setCollisionFlags( btCollisionObject::CF_CHARACTER_OBJECT );
		ghostObject->setWorldTransform( transform );

		btCapsuleShapeZ *shape = new btCapsuleShapeZ( capsuleRadius, capsuleHeight );
		int upAxis = shape->getUpAxis();
		ghostObject->setCollisionShape( shape );

		s3dPlayer.controller = new btKinematicCharacterController( ghostObject, shape, stepHeight, upAxis );
		btVector3 gravity = dynamicsWorld->getGravity();
		s3dPlayer.controller->setGravity( -gravity[upAxis] );
		s3dPlayer.controller->setJumpSpeed( jumpSpeed );
		s3dPlayer.controller->setFallSpeed( jumpSpeed );
		
		dynamicsWorld->addCollisionObject( ghostObject, btBroadphaseProxy::CharacterFilter, 
			btBroadphaseProxy::StaticFilter | btBroadphaseProxy::DefaultFilter );
	}
	else
	{
		ghostObject = s3dPlayer.controller->getGhostObject();
		ghostObject->setWorldTransform( transform );

		btCapsuleShapeZ* old_shape = (btCapsuleShapeZ*)ghostObject->getCollisionShape();
		if ( old_shape ) delete old_shape;

		btCapsuleShapeZ* shape = new btCapsuleShapeZ( capsuleRadius, capsuleHeight );
		int upAxis = shape->getUpAxis();
		ghostObject->setCollisionShape( shape );

		dynamicsWorld->removeAction( s3dPlayer.controller );
		delete s3dPlayer.controller;
		s3dPlayer.controller = new btKinematicCharacterController( ghostObject, shape, stepHeight, upAxis );

		btVector3 gravity = dynamicsWorld->getGravity();
		s3dPlayer.controller->setGravity( -gravity[upAxis] );
		s3dPlayer.controller->setJumpSpeed( jumpSpeed );
		s3dPlayer.controller->setFallSpeed( jumpSpeed );
	}

	dynamicsWorld->addAction( s3dPlayer.controller );

	overlappingPairCache->getOverlappingPairCache()->cleanProxyFromPairs( ghostObject->getBroadphaseHandle(), dispatcher );
	s3dPlayer.controller->reset( dynamicsWorld );

	obj.handle = (void*)ghostObject;
}

void s3dControlCharacter( int l, int r, int f, int b, int j )
{
	s3dPlayer.control[LEFT] = l;
	s3dPlayer.control[RIGHT] = r;
	s3dPlayer.control[FORWARD] = f;
	s3dPlayer.control[BACKWARD] = b;
	s3dPlayer.control[JUMP] = j;
}

void s3dAddRigidBox( S3D_PhysicalObject &obj,  const CS3DVec3f &size )
{
	btTransform transform;
	transform.setFromOpenGLMatrix( (const btScalar*)&obj.mat );
	btCollisionShape *shape = new btBoxShape( btVector3( size.x * .5f, size.y * .5f, size.z * .5f ) );
	obj.handle = (void*)btuAddRigidBody( (btScalar)obj.mass, shape, transform );
}

CS3DVec3f s3dGetLinearVelocity( S3D_PhysicalObject &obj )
{
	btRigidBody *body = btRigidBody::upcast( (btCollisionObject*)obj.handle );
	btVector3 vel( 0, 0, 0 );
	if ( body )
	{
		vel = body->getLinearVelocity();
	}
	return CS3DVec3f( vel.getX(), vel.getY(), vel.getZ() );
}

void s3dSetVelocity( S3D_PhysicalObject &obj, const CS3DVec3f &linear, const CS3DVec3f &angular )
{
	btRigidBody *body = btRigidBody::upcast( (btCollisionObject*)obj.handle );
	if ( body )
	{
		body->setLinearVelocity( btVector3( linear.x, linear.y, linear.z ) );
		body->setAngularVelocity( btVector3( angular.x, angular.y, angular.z ) );
	}
}

void s3dProceedToTransform( S3D_PhysicalObject &obj )
{
	btRigidBody *body = btRigidBody::upcast( (btCollisionObject*)obj.handle );
	if ( body )
	{
		btMotionState* motionState = body->getMotionState();
		if ( motionState )
		{
			btTransform transform;
			transform.setFromOpenGLMatrix( (const btScalar*)&obj.mat );
			body->proceedToTransform( transform );
		}
	}
}

void s3dUpdatePhysicalObject( S3D_PhysicalObject &obj )
{
	btCollisionObject *cobj = (btCollisionObject*)obj.handle;
	btTransform transform;
	int flags = cobj->getCollisionFlags();
	if ( flags == btCollisionObject::CF_CHARACTER_OBJECT )
	{
		btGhostObject *ghostObject = btPairCachingGhostObject::upcast( cobj );
		if ( ghostObject )
		{
			transform = ghostObject->getWorldTransform();
			transform.getOpenGLMatrix( (btScalar*)&obj.mat );
		}
	}
	else
	{
		btRigidBody *body = btRigidBody::upcast( cobj );
		btMotionState* motionState = body->getMotionState();
		if ( motionState )
		{
			motionState->getWorldTransform( transform );
			transform.getOpenGLMatrix( (btScalar*)&obj.mat );
		}
	}
}

CS3DVec3f s3dGetAabbSize( S3D_PhysicalObject &obj )
{
	btCollisionObject *cobj = (btCollisionObject*)obj.handle;
	btVector3 aabb = btuGetShapeAabbSize( cobj->getCollisionShape() );
	return CS3DVec3f( aabb.getX(), aabb.getY(), aabb.getZ() );
}

void s3dInitPhysics( CS3DVec3f gravity )
{
	///collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
	collisionConfiguration = new btDefaultCollisionConfiguration();

	///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
	dispatcher = new btCollisionDispatcher( collisionConfiguration );

	///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
	btVector3 worldAabbMin( -10000, -10000, -10000 );
	btVector3 worldAabbMax(  10000,  10000,  10000 );
	overlappingPairCache = new btAxisSweep3( worldAabbMin, worldAabbMax );
	ghostPairCallback = new btGhostPairCallback();
	overlappingPairCache->getOverlappingPairCache()->setInternalGhostPairCallback( ghostPairCallback );

	///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
	solver = new btSequentialImpulseConstraintSolver;

	dynamicsWorld = new btDiscreteDynamicsWorld( dispatcher, overlappingPairCache, solver, collisionConfiguration );
	dynamicsWorld->setGravity( btVector3( gravity.x, gravity.y, gravity.z ) );
}

void s3dSimulationPhysics()
{
	if ( !lastTimeStamp ) lastTimeStamp = s3dCurTime();
	float dt = (float)( s3dCurTime() - lastTimeStamp ) / 1000;
	lastTimeStamp = s3dCurTime();

	if ( s3dPlayer.controller )
	{
		btPairCachingGhostObject *ghostObject = s3dPlayer.controller->getGhostObject();
		btTransform &transform = ghostObject->getWorldTransform ();

		btVector3 walkDirection = btVector3( 0.f, 0.f, 0.f );
		btScalar walkVelocity = s3dPlayer.walkVelocity;//btScalar(1.1f) * 4.0f * 10.f; // 4 km/h -> 1.1 m/s
		btScalar walkSpeed = walkVelocity * dt;

		//rotate view
		if ( s3dPlayer.control[LEFT] == 2 )
		{
			btMatrix3x3 &orn = transform.getBasis();
			btMatrix3x3 mat = btMatrix3x3(btQuaternion(btVector3(0,0,1), s3dPlayer.rotationStep));
			orn *= mat;
		}
		if ( s3dPlayer.control[RIGHT] == 2 )
		{
			btMatrix3x3 &orn = transform.getBasis();
			btMatrix3x3 mat = btMatrix3x3(btQuaternion(btVector3(0,0,1), -s3dPlayer.rotationStep));
			orn *= mat;
		}
		
		btMatrix3x3 orn = transform.getBasis();
		orn.transpose();
		btVector3 forwardDir = orn[1];
		forwardDir[1] = -forwardDir[1];
		forwardDir.normalize ();

		if ( s3dPlayer.control[FORWARD] == 2 )
			walkDirection += forwardDir;

		if ( s3dPlayer.control[BACKWARD] == 2 )
			walkDirection -= forwardDir;	

		if ( s3dPlayer.control[JUMP] == 1 )
			s3dPlayer.controller->jump();

		s3dPlayer.controller->setWalkDirection( walkDirection * walkSpeed );
	}

	dynamicsWorld->stepSimulation( dt );
}

void s3dFreePhysics()
{
	int n;
	//remove the collision objects from the dynamics world and delete them
	n = dynamicsWorld->getNumCollisionObjects();
	btCollisionObjectArray &CollisionObjectArray = dynamicsWorld->getCollisionObjectArray();
	for ( int i = n - 1; i >= 0; i-- )
	{
		btCollisionObject* obj = CollisionObjectArray[i];
		int flags = obj->getCollisionFlags();
		if ( flags != btCollisionObject::CF_CHARACTER_OBJECT )
		{
			btRigidBody* body = btRigidBody::upcast( obj );
			if ( body )
			{
				btMotionState* motionState = body->getMotionState();
				if ( motionState ) delete motionState;
			}
		}
		btCollisionShape *shape = obj->getCollisionShape();
		if ( shape ) delete shape;
		dynamicsWorld->removeCollisionObject( obj );
		delete obj;
	}

	//delete player
	if ( s3dPlayer.controller ) delete s3dPlayer.controller;

	//delete dynamics world
	delete dynamicsWorld;
	//delete solver
	delete solver;
	//delete broadphase
	delete overlappingPairCache;
	delete ghostPairCallback;
	//delete dispatcher
	delete dispatcher;
	//delete collision configuration
	delete collisionConfiguration;

	lastTimeStamp = 0;
}

#endif