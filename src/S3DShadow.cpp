#include <S3D.h>

#if ( S3D_USE_SHADOWS )

S3D_ShadowMap *s3dShadow = 0;

S3D_ShadowMap::S3D_ShadowMap()
{
	enabled = false;
	colorTexture = depthTexture = blurTexture = 0;
	depthFrameBuffer = blurFrameBuffer = 0;
	size = 0;
	depthTextureShader = 0;
}

S3D_ShadowMap::~S3D_ShadowMap()
{
	if ( depthTextureShader ) delete depthTextureShader;

	if ( depthTexture ) S3D_OpenGLOp::DeleteTextures( 1, &depthTexture );
	if ( colorTexture ) S3D_OpenGLOp::DeleteTextures( 1, &colorTexture );
	if ( blurTexture ) S3D_OpenGLOp::DeleteTextures( 1, &blurTexture );
	
	if ( depthFrameBuffer ) s3dBuffer->FreeBuffer( depthFrameBuffer, S3D_FBO );
	if ( blurFrameBuffer ) s3dBuffer->FreeBuffer( blurFrameBuffer, S3D_FBO );
}

bool S3D_ShadowMap::Create()
{
	enabled = false;
	soft = false;
#ifdef S3D_IPHONE_PLATFORM
	if ( !S3D_OpenGLOp::ExtensionSupported( "GL_OES_depth_texture" )
#else
	if ( !S3D_OpenGLOp::ExtensionSupported( "GL_ARB_depth_texture" )
#endif
	  || !s3dLoadedShaders )
	{
		return false;
	}
#if ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
    S3D_ENUM colorTexDataType = 3;
#ifdef S3D_IPHONE_PLATFORM
    if ( S3D_OpenGLOp::ExtensionSupported( "GL_OES_texture_float" )
      && S3D_OpenGLOp::ExtensionSupported( "GL_OES_texture_half_float" )
      && S3D_OpenGLOp::ExtensionSupported( "GL_OES_texture_half_float_linear" )
      /*&& S3D_OpenGLOp::ExtensionSupported( "GL_EXT_color_buffer_half_float" )*/ )
#else
    if ( S3D_OpenGLOp::ExtensionSupported( "GL_ARB_texture_float" ) )
#endif
    {
        colorTexDataType = S3D_FLOAT_TEXTURE;
    }
#endif

	depthTextureShader = new CS3DShader;
#if ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
	if ( depthTextureShader->Load( S3D_VERTEX_SHADER_DEPTH_TEXTURE, S3D_FRAGMENT_SHADER_DEPTH_TEXTURE ) )
#else
	if ( depthTextureShader->Load( S3D_VERTEX_SHADER_DEPTH_TEXTURE, S3D_UNDEFINED ) )
#endif
	{
#if ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
		size = 2048;
#else
		size = 2048;
#endif
        
#if ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
		S3D_OpenGLOp::CreateTexture( depthTexture, 0, size, size, S3D_DEPTH_TEXTURE, false );
		S3D_OpenGLOp::Clamp( true, true );
		S3D_OpenGLOp::CreateTexture( colorTexture, 0, size, size, colorTexDataType, true );
		S3D_OpenGLOp::Clamp( true, true );
		unsigned int textures[] = { depthTexture, colorTexture };
		enabled = s3dBuffer->SetBufferData( depthFrameBuffer, textures, 2, 0, sizeof( unsigned int ), S3D_FBO );
#else
		S3D_OpenGLOp::CreateTexture( depthTexture, 0, size, size, S3D_DEPTH_TEXTURE, false );
		S3D_OpenGLOp::Clamp( true, true );
		unsigned int textures[] = { depthTexture, 0 };
		enabled = s3dBuffer->SetBufferData( depthFrameBuffer, textures, 1, 0, sizeof( unsigned int ), S3D_FBO );
#endif

#if ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
		if ( s3dGaussianBlur )
		{
			S3D_OpenGLOp::CreateTexture( blurTexture, 0, size, size, colorTexDataType, false );
            S3D_OpenGLOp::Clamp( true, true );

			unsigned int textures[] = { 0, blurTexture };
			enabled = s3dBuffer->SetBufferData( blurFrameBuffer, textures, 2, 0, sizeof( unsigned int ), S3D_FBO );
			soft = true;
        }
#endif
        S3D_OpenGLOp::DisableTexture();
        
		fovy = 45.f;
		nearFactor = 0.5f;
		farFactor = 1.0f;
	}
	return enabled;
}

unsigned int S3D_ShadowMap::GetSize()
{
	return size;
}

void S3D_ShadowMap::Draw( CS3DTransformedObject **shadow_casters, unsigned int count )
{
	if ( shadow_casters == 0x0 || !enabled ) return;
    
	s3dBuffer->BindBuffer( depthFrameBuffer, S3D_FBO );
	S3D_OpenGLOp::EnableShadowBuffer( size );
	
	S3D_Light *light = s3dLight->GetLight( 0 );
	CS3DMat16f P, MV;
	CS3DVec3f lightDir;
	CS3DMat16f camMat = s3dCamera->Get();
	
	CS3DVec3f center;
	CS3DMat16f mat;
	mat.rotate( -90, S3D_AXIS_X );
	center = mat * center;
	lightDir = center - light->pos;

	MV.lookAt( CS3DVec3f(), lightDir, CS3DVec3f( 0, 1, 0 ) );
	MV.translate( -light->pos );

#if ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
	float half_size = 500.f;
#else
	CS3DVec3f camPos = s3dCamera->GetPosition();
	float camDist = camPos.magnitude();
	float half_size = camDist * 0.8f;
#endif
	float dist = lightDir.magnitude();
	float zFar = dist * ( 1.0f + farFactor );
	P.ortho( -half_size, half_size, -half_size, half_size, -zFar, zFar );

	float BiasMat[16] = { 0.5f, 0.0f, 0.0f, 0.0f, 
						  0.0f, 0.5f, 0.0f, 0.0f, 
						  0.0f, 0.0f, 0.5f, 0.0f, 
						  0.5f, 0.5f, 0.5f, 1.0f };
	memcpy( &light->mat, BiasMat, sizeof( float ) * 16 );
	CS3DMat16f invCameraMat = camMat;
	invCameraMat.inverse();
	light->mat = invCameraMat * MV * P * light->mat;

	depthTextureShader->BeginShaderProgram();
	for ( unsigned int i = 0; i < count; i++ )
	{
		shadow_casters[i]->DrawShadow( MV, P );
	}
	depthTextureShader->EndShaderProgram();

#if ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
	if ( s3dGaussianBlur && soft )
	{
		CS3DMat16f m;
		m.ortho( 0, (float)size, (float)size, 0, -1, 1 );
		s3dGaussianBlur->Draw( blurFrameBuffer, colorTexture, depthFrameBuffer, blurTexture, size, m );
	}
#endif
	s3dBuffer->UnbindBuffer( S3D_FBO );
	
	S3D_OpenGLOp::DisableShadowBuffer();

	if ( s3dSimpleShader )
	{

		CS3DMat16f invLightMV = MV;
		invLightMV.inverse();
		CS3DMat16f invLightP = P;
		invLightP.inverse();
		
		CS3DMat16f mvpMat = invLightMV * camMat * s3dProjection;
		S3D_Color color( 1, 1, 0, 1 );
		
		CS3DVec4f vnear( -1, 1, -1, 1 );
		vnear = invLightP * vnear;
		vnear /= vnear.w;

		CS3DVec4f vfar( -1, 1, 1, 1 );
		vfar = invLightP * vfar;
		vfar /= vfar.w;

		s3dSimpleShader->BeginShaderProgram();
		s3dSimpleShader->EnableAttribute( 0 );
		s3dSimpleShader->SetUniformf( "u_color", 4, (float*)&color );
		s3dSimpleShader->SetUniformMatrixf( "mvpMat", 4, (float*)&mvpMat );

		CS3DBox box;
        CS3DVec3f points[4] = { vnear.xyz(), vfar.xyz(), -vnear.xyz(), -vfar.xyz() };
		for ( int i = 0; i < 4; i++ )
            box.Set( points[i] );
		S3D_BoundingBox bbox = box.GetBBox();
		float verts0[] = {
			bbox.max[0], bbox.max[1], bbox.min[2],
			bbox.min[0], bbox.max[1], bbox.min[2],
			bbox.min[0], bbox.min[1], bbox.min[2],
			bbox.max[0], bbox.min[1], bbox.min[2],
		};
		s3dSimpleShader->SetAttribPointerf( 0, 3, verts0 );
		S3D_OpenGLOp::DrawPrimitives( S3D_LINE_LOOP, 0, 4 );
		float verts1[] = {
			bbox.max[0], bbox.min[1], bbox.max[2],
			bbox.max[0], bbox.max[1], bbox.max[2],
			bbox.min[0], bbox.max[1], bbox.max[2],
			bbox.min[0], bbox.min[1], bbox.max[2],
		};
		s3dSimpleShader->SetAttribPointerf( 0, 3, verts1 );
		S3D_OpenGLOp::DrawPrimitives( S3D_LINE_LOOP, 0, 4 );
		float verts2[] = {
			bbox.max[0], bbox.max[1], bbox.min[2],
			bbox.max[0], bbox.max[1], bbox.max[2],
			bbox.min[0], bbox.max[1], bbox.max[2],
			bbox.min[0], bbox.max[1], bbox.min[2],
		};
		s3dSimpleShader->SetAttribPointerf( 0, 3, verts2 );
		S3D_OpenGLOp::DrawPrimitives( S3D_LINE_LOOP, 0, 4 );
		float verts3[] = {
			bbox.max[0], bbox.min[1], bbox.max[2],
			bbox.min[0], bbox.min[1], bbox.max[2],
			bbox.min[0], bbox.min[1], bbox.min[2],
			bbox.max[0], bbox.min[1], bbox.min[2],
		};
		s3dSimpleShader->SetAttribPointerf( 0, 3, verts3 );
		S3D_OpenGLOp::DrawPrimitives( S3D_LINE_LOOP, 0, 4 );

		s3dSimpleShader->DisableAttribute( 0 );
		s3dSimpleShader->EndShaderProgram();
	}
}
#endif
