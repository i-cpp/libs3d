#include <S3D.h>
#include <S3DPlatform.h>

#if ( !S3D_OGLES_2_0 )
// VBO:
PFNGLISBUFFERPROC glIsBuffer = 0;
PFNGLDELETEBUFFERSPROC glDeleteBuffers = 0;
PFNGLGENBUFFERSPROC glGenBuffers = 0;
PFNGLBINDBUFFERPROC glBindBuffer = 0;
PFNGLBUFFERDATAPROC glBufferData = 0;
PFNGLBUFFERSUBDATAPROC glBufferSubData = 0;
PFNGLGETBUFFERSUBDATAPROC glGetBufferSubData = 0;
PFNGLGETBUFFERPARAMETERIVPROC glGetBufferParameteriv = 0;
// VAO:
PFNGLBINDVERTEXARRAYPROC glBindVertexArray = 0;
PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays = 0;
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = 0;
PFNGLISVERTEXARRAYPROC glIsVertexArray = 0;
// FBO:
PFNGLGENFRAMEBUFFERSPROC glGenFramebuffers = 0;
PFNGLBINDFRAMEBUFFERPROC glBindFramebuffer = 0;
PFNGLFRAMEBUFFERTEXTURE2DPROC glFramebufferTexture2D = 0;
PFNGLDELETEFRAMEBUFFERSPROC glDeleteFramebuffers = 0;
PFNGLCHECKFRAMEBUFFERSTATUSPROC glCheckFramebufferStatus = 0;
#endif

#ifdef S3D_VAO
bool vaoSupported = false;
#endif

S3D_TexMemChunks *s3dVideoMemAlloted = 0;

CS3DBuffer::CS3DBuffer()
{
	s3dVideoMemAlloted = new S3D_TexMemChunks;
	s3dVideoMemAlloted->clear();
#if ( !S3D_OGLES_2_0 )
	S3D_GET_FUNC( PFNGLISBUFFERPROC, glIsBuffer, "glIsBuffer" )
	S3D_GET_FUNC( PFNGLDELETEBUFFERSPROC, glDeleteBuffers, "glDeleteBuffers" )
	S3D_GET_FUNC( PFNGLGENBUFFERSPROC, glGenBuffers, "glGenBuffers" )
	S3D_GET_FUNC( PFNGLBINDBUFFERPROC, glBindBuffer, "glBindBuffer" )
	S3D_GET_FUNC( PFNGLBUFFERDATAPROC, glBufferData, "glBufferData" )
	S3D_GET_FUNC( PFNGLBUFFERSUBDATAPROC, glBufferSubData, "glBufferSubData" )
	S3D_GET_FUNC( PFNGLGETBUFFERSUBDATAPROC, glGetBufferSubData, "glGetBufferSubData" )
	S3D_GET_FUNC( PFNGLGETBUFFERPARAMETERIVPROC, glGetBufferParameteriv, "glGetBufferParameteriv" )
#ifdef S3D_VAO
	if ( S3D_OpenGLOp::ExtensionSupported( "GL_ARB_vertex_array_object" ) )
	{
		vaoSupported = true;
		S3D_GET_FUNC( PFNGLBINDVERTEXARRAYPROC, glBindVertexArray, "glBindVertexArray" )
		S3D_GET_FUNC( PFNGLDELETEVERTEXARRAYSPROC, glDeleteVertexArrays, "glDeleteVertexArrays" )
		S3D_GET_FUNC( PFNGLGENVERTEXARRAYSPROC, glGenVertexArrays, "glGenVertexArrays" )
		S3D_GET_FUNC( PFNGLISVERTEXARRAYPROC, glIsVertexArray, "glIsVertexArray" )
	}
#endif
	if ( S3D_OpenGLOp::ExtensionSupported( "GL_ARB_framebuffer_object" ) )
	{
		S3D_GET_FUNC( PFNGLGENFRAMEBUFFERSPROC, glGenFramebuffers, "glGenFramebuffers" )
		S3D_GET_FUNC( PFNGLBINDFRAMEBUFFERPROC, glBindFramebuffer, "glBindFramebuffer" )
		S3D_GET_FUNC( PFNGLFRAMEBUFFERTEXTURE2DPROC, glFramebufferTexture2D, "glFramebufferTexture2D" )
		S3D_GET_FUNC( PFNGLDELETEFRAMEBUFFERSPROC, glDeleteFramebuffers, "glDeleteFramebuffers" )
		S3D_GET_FUNC( PFNGLCHECKFRAMEBUFFERSTATUSPROC, glCheckFramebufferStatus, "glCheckFramebufferStatus" )
	}
	else if ( S3D_OpenGLOp::ExtensionSupported( "GL_EXT_framebuffer_object" ) )
	{
		S3D_GET_FUNC( PFNGLGENFRAMEBUFFERSPROC, glGenFramebuffers, "glGenFramebuffersEXT" )
		S3D_GET_FUNC( PFNGLBINDFRAMEBUFFERPROC, glBindFramebuffer, "glBindFramebufferEXT" )
		S3D_GET_FUNC( PFNGLFRAMEBUFFERTEXTURE2DPROC, glFramebufferTexture2D, "glFramebufferTexture2DEXT" )
		S3D_GET_FUNC( PFNGLDELETEFRAMEBUFFERSPROC, glDeleteFramebuffers, "glDeleteFramebuffersEXT" )
		S3D_GET_FUNC( PFNGLCHECKFRAMEBUFFERSTATUSPROC, glCheckFramebufferStatus, "glCheckFramebufferStatusEXT" )
	}
#endif
}

CS3DBuffer::~CS3DBuffer()
{
	if ( s3dVideoMemAlloted )
	{
		delete s3dVideoMemAlloted;
	}
}

unsigned int CS3DBuffer::MemAlloted()
{
	if ( s3dVideoMemAlloted )
	{
		unsigned int total_size = 0;
		unsigned int count_of_alloted = (unsigned int)s3dVideoMemAlloted->size();
		for ( unsigned int i = 0; i < count_of_alloted; i++ )
		{
			total_size += (*s3dVideoMemAlloted)[i].size;
		}
		return total_size;
	}
	return 0;
}

void CS3DBuffer::FreeBuffer( unsigned int &id, unsigned int tech )
{
	if ( tech == S3D_VBO || tech == S3D_IBO )
	{
		if ( glIsBuffer( id ) )
		{
			glDeleteBuffers( 1, &id );
			if ( s3dVideoMemAlloted )
			{
				unsigned int count_of_alloted = (unsigned int)s3dVideoMemAlloted->size();
				for ( unsigned int i = 0; i < count_of_alloted; i++ )
				{
					if ( id == (*s3dVideoMemAlloted)[i].id )
					{
						s3dVideoMemAlloted->erase( s3dVideoMemAlloted->begin() + i );
						break;
					}
				}
			}
		}
	}
#ifdef S3D_VAO
	else if ( vaoSupported && tech == S3D_VAO )
	{
		glDeleteVertexArrays( 1, &id );
	}
#endif
#ifdef S3D_FBO
	else if ( tech == S3D_FBO )
	{
		glDeleteFramebuffers( 1, &id );
	}
#endif
}

bool CS3DBuffer::Supported( unsigned int tech )
{
	switch( tech )
	{
	case S3D_VBO: { return true; } break;
	case S3D_IBO: { return true; } break;
#ifdef S3D_VAO
	case S3D_VAO: { return vaoSupported; } break;
#endif
#ifdef S3D_FBO
	case S3D_FBO: { return true; } break;
#endif
	}
	return false;
}

void CS3DBuffer::BindBuffer( unsigned int &id, unsigned int tech )
{
	switch( tech )
	{
	case S3D_VBO:
		{
			if ( glIsBuffer( id ) )
				glBindBuffer( GL_ARRAY_BUFFER, id );
		}
		break;
	case S3D_IBO:
		{
			if ( glIsBuffer( id ) )
				glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, id );
		}
		break;
#ifdef S3D_VAO
	case S3D_VAO:
		{
			if ( vaoSupported )
				glBindVertexArray( id );
		}
		break;
#endif
#ifdef S3D_FBO
	case S3D_FBO:
		{
			glBindFramebuffer( GL_FRAMEBUFFER, id );
		}
		break;
#endif
	}
}

void CS3DBuffer::UnbindBuffer( unsigned int tech )
{
	switch( tech )
	{
	case S3D_VBO:
		glBindBuffer( GL_ARRAY_BUFFER, 0 );
		break;
	case S3D_IBO:
		glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
		break;
#ifdef S3D_VAO
	case S3D_VAO:
		if ( vaoSupported ) glBindVertexArray( 0 );
		break;
#endif
#ifdef S3D_FBO
	case S3D_FBO:
		glBindFramebuffer( GL_FRAMEBUFFER, 0 );
		break;
#endif
	}
}

bool CS3DBuffer::SetBufferData( unsigned int &id, void *data,
	int len, int offset, unsigned int sizeElem, unsigned int tech )
{
	if ( tech == S3D_VBO || tech == S3D_IBO )
	{
		unsigned int mode = tech == S3D_IBO ? GL_ELEMENT_ARRAY_BUFFER : GL_ARRAY_BUFFER;
		if ( !glIsBuffer( id ) )
		{
			glGenBuffers( 1, &id );
			glBindBuffer( mode, id );
			glBufferData( mode, sizeElem * len, data, GL_STATIC_DRAW );
			glBindBuffer( mode, 0 );
		}
		else
		{
			glBindBuffer( mode, id );
			glBufferSubData( mode, sizeElem * offset, sizeElem * len, data );
			glBindBuffer( mode, 0 );
		}
		if ( s3dVideoMemAlloted )
			s3dVideoMemAlloted->push_back( S3D_VideoMemChunk( id, sizeElem * len ) );
        return true;
	}
#ifdef S3D_VAO
	else if ( vaoSupported && tech == S3D_VAO )
	{
		glGenVertexArrays( 1, &id );
        return true;
	}
#endif
#ifdef S3D_FBO
	else if ( tech == S3D_FBO && sizeElem == sizeof( unsigned int ) )
	{
		glGenFramebuffers( 1, &id );
		glBindFramebuffer( GL_FRAMEBUFFER, id );
        if ( data )
        {
            unsigned int depth = *((unsigned int*)data);
            if ( depth )
            {
                glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth, 0 );
            }
            for ( int i = 1; i < len; i++ )
            {
                GLenum att = GL_COLOR_ATTACHMENT0 + ( i - 1 );
                unsigned int color = ((unsigned int*)data)[i];
                glFramebufferTexture2D( GL_FRAMEBUFFER, att, GL_TEXTURE_2D, color, 0 );
            }
#if ( !S3D_OGLES_2_0 )
			if ( len < 2 )
			{
				glDrawBuffer( 0 );
			}
#endif
            GLenum status = glCheckFramebufferStatus( GL_FRAMEBUFFER );
            glBindFramebuffer( GL_FRAMEBUFFER, 0 );
            return status == GL_FRAMEBUFFER_COMPLETE;
        }
	}
#endif
	else id = ~0;
    return false;
}


