#include <S3D.h>

CS3DVec3f::CS3DVec3f()
{
    v[0] = 0;
    v[1] = 0;
    v[2] = 0;
}

CS3DVec3f::CS3DVec3f( float scalar )
{
    v[0] = scalar;
    v[1] = scalar;
    v[2] = scalar;
}

CS3DVec3f::CS3DVec3f( float x, float y, float z )
{
    v[0] = x;
    v[1] = y;
    v[2] = z;
}

void CS3DVec3f::set( float *values )
{
	if ( !values ) return;
    v[0] = values[0];
    v[1] = values[1];
    v[2] = values[2];
}

float &CS3DVec3f::operator[]( int index )
{
    return v[index];
}

float CS3DVec3f::operator[]( int index ) const
{
    return v[index];
}

CS3DVec3f CS3DVec3f::operator+( float scalar ) const
{
    return CS3DVec3f( v[0] + scalar, v[1] + scalar, v[2] + scalar );
}

CS3DVec3f CS3DVec3f::operator-( float scalar ) const
{
    return CS3DVec3f( v[0] - scalar, v[1] - scalar, v[2] - scalar );
}

CS3DVec3f CS3DVec3f::operator*( float scalar ) const
{
    return CS3DVec3f( v[0] * scalar, v[1] * scalar, v[2] * scalar );
}

CS3DVec3f CS3DVec3f::operator/( float scalar ) const
{
    return CS3DVec3f( v[0] / scalar, v[1] / scalar, v[2] / scalar );
}

CS3DVec3f CS3DVec3f::operator+( const CS3DVec3f &other ) const
{
    return CS3DVec3f( v[0] + other.v[0], v[1] + other.v[1], v[2] + other.v[2] );
}

CS3DVec3f CS3DVec3f::operator-( const CS3DVec3f &other ) const
{
    return CS3DVec3f( v[0] - other.v[0], v[1] - other.v[1], v[2] - other.v[2] );
}

CS3DVec3f CS3DVec3f::operator*( const CS3DVec3f &other ) const
{
    return CS3DVec3f( v[0] * other.v[0], v[1] * other.v[1], v[2] * other.v[2] );
}

CS3DVec3f CS3DVec3f::operator/( const CS3DVec3f &other ) const
{
    return CS3DVec3f( v[0] / other.v[0], v[1] / other.v[1], v[2] / other.v[2] );
}

CS3DVec3f CS3DVec3f::operator^( const CS3DVec3f &other ) const
{
	return CS3DVec3f( v[1] * other.v[2] - v[2] * other.v[1],
					  v[2] * other.v[0] - v[0] * other.v[2],
					  v[0] * other.v[1] - v[1] * other.v[0] );
}

CS3DVec3f CS3DVec3f::operator-() const
{
    return CS3DVec3f( -v[0], -v[1], -v[2] );
}

bool CS3DVec3f::operator!() const
{
    return ( !v[0] && !v[1] && !v[2] );
}

bool CS3DVec3f::operator==( const CS3DVec3f &other ) const
{
    return ( v[0] == other.v[0] && v[1] == other.v[1] && v[2] == other.v[2] );
}

bool CS3DVec3f::operator!=( const CS3DVec3f &other ) const
{
    return ( v[0] != other.v[0] || v[1] != other.v[1] || v[2] != other.v[2] );
}

const CS3DVec3f &CS3DVec3f::operator+=( float scalar )
{
    v[0] += scalar;
    v[1] += scalar;
    v[2] += scalar;
    return *this;
}

const CS3DVec3f &CS3DVec3f::operator-=( float scalar )
{
    v[0] -= scalar;
    v[1] -= scalar;
    v[2] -= scalar;
    return *this;
}

const CS3DVec3f &CS3DVec3f::operator*=( float scalar )
{
    v[0] *= scalar;
    v[1] *= scalar;
    v[2] *= scalar;
    return *this;
}

const CS3DVec3f &CS3DVec3f::operator/=( float scalar )
{
    v[0] /= scalar;
    v[1] /= scalar;
    v[2] /= scalar;
    return *this;
}

const CS3DVec3f &CS3DVec3f::operator+=( const CS3DVec3f &other )
{
    v[0] += other.v[0];
    v[1] += other.v[1];
    v[2] += other.v[2];
    return *this;
}

const CS3DVec3f &CS3DVec3f::operator-=( const CS3DVec3f &other )
{
    v[0] -= other.v[0];
    v[1] -= other.v[1];
    v[2] -= other.v[2];
    return *this;
}

const CS3DVec3f &CS3DVec3f::operator*=( const CS3DVec3f &other )
{
    v[0] *= other.v[0];
    v[1] *= other.v[1];
    v[2] *= other.v[2];
    return *this;
}

const CS3DVec3f &CS3DVec3f::operator/=( const CS3DVec3f &other )
{
    v[0] /= other.v[0];
    v[1] /= other.v[1];
    v[2] /= other.v[2];
    return *this;
}

float CS3DVec3f::magnitude() const
{
    return sqrtf( v[0] * v[0] + v[1] * v[1] + v[2] * v[2] );
}

float CS3DVec3f::magnitudeSquared() const
{
    return v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
}

void CS3DVec3f::normalize()
{
    float m = magnitude();
	if ( m == 0.f ) return;
	m = 1.f / m;
	v[0] *= m;
	v[1] *= m;
	v[2] *= m;
}

float CS3DVec3f::dot( const CS3DVec3f &other ) const
{
    return v[0] * other.v[0] + v[1] * other.v[1] + v[2] * other.v[2];
}

CS3DVec3f CS3DVec3f::maximize( const CS3DVec3f &other )
{
    CS3DVec3f result = *this;
    if ( other.v[0] > v[0] ) result[0] = other.v[0];
    if ( other.v[1] > v[1] ) result[1] = other.v[1];
    if ( other.v[2] > v[2] ) result[2] = other.v[2];
    return result;
}

CS3DVec3f CS3DVec3f::minimize( const CS3DVec3f &other )
{
    CS3DVec3f result = *this;
    if ( other.v[0] < v[0] ) result[0] = other.v[0];
    if ( other.v[1] < v[1] ) result[1] = other.v[1];
    if ( other.v[2] < v[2] ) result[2] = other.v[2];
    return result;
}
