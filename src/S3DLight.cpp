#include <S3D.h>

CS3DLight::CS3DLight()
{
	memset( &LightSources, 0, sizeof( S3D_Light ) * S3D_MAX_LIGHTS );
}

CS3DLight::~CS3DLight()
{
}

int CS3DLight::NumLights()
{
	int count = 0;
	for ( int i = 0; i < S3D_MAX_LIGHTS; i++ )
	{
		if ( LightSources[i].enabled )
			count++;
	}
	return count;
}

void CS3DLight::LoadLight( const char *filename )
{
	memset( &LightSources, 0, sizeof( S3D_Light ) * S3D_MAX_LIGHTS );
	for ( S3D_ENUM i = 0; i < S3D_MAX_LIGHTS; i++ )
	{
		S3D_Light *l = &LightSources[i];
		l->ambient.a = l->diffuse.a = l->specular.a = 1;
		l->mat.identity();
	}
	void *io = s3dIOController->OpenFile( filename );
	if ( io )
	{
		char buffer[0x100] = { 0 };
		char *cur_section = 0;
		char cur_param[0x10] = { 0 };
		int cur_param_id = 0;
		bool change_param = false;
		bool change_token = false;
		enum STATE {
			UNDEFINED,
			BEGIN_SECTION,
			IN_SECTION,
			END_SECTION,
			BEGIN_TOKEN,
		};
		STATE state = UNDEFINED;
		while ( !s3dIOController->Eof() )
		{
			char ch = (char)s3dIOController->ReadByte();

			if ( ch == '{' )
			{
				state = BEGIN_SECTION;
				ch = (char)s3dIOController->ReadByte();
			}
			else if ( ch == '}' )
			{
				state = END_SECTION;
				ch = 0;
			}
			else if ( ch == '[' )
			{
				state = BEGIN_TOKEN;
				ch = (char)s3dIOController->ReadByte();
			}
			else if ( ch == ']' )
			{
				change_token = true;
				change_param = true;
			}
			else if ( ch == ',' )
			{
				change_param = true;
			}
			if ( s3dIOController->Eof() )
				break;
			if ( S3D_ISSPASE( ch ) ) continue;

			switch ( state )
			{
			case BEGIN_SECTION:
				buffer[ strlen( buffer ) ] = ch;
				break;
			case IN_SECTION:
				buffer[ strlen( buffer ) ] = ch;
				break;
			case END_SECTION:
				{
					cur_param_id = 0;
					if ( *buffer != S3D_SLASH )
					{
						if ( cur_section ) free( cur_section );
						int size = sizeof( char ) * ( strlen( buffer ) + 1 );
						cur_section = (char *)malloc( size );
						strcpy( cur_section, buffer );
						memset( buffer, 0, sizeof( buffer ) );
						cur_section[ size - 1 ] = 0;
						*buffer = ch;
						state = IN_SECTION;
					}
					else
					{
						memset( buffer, 0, sizeof( buffer ) );
						state = UNDEFINED;
					}
				}
				break;
			case BEGIN_TOKEN:
				{
					if ( cur_section )
					{
						if ( change_param )
						{
							if ( ( cur_section[0] == 'l' )
							  && ( cur_section[1] == 'i' )
							  && ( cur_section[2] == 'g' )
							  && ( cur_section[3] == 'h' )
							  && ( cur_section[4] == 't' ) )
							{
								int cur = cur_section[5] - 48;
								if ( cur < S3D_MAX_LIGHTS )
								{
									if ( !strcmp( buffer, "ambient" ) )
										LightSources[ cur ].ambient[ cur_param_id ] = (float)(atoi( cur_param ) / 255.f);
									else if ( !strcmp( buffer, "diffuse" ) )
										LightSources[ cur ].diffuse[ cur_param_id ] = (float)(atoi( cur_param ) / 255.f);
									else if ( !strcmp( buffer, "specular" ) )
										LightSources[ cur ].specular[ cur_param_id ] = (float)(atoi( cur_param ) / 255.f);
									else if ( !strcmp( buffer, "position" ) )
										LightSources[ cur ].pos[ cur_param_id ] = (float)atof( cur_param );
									LightSources[ cur ].enabled = true;
								}
								else s3dIOController->PrintLog( "Can not load the light source.\n" );
							}
							cur_param_id++;
							memset( cur_param, 0, sizeof( cur_param ) );
							change_param = false;
						}
						else cur_param[ strlen( cur_param ) ] = ch;
						if ( change_token )
						{
							memset( buffer, 0, sizeof( buffer ) );
							cur_param_id = 0;
							change_token = false;
							state = IN_SECTION;
						}
					}
				}
				break;
			case UNDEFINED:
				/*do nothing*/
				break;
			}
		}
		free ( cur_section );
		s3dIOController->CloseFile( io );
	}
}

void CS3DLight::SetLight( S3D_Light *light, unsigned int id )
{
    if ( light && id < S3D_MAX_LIGHTS )
		LightSources[ id ] = *light;
}

S3D_Light *CS3DLight::GetLight( int id )
{
	if ( id < S3D_MAX_LIGHTS )
		return &LightSources[id];
	return 0;
}

void CS3DLight::EnableLighting()
{
#if ( !S3D_OGLES_2_0 )
	S3D_OpenGLOp::EnableLighting( LightSources );
#endif
}

void CS3DLight::DisableLighting()
{
#if ( !S3D_OGLES_2_0 )
	S3D_OpenGLOp::DisableLighting( LightSources );
#endif
}
