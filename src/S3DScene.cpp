#include <S3D.h>

struct point {
	point() {}
	point( float _x, float _y ) { x = _x; y = _y; }
	float x, y;
};

struct segment { point a, b; };
struct corner { point a, b, c; };

struct SceneDef
{
	std::vector< corner > corners;
	std::vector< segment > walls;
	std::vector< std::vector< int > > neighbours;
	std::vector< int > border;
	std::vector< float > potentials;
	std::vector< int > links;
	std::vector< std::vector< point > > polygons;
	segment normal;
};

point operator+ ( point a, point b );
point operator- ( point a, point b );
point operator* ( point a, float k );
point operator/ ( point a, float k );
bool operator== ( point a, point b );
bool operator!= ( point a, point b );
float magnitude( point a );
float dist( point a, point b );
float relPos( point d1, point d2 );
bool isSideCorner( corner c, point p );
bool difSides( point p1, point p2, segment s );
bool crosses( segment s1, segment s2 );
bool isOuter( corner c );
float dot( point a, point b );
bool insideCircle( point center, float r, point pt );

point operator+ ( point a, point b )
{
	return point( a.x + b.x, a.y + b.y );
}

point operator- ( point a, point b )
{
	return point( a.x - b.x, a.y - b.y );
}

point operator* ( point a, float k )
{
	return point( a.x * k, a.y * k );
}

point operator/ ( point a, float k )
{
	return point( a.x / k, a.y / k );
}

bool operator== ( point a, point b )
{
	return ( ( a.x == b.x ) && ( a.y == b.y ) );
}

bool operator!= ( point a, point b )
{
	return ( ( a.x != b.x ) || ( a.y != b.y ) );
}

float magnitude( point a )
{
	return sqrtf( a.x * a.x + a.y * a.y );
}

float dist( point a, point b )
{
	return magnitude( a - b );
}

float relPos( point d1, point d2 )
{
	return -d1.x * d2.y + d2.x * d1.y;
}

bool isSideCorner( corner c, point p )
{
	point dir = c.b - p;
	return relPos( dir, c.a - c.b ) * relPos( dir, c.c - c.b ) >= -0.0001f;
}

bool difSides( point p1, point p2, segment s )
{
	float a = s.b.y - s.a.y;
	float b = s.a.x - s.b.x;
	float c = -s.a.x * a - s.a.y * b;
	return ( a * p1.x + b * p1.y + c ) * 
		   ( a * p2.x + b * p2.y + c ) <= 0;
}

bool crosses( segment s1, segment s2 )
{
	return difSides( s1.a, s1.b, s2 ) 
		&& difSides( s2.a, s2.b, s1 );
}

bool isOuter( corner c )
{
	return ( c.a.x * ( c.b.y - c.c.y ) + 
		     c.b.x * ( c.c.y - c.a.y ) + 
			 c.c.x * ( c.a.y - c.b.y ) ) > 0;
}

float dot( point a, point b )
{
	return a.x * b.x + a.y * b.y;
}

bool insideCircle( point center, float r, point pt )
{
	return sqrt( (center.x-pt.x) * (center.x-pt.x) + 
				 (center.y-pt.y) * (center.y-pt.y) ) <= r;
}

bool s3dInsidePoly( float poly[], int num_verts, float x, float y )
{
	bool parity = false;
	int i, j;
	point pi, pj;
	point *pts = new point[ num_verts ];
	point s( x, y );
	j = 0;
	for ( i = 0; i < num_verts * 2; i += 2 )
	{
		pts[j].x = poly[i];
		pts[j].y = poly[i + 1];
		j++;
	}
	for( i = 0, j = num_verts - 1; i < num_verts; j = i++ )
	{
		pi = pts[i]; pj = pts[j];
		if ( ( ( pi.y < s.y && s.y <= pj.y ) || ( pj.y < s.y && s.y <= pi.y ) )
		  && ( s.x > ( pj.x - pi.x ) * ( s.y - pi.y ) / ( pj.y - pi.y ) + pi.x ) )
		{
			parity = !parity;
		}
	}

	delete [] pts;
	return parity;
}

CS3DScene::CS3DScene()
{
	bInit = false;
	Scene = new SceneDef();
}

CS3DScene::~CS3DScene()
{
	delete Scene;
}

void CS3DScene::ProcessCorner( float *p0, float *p1, float *p )
{
	corner c = { 
		point( p0[0], p0[1] ), 
		point( p1[0], p1[1] ), 
		point( p [0], p [1] ) };
	if ( isOuter( c ) )
		Scene->corners.push_back( c );
	segment w = { point( p1[0], p1[1] ), point( p[0], p[1] ) };
	Scene->walls.push_back( w );
	p0[0] = p1[0]; p0[1] = p1[1];
	p1[0] =  p[0]; p1[1] =  p[1];
}

void CS3DScene::ProcessPair( int i, int j )
{
	point a = Scene->corners[i].b;
	point b = Scene->corners[j].b;
	if ( IsClearLine( a.x, a.y, b.x, b.y ) )
	{
		Scene->neighbours[i].push_back( j );
		Scene->neighbours[j].push_back( i );
	}
}

bool CS3DScene::IsInit()
{
	return bInit;
}

bool CS3DScene::Hover( float x, float y, float *pt, float max_len )
{
	bool parity = false;
	int size;
	float* poly;
	for ( int n = 0; n < (int)Scene->polygons.size(); n++ )
	{
		size = (int)Scene->polygons[n].size();
		poly = (float*)&Scene->polygons[n][0];
		parity = s3dInsidePoly( poly, size, x, y );
		if ( parity ) break;
	}
	if ( pt )
	{
		if ( !parity )
		{
			Scene->normal.b.x = x;   Scene->normal.b.y = y;
			Scene->normal.a.x = S3D_INFINITY; Scene->normal.a.y = S3D_INFINITY;
			point r( S3D_INFINITY, S3D_INFINITY );
			for ( int i = 0; i < (int)Scene->walls.size(); i++ )
			{
				segment s = Scene->walls[i];
				point v = s.b - s.a;
				point w = Scene->normal.b - s.a;

				float c1 = dot( w, v );
				if ( c1 <= 0 ) r = s.a * 0.8f + s.b * 0.2f;
				else
				{
					float c2 = dot( v, v );
					if ( c2 <= c1 ) r = s.b * 0.8f + s.a * 0.2f;
					else r = s.a + v * ( c1 / c2 );
				}
				if ( dist( r, Scene->normal.b ) < dist( Scene->normal.a, Scene->normal.b ) )
				{
					Scene->normal.a.x = r.x;
					Scene->normal.a.y = r.y;
				}
			}

			float len = sqrtf( powf(Scene->normal.a.x - x, 2) + powf(Scene->normal.a.y - y, 2) );
			if ( len > max_len )
			{
				pt[0] = (float)(unsigned)~0;
				pt[1] = (float)(unsigned)~0;
				return false;
			}

			float into = 0.1f;
			float blend;
			point w;
			const float EPS = 0.00001f;
			while ( !parity )
			{
				if ( into < -(EPS * 5.0f) )
				{
					w = Scene->normal.a;
					break;
				}
				blend = 1.0f + into;
				w = Scene->normal.b * ( 1.0f - blend ) + Scene->normal.a * blend;
				for ( int n = 0; n < (int)Scene->polygons.size(); n++ )
				{
					size = (int)Scene->polygons[n].size();
					poly = (float*)&Scene->polygons[n][0];
					parity = s3dInsidePoly( poly, size, w.x, w.y );
					if ( parity ) break;
				}
				into = ( into >= EPS ? into / 2.0f : into - EPS);
			}
			parity = false;
			Scene->normal.a = w;

			pt[0] = Scene->normal.a.x;
			pt[1] = Scene->normal.a.y;  
		}
		else
		{
			pt[0] = x;
			pt[1] = y;  
		}
	}
	return parity;
}

void CS3DScene::Init( float **polys, int size[], int num_polys )
{
	if ( !polys ) return;

	int i, j;
	point p0, p1, p, rp0, rp1;
	for ( i = 0; i < num_polys; i++ )
	{
		std::vector< point > verts;
		verts.resize( size[i] );
		int cur_id = 0;
		for ( j = 0; j < (int)verts.size(); j++ )
		{
			verts[j].x = polys[i][cur_id++];
			verts[j].y = polys[i][cur_id++];
		}
		Scene->polygons.push_back( verts );

		p0 = verts[0];
		p1 = verts[1];
		rp0 = p0;
		rp1 = p1;
		for ( j = 2; j < size[i]; j++ )
		{
			p = verts[j];
			ProcessCorner( (float*)&p0, (float*)&p1, (float*)&p );
		}
		p = rp0;
		ProcessCorner( (float*)&p0, (float*)&p1, (float*)&p );
		p = rp1;
		ProcessCorner( (float*)&p0, (float*)&p1, (float*)&p );
	}

	Scene->neighbours.resize( Scene->corners.size() + 2 );
	for ( i = 0; i < (int)Scene->corners.size(); i++ )
	{
		for ( j = i + 1; j < (int)Scene->corners.size(); j++ )
		{
			if ( isSideCorner( Scene->corners[i],Scene->corners[j].b )
			  && isSideCorner( Scene->corners[j],Scene->corners[i].b ) )
			{
				ProcessPair( i, j );
			}
		}
	}
	corner c;
	Scene->corners.push_back( c );
	Scene->corners.push_back( c );
	Scene->potentials.resize( Scene->corners.size() );
	Scene->links.resize( Scene->corners.size() );
	bInit = true;
}

bool CS3DScene::IsClearLine( float x1, float y1, float x2, float y2 )
{
	point a( x1, y1 );
	point b( x2, y2 );
	bool stop = 0;
	segment w, l = { a, b };
	for ( int i = 0; ( i < (int)Scene->walls.size() ) && (!stop); i++ )
	{
		w = Scene->walls[i];
		if ( ( w.a != a ) && ( w.a != b ) 
		  && ( w.b != a ) && ( w.b != b ) )
		{
			stop = crosses( w, l );
		}
	}
	return !stop;
}

void CS3DScene::SetEnds( float x1, float y1, float x2, float y2 )
{
	point start( x1, y1 );
	point finish( x2, y2 );
	std::vector< int > *n = &(Scene->neighbours[ Scene->neighbours.size() - 1 ]);
	int i, j;
	for ( i = n->size() - 1; i >= 0; i-- )
	{
		j = n->back();
		n->pop_back();
		Scene->neighbours[j].pop_back();
	}
	n = &(Scene->neighbours[ Scene->neighbours.size() - 2 ]);
	for ( i = n->size() - 1; i >= 0; i-- )
	{
		j = n->back();
		n->pop_back();
		Scene->neighbours[j].pop_back();
	}
	Scene->corners[ Scene->corners.size()-2 ].b = start;
	Scene->corners[ Scene->corners.size()-1 ].b = finish;
	j = Scene->neighbours.size() - 2;
	for ( i = 0; i < j; i++ )
	{
		if ( isSideCorner( Scene->corners[i], Scene->corners[j].b ) )
			ProcessPair( i, j );
	}
	j = Scene->neighbours.size() - 1;
	for ( i = 0; i < j - 1; i++ )
	{
		if ( isSideCorner(Scene->corners[i], Scene->corners[j].b) )
			ProcessPair( i, j );
	}
	ProcessPair( j - 1, j );
}

void CS3DScene::FindPath( float &x, float &y )
{
	int i, j, k, imin;
	for ( i = 0; i < (int)Scene->potentials.size(); i++ )
		Scene->potentials[i] = S3D_INFINITY;
	Scene->border.push_back( Scene->corners.size() - 2 );
	Scene->potentials[ Scene->potentials.size() - 2 ] = 0;
	bool find = 0;
	float min;
	std::vector< int > *n;
	while ( !find && !Scene->border.empty() )
	{
		min = Scene->potentials[ Scene->border[0] ];
		imin = 0;
		for ( i = 1; i < (int)Scene->border.size(); i++ )
			if ( Scene->potentials[ Scene->border[i] ] < min )
			{
				min = Scene->potentials[ Scene->border[i] ];
				imin = i;
			}
		i = imin;
		j = Scene->border[i];
		if ( j == (int)Scene->corners.size() - 1 ) find = 1;
		else
		{
			Scene->border[i] = Scene->border.back();
			Scene->border.pop_back();
			n = &Scene->neighbours[j];
			for ( i = 0; i < (int)n->size(); i++ )
			{
				k = n->operator[](i);
				min = dist( Scene->corners[j].b, Scene->corners[k].b ) + Scene->potentials[j];
				if ( Scene->potentials[k] == S3D_INFINITY )
					Scene->border.push_back( k );
				if ( min < Scene->potentials[k] )
				{
					Scene->potentials[k] = min;
					Scene->links[k] = j;
				}
			}
		}
	}
	Scene->border.clear();
	int current, closest = 0;
	if ( find )
	{
		current = Scene->corners.size() - 1;
		while (current != (int)Scene->corners.size() - 2)
		{
			closest = current;
			current = Scene->links[current];
		}
	}

	x = Scene->corners[closest].b.x;
	y = Scene->corners[closest].b.y;
}

void CS3DScene::DrawWalls()
{
	if ( !s3dSimpleShader || !bInit ) return;
	s3dSimpleShader->BeginShaderProgram();
	s3dSimpleShader->EnableAttribute( 0 );

	CS3DMat16f mvpMat;
	mvpMat = s3dCamera->Get() * s3dProjection;
	s3dSimpleShader->SetUniformMatrixf( "mvpMat", 4, (float*)&mvpMat );

	float Y = s3dCamera->GroundLevel() + 0.01f;
	int numWalls = (int)Scene->walls.size();
	const int minSize = 20;
	float *verts = 0;
	if ( numWalls < minSize )
		verts = new float[ minSize * 6 ] ;
	else verts = new float[ numWalls * 6 ] ;

	s3dSimpleShader->SetAttribPointerf( 0, 3, verts );

	// draw walls
	int cur = 0;
	for ( int i = 0; i < numWalls; i++ )
	{
		verts[ cur++ ] = Scene->walls[i].a.x;
		verts[ cur++ ] = Y;
		verts[ cur++ ] = Scene->walls[i].a.y;
		verts[ cur++ ] = Scene->walls[i].b.x;
		verts[ cur++ ] = Y;
		verts[ cur++ ] = Scene->walls[i].b.y;
	}
	float walls_color[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	s3dSimpleShader->SetUniformf( "u_color", 4, walls_color );
	S3D_OpenGLOp::DrawPrimitives( S3D_LINE_LOOP, 0, numWalls * 2 );

	// draw normal
	cur = 0;
	verts[ cur++ ] = Scene->normal.a.x;
	verts[ cur++ ] = Y;
	verts[ cur++ ] = Scene->normal.a.y;
	verts[ cur++ ] = Scene->normal.b.x;
	verts[ cur++ ] = Y;
	verts[ cur++ ] = Scene->normal.b.y;
	float normal_color[] = { 1.0f, 0.0f, 0.0f, 1.0f };
	s3dSimpleShader->SetUniformf( "u_color", 4, normal_color );
	S3D_OpenGLOp::DrawPrimitives( S3D_LINE_LOOP, 0, 2 );

	// draw lines
	cur = 0;
	float X;
	for( int j = 0; j < minSize; j++ )
	{
		X = (-1 + 2*((float)j/minSize)) * 5;
		verts[ cur++ ] = X; verts[ cur++ ] = Y; verts[ cur++ ] = -S3D_NEAR;
		verts[ cur++ ] = X; verts[ cur++ ] = Y; verts[ cur++ ] = -S3D_FAR;
	}
	float lines_color[] = { 0.3f, 0.3f, 0.3f, 1.0f };
	s3dSimpleShader->SetUniformf( "u_color", 4, lines_color );
	S3D_OpenGLOp::DrawPrimitives( S3D_LINES, 0, minSize * 2 );

	s3dSimpleShader->DisableAttribute( 0 );
	s3dSimpleShader->EndShaderProgram();
	delete [] verts;
}
