#include <S3D.h>

#define BUILD_BUFFERS( id, d1, elemSize, repetition )\
if ( s3dBuffer && s3dLoadedShaders )\
{\
	unsigned int elemLen = elemSize / sizeof( float );\
	unsigned int Len = elemLen * 3 * Header.numFaces * repetition;\
	float *data = (float*)malloc( Len * sizeof( float ) );\
	for ( unsigned int r = 0; r < repetition; r++ )\
	{\
		for ( unsigned int i = 0; i < Header.numFaces; i++ )\
		{\
			int i0 = r * Header.numFaces + i;\
			for ( int j = 0; j < 3; j++ )\
			{\
				int m = ( i0 * 3 + j ) * elemLen;\
				for ( unsigned int n = 0; n < elemLen; n++ )\
					data[ m + n ] = d1;\
			}\
		}\
	}\
	s3dBuffer->SetBufferData( id, data, Len, 0, sizeof( float ), S3D_VBO );\
	free( data );\
}\

CS3DModel::CS3DModel()
{
	Name = Path = 0;
	SelectedBoneName = "";
	reflectTexture = 0;
	DisableBackFace = false;
	SetPosition( S3D_AXIS_Y, s3dCamera->GroundLevel() );
#if ( S3D_USE_LIGTHING )
	Fur = 0;
#endif
}

CS3DModel::~CS3DModel()
{
#if ( S3D_USE_LIGTHING )
	if ( Fur ) delete Fur;
#endif
	if ( Name ) free( Name );
	if ( Path ) free( Path );
	if ( reflectTexture )
	{
		S3D_OpenGLOp::DeleteTextures( 1, &reflectTexture );
	}
}

const char *CS3DModel::GetSelectedBoneName()
{
	return SelectedBoneName;
}

const char *CS3DModel::GetName()
{
	return Name ? Name : "";
}

void CS3DModel::ParseFilename( const char *filename )
{
	std::vector< char > chBuf;
	int len = (int)strlen( filename );
	bool bStart = false;
	int i;
	for ( i = len - 1; i >= 0 && !strchr( "/\\", filename[i] ); --i )
	{
		if ( !bStart ) bStart = filename[i] == '.';
		else chBuf.insert( chBuf.begin(), filename[i] );
	}
	Path = (char*)malloc( ( i + 2 ) * sizeof( char ) );
	memcpy( Path, filename, ( i + 1 ) * sizeof( char ) );
	Path[i + 1] = 0;
	len = chBuf.size();
	Name = (char*)malloc( ( len + 1 ) * sizeof( char ) );
	memcpy( Name, &chBuf[0], len * sizeof( char ) );
	Name[len] = 0;
}

void CS3DModel::Load( const char *filename )
{
	ParseFilename( filename );

	S3D_ENUM fragShaderName = S3D_FRAGMENT_SHADER_LAMBERT;
#if ( S3D_USE_CUBEMAPS )
	int reflectMatId = -1;
#endif
	void *io = s3dIOController->OpenFile( filename );
	if ( io )
	{
		char Filename[0x100] = { 0 };
		char buffer[0x100] = { 0 };
		char *cur_token = 0;
		bool open_string = false;
		while ( !s3dIOController->Eof() )
		{
			char ch = (char)s3dIOController->ReadByte();
			if ( ch == '=' )
			{
				int size = sizeof( char ) * ( strlen( buffer ) + 1 );
				if ( cur_token ) free( cur_token );
				cur_token = (char *)malloc( size );
				strcpy( cur_token, buffer );
				memset( buffer, 0, sizeof( buffer ) );
			}
			else if ( ch == '"' )
			{
				open_string = !open_string;
				if ( !open_string )
				{
					if ( !strcmp( cur_token, "mesh" ) )
					{
						strcpy( Filename, Path );
						strcat( Filename, buffer );
						LoadMesh( Filename );
					}
					else if ( !strcmp( cur_token, "skeleton" ) && !Skeleton )
					{
						Skeleton = new S3D_Skeleton;
						strcpy( Filename, Path );
						strcat( Filename, buffer );
						if ( Skeleton->LoadSkeleton( Filename ) )
						{
							BUILD_BUFFERS( Skeleton->weightBuffer, Skeleton->weights[n][ Faces[i * 3 + j] ], sizeof( float ) * 4, 1 )
							BUILD_BUFFERS( Skeleton->indexBuffer, (float)Skeleton->indices[n][ Faces[i * 3 + j] ], sizeof( float ) * 4, 1 )
						}
					}
					else if ( !strcmp( cur_token, "animation" ) )
					{
						char animName[0x100] = { 0 };
						int len = strlen( buffer );
						int i;
						for ( i = 0; i < len; i++ )
						{
							animName[i] = buffer[i];
							if ( !memcmp( animName + i - 4, ".anm", sizeof( char ) * 4 ) )
							{
								animName[i] = 0;
								break;
							}
						}
						unsigned int mixedWithAnimID = ~0; // undefined
						if ( i != len )
						{
							const char *token = "mixedWith";
							unsigned int tokenLen = strlen( token );
							int tokenIsFined = 0;
							char Prop[0x100] = { 0 };
							for ( int j = i; j < len; j++ )
							{
								if ( !tokenIsFined && !memcmp( buffer + j - tokenLen, token, sizeof( char ) * tokenLen ) )
								{
									tokenIsFined = j;
								}
								else if ( tokenIsFined )
								{
									Prop[ j - tokenIsFined - 1 ] = buffer[j];
								}
							}
							mixedWithAnimID = atoi( Prop );
							if ( !mixedWithAnimID ) mixedWithAnimID = ~0;
							else mixedWithAnimID--;
						}
						strcpy( Filename, Path );
						strcat( Filename, animName );
						bool loaded = LoadAnimation( Filename, Header, mixedWithAnimID );
#if ( S3D_USE_SHADERS )
						if ( loaded && s3dLoadedShaders )
						{
							unsigned animId = Animations.size() - 1;
							S3D_Animation *anim = Animations[ animId ];
							anim->Shader = new CS3DShader;
							S3D_ENUM vertShaderName = S3D_VERTEX_SHADER_SKIN; // by default SkinAnimation
							//if ( anim->Type == SkinAnimation )
							//	vertShaderName = S3D_VERTEX_SHADER_SKIN;
							if ( !anim->Shader->Load( vertShaderName, fragShaderName ) )
							{
								s3dIOController->PrintLog( "Can not create shader for animation " );
								s3dIOController->PrintLog( buffer );
								s3dIOController->PrintLog( ".\n" );
								delete anim->Shader;
								anim->Shader = 0;
							}
#ifdef S3D_VAO
							if ( anim->Shader && s3dBuffer->Supported( S3D_VAO ) )
							{
								int numMat = NumMaterials();
								VertexArray.resize( VertexArray.size() + numMat );
								for ( int i = 0; i < numMat; i++ )
								{
									unsigned vaoId = animId * numMat + i;
									s3dBuffer->SetBufferData( VertexArray[vaoId], 0, 0, 0, 0, S3D_VAO );
									s3dBuffer->BindBuffer( VertexArray[vaoId], S3D_VAO );
									anim->Shader->EnableAttributes();
									if ( anim->Type == SkinAnimation && Skeleton )
									{
										unsigned int attribID = 6;
#if ( !S3D_USE_LIGTHING )
										--attribID;
										--attribID;
#else
#if ( !S3D_BUMPMAPPING )
										--attribID;
#endif
#endif
										s3dBuffer->BindBuffer( Skeleton->indexBuffer, S3D_VBO );
										anim->Shader->SetAttribPointerf( --attribID, 4, 0x0 );
										s3dBuffer->BindBuffer( Skeleton->weightBuffer, S3D_VBO );
										anim->Shader->SetAttribPointerf( --attribID, 4, 0x0 );

										static S3D_Body *meshOffset = 0;
										s3dBuffer->BindBuffer( Buffer, S3D_VBO );
										if ( Header.numTVerts )
										{
											anim->Shader->SetAttribPointerf( --attribID, 2, &meshOffset->tcoord, sizeof( S3D_Body ) );
										}
#if ( S3D_USE_LIGTHING )
#if ( S3D_BUMPMAPPING )
										anim->Shader->SetAttribPointerf( --attribID, 4, &meshOffset->tangent, sizeof( S3D_Body ) );
#endif
										anim->Shader->SetAttribPointerf( --attribID, 3, &meshOffset->normal, sizeof( S3D_Body ) );
#endif
										anim->Shader->SetAttribPointerf( --attribID, 3, &meshOffset->vertex, sizeof( S3D_Body ) );
									}
									if ( MatIDs && !MatIDs[i].IDs )
										s3dBuffer->BindBuffer( MatIDs[i].buffID, S3D_IBO );

									s3dBuffer->UnbindBuffer( S3D_VAO );

									if ( MatIDs && !MatIDs[i].IDs )
										s3dBuffer->UnbindBuffer( S3D_IBO );
									anim->Shader->DisableAttributes();
									s3dBuffer->UnbindBuffer( S3D_VBO );
								}
							}
#endif
						}
#endif
					}
#if ( S3D_USE_SHADERS )
					else if ( !strcmp( cur_token, "shader" ) )
					{
						if ( !strcmp( buffer, "OrenNayarPhong" ) )
						{
							fragShaderName = S3D_FRAGMENT_SHADER_ORENNAYARPHONG;
						}
						else if ( !strcmp( buffer, "LambertPhong" ) )
						{
							fragShaderName = S3D_FRAGMENT_SHADER_LAMBERTPHONG;
						}
						else if ( !strcmp( buffer, "Lambert" ) )
						{
							fragShaderName = S3D_FRAGMENT_SHADER_LAMBERT;
						}
						else
						{
							s3dIOController->PrintLog( "Error: Shader " );
							s3dIOController->PrintLog( buffer );
							s3dIOController->PrintLog( " not supported.\n" );
						}
					}
					else if ( !strcmp( cur_token, "DiffuseLevel" ) )
					{
						int matID = -1;
						float level = 1.f;
						int len = (int)strlen( buffer );
						for ( int i = 0; i < len; i++ )
						{
							if ( buffer[i] == ',' )
							{
								if ( !strstr( buffer, "all" ) )
									matID = (int)atof( buffer );
								level = (float)atof( buffer + i + 1 );
								break;
							}
						}
						SetDiffuseLevel( matID, level );
					}
#endif
					else if ( !strcmp( cur_token, "DisableBackFace" ) )
					{
						DisableBackFace = !strcmp( buffer, "true" );
					}
					else if ( !strcmp( cur_token, "BBLevel" ) )
					{
						BBLevel = max( (int)atof( buffer ), 1 );
					}
#if ( S3D_USE_CUBEMAPS )
					else if ( !strcmp( cur_token, "reflectionMatId" ) )
					{
						reflectMatId = (int)atof( buffer );
					}
					else if ( !strcmp( cur_token, "reflectionMap" ) )
					{
#if ( !S3D_OGLES_2_0 )
						if ( S3D_OpenGLOp::ExtensionSupported( "GL_ARB_framebuffer_object" ) 
						  || S3D_OpenGLOp::ExtensionSupported( "GL_EXT_framebuffer_object" ) )
#endif
						{
							strcpy( Filename, Path );
							strcat( Filename, buffer );
							reflectTexture = S3D_OpenGLOp::CreateCubeMap( Filename );
						}
#if ( !S3D_OGLES_2_0 )
						else
						{
							s3dIOController->PrintLog( "Can not create reflectionMap for " );
							s3dIOController->PrintLog( filename );
							s3dIOController->PrintLog( ".\n" );
						}
#endif
					}
#endif
#if ( S3D_USE_LIGTHING )
					else if ( !strcmp( cur_token, "ProduceFur" ) )
					{
						if ( !Fur )
						{
							strcpy( Filename, Path );
							strcat( Filename, buffer );
							Fur = new CS3DFur( this, buffer );
							if ( !Fur || !Fur->Initialized() )
							{
								delete Fur;
								Fur = 0;
							}
						}
					}
#endif
				}
				memset( buffer, 0, sizeof( buffer ) );
			}
			else
			{
				if ( s3dIOController->Eof() ) break;
				if ( S3D_ISSPASE( ch ) && !open_string ) continue;
				buffer[ strlen( buffer ) ] = ch;
			}
		}
		if ( cur_token ) free( cur_token );
		s3dIOController->CloseFile( io );

#if ( S3D_USE_CUBEMAPS )
		if ( reflectTexture && reflectMatId >= 0 )
		{
			MaterialTexture( reflectMatId, S3D_ReflectionMap, reflectTexture );
		}
#endif

		if ( !Animated() )
		{
			// create a list of bounding boxes for mesh
			BBList = new S3D_BBList;
			BBList->Create( BBLevel, Body, 0, Header.numFaces );
		}
		else if ( Skeleton )
		{
			// create a list of bounding boxes for skeleton
			BBList = new S3D_BBList;
			BBList->Create( Skeleton->numBones, Body, 0, Header.numFaces, true );
			//*
			for ( unsigned int i = 0; i < Header.numFaces; ++i )
			{
				unsigned i3 = i * 3;
				for( int j = 0; j < S3D_BONE_AFFECT_LIMITED; j++ )
				{
					S3D_BBNode **node = &BBList->nodes[ Skeleton->indices[j][Faces[i3]] ];
					if ( Skeleton->weights[j][Faces[i3]] > S3D_EPS )
					{
						if ( *node == 0x0 )
						{
							*node = new S3D_BBNode;
							(*node)->IDs = new S3D_Indices;
						}
						(*node)->bbox.Set( Body[i3].vertex );
						(*node)->IDs->push_back( i );
					}
				}
			}
		}
		if ( BBList )
		{
			if ( originalBox )
				*originalBox = BBList->parent;
			Pivot = BBList->parent.Center();
		}

		if ( s3dBuffer )
		{
			if ( Animations.empty() )
			{
#if ( S3D_USE_SHADERS )
				if ( (unsigned)fragShaderName != S3D_UNDEFINED && s3dLoadedShaders )
				{
					MeshShader = new CS3DShader;
					if ( !MeshShader->Load( S3D_VERTEX_SHADER_MESH, fragShaderName ) )
					{
						s3dIOController->PrintLog( "Can not create shader for mesh " );
						s3dIOController->PrintLog( filename );
						s3dIOController->PrintLog( ".\n" );
						delete MeshShader;
						MeshShader = 0;
					}
				}
#ifdef S3D_VAO
				if ( MeshShader && s3dBuffer->Supported( S3D_VAO ) )
				{
					VertexArray.resize( NumMaterials() );
					for ( unsigned i = 0; i < VertexArray.size(); i++ )
					{
						unsigned int attribID = 4;
#if ( !S3D_USE_LIGTHING )
						--attribID;
						--attribID;
#else
#if ( !S3D_BUMPMAPPING )
						--attribID;
#endif
#endif
						s3dBuffer->SetBufferData( VertexArray[i], 0, 0, 0, 0, S3D_VAO );
						s3dBuffer->BindBuffer( VertexArray[i], S3D_VAO );
						MeshShader->EnableAttributes();
						static S3D_Body *meshOffset = 0;
						s3dBuffer->BindBuffer( Buffer, S3D_VBO );
						if ( Header.numTVerts )
						{
							MeshShader->SetAttribPointerf( --attribID, 2, &meshOffset->tcoord, sizeof( S3D_Body ) );
						}
#if ( S3D_USE_LIGTHING )
#if ( S3D_BUMPMAPPING )
						MeshShader->SetAttribPointerf( --attribID, 4, &meshOffset->tangent, sizeof( S3D_Body ) );
#endif
						MeshShader->SetAttribPointerf( --attribID, 3, &meshOffset->normal, sizeof( S3D_Body ) );
#endif
						MeshShader->SetAttribPointerf( --attribID, 3, &meshOffset->vertex, sizeof( S3D_Body ) );
						if ( MatIDs && !MatIDs[i].IDs )
							s3dBuffer->BindBuffer( MatIDs[i].buffID, S3D_IBO );
						s3dBuffer->UnbindBuffer( S3D_VAO );
						if ( MatIDs && !MatIDs[i].IDs )
							s3dBuffer->UnbindBuffer( S3D_IBO );
						s3dBuffer->UnbindBuffer( S3D_VBO );
						MeshShader->DisableAttributes();
					}
				}
#endif
#endif
			}
		}
	}
	else
	{
		char Err[0x100] = { 0 };
		strcpy( Err, "Can not load model: " );
		strcat( Err, filename ); strcat( Err, ".\n" );
		s3dIOController->PrintLog( Err );
	}
}

S3D_BBList *CS3DModel::GetBBList()
{
	return BBList;
}

S3D_BoundingBox CS3DModel::GetBBox()
{
	if ( BBList )
	{
		return BBList->parent.GetBBox();
	}
	S3D_BoundingBox bbox;
	bbox.min = CS3DVec3f(  S3D_INFINITY,  S3D_INFINITY,  S3D_INFINITY );
	bbox.max = CS3DVec3f( -S3D_INFINITY, -S3D_INFINITY, -S3D_INFINITY );
	return bbox;
}

S3D_BoundingBox CS3DModel::GetBBoxTransformed()
{
	S3D_BoundingBox out = GetBBox();
	out.max = mvpMat * out.max;
	out.min = mvpMat * out.min;
	return out;
}

bool CS3DModel::Animated()
{
	S3D_Animation *anim = !Animations.empty() ? Animations[ CurAnimation.curID ] : 0;
	bool isAnim = anim && anim->Type == SkinAnimation && Skeleton;
	return isAnim;
}

// -------------------------------------------------------------------------
// ��������� ��������� � ������������-->
// -------------------------------------------------------------------------

bool CS3DModel::CollisionModel( CS3DModel *otherModel )
{
	if ( !otherModel ) return false;
	S3D_BBList *other = otherModel->GetBBList();
	CS3DBox bbox;
	S3D_BoundingBox bbox1, bbox2;

	BBList->parent.SetTransforms( *this );
	other->parent.SetTransforms( *otherModel );
	bbox1 = BBList->parent.GetBBoxTransformed();
	bbox2 = other->parent.GetBBoxTransformed();
	bbox.Set( bbox1 );
	if ( bbox.CollisionBBox( bbox2 ) )
	{
		for ( unsigned j = 0; j < other->numNodes; j++ )
		{
			for ( unsigned i = 0; i < BBList->numNodes; i++ )
			{
				if ( BBList->nodes[i] && other->nodes[j] )
				{
					BBList->nodes[i]->bbox.SetTransforms( *this );
					other->nodes[j]->bbox.SetTransforms( *otherModel );
					bbox1 = BBList->nodes[i]->bbox.GetBBoxTransformed();
					bbox2 = other->nodes[j]->bbox.GetBBoxTransformed();
					bbox.Set( bbox1 );
					if ( bbox.CollisionBBox( bbox2 ) )
						return true;
				}
			}
		}
	}
	return false;
}

#define S3D_DET(v1, v2, v3) (v1.x*(v2.y*v3.z-v2.z*v3.y)-v1.y*(v2.x*v3.z-v2.z*v3.x)+v1.z*(v2.x*v3.y-v2.y*v3.x))
int s3d_triangle_collision_ray( CS3DVec3f tri[3], S3D_Ray &ray )
{
	tri[0] -= ray.origin;
	tri[1] -= ray.origin;
	tri[2] -= ray.origin;

	float det = S3D_DET( tri[0], tri[1], tri[2] );
	if ( !det ) return -1;
	float det1 = S3D_DET( tri[0], ray.direct, tri[2] );
	float det2 = S3D_DET( tri[0], tri[1], ray.direct );
	float det3 = S3D_DET( tri[2], ray.direct, tri[1] );

	if ( det >= 0 && det1 >= 0 && det2 >= 0 && det3 >= 0 ) return 1;
	if ( det < 0 && det1 < 0 && det2 < 0 && det3 < 0 ) return 2;
	return 0;
}

bool CS3DModel::Hover( int win_x, int win_y, S3D_TexCoord *tc )
{
	int result = -1;
	CS3DVec3f src, near_value, far_value;
	S3D_Ray Ray;
	CS3DMat16f invMat = mvpMat;
	if ( !invMat.inverse() ) return false;

	src = CS3DVec3f( (float)win_x, (float)win_y, -1.0f );
	s3dUnProject( &src, &Ray.origin, (float*)&invMat );
	
	src = CS3DVec3f( (float)win_x, (float)win_y, 1.0f );
	s3dUnProject( &src, &Ray.direct, (float*)&invMat );
	Ray.direct -= Ray.origin;

	SelectedBoneName = "";
	S3D_Ray nodeRay = Ray;
	CS3DBox BBox = BBList->parent;
	if ( BBox.CollisionRay( Ray ) )
	{
		S3D_Animation *anim = !Animations.empty() ? Animations[ CurAnimation.curID ] : 0;
		bool isAnim = Animated();
		//for ( unsigned i = 0; i < BBList->numNodes; i++ )
		for ( int i = BBList->numNodes - 1; i >= 0; --i )
		{
			if ( BBList->nodes[i] && BBList->nodes[i]->bbox.IsInit() )
			{
				BBox = BBList->nodes[i]->bbox;
				if ( isAnim )
				{
					invMat = Skeleton->bones[i].transfMat * mvpMat;
					if ( !invMat.inverse() ) return false;
					src = CS3DVec3f( (float)win_x, (float)win_y, -1.0f );
					s3dUnProject( &src, &nodeRay.origin, (float*)&invMat );
					src = CS3DVec3f( (float)win_x, (float)win_y, 1.0f );
					s3dUnProject( &src, &nodeRay.direct, (float*)&invMat );
					nodeRay.direct -= nodeRay.origin;
				}
				if ( BBox.CollisionRay( nodeRay ) )
				{
					S3D_Indices *IDs = BBList->nodes[i]->IDs;
					unsigned int numIDs = IDs->size();
					for ( unsigned int j = 0; j < numIDs; ++j )
					{
						CS3DVec3f triangle[3];
						S3D_Index faceID = (*IDs)[j];
						int vertID;
						if ( isAnim )
						{
							S3D_Transform *t;
							for ( int x = 0; x < 3; ++x )
							{
								vertID = Faces[ faceID * 3 + x ];
								for ( int w = 0; w < S3D_BONE_AFFECT_LIMITED; ++w )
								{
									if ( Skeleton->weights[w][vertID] > S3D_EPS )
									{
										t = &anim->Transforms[Skeleton->indices[w][vertID]][CurFrame.curID];
										triangle[x] += ( t->position + S3D_QuatOp::rotate( Body[ faceID * 3 + x ].vertex, t->rotation ) ) * Skeleton->weights[w][vertID];
									}
								}
							}
						}
						else if ( !anim )
						{
							for ( int x = 0; x < 3; ++x )
								triangle[x] = Body[ faceID * 3 + x ].vertex;
						}

						result = s3d_triangle_collision_ray( triangle, Ray );
						if ( result > 0 )
						{
							if ( !Textured || !tc )
							{
								if ( isAnim ) SelectedBoneName = Skeleton->bones[i].name;
								return true;
							}
							else
							{
								if ( result == 2 )
								{
									CS3DMat16f m;
									CS3DVec3f win;
									//CS3DMat16f mvp = s3dProjection * modelViewMat;
									for ( int x = 0; x < 3; x++ )
									{
										s3dProject( &Body[ faceID * 3 + x ].vertex, &win, (float*)&modelViewMat );
										m.setRow( x, (float*)&win, 2 );
									}
									m[2] = 1; m[6] = 1;
									m.inverse();

									CS3DVec3f v( (float)win_x, (float)win_y, 1 );
									v = m.multVecWithoutTranslate( v );

									tc->S = 0; tc->T = 0;
									for ( int x = 0; x < 3; x++ ) 
									{
										tc->S += v[x] * Body[ faceID * 3 + x ].tcoord.S;
										tc->T += v[x] * Body[ faceID * 3 + x ].tcoord.T;
									}
				
									if ( isAnim ) SelectedBoneName = Skeleton->bones[i].name;
									return true;
								}
							}
						}
					}
				}
			}
		}
	}

	return result > 0;
}
// -------------------------------------------------------------------------
// ��������� ��������� � ������������ <--
// -------------------------------------------------------------------------

#if ( S3D_USE_LIGTHING )
#if ( S3D_USE_SHADOWS )
void CS3DModel::DrawShadow( const CS3DMat16f &lmvMat, const CS3DMat16f &lpMat )
{
	if ( MeshSize == 0 || !s3dBuffer ) return;

	Move();

	CS3DMat16f mvMat = modelMat * lmvMat;

	if ( DisableBackFace ) S3D_OpenGLOp::DisableBackFace();
	else S3D_OpenGLOp::EnableBackFace();

	CS3DShader *shader = s3dShadow->depthTextureShader;
	CS3DMat16f mvpMat = mvMat * lpMat;
	shader->SetUniformMatrixf( "mvpMat", 4, (float*)&mvpMat );

	int Animated = 0;
	if ( !Animations.empty() )
	{
		if ( Animations[ CurAnimation.curID ]->Type == SkinAnimation )
		{
			SetTransformsToSkinShader( shader );
			Animated = 1;
		}
	}
	shader->SetUniformi( "Animated", 1, &Animated );

	static S3D_Body *meshOffset = 0;
	if ( Animated )
	{
		shader->EnableAttributes();
		if ( Animations[ CurAnimation.curID ]->Type == SkinAnimation && Skeleton )
		{
			s3dBuffer->BindBuffer( Skeleton->indexBuffer, S3D_VBO );
			shader->SetAttribPointerf( 2, 4, 0x0 );
			s3dBuffer->BindBuffer( Skeleton->weightBuffer, S3D_VBO );
			shader->SetAttribPointerf( 1, 4, 0x0 );
		}
		s3dBuffer->BindBuffer( Buffer, S3D_VBO );
		shader->SetAttribPointerf( 0, 3, &meshOffset->vertex, sizeof( S3D_Body ) );
        int er = S3D_OpenGLOp::GetError();
		S3D_OpenGLOp::DrawPrimitives( TriangleType, 0, Header.numFaces * 3 );
         er = S3D_OpenGLOp::GetError();
		shader->DisableAttributes();
	}
	else
	{
		shader->EnableAttribute( 0 );
		s3dBuffer->BindBuffer( Buffer, S3D_VBO );
		shader->SetAttribPointerf( 0, 3, &meshOffset->vertex, sizeof( S3D_Body ) );
		S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLES, 0, Header.numFaces * 3 );
		shader->DisableAttribute( 0 );
	}
	s3dBuffer->UnbindBuffer( S3D_VBO );

	if ( DisableBackFace ) S3D_OpenGLOp::EnableBackFace();
}
#endif
#endif

void CS3DModel::DrawSkeleton()
{
	CS3DAnimObject::DrawSkeleton( modelViewMat, Animations.empty(), BBList->parent );
}

void CS3DModel::BackFace( bool is_discarded )
{
	DisableBackFace = is_discarded;
}

void CS3DModel::Draw()
{
	if ( MeshSize == 0 ) return;
	float maxLen = BBList->parent.Size()[0];
	SetNormalMaxLength( maxLen );

	bool lookUpdateTransforms = false;
#if ( S3D_USE_SHADOWS )
	lookUpdateTransforms = s3dShadow && s3dShadow->enabled;
#endif
	lookUpdateTransforms ? Move() : ApplyTransforms();

	modelViewMat = modelMat * s3dCamera->Get();
	mvpMat = modelViewMat * s3dProjection;

	if ( DisableBackFace ) S3D_OpenGLOp::DisableBackFace();
	else S3D_OpenGLOp::EnableBackFace();

	if ( !Animations.empty() )
	{
		DrawAnimation();
#if ( S3D_USE_LIGTHING )
		if ( Fur ) Fur->Draw();
#endif
		UpdateAnimation();
	}
	else
	{
		DrawMesh();
#if ( S3D_USE_LIGTHING )
		if ( Fur ) Fur->Draw();
#endif
	}
	if ( DisableBackFace ) S3D_OpenGLOp::EnableBackFace();

	bool isAnim = Animated();
	if ( isAnim )
	{
		BBList->parent.Reset();
		for ( unsigned i = 0; i < BBList->numNodes; i++ )
		{
			S3D_BBNode *node = BBList->nodes[i];
			if ( node && node->bbox.IsInit() )
			{
				CS3DMat16f &mat = Skeleton->bones[i].transfMat;
				const S3D_BoundingBox &bbox = node->bbox.GetBBox();
				CS3DVec3f verts[8] = {
					mat * CS3DVec3f( bbox.max[0], bbox.min[1], bbox.min[2] ),
					mat * CS3DVec3f( bbox.max[0], bbox.max[1], bbox.min[2] ),
					mat * CS3DVec3f( bbox.max[0], bbox.min[1], bbox.max[2] ),
					mat * CS3DVec3f( bbox.max[0], bbox.max[1], bbox.max[2] ),
					mat * CS3DVec3f( bbox.min[0], bbox.min[1], bbox.min[2] ),
					mat * CS3DVec3f( bbox.min[0], bbox.max[1], bbox.min[2] ),
					mat * CS3DVec3f( bbox.min[0], bbox.min[1], bbox.max[2] ),
					mat * CS3DVec3f( bbox.min[0], bbox.max[1], bbox.max[2] )
				};
				for ( int i = 0; i < 8; i++ )
					BBList->parent.Set( verts[i] );
			}
		}
	}
}

void CS3DModel::DrawAnimation()
{
	S3D_Animation *anim = Animations[ CurAnimation.curID ];

	if ( anim->Shader )
	{
		anim->Shader->BeginShaderProgram();
#if ( S3D_USE_LIGTHING )
		CS3DVec3f lightPosition = s3dCamera->Get() * s3dLight->GetLight( 0 )->pos;
		anim->Shader->SetUniformf( "mvLightPos", 3, (float*)&lightPosition );
#if ( S3D_USE_SHADOWS )
		anim->Shader->SetUniformMatrixf( "lMat", 4, (float*)&s3dLight->GetLight( 0 )->mat );
#endif
#endif
		anim->Shader->SetUniformMatrixf( "mvpMat", 4, (float*)&mvpMat );
#if ( S3D_USE_LIGTHING )
		anim->Shader->SetUniformMatrixf( "mvMat", 4, (float*)&modelViewMat );
		
#if ( S3D_USE_CUBEMAPS )
		float mat[9];
		modelMat.getNormalMat( mat );
		anim->Shader->SetUniformMatrixf( "mNormalMat", 3, mat );
		memcpy( &mat[0], &modelMat[0], 3 * sizeof(float) );
		memcpy( &mat[3], &modelMat[4], 3 * sizeof(float) );
		memcpy( &mat[6], &modelMat[8], 3 * sizeof(float) );
		anim->Shader->SetUniformMatrixf( "mMat", 3, (float*)&mat );
		CS3DVec3f camPos = s3dCamera->GetPosition();
		anim->Shader->SetUniformf( "wsCamPos", 3, (float*)&camPos );
#endif

		float nMat[9];
		CS3DMat16f temp;
		temp.set( (float*)&modelViewMat );
		temp.getNormalMat( nMat );
		anim->Shader->SetUniformMatrixf( "nMat", 3, nMat );
#endif
	}
#if ( !S3D_OGLES_2_0 )
	else
	{
		S3D_OpenGLOp::SetMatrices( (float*)&modelViewMat );
	}
#endif

	if ( anim->Type == SkinAnimation && Skeleton )
	{
		CalcTransformMatrices();
		/*
		BBList->parent.Reset();
		for ( unsigned i = 0; i < BBList->numNodes; i++ )
		{
			S3D_BBNode *node = BBList->nodes[i];
			if ( node && node->bbox.IsInit() )
			{
				const S3D_BoundingBox *pBB = node->bbox.Get();
				CS3DMat16f tMat = Skeleton->bones[i].transfMat * mvpMat;
				BBList->parent.Set( tMat * pBB->max );
				BBList->parent.Set( tMat * pBB->min );
			}
		}
		//*/
	}
	if ( s3dBuffer && anim->Shader )
	{
#ifdef S3D_VAO
		if ( !VertexArray.empty() )
		{
			if ( anim->Type == SkinAnimation && Skeleton )
			{
				SetTransformsToSkinShader( anim->Shader );
			}
			int numMat = NumMaterials();
			for ( int i = 0; i < numMat; i++ )
			{
				s3dBuffer->BindBuffer( VertexArray[CurAnimation.curID * numMat + i], S3D_VAO );
				EnableMaterial( i, anim->Shader, Header.numTVerts != 0 );
				S3D_OpenGLOp::DrawPrimitives( TriangleType, MatIDs[i].IDs, MatIDs[i].count );
				DisableMaterial( i, anim->Shader, Header.numTVerts != 0 );
				s3dBuffer->UnbindBuffer( S3D_VAO );
			}
		}
		else
#endif
		{
			anim->Shader->EnableAttributes();
			if ( anim->Type == SkinAnimation && Skeleton )
			{
				SetTransformsToSkinShader( anim->Shader );

				unsigned int attribID = 6;
#if ( !S3D_USE_LIGTHING )
				--attribID;
				--attribID;
#else
#if ( !S3D_BUMPMAPPING )
				--attribID;
#endif
#endif
				s3dBuffer->BindBuffer( Skeleton->indexBuffer, S3D_VBO );
				anim->Shader->SetAttribPointerf( --attribID, 4, 0x0 );
				s3dBuffer->BindBuffer( Skeleton->weightBuffer, S3D_VBO );
				anim->Shader->SetAttribPointerf( --attribID, 4, 0x0 );

				static S3D_Body *meshOffset = 0;
				s3dBuffer->BindBuffer( Buffer, S3D_VBO );

				if ( Header.numTVerts )
				{
					anim->Shader->SetAttribPointerf( --attribID, 2, &meshOffset->tcoord, sizeof( S3D_Body ) );
				}
#if ( S3D_USE_LIGTHING )
#if ( S3D_BUMPMAPPING )
				anim->Shader->SetAttribPointerf( --attribID, 4, &meshOffset->tangent, sizeof( S3D_Body ) );
#endif
				anim->Shader->SetAttribPointerf( --attribID, 3, &meshOffset->normal, sizeof( S3D_Body ) );
#endif
				anim->Shader->SetAttribPointerf( --attribID, 3, &meshOffset->vertex, sizeof( S3D_Body ) );
			}
			if ( MatIDs )
			{
				for ( int i = 0; i < NumMaterials(); i++ )
				{
					EnableMaterial( i, anim->Shader, Header.numTVerts != 0 );
					if ( !MatIDs[i].IDs )
					{
						s3dBuffer->BindBuffer( MatIDs[i].buffID, S3D_IBO );
						S3D_OpenGLOp::DrawPrimitives( TriangleType, (S3D_Index*)0, MatIDs[i].count );
						s3dBuffer->UnbindBuffer( S3D_IBO );
					}
					else
					{
						S3D_OpenGLOp::DrawPrimitives( TriangleType, MatIDs[i].IDs, MatIDs[i].count );
					}
					DisableMaterial( i, anim->Shader, Header.numTVerts != 0 );
				}
			}
			else
			{
				EnableMaterial( 0, anim->Shader, Header.numTVerts != 0 );
				S3D_OpenGLOp::DrawPrimitives( TriangleType, 0, Header.numFaces * 3 );
				DisableMaterial( 0, anim->Shader, Header.numTVerts != 0 );
			}
			anim->Shader->DisableAttributes();
			s3dBuffer->UnbindBuffer( S3D_VBO );
		}
	}
#if ( !S3D_OGLES_2_0 )
	else
	{
		if ( anim->Type == SkinAnimation && Skeleton )
		{
#if ( S3D_USE_LIGTHING )
			for ( unsigned int i = 0; i < Header.numFaces * 3; i++ )
			{
				Transformed[i].vertex = Skeleton->GetTransformedVertex( Faces[i], Body[i].vertex );
				Transformed[i].normal = Skeleton->GetTransformedNormal( Faces[i], Body[i].normal );
				Transformed[i].tcoord = Body[i].tcoord;
			}
			S3D_OpenGLOp::BindMesh( Transformed, true, Textured );
#else
			for ( unsigned int i = 0; i < Header.numFaces * 3; i++ )
			{
				Transformed[i].vertex = Skeleton->GetTransformedVertex( Faces[i], Body[i].vertex );
			}
			S3D_OpenGLOp::BindMesh( Transformed, false, Textured );
#endif
			if ( MatIDs )
			{
				for ( int i = 0; i < NumMaterials(); i++ )
				{
					EnableMaterial( i, 0, Header.numTVerts != 0 );
					S3D_OpenGLOp::DrawPrimitives( TriangleType, MatIDs[i].IDs, MatIDs[i].count );
					DisableMaterial( i, 0, Header.numTVerts != 0 );
				}
			}
			else
			{
				EnableMaterial( 0, 0, Header.numTVerts != 0 );
				S3D_OpenGLOp::DrawPrimitives( TriangleType, 0, Header.numFaces * 3 );
				DisableMaterial( 0, 0, Header.numTVerts != 0 );
			}
			S3D_OpenGLOp::UnbindMesh();
		}
	}
#endif

	if ( anim->Shader ) anim->Shader->EndShaderProgram();
#if ( S3D_USE_LIGTHING )
	else s3dLight->DisableLighting();
#endif
}

unsigned int CS3DModel::TrianglesCount()
{
	unsigned int numTris = Header.numFaces;
#if ( S3D_USE_LIGTHING )
	if ( Fur ) numTris += Fur->TrianglesCount();
#endif
	return numTris;
}

S3D_Skeleton *CS3DModel::GetSkeleton()
{
	return Skeleton;
}

void CS3DModel::DrawBBoxes( bool parent )
{
	BBList->parent.SetTransforms( *this );
	if ( !parent )
	{
		bool isAnim = Animated();
		for ( unsigned i = 0; i < BBList->numNodes; i++ )
		{
			S3D_BBNode *node = BBList->nodes[i];
			if ( node && node->bbox.IsInit() )
			{
				if ( isAnim )
				{
					node->bbox.SetSubTransforms( Skeleton->bones[i].transfMat );
				}
				node->bbox.SetTransforms( *this );
				node->bbox.Draw();
			}
		}
	}
	BBList->parent.Draw();
}