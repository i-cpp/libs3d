#include <S3D.h>

#if ( S3D_USE_LIGTHING )

CS3DFur::CS3DFur( CS3DModel *m, const char* filename )
: ShadowCoef( 0.25f )
{
	bInit = false;
	NumTriengles = 0;
	BindedModel = m;
	MaskImg = 0;
	MaskTexture = 0;
	EdgeBuffer = 0;
	EdgeTexture = 0;
	ShellTextures = 0;
	NumShells = 0;
	Whiskers = 0;
	memset( &ShellBuffers, 0, sizeof( unsigned int ) * S3D_MAX_FURSHELLS );

	bool bLoad = LoadSettings( filename );
	if ( m && bLoad )
	{
		if ( EdgeTexture && MaskTexture && s3dBuffer && s3dLoadTextureFromData )
		{
			GenShellTextures();
			GenEdges();
			GenShells();
			bInit = true;
		}
	}
}

CS3DFur::~CS3DFur()
{
	if ( Whiskers ) delete Whiskers;
	if ( MaskImg )
	{
		if ( MaskImg->data ) free( MaskImg->data );
		delete MaskImg;
	}
	if ( ShellTextures )
	{
		S3D_OpenGLOp::DeleteTextures( NumShells, ShellTextures );
		delete [] ShellTextures;
	}
	if ( s3dBuffer )
	{
		if ( EdgeBuffer )
			s3dBuffer->FreeBuffer( EdgeBuffer, S3D_VBO );
		for ( unsigned int i = 0; i < NumShells; i++ )
			s3dBuffer->FreeBuffer( ShellBuffers[i], S3D_VBO );
	}
}

bool CS3DFur::LoadSettings( const char *filename )
{
	if ( !filename || !*filename ) return false;
	
	// default values:
	Lenght = 0.75f;
	Scale = 0.25f;
	EdgeLenght = 2.0f;
	NumShells = 4;
	Density = 1000;
	ShellTextureSize = 64;
	ShellTextures = 0;

	char Filename[0x100] = { 0 };
	strcpy( Filename, BindedModel->Path );
	strcat( Filename, filename );
	void *io = s3dIOController->OpenFile( Filename );
	if ( io )
	{
		char buffer[0x100] = { 0 };
		char *cur_token = 0;
		bool open_string = false;
		while ( !s3dIOController->Eof() )
		{
			char ch = (char)s3dIOController->ReadByte();
			if ( ch == '=' )
			{
				int size = sizeof( char ) * ( strlen( buffer ) + 1 );
				if ( cur_token ) free( cur_token );
				cur_token = (char *)malloc( size );
				strcpy( cur_token, buffer );
				memset( buffer, 0, sizeof( buffer ) );
			}
			else if ( ch == '"' )
			{
				open_string = !open_string;
				if ( !open_string )
				{
					if ( !strcmp( cur_token, "edgeTexture" ) && s3dLoadTexture )
					{
						strcpy( Filename, BindedModel->Path );
						strcat( Filename, buffer );
						EdgeTexture = s3dLoadTexture( Filename, 0, true );
						S3D_OpenGLOp::EnableTexture( EdgeTexture );
						S3D_OpenGLOp::Clamp( false, true );
						S3D_OpenGLOp::DisableTexture();
					}
					else if ( !strcmp( cur_token, "maskTexture" ) && s3dLoadTexture )
					{
						if ( !MaskImg ) MaskImg = new S3D_Image;
						strcpy( Filename, BindedModel->Path );
						strcat( Filename, buffer );
						MaskTexture = s3dLoadTexture( Filename, MaskImg, true );
						if ( !MaskTexture )
						{
							if ( MaskImg->data ) free( MaskImg->data );
							delete MaskImg;
							MaskImg = 0;
						}
					}
					else if ( !strcmp( cur_token, "edgeLength" ) )
					{
						EdgeLenght = (float)atof( buffer );
					}
					else if ( !strcmp( cur_token, "shellTextureSize" ) )
					{
						ShellTextureSize = atoi( buffer );
					}
					else if ( !strcmp( cur_token, "numShells" ) )
					{
						NumShells = min( atoi( buffer ), S3D_MAX_FURSHELLS );
					}
					else if ( !strcmp( cur_token, "density" ) )
					{
						Density = atoi( buffer );
					}
					else if ( !strcmp( cur_token, "length" ) )
					{
						Lenght = (float)atof( buffer );
					}
					else if ( !strcmp( cur_token, "scale" ) )
					{
						Scale = (float)atof( buffer );
					}
					else if ( !strcmp( cur_token, "whiskers" ) )
					{
						if ( !Whiskers ) Whiskers = new CS3DWhiskers( BindedModel, buffer );
					}
				}
				memset( buffer, 0, sizeof( buffer ) );
			}
			else
			{
				if ( s3dIOController->Eof() ) break;
				if ( S3D_ISSPASE( ch ) && !open_string ) continue;
				buffer[ strlen( buffer ) ] = ch;
			}
		}
		if ( cur_token ) free( cur_token );
		s3dIOController->CloseFile( io );
		return true;
	}
	return false;
}

unsigned int CS3DFur::TrianglesCount()
{
	return NumTriengles;
}

void CS3DFur::GenShellTextures()
{
	ShellTextures = new unsigned int[ NumShells ];
	memset( ShellTextures, 0, sizeof( unsigned int ) * NumShells );

	unsigned int w, h, bpp, size;
	w = h = ShellTextureSize;
	bpp = 4;
	size = bpp * w * h * sizeof( unsigned char );

	int i;
	unsigned char *data = (unsigned char *)malloc( size );
	int wh = w * h;
	for ( i = 0; i < wh; i++ )
	{
		data[ bpp* i + 0 ] = 255;
		data[ bpp *i + 1 ] = 255;
		data[ bpp *i + 2 ] = 255;
		data[ bpp *i + 3 ] = 0;
	}

	for ( i = (int)NumShells - 1; i >= 0; i-- )
	{
		// add some noise
		int x, y;
		for ( unsigned j = 0; j < Density; j++ )
		{
			x = rand() % w;
			y = rand() % h;
			data[ bpp * ( w * y + x ) + 3 ] = (unsigned char)( 128.0f + 128.0f / i );
		}
		ShellTextures[i] = s3dLoadTextureFromData( data, w, h, bpp, true, true );
	}
	free( data );
}

void CS3DFur::GenEdges()
{
	S3D_Vertex edge;
	int edgeIDs[] = { 0, 1, 2, 1, 0, 2 };
	S3D_Vertex verts[4];
	S3D_TexCoord texCoords[4];

	const int numEdges = 3;
	const int numVerts = 6;

	int numOutVerts = BindedModel->Header.numFaces * numEdges * numVerts;
	S3D_FurBody *furBody = new S3D_FurBody[ numOutVerts ];

	float z;
	int i3, e, e1;
	int cur = 0;
	for ( unsigned int i = 0; i < BindedModel->Header.numFaces; i++ )
	{
		i3 = i * 3;
		for ( int ei = 0; ei < numEdges; ei++ )
		{
			e = i3 + edgeIDs[ei * 2];
			e1 = i3 + edgeIDs[ei * 2 + 1];

			edge = BindedModel->Body[e].vertex - BindedModel->Body[e1].vertex;
			z = Scale * edge.magnitude();

			float mask[2] = { 1, 1 };
			if ( MaskImg )
			{
				int x = max( (int)(BindedModel->Body[ e ].tcoord.S * (MaskImg->width - 1)), 0 );
				int y = max( (int)(BindedModel->Body[ e ].tcoord.T * (MaskImg->height - 1)), 0 );
				mask[0] = (float)MaskImg->data[ (x + y * MaskImg->width) * MaskImg->bpp ] / 255;
				x = max( (int)(BindedModel->Body[ e1 ].tcoord.S * (MaskImg->width - 1)), 0 );
				y = max( (int)(BindedModel->Body[ e1 ].tcoord.T * (MaskImg->height - 1)), 0 );
				mask[1] = (float)MaskImg->data[ (x + y * MaskImg->width) * MaskImg->bpp ] / 255;
			}

			verts[0] = BindedModel->Body[e].vertex;
			verts[1] = BindedModel->Body[e].vertex + BindedModel->Body[e].normal * EdgeLenght * Lenght * mask[0];
			verts[2] = BindedModel->Body[e1].vertex + BindedModel->Body[e1].normal * EdgeLenght * Lenght * mask[1];
			verts[3] = BindedModel->Body[e1].vertex;

			texCoords[0].S = 0; texCoords[0].T = 1;
			texCoords[1].S = 0; texCoords[1].T = 0.1f;
			texCoords[2].S = z; texCoords[2].T = 0.1f;
			texCoords[3].S = z; texCoords[3].T = 1;

			int scenario[numVerts] = { 0, 1, 2, 2, 3, 0 };
			int scenario_v[numVerts] = { e, e, e1, e1, e1, e };
			for ( int j = 0; j < numVerts; j ++ )
			{
				furBody[cur].vertex = verts[ scenario[j] ];
				furBody[cur].normal = BindedModel->Body[ scenario_v[j] ].normal;
				furBody[cur].tcoord1 = texCoords[ scenario[j] ];
				furBody[cur].tcoord2 = BindedModel->Body[ scenario_v[j] ].tcoord;
				if ( BindedModel->Skeleton )
				{
					for ( int w = 0; w < 4; w++ )
						furBody[cur].weights[w] = BindedModel->Skeleton->weights[w][ BindedModel->Faces[ scenario_v[j] ] ];
					for ( int id = 0; id < 4; id++ )
						furBody[cur].indices[id] = BindedModel->Skeleton->indices[id][ BindedModel->Faces[ scenario_v[j] ] ];
				}
				cur++;
			}
		}
	}

	s3dBuffer->SetBufferData( EdgeBuffer, furBody, numOutVerts * 18, 0, sizeof( float ), S3D_VBO );
	delete [] furBody;
	NumTriengles += BindedModel->Header.numFaces * 6;
}

void CS3DFur::GenShells()
{
	float z;
	int i3;

	S3D_Vertex v1, v2;
	float l1, l2;
	float v1_dot_v2;

	for ( unsigned int s = 0; s < NumShells; s++ )
	{
		float offset = Lenght / ( NumShells - 1 );
		z = offset + (float)s * offset;

		int numOutVerts = BindedModel->Header.numFaces * 3;
		S3D_FurBody *furBody = new S3D_FurBody[ numOutVerts ];

		for ( unsigned int i = 0; i < BindedModel->Header.numFaces; i++ )
		{
			i3 = i * 3;

			float mask = 1;
			if ( MaskImg )
			{
				int x = max( (int)(BindedModel->Body[ i3 ].tcoord.S * (MaskImg->width - 1)), 0 );
				int y = max( (int)(BindedModel->Body[ i3 ].tcoord.T * (MaskImg->height - 1)), 0 );
				mask = (float)MaskImg->data[ (x + y * MaskImg->width) * MaskImg->bpp ] / 255;
				z = offset * ( s + 1 ) * mask;
			}
			furBody[ i3 ].vertex = BindedModel->Body[ i3 ].vertex + BindedModel->Body[ i3 ].normal * z;
			furBody[ i3 ].normal = BindedModel->Body[ i3 ].normal;
			furBody[ i3 ].tcoord1.S = 0;
			furBody[ i3 ].tcoord1.T = 0;
			furBody[ i3 ].tcoord2 = BindedModel->Body[ i3 ].tcoord;
			if ( BindedModel->Skeleton )
			{
				for ( int w = 0; w < 4; w++ )
					furBody[ i3 ].weights[w] = BindedModel->Skeleton->weights[w][ BindedModel->Faces[ i3 ] ];
				for ( int id = 0; id < 4; id++ )
					furBody[ i3 ].indices[id] = BindedModel->Skeleton->indices[id][ BindedModel->Faces[ i3 ] ];
			}

			v1 = BindedModel->Body[i3 + 1].vertex - BindedModel->Body[i3].vertex;
			l1 = v1.magnitude();
			if ( l1 > 0 ) v1 /= l1;

			i3++;
			mask = 1;
			if ( MaskImg )
			{
				int x = max( (int)(BindedModel->Body[ i3 ].tcoord.S * (MaskImg->width - 1)), 0 );
				int y = max( (int)(BindedModel->Body[ i3 ].tcoord.T * (MaskImg->height - 1)), 0 );
				mask = (float)MaskImg->data[ (x + y * MaskImg->width) * MaskImg->bpp ] / 255;
				z = offset * ( s + 1 ) * mask;
			}
			furBody[ i3 ].vertex = BindedModel->Body[ i3 ].vertex + BindedModel->Body[ i3 ].normal * z;
			furBody[ i3 ].normal = BindedModel->Body[ i3 ].normal;
			furBody[ i3 ].tcoord1.S = Scale * l1;
			furBody[ i3 ].tcoord1.T = 0;
			furBody[ i3 ].tcoord2 = BindedModel->Body[ i3 ].tcoord;
			if ( BindedModel->Skeleton )
			{
				for ( int w = 0; w < 4; w++ )
					furBody[ i3 ].weights[w] = BindedModel->Skeleton->weights[w][ BindedModel->Faces[ i3 ] ];
				for ( int id = 0; id < 4; id++ )
					furBody[ i3 ].indices[id] = BindedModel->Skeleton->indices[id][ BindedModel->Faces[ i3 ] ];
			}

			v2 = BindedModel->Body[i3 + 1].vertex - BindedModel->Body[i3 - 1].vertex;
			l2 = v2.magnitude();
			if ( l2 > 0 ) v2 /= l2;
			v1_dot_v2 = v1.dot( v2 );

			i3++;
			mask = 1;
			if ( MaskImg )
			{
				int x = max( (int)(BindedModel->Body[ i3 ].tcoord.S * (MaskImg->width - 1)), 0 );
				int y = max( (int)(BindedModel->Body[ i3 ].tcoord.T * (MaskImg->height - 1)), 0 );
				mask = (float)MaskImg->data[ (x + y * MaskImg->width) * MaskImg->bpp ] / 255;
				z = offset * ( s + 1 ) * mask;
			}
			furBody[ i3 ].vertex = BindedModel->Body[ i3 ].vertex + BindedModel->Body[ i3 ].normal * z;
			furBody[ i3 ].normal = BindedModel->Body[ i3 ].normal;
			furBody[ i3 ].tcoord1.S = Scale * l2 * v1_dot_v2;
			furBody[ i3 ].tcoord1.T = Scale * l2 * sinf( acosf( v1_dot_v2 ) );
			furBody[ i3 ].tcoord2 = BindedModel->Body[ i3 ].tcoord;
			if ( BindedModel->Skeleton )
			{
				for ( int w = 0; w < 4; w++ )
					furBody[ i3 ].weights[w] = BindedModel->Skeleton->weights[w][ BindedModel->Faces[ i3 ] ];
				for ( int id = 0; id < 4; id++ )
					furBody[ i3 ].indices[id] = BindedModel->Skeleton->indices[id][ BindedModel->Faces[ i3 ] ];
			}
		}

		s3dBuffer->SetBufferData( ShellBuffers[s], furBody, numOutVerts * 18, 0, sizeof( float ), S3D_VBO );
		delete [] furBody;
	}
	NumTriengles += BindedModel->Header.numFaces * NumShells;
}


void CS3DFur::Draw()
{
	if ( !bInit || !s3dBuffer || !s3dFurShader ) return;

	S3D_OpenGLOp::EnableBackFace();
	DrawEdges();
	if ( Whiskers ) Whiskers->Draw();
	S3D_OpenGLOp::DisableBackFace();
	DrawShells();
	S3D_OpenGLOp::EnableBackFace();

	S3D_OpenGLOp::DisableTexture( 2 );
	S3D_OpenGLOp::DisableTexture( 1 );
	S3D_OpenGLOp::DisableTexture( 0 );
}

void CS3DFur::BeginFurShader( CS3DShader *shader )
{
	shader->BeginShaderProgram();
	if ( BindedModel->Skeleton )
	{
		shader->EnableAttributes();
	}
	else
	{
		shader->EnableAttribute( 3 );
		shader->EnableAttribute( 2 );
		shader->EnableAttribute( 1 );
		shader->EnableAttribute( 0 );
	}
	shader->SetUniformMatrixf( "mvpMat", 4, (float*)&BindedModel->mvpMat );

	CS3DVec3f lightPosition = s3dCamera->Get() * s3dLight->GetLight( 0 )->pos;
	shader->SetUniformf( "mvLightPos", 3, (float*)&lightPosition );
	shader->SetUniformMatrixf( "mvMat", 4, (float*)&BindedModel->modelViewMat );
	float nMat[9];
	CS3DMat16f temp;
	temp.set( (float*)&BindedModel->modelViewMat );
	temp.getNormalMat( nMat );
	shader->SetUniformMatrixf( "nMat", 3, nMat );

	S3D_OpenGLOp::EnableTexture( MaskTexture, 2 );
	shader->SetSampler( "maskMap", 2 );
	S3D_OpenGLOp::EnableTexture( BindedModel->MaterialTexture( 0, S3D_DiffuseMap ), 1 );
	shader->SetSampler( "diffuseMap", 1 );
}

void CS3DFur::SetAttributes( CS3DShader *shader, unsigned int buffID, unsigned int furTexID )
{
	S3D_OpenGLOp::EnableTexture( furTexID, 0 );
	shader->SetSampler( "furMap", 0 );
	if ( BindedModel->Animated() )
		BindedModel->SetTransformsToSkinShader( shader );
	static S3D_FurBody *furBody = 0;
	s3dBuffer->BindBuffer( buffID, S3D_VBO );
	if ( BindedModel->Skeleton )
	{
		shader->SetAttribPointerf( 5, 4, &furBody->indices, sizeof( S3D_FurBody ) );
		shader->SetAttribPointerf( 4, 4, &furBody->weights, sizeof( S3D_FurBody ) );
	}
	shader->SetAttribPointerf( 3, 2, &furBody->tcoord2, sizeof( S3D_FurBody ) );
	shader->SetAttribPointerf( 2, 2, &furBody->tcoord1, sizeof( S3D_FurBody ) );
	shader->SetAttribPointerf( 1, 3, &furBody->normal, sizeof( S3D_FurBody ) );
	shader->SetAttribPointerf( 0, 3, &furBody->vertex, sizeof( S3D_FurBody ) );
}

void CS3DFur::EndFurShader( CS3DShader *shader )
{
	if ( BindedModel->Skeleton )
	{
		shader->DisableAttributes();
	}
	else
	{
		shader->DisableAttribute( 3 );
		shader->DisableAttribute( 2 );
		shader->DisableAttribute( 1 );
		shader->DisableAttribute( 0 );
	}
	shader->EndShaderProgram();
	s3dBuffer->UnbindBuffer( S3D_VBO );
}

void CS3DFur::DrawEdges()
{
	S3D_OpenGLOp::DisableDepthMask();
	BeginFurShader( s3dFurShader );
	const int numEdges = 3;
	const int numVerts = 6;
	float shadow = ShadowCoef;
	s3dFurShader->SetUniformf( "shadow", 1, &shadow );
	float edges = 0.5;
	s3dFurShader->SetUniformf( "edges", 1, &edges );
	float staticFactor = BindedModel->Animated() ? 0.f : 1.f;
	s3dFurShader->SetUniformf( "staticFactor", 1, &staticFactor );
	SetAttributes( s3dFurShader, EdgeBuffer, EdgeTexture );
	S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLES, 0, BindedModel->Header.numFaces * numEdges * numVerts );
	EndFurShader( s3dFurShader );
	S3D_OpenGLOp::EnableDepthMask();
}

void CS3DFur::DrawShells()
{
	BeginFurShader( s3dFurShader );
	float edges = 0;
	s3dFurShader->SetUniformf( "edges", 1, &edges );
	float staticFactor = BindedModel->Animated() ? 0.f : 1.f;
	s3dFurShader->SetUniformf( "staticFactor", 1, &staticFactor );
	float shadow, z;
	for ( unsigned int s = 0; s < NumShells; s++ )
	{
		float offset = Lenght / ( NumShells - 1 );
		z = offset + (float)s * offset;
		shadow = ShadowCoef + ( 1 - ShadowCoef ) * ( (float)s / ( NumShells - 1 ) );
		s3dFurShader->SetUniformf( "shadow", 1, &shadow );
		SetAttributes( s3dFurShader, ShellBuffers[s], ShellTextures[s] );
		S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLES, 0, BindedModel->Header.numFaces * 3 );
	}
	EndFurShader( s3dFurShader );
}

CS3DWhiskers::CS3DWhiskers( CS3DModel *m, const char* filename )
{
	Color = S3D_Color( 1, 1, 1, 1 );
	Lenght = 1.0f;
	BindedModel = m;
	Geometry = 0;
	Transformed = 0;
	NumParts = 1;
	NumVerts = 0;
	if ( !BindedModel ) return;

	char Filename[0x100] = { 0 };
	strcpy( Filename, BindedModel->Path );
	strcat( Filename, filename );
	S3D_Vertex ctrl_1, ctrl_2, offset_ends;
	void *io = s3dIOController->OpenFile( Filename );
	if ( io )
	{
		char buffer[0x100] = { 0 };
		char *cur_token = 0;
		bool open_string = false;
		while ( !s3dIOController->Eof() )
		{
			char ch = (char)s3dIOController->ReadByte();
			if ( ch == '=' )
			{
				int size = sizeof( char ) * ( strlen( buffer ) + 1 );
				if ( cur_token ) free( cur_token );
				cur_token = (char *)malloc( size );
				strcpy( cur_token, buffer );
				memset( buffer, 0, sizeof( buffer ) );
			}
			else if ( ch == '"' )
			{
				open_string = !open_string;
				if ( !open_string )
				{
					if ( !strcmp( cur_token, "indices" ) )
					{
						const char *indecesStr = buffer;
						unsigned int indecesStrLen = strlen( indecesStr );

						S3D_Index index, realIndex;
						index = (S3D_Index)atoi( indecesStr );
						for ( realIndex = 0; realIndex < BindedModel->Header.numFaces * 3; realIndex++ )
						{
							if ( BindedModel->Faces[ realIndex ] == index ) break;
						}
						if ( realIndex < BindedModel->Header.numFaces * 3 )
							Indices.push_back( realIndex );
						for ( unsigned int i = 0; i < indecesStrLen; i++ )
						{
							if ( i && indecesStr[i - 1] == ' ' )
							{
								index = (S3D_Index)atoi( &indecesStr[i] );
								for ( realIndex = 0; realIndex < BindedModel->Header.numFaces * 3; realIndex++ )
								{
									if ( BindedModel->Faces[ realIndex ] == index ) break;
								}
								if ( realIndex < BindedModel->Header.numFaces * 3 )
									Indices.push_back( realIndex );
							}
						}
					}
					else if ( !strcmp( cur_token, "color" ) )
					{
						const char *colorStr = buffer;
						unsigned int colorStrLen = strlen( colorStr );
						Color[ 0 ] = (float)atof( colorStr );
						int cur = 1;
						for ( unsigned int i = 1; i < colorStrLen; i++ )
						{
							if ( i && colorStr[i - 1] == ' ' )
							{
								Color[ cur++ ] = (float)atof( &colorStr[i] );
								if ( cur == 4 ) { i++; break; }
							}
						}
					}
					else if ( !strcmp( cur_token, "offset_ends" ) )
					{
						const char *offsetStr = buffer;
						unsigned int offsetStrLen = strlen( offsetStr );
						offset_ends[ 0 ] = (float)atof( offsetStr );
						int cur = 1;
						for ( unsigned int i = 1; i < offsetStrLen; i++ )
						{
							if ( i && offsetStr[i - 1] == ' ' )
							{
								offset_ends[ cur++ ] = (float)atof( &offsetStr[i] );
								if ( cur == 3 ) { i++; break; }
							}
						}
					}
					else if ( !strcmp( cur_token, "control_points" ) )
					{
						const char *controlStr = buffer;
						unsigned int controlStrLen = strlen( controlStr );
						ctrl_1[ 0 ] = (float)atof( controlStr );
						int cur = 1;
						unsigned int i;
						for ( i = 1; i < controlStrLen; i++ )
						{
							if ( i && controlStr[i - 1] == ' ' )
							{
								ctrl_1[ cur++ ] = (float)atof( &controlStr[i] );
								if ( cur == 3 ) { i++; break; }
							}
						}
						cur = 0;
						while ( i < controlStrLen )
						{
							if ( i && controlStr[i - 1] == ' ' )
							{
								ctrl_2[ cur++ ] = (float)atof( &controlStr[i] );
								if ( cur == 3 ) { i++; break; }
							}
							i++;
						}
					}
					else if ( !strcmp( cur_token, "length" ) )
					{
						Lenght = (float)atof( buffer );
					}
					else if ( !strcmp( cur_token, "numParts" ) )
					{
						NumParts = atoi( buffer );
					}
				}
				memset( buffer, 0, sizeof( buffer ) );
			}
			else
			{
				if ( s3dIOController->Eof() ) break;
				if ( S3D_ISSPASE( ch ) && !open_string ) continue;
				buffer[ strlen( buffer ) ] = ch;
			}
		}
		if ( cur_token ) free( cur_token );
		s3dIOController->CloseFile( io );
		// calc geometry:
		if ( !Indices.empty() && NumParts )
		{
			NumVerts = Indices.size() * 2 * NumParts;
			Geometry = new S3D_Vertex[ NumVerts ];
			Transformed = new S3D_Vertex[ NumVerts ];
			S3D_Index index;
			S3D_Vertex *pCtrl_1 = 0, *pCtrl_2 = 0;
			S3D_Vertex localOffset, localCtrl_1, localCtrl_2;
			S3D_Vertex end;
			float t;
			for ( unsigned int i = 0; i < NumVerts; i += 2 * NumParts )
			{
				index = (S3D_Index)((float)i / NumParts * 0.5f);
				S3D_Body *b = &BindedModel->Body[ Indices[ index ] ];
				Geometry[i] = b->vertex;
				localOffset = offset_ends;
				localCtrl_1 = ctrl_1;
				localCtrl_2 = ctrl_2;
				if ( b->normal[0] < 0 )
				{
					localOffset[0] = -localOffset[0];
					localCtrl_1[0] = -ctrl_1[0];
					localCtrl_2[0] = -ctrl_2[0];
				}
				end = b->vertex + b->normal * Lenght + localOffset;
				S3D_Vertex ctrl1 = b->vertex + localCtrl_1, 
						   ctrl2 = end + localCtrl_2;
				pCtrl_1 = &ctrl1;
				pCtrl_2 = &ctrl2;
				if ( NumParts != 1 )
				{
					for ( short p = 1; p < NumParts; p++ )
					{
						t = (float)p / NumParts;
						Geometry[i + p * 2 - 1] = CalcSpline( b->vertex, end, t, pCtrl_1, pCtrl_2 );
						Geometry[i + p * 2] = Geometry[i + p * 2 - 1];
					}
				}
				Geometry[i + NumParts * 2 - 1] = end;
			}
			for ( unsigned int i = 0; i < NumVerts; i ++ )
				Transformed[i] = Geometry[i];
		}
	}
}

CS3DWhiskers::~CS3DWhiskers()
{
	if ( Transformed ) delete [] Transformed;
	if ( Geometry ) delete [] Geometry;
}

S3D_Vertex CS3DWhiskers::AnimateVertex( S3D_Vertex in, S3D_Index index )
{
	S3D_Vertex out;
	S3D_Index vertID = BindedModel->Faces[ Indices[ index ] ];
	S3D_Transform *t;
	for ( int w = 0; w < S3D_BONE_AFFECT_LIMITED; w++ )
	{
		if ( BindedModel->Skeleton->weights[w][vertID] > S3D_EPS )
		{
			t = &BindedModel->Animations[ BindedModel->CurAnimation.curID ]->Transforms[BindedModel->Skeleton->indices[w][vertID]][BindedModel->CurFrame.curID];
			out += ( t->position + S3D_QuatOp::rotate( in, t->rotation ) ) * BindedModel->Skeleton->weights[w][vertID];
		}
	}
	return out;
}

S3D_Vertex CS3DWhiskers::CalcSpline( S3D_Vertex &start, S3D_Vertex &end, float t,  
	S3D_Vertex *ctrl_1, S3D_Vertex *ctrl_2 )
{
	float n = 3;
	if ( !ctrl_1 ) n--;
	if ( !ctrl_2 ) n--;
	float coef1 = powf( 1 - t, n );
	float coef2 = powf( 1 - t, n - 1 ) * n * t;
	float coef3 = powf(     t, n - 1 ) * n * ( 1 - t );
	float coef4 = powf( t, n );
    S3D_Vertex arDsc[] = {
        start * coef1,
        ctrl_1 ? (*ctrl_1) * coef2 : S3D_Vertex(0,0,0),
        ctrl_2 ? (*ctrl_2) * coef3 : S3D_Vertex(0,0,0),
        end * coef4,
    };
	return arDsc[0] + arDsc[1] + arDsc[2] + arDsc[3];
}

void CS3DWhiskers::Draw()
{
	if ( Indices.empty() || !BindedModel ) return;
	s3dSimpleShader->BeginShaderProgram();
	s3dSimpleShader->EnableAttribute( 0 );
	s3dSimpleShader->SetUniformMatrixf( "mvpMat", 4, (float*)&BindedModel->mvpMat );
	s3dSimpleShader->SetUniformf( "u_color", 4, (float*)&Color );

	if ( BindedModel->Animated() )
	{
		for ( unsigned int i = 0; i < NumVerts; i ++ )
			Transformed[i] = AnimateVertex( Geometry[i], (S3D_Index)((float)i / NumParts * 0.5f) );
	}
	s3dSimpleShader->SetAttribPointerf( 0, 3, Transformed );
	S3D_OpenGLOp::DrawPrimitives( S3D_LINES, 0, NumVerts );

	s3dSimpleShader->DisableAttribute( 0 );
	s3dSimpleShader->EndShaderProgram();
}

#endif
