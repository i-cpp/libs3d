#include <S3D.h>

#if ( S3D_USE_STDIO )

#include <stdarg.h>
#include <stdio.h>

void *def_open( const char *filename )
{
	return (void*)fopen( filename, "rb" );
}

bool def_read( void *io, void *data, unsigned int size )
{
	fread( data, (unsigned int)size, 1, (FILE*)io );
	return !feof( (FILE*)io );
}

void def_close( void *io )
{
	fclose( (FILE*)io );
}

void def_log( const char *msg )
{
	FILE *f = fopen( S3D_LOG_FILE, "at" );
	if ( f )
	{
		fwrite( msg, sizeof( char ), strlen( msg ), f );
		fclose ( f );
	}
}

void def_delete( const char *filename )
{
	remove( filename );
}

#endif

CS3DIOController::CS3DIOController()
{
#if ( S3D_USE_STDIO )
	Open = def_open;
	Read = def_read;
	Close = def_close;
	Log = def_log;
	Delete = def_delete;
#else
	Open = 0;
	Read = 0;
	Close = 0;
	Log = 0;
	Delete = 0;
#endif
	for ( int i = 0; i < S3D_MAX_IO; i++ )
		IOs[i] = 0;
}

CS3DIOController::~CS3DIOController()
{
}

void CS3DIOController::SetIOFunctions( void *(*open_func) ( const char *filename ),
									   bool  (*read_func) ( void* io, void *data, unsigned int size ),
									   void  (*close_func)( void* io ),
									   void  (*log_func) ( const char *msg ),
									   void  (*delete_func) ( const char *filename ) )
{
	if ( open_func ) Open = open_func;
	if ( read_func ) Read = read_func;
	if ( close_func ) Close = close_func;
	if ( log_func ) Log = log_func;
	if ( delete_func ) Delete = delete_func;
}

void CS3DIOController::DeleteFile( const char *filename )
{
	if ( Delete ) Delete( filename );
}

void CS3DIOController::PrintLog( const char *msg )
{
	if ( Log ) Log( msg );
}
void *CS3DIOController::OpenFile( const char *filename )
{
	if ( !Open ) return 0;
	int len = (int)strlen( filename );
	char *filename_dub = (char*)malloc( ( len + 1 ) * sizeof( char ) );
	memcpy( filename_dub, filename, len );
	filename_dub[len] = 0;
	for ( int i = 0; i < len; ++i )
	{
		if ( strchr( "/\\", filename_dub[i] ) )
			filename_dub[i] = S3D_SLASH;
	}
	void *io = Open( filename_dub );
	free( filename_dub );
	if ( io )
	{
		int i;
		for ( i = 0; i < S3D_MAX_IO; i++ )
		{
			if ( !IOs[i] )
			{
				IOs[i] = io;
				pos = 0;
				eof = false;
				break;
			}
		}
		if ( i == S3D_MAX_IO )
		{
			PrintLog( "Error: Number of IO more than S3D_MAX_IO!\n" );
		}
	}
	return io;
}

void CS3DIOController::CloseFile( void* io )
{
	if ( !Close ) return;
	if ( io )
	{
		Close( io );
		for ( int i = S3D_MAX_IO - 1; i >= 0; i-- )
		{
			if ( IOs[i] )
			{
				IOs[i] = 0;
				eof = false;
				break;
			}
		}
	}
}

int CS3DIOController::ToInt( const char* bytes )
{
	return (int)(((unsigned char)bytes[3] << 24) |
				 ((unsigned char)bytes[2] << 16) |
				 ((unsigned char)bytes[1] << 8) |
				  (unsigned char)bytes[0]);
}

short CS3DIOController::ToShort( const char* bytes )
{
	return (short)(((unsigned char)bytes[1] << 8) |
				    (unsigned char)bytes[0]);
}

unsigned short CS3DIOController::ToUShort( const char* bytes )
{
	return (unsigned short)(((unsigned char)bytes[1] << 8) |
							 (unsigned char)bytes[0]);
}

float CS3DIOController::ToFloat( const char* bytes )
{
	float f;
	((char*)&f)[0] = bytes[0];
	((char*)&f)[1] = bytes[1];
	((char*)&f)[2] = bytes[2];
	((char*)&f)[3] = bytes[3];
	return f;
}

int CS3DIOController::ReadInt()
{
	if ( !Read ) return ~0;
	for ( int i = S3D_MAX_IO - 1; i >= 0; i-- )
	{
		if ( IOs[i] )
		{
			char buffer[4];
			eof = !Read( IOs[i], &buffer, sizeof( char ) * 4 );
			pos += ( sizeof( char ) * 4 );
			return ToInt(buffer);
		}
	}
	return 0;
}

short CS3DIOController::ReadShort()
{
	if ( !Read ) return ~0;
	for ( int i = S3D_MAX_IO - 1; i >= 0; i-- )
	{
		if ( IOs[i] )
		{
			char buffer[2];
			eof = !Read( IOs[i], &buffer, sizeof( char ) * 2 );
			pos += ( sizeof( char ) * 2 );
			return ToShort(buffer);
		}
	}
	return 0;
}

unsigned short CS3DIOController::ReadUShort()
{
	if ( !Read ) return ~0;
	for ( int i = S3D_MAX_IO - 1; i >= 0; i-- )
	{
		if ( IOs[i] )
		{
			char buffer[2];
			eof = !Read( IOs[i], &buffer, sizeof( char ) * 2 );
			pos += ( sizeof( char ) * 2 );
			return ToUShort(buffer);
		}
	}
	return 0;
}

float CS3DIOController::ReadFloat()
{
	if ( !Read ) return ~0;
	for ( int i = S3D_MAX_IO - 1; i >= 0; i-- )
	{
		if ( IOs[i] )
		{
			char buffer[4];
			eof = !Read( IOs[i], &buffer, sizeof( char ) * 4 );
			pos += ( sizeof( char ) * 4 );
			return ToFloat(buffer);
		}
	}
	return 0;
}

unsigned char CS3DIOController::ReadByte()
{
	if ( !Read ) return ~0;
	for ( int i = S3D_MAX_IO - 1; i >= 0; i-- )
	{
		if ( IOs[i] )
		{
			unsigned char buffer;
			eof = !Read( IOs[i], &buffer, sizeof( unsigned char ) );
			pos += sizeof( unsigned char );
			return buffer;
		}
	}
	return 0;
}

void CS3DIOController::ReadBlock( unsigned char *buffer, int size )
{
	if ( !Read ) return;
	for ( int i = S3D_MAX_IO - 1; i >= 0; i-- )
	{
		if ( IOs[i] )
		{
			eof = !Read( IOs[i], buffer, size );
			pos += size;
			return;
		}
	}
}

CS3DVec3f CS3DIOController::ReadVec3f()
{
	float x = ReadFloat();
	float y = ReadFloat();
	float z = ReadFloat();
	return CS3DVec3f(x, y, z);
}

S3D_Quat CS3DIOController::ReadQuat()
{
	float x = ReadFloat();
	float y = ReadFloat();
	float z = ReadFloat();
	float w = ReadFloat();
	return S3D_Quat(x, y, z, w);
}

