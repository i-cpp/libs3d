#include <S3D.h>

CS3DMesh::CS3DMesh()
{
	MeshShader = 0;
	MeshSize = 0;
	Faces = 0;
	MatIDs = 0;
	Textured = false;
	memset( &Header, 0, sizeof( S3D_Header ) );
	Body = 0;
	Buffer = -1;
	TriangleType = S3D_TRIANGLES;
	BBList = 0;
	BBLevel = 10;
	originalBox = 0;
}

CS3DMesh::~CS3DMesh()
{
	if ( originalBox ) delete originalBox;
	if ( s3dBuffer )
	{
#ifdef S3D_VAO
		for ( unsigned i = 0; i < VertexArray.size(); i++ )
			s3dBuffer->FreeBuffer( VertexArray[i], S3D_VAO );
#endif
		s3dBuffer->FreeBuffer( Buffer, S3D_VBO );
	}
	if ( BBList ) delete BBList;
	if ( MatIDs ) delete [] MatIDs;
	if ( Body ) delete [] Body;
	if ( Faces ) delete[] Faces;
	if ( MeshShader ) delete MeshShader;
}

void CS3DMesh::GetOriginalBox( class CS3DBox &box )
{
	box = *originalBox;
}

void CS3DMesh::GetBody( S3D_Body **body, S3D_Header &header )
{
	*body = Body;
	header = Header;
}

int CS3DMesh::LoadMesh( const char *filename )
{
	if ( MeshSize ) return MeshSize;
	void *io = s3dIOController->OpenFile( filename );
	if ( io )
	{
		S3D_Vertex *Verts = 0;
		CS3DVec3f *Normals = 0;
		S3D_TexCoord *TVerts = 0;
		S3D_Index *TFaces = 0;
		unsigned int i;
		//////////////////////////////////////////////////////////////
		// READ MESH SECTION										//
		//////////////////////////////////////////////////////////////
		// read header
		s3dIOController->ReadBlock( (unsigned char *)&Header, sizeof( S3D_Header ) );
		if ( Header.numFaces > S3D_MAX_TRIANGLES )
		{
			s3dIOController->CloseFile( io );
			char Err[0x100] = { 0 };
			strcpy( Err, "Too many triangles in the mesh: " );
			strcat( Err, filename ); strcat( Err, ".\n" );
			s3dIOController->PrintLog( Err );
			memset( &Header, 0, sizeof( S3D_Header ) );
			return MeshSize;
		}
		// read verteces
		Verts = new S3D_Vertex[Header.numVerts];
		s3dIOController->ReadBlock( (unsigned char *)Verts, Header.numVerts * sizeof( S3D_Vertex ) );
		// read faces
		unsigned int numIndices = Header.numFaces * 3;
		Faces = new S3D_Index[numIndices];
		s3dIOController->ReadBlock( (unsigned char *)Faces, numIndices * sizeof( S3D_Index ) );
		// if exist TVerts
		if ( Header.numTVerts )
		{
			// read texture verts
			TVerts = new S3D_TexCoord[Header.numTVerts];
			s3dIOController->ReadBlock( (unsigned char *)TVerts, Header.numTVerts * sizeof( S3D_TexCoord ) );
			// read texture faces
			TFaces = new S3D_Index[numIndices];
			s3dIOController->ReadBlock( (unsigned char *)TFaces, numIndices * sizeof( S3D_Index ) );
			Textured = true;
		}
		// read normals
		Normals = new CS3DVec3f[numIndices];
		s3dIOController->ReadBlock( (unsigned char *)Normals, numIndices * sizeof( CS3DVec3f ) );
		// read materials
		LoadMaterialFromMesh( filename, Header.numFaces );
		MeshSize = s3dIOController->GetCurPos();
		s3dIOController->CloseFile( io );
		//////////////////////////////////////////////////////////////
		// FILL MESH BODY SECTION									//
		//////////////////////////////////////////////////////////////
		// fill geometry, normals and texture coords
		Body = new S3D_Body[ numIndices ];
		for ( i = 0; i < numIndices; i++ )
		{
			Body[i].vertex = Verts[ Faces[i] ];
#if ( S3D_USE_LIGTHING )
			Body[i].normal = Normals[i];
#endif
			if ( Header.numTVerts )
			{
				Body[i].tcoord = TVerts[ TFaces[i] ];
			}
		}

		if ( Verts )	delete [] Verts;
		if ( Normals )	delete [] Normals;
		if ( TVerts )	delete [] TVerts;
		if ( TFaces )	delete [] TFaces;
#if ( S3D_USE_LIGTHING )
#if ( S3D_BUMPMAPPING )
		// fill tangents
		if ( Header.numTVerts ) CalculateTangents();
#endif
#endif
		originalBox = new CS3DBox();
		BuildBuffers();
	}
	else
	{
		char Err[0x100] = { 0 };
		strcpy( Err, "Can not load mesh: " );
		strcat( Err, filename ); strcat( Err, ".\n" );
		s3dIOController->PrintLog( Err );
	}

	return MeshSize;
}

#if ( S3D_USE_LIGTHING )
#if ( S3D_BUMPMAPPING )
void CS3DMesh::CalculateTangents()
{
	CS3DVec3f e21, e31, sdir, tdir, temp;
	float s1, t1, s2, t2, r;
	CS3DVec3f *v1, *v2, *v3;
	S3D_TexCoord *tv1, *tv2, *tv3;
	for( unsigned int i = 0; i < Header.numFaces; i++ )
	{
		int i3 = i * 3;
		v1 = &Body[ i3 + 0 ].vertex; tv1 = &Body[ i3 + 0 ].tcoord;
		v2 = &Body[ i3 + 1 ].vertex; tv2 = &Body[ i3 + 1 ].tcoord;
		v3 = &Body[ i3 + 2 ].vertex; tv3 = &Body[ i3 + 2 ].tcoord;

		e21 = *v2 - *v1;
		e31 = *v3 - *v1;
	    
		s1 = tv2->S - tv1->S;
		t1 = tv2->T - tv1->T;

		s2 = tv3->S - tv1->S;
		t2 = tv3->T - tv1->T;
	    
		float den = s1 * t2 - s2 * t1;
		if ( den == 0.0f ) r = 1.0f;
		else r = 1.0f / den;
		sdir = ( e21 * t2 - e31 * t1 ) * r;
		tdir = ( e31 * s1 - e21 * s2 ) * r;

		for ( int j = 0; j < 3; j++ )
		{
			int ij = i3 + j;
			temp = sdir - Body[ij].normal * Body[ij].normal.dot( sdir );
			temp.normalize();
			Body[ij].tangent[0] = temp[0];
			Body[ij].tangent[1] = temp[1];
			Body[ij].tangent[2] = temp[2];
			Body[ij].tangent[3] = ( Body[ij].normal ^ sdir ).dot( tdir ) < 0.0f ? -1.0f : 1.0f;
		}
	}
}
#endif
#endif

void CS3DMesh::BuildBuffers()
{
	if ( s3dBuffer && s3dLoadedShaders )
	{
		unsigned int BufferSize = 0;
		unsigned int numVerts = 3 * Header.numFaces;
		if ( Header.numTVerts )
		{
			BufferSize += 2 * numVerts;
#if ( S3D_USE_LIGTHING )
#if ( S3D_BUMPMAPPING )
			BufferSize += 4 * numVerts;
#endif
#endif
		}
		BufferSize += 3 * numVerts;
#if ( S3D_USE_LIGTHING )
		BufferSize += 3 * numVerts;
#endif
		s3dBuffer->SetBufferData( Buffer, Body, BufferSize, 0, sizeof( float ), S3D_VBO );
		bool makeIBO = true;
#ifdef S3D_VAO
		const char *vendor = (const char*)s3dGetInfo( S3D_VENDOR );
		if ( s3dBuffer->Supported( S3D_VAO ) && strstr( vendor, "Intel" ) )
		{
			makeIBO = false;
		}
#endif
		if ( MatIDs && makeIBO )
		{
			for ( int i = 0; i < NumMaterials(); i++ )
			{
				s3dBuffer->SetBufferData( MatIDs[i].buffID, MatIDs[i].IDs, 
					MatIDs[i].count, 0, sizeof( S3D_Index ), S3D_IBO );
				delete [] MatIDs[i].IDs;
				MatIDs[i].IDs = 0;
			}
		}
	}
}

void CS3DMesh::DrawMesh()
{
	const CS3DMat16f &camMat = s3dCamera->Get();
	CS3DMat16f mvMat = modelMat * camMat;
	if ( MeshShader )
	{
		MeshShader->BeginShaderProgram();
#if ( S3D_USE_LIGTHING )
		CS3DVec3f lightPosition = camMat * s3dLight->GetLight( 0 )->pos;
		MeshShader->SetUniformf( "mvLightPos", 3, (float*)&lightPosition );
#if ( S3D_USE_SHADOWS )
		MeshShader->SetUniformMatrixf( "lMat", 4, (float*)&s3dLight->GetLight( 0 )->mat );
#endif
#endif
		CS3DMat16f mvpMat = mvMat * s3dProjection;
		MeshShader->SetUniformMatrixf( "mvpMat", 4, (float*)&mvpMat );
#if ( S3D_USE_LIGTHING )
		MeshShader->SetUniformMatrixf( "mvMat", 4, (float*)&mvMat );

#if ( S3D_USE_CUBEMAPS )
		float mat[9];
		modelMat.getNormalMat( mat );
		MeshShader->SetUniformMatrixf( "mNormalMat", 3, mat );
		memcpy( &mat[0], &modelMat[0], 3 * sizeof(float) );
		memcpy( &mat[3], &modelMat[4], 3 * sizeof(float) );
		memcpy( &mat[6], &modelMat[8], 3 * sizeof(float) );
		MeshShader->SetUniformMatrixf( "mMat", 3, (float*)&mat );
		CS3DVec3f camPos = s3dCamera->GetPosition();
		MeshShader->SetUniformf( "wsCamPos", 3, (float*)&camPos );
#endif

		float nMat[9];
		mvMat.getNormalMat( nMat );
		MeshShader->SetUniformMatrixf( "nMat", 3, nMat );
#endif
	}
#if ( !S3D_OGLES_2_0 )
	else
	{
		S3D_OpenGLOp::SetMatrices( (float*)&mvMat );
	}
#endif
    if ( s3dBuffer && MeshShader )
    {
#ifdef S3D_VAO
		if ( !VertexArray.empty() )
		{
			for ( unsigned i = 0; i < VertexArray.size(); i++ )
			{
				s3dBuffer->BindBuffer( VertexArray[i], S3D_VAO );
				EnableMaterial( i, MeshShader, Header.numTVerts != 0 );
				S3D_OpenGLOp::DrawPrimitives( TriangleType, MatIDs[i].IDs, MatIDs[i].count );
				DisableMaterial( i, MeshShader, Header.numTVerts != 0 );
				s3dBuffer->UnbindBuffer( S3D_VAO );
			}
		}
		else
#endif
		{
			unsigned int attribID = 4;
#if ( !S3D_USE_LIGTHING )
				--attribID;
				--attribID;
#else
#if ( !S3D_BUMPMAPPING )
				--attribID;
#endif
#endif
			static S3D_Body *meshOffset = 0;
			s3dBuffer->BindBuffer( Buffer, S3D_VBO );
			MeshShader->EnableAttributes();

			if ( Header.numTVerts )
			{
				MeshShader->SetAttribPointerf( --attribID, 2, &meshOffset->tcoord, sizeof( S3D_Body ) );
			}
#if ( S3D_USE_LIGTHING )
#if ( S3D_BUMPMAPPING )
			MeshShader->SetAttribPointerf( --attribID, 4, &meshOffset->tangent, sizeof( S3D_Body ) );
#endif
			MeshShader->SetAttribPointerf( --attribID, 3, &meshOffset->normal, sizeof( S3D_Body ) );
#endif
			MeshShader->SetAttribPointerf( --attribID, 3, &meshOffset->vertex, sizeof( S3D_Body ) );

			if ( MatIDs )
			{
				for ( int i = 0; i < NumMaterials(); i++ )
				{
					EnableMaterial( i, MeshShader, Header.numTVerts != 0 );
					if ( !MatIDs[i].IDs )
					{
						s3dBuffer->BindBuffer( MatIDs[i].buffID, S3D_IBO );
						S3D_OpenGLOp::DrawPrimitives( TriangleType, (S3D_Index*)0, MatIDs[i].count );
						s3dBuffer->UnbindBuffer( S3D_IBO );
					}
					else
					{
						S3D_OpenGLOp::DrawPrimitives( TriangleType, MatIDs[i].IDs, MatIDs[i].count );
					}
					DisableMaterial( i, MeshShader, Header.numTVerts != 0 );
				}
			}
			else
			{
				EnableMaterial( 0, MeshShader );
				S3D_OpenGLOp::DrawPrimitives( TriangleType, 0, Header.numFaces * 3 );
				DisableMaterial( 0, MeshShader );
			}
			MeshShader->DisableAttributes();

			s3dBuffer->UnbindBuffer( S3D_VBO );
		}
    }
#if ( !S3D_OGLES_2_0 )
    else
    {
#if ( S3D_USE_LIGTHING )
		S3D_OpenGLOp::BindMesh( Body, true, Textured );
#else
		S3D_OpenGLOp::BindMesh( Body, false, Textured );
#endif
		if ( MatIDs )
		{
			for ( int i = 0; i < NumMaterials(); i++ )
			{
				EnableMaterial( i, 0, Header.numTVerts != 0 );
				S3D_OpenGLOp::DrawPrimitives( TriangleType, MatIDs[i].IDs, MatIDs[i].count );
				DisableMaterial( i, 0, Header.numTVerts != 0 );
			}
		}
		else
		{
			EnableMaterial( 0, 0 );
			S3D_OpenGLOp::DrawPrimitives( TriangleType, 0, Header.numFaces * 3 );
			DisableMaterial( 0, 0 );
		}
		S3D_OpenGLOp::UnbindMesh();
    }
#endif
	if ( MeshShader ) MeshShader->EndShaderProgram();
#if ( S3D_USE_LIGTHING )
	else s3dLight->DisableLighting();
#endif
}
