#include <S3D.h>

S3D_Skeleton::S3D_Skeleton()
{
	weights = 0;
	indices = 0;
	bones = 0;
	numWeights = 0;
	numBones = 0;
	weightBuffer = 0;
	indexBuffer = 0;
}

S3D_Skeleton::~S3D_Skeleton()
{
	numWeights = numBones = 0;
	if ( bones ) delete [] bones;
	if ( weights )
    {
		for( int i = 0; i < S3D_BONE_AFFECT_LIMITED; i++ )
			if ( weights[ i ] ) delete [] weights[ i ];
		delete [] weights;
    }
	if ( indices )
    {
		for( int i = 0; i < S3D_BONE_AFFECT_LIMITED; i++ )
			if ( indices[ i ] ) delete [] indices[ i ];
		delete [] indices;
    }
	if ( s3dBuffer )
	{
		s3dBuffer->FreeBuffer( weightBuffer, S3D_VBO );
		s3dBuffer->FreeBuffer( indexBuffer, S3D_VBO );
	}
}

char* strtolower( char s[] )
{
    char* p;
    for (p = s; *p != 0; p++)
        if (*p >= 'A' && *p <= 'Z')
            *p += 'a' - 'A';
    return s;
}

S3D_Bone *S3D_Skeleton::GetBoneByName( const char *boneName, bool substr )
{
	char *name1 = (char*)malloc( ( strlen( boneName ) + 1 ) * sizeof( char ) );
	strcpy( name1, boneName );
	for ( unsigned id = 0; id < numBones; id++ )
	{
		char *name2 = (char*)malloc( ( strlen( bones[id].name ) + 1 ) * sizeof( char ) );
		strcpy( name2, bones[id].name );
		if ( substr )
		{
			if ( strstr( strtolower( name2 ), strtolower( name1 ) ) )
			{
				free( name1 );
				free( name2 );
				return &bones[id];
			}
		}
		else
		{
			if ( !strcmp( strtolower( name2 ), strtolower( name1 ) ) )
			{
				free( name1 );
				free( name2 );
				return &bones[id];
			}
		}
		free( name2 );
	}
	free( name1 );
	return 0;
}

CS3DVec3f S3D_Skeleton::GetTransformedVertex( int vertID, const S3D_Vertex &base )
{
	CS3DVec3f tvec;
	for ( int j = 0; j < S3D_BONE_AFFECT_LIMITED; j++ )
	{
		if ( weights[j][vertID] > S3D_EPS )
		{
			tvec += bones[ indices[j][vertID] ].transfMat * base * weights[j][vertID];
		}
	}
	return tvec;
}

#if ( S3D_USE_LIGTHING )
CS3DVec3f S3D_Skeleton::GetTransformedNormal( int vertID, const S3D_Normal &base )
{
	CS3DVec3f tvec;
	for ( int j = 0; j < S3D_BONE_AFFECT_LIMITED; j++ )
	{
		if ( weights[j][vertID] > S3D_EPS )
		{
			tvec += bones[ indices[j][vertID] ].transfMat.multVecWithoutTranslate( base ) * weights[j][vertID];
		}
	}
	return tvec;
}
#endif

bool S3D_Skeleton::LoadSkeleton( const char *filename )
{
	bool Result = false;
	void *io = s3dIOController->OpenFile( filename );
	if ( io )
	{
		// read header
		numBones = (unsigned)s3dIOController->ReadInt();
		int maxBones = atoi( S3D_MAX_BONES );
		if ( numBones > (unsigned)maxBones )
		{
			char Err[0x100] = { 0 };
			strcpy( Err, "Too many bones in the skeleton: " );
			strcat( Err, filename ); strcat( Err, ". Max value = " S3D_MAX_BONES ".\n" );
			s3dIOController->PrintLog( Err );
		}
		int numVerts = s3dIOController->ReadInt();
		numWeights = numVerts * S3D_BONE_AFFECT_LIMITED;
		// read bone indeces and weights
		indices = new S3D_BoneIndex*[ S3D_BONE_AFFECT_LIMITED ];
		weights = new S3D_Weight*[ S3D_BONE_AFFECT_LIMITED ];
		for( int i = 0; i < S3D_BONE_AFFECT_LIMITED; i++ )
		{
			indices[i] = new S3D_BoneIndex[ numVerts ];
			weights[i] = new S3D_Weight[ numVerts ];
		}
		for( int i = 0; i < numVerts; i++ )
		{
			int count = s3dIOController->ReadInt();
			for( int j = 0; j < S3D_BONE_AFFECT_LIMITED; j++ )
			{
				if ( j < count )
				{
					indices[j][i] = s3dIOController->ReadShort();
					weights[j][i] = s3dIOController->ReadFloat();
				}
				else
				{
					indices[j][i] = 0;
					weights[j][i] = 0;
				}
			}
		}

		// read bones
#ifdef PRINT_BONE_NAMES
		FILE *f = fopen( "boneNames.txt", "wt" );
#endif
		bones = new S3D_Bone[ numBones ];
		for( unsigned b = 0; b < numBones; b++ )
		{
			// set bone index
			bones[b].index = b;
			// read bone name
			int boneNameLen = s3dIOController->ReadShort() + 1;
			bones[b].name = (char *)malloc( boneNameLen * sizeof( char ) );
			s3dIOController->ReadBlock( (unsigned char *)bones[b].name, boneNameLen );
#ifdef PRINT_BONE_NAMES
			if ( f ) fprintf( f, "%i - %s\n", bones[b].index, bones[b].name );
#endif
			// read parent bone
			int parentID = s3dIOController->ReadShort();
			bones[b].parent = ( parentID == -1 ? 0x0 : &bones[parentID] );
			// read joint
			bones[b].joint = s3dIOController->ReadVec3f();
			// read orient
			bones[b].orient = s3dIOController->ReadQuat();
		}
#ifdef PRINT_BONE_NAMES
		if ( f ) fclose( f );
#endif
		//int size = s3dIOController->GetCurPos();
		s3dIOController->CloseFile( io );
		Result = true;
	}
	if ( !Result )
	{
		char Err[0x100] = { 0 };
		strcpy( Err, "Can not load skeleton: " );
		strcat( Err, filename ); strcat( Err, ".\n" );
		s3dIOController->PrintLog( Err );
	}
	return Result;
}