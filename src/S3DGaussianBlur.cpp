#include <S3D.h>

S3D_GaussianBlur *s3dGaussianBlur = 0;

S3D_GaussianBlur::S3D_GaussianBlur()
{
	blurPresent = 0;
	kernelSize = 0;
	shader = 0;
	weights = 0;
}

S3D_GaussianBlur::~S3D_GaussianBlur()
{
	if ( shader ) delete shader;
	if ( weights ) delete [] weights;
}

void S3D_GaussianBlur::SetBlurPresent( unsigned id )
{
	if ( id >= 7 ) blurPresent = 6;
	else blurPresent = id;

	if ( weights ) delete [] weights;

	// Evaluate the gaussian function over this kernel size
	const unsigned kernels[7]  = {   0,    1,    2,    3,    4,    5,    6 };
	const float sigmas[7] = {1.0f, 0.8f, 1.6f, 3.0f, 4.0f, 5.0f, 6.0f };

	kernelSize = kernels[blurPresent];
	float sigma = sigmas[blurPresent];
	weights = new float[kernelSize + 1];
	float total = 0.0f;
	for (unsigned i = 0; i <= kernelSize; ++i)
	{
		weights[i] = expf(-(float)(i*i) / (2.0f * sigma * sigma)) / sigma;
		total += (i == 0 ? 1 : 2) * weights[i];
	}
	// Normalize the distribution function
	for (unsigned i = 0; i <= kernelSize; ++i)
	  weights[i] /= total;
}

bool S3D_GaussianBlur::Create( unsigned defaultPresentId )
{
	shader = new CS3DShader;
	if ( !shader->Load( S3D_VERTEX_SHADER_SPRITE, S3D_FRAGMENT_SHADER_GAUSS_BLUR ) )
	{
		delete shader;
		shader = 0;
	}
	else
	{
		SetBlurPresent( defaultPresentId );
		return true;
	}
	return false;
}

void S3D_GaussianBlur::Draw( unsigned v_fbo, unsigned v_tex, unsigned h_fbo, unsigned h_tex, unsigned size, CS3DMat16f &mvpMat )
{
	unsigned int fb[2] = { v_fbo, h_fbo };
	unsigned int tx[2] = { v_tex, h_tex };
	float s = float( size );
	float invS = 1.f / s;
	float dx[2][2] = { { 0, invS }, { invS, 0 } };
	S3D_Vertex verts[4] = {
		S3D_Vertex( 0, 0, 0 ),
		S3D_Vertex( s, 0, 0 ),
		S3D_Vertex( 0, s, 0 ),
		S3D_Vertex( s, s, 0 )
	};
	S3D_TexCoord tverts[4] = { {0,0},{1,0},{0,1},{1,1} };
	for ( int i = 0; i < 2; i++ )
	{
		s3dBuffer->BindBuffer( fb[i], S3D_FBO );
		shader->BeginShaderProgram();
		shader->EnableAttributes();
		
		char uniformName[0x10];
		char idName[0x10];
		for ( unsigned j = 0; j <= kernelSize; j++ )
		{
			memset( idName, 0, sizeof( char ) * 0x10 );
			s3dIntToStr( idName, j );

			memset( uniformName, 0, sizeof( char ) * 0x10 );
			strcpy( uniformName, "weights[" );
			strcat( uniformName, idName );
			uniformName[ strlen( uniformName ) ] = ']';
			shader->SetUniformf( uniformName, 1, &weights[j] );
		}
		int numSamp = kernelSize + 1;
		shader->SetUniformi( "numSamples", 1, &numSamp );

		S3D_OpenGLOp::EnableTexture( tx[i], 0 );
		shader->SetSampler( "texture", 0 );
		shader->SetUniformMatrixf( "mvpMat", 4, (float*)&mvpMat );
		shader->SetAttribPointerf( 1, 2, tverts, 0 );
		shader->SetAttribPointerf( 0, 3, verts, 0 );
		shader->SetUniformf( "dx", 2, dx[i] );
		S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );
		shader->DisableAttributes();
		shader->EndShaderProgram();
	}
	S3D_OpenGLOp::DisableTexture();
}