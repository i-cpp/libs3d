#include <S3D.h>

CS3DTransformedObject::CS3DTransformedObject()
: CurScene ( 0 )
, NormalMaxLength( 1.0f )
{
	memset( &Movement, 0, sizeof( S3D_Movement ) );
}

CS3DTransformedObject::~CS3DTransformedObject()
{
	EndMovement();
}

void CS3DTransformedObject::SetTransforms( CS3DTransformedObject &other )
{
	rotMat = *other.GetRotation();
	SetPosition( other.GetPosition() );
	SetPivot( other.GetPivot() );
}

void CS3DTransformedObject::ApplyTransforms()
{
	modelMat.identity();
	modelMat.rotate( -90.0f, S3D_AXIS_X ); // set as part of import
	modelMat.translate( Position );
	modelMat = rotMat * modelMat;
	modelMat.translate( -Pivot );
}

void CS3DTransformedObject::DrawAxis( S3D_ENUM axis )
{
	S3D_BoundingBox bbox = GetBBox();
	CS3DVec3f size = bbox.max - bbox.min;
	float len = size.magnitude() * 0.5f;
	float verts[ 18 ] = { 0.0f };
	int offset = ( axis == S3D_AXIS_Z ? S3D_AXIS_Z : 0 );
	verts[3+axis] = len;
	verts[6+axis] = len;
	verts[9+axis] = len * 0.8f;
	verts[11-offset] = len * 0.05f;
	verts[12+axis] = len;
	verts[15+axis] = len * 0.8f;
	verts[17-offset] = -len * 0.05f;
	float colorRGB[4] = { 0, 0, 0, 1 };
	colorRGB[axis] = 1;
	s3dSimpleShader->SetUniformf( "u_color", 4, colorRGB );
	s3dSimpleShader->SetAttribPointerf( 0, 3, verts );
	S3D_OpenGLOp::DrawPrimitives( S3D_LINES, 0, 6 );
}

void CS3DTransformedObject::DrawPivot()
{
	if ( !s3dSimpleShader ) return;

	s3dSimpleShader->BeginShaderProgram();
	s3dSimpleShader->EnableAttribute( 0 );

	CS3DMat16f mvMat = modelMat * s3dCamera->Get();
	mvMat.translate( Pivot );
	CS3DMat16f mvpMat = mvMat * s3dProjection;
	s3dSimpleShader->SetUniformMatrixf( "mvpMat", 4, (float*)&mvpMat );
	DrawAxis( S3D_AXIS_X );
	DrawAxis( S3D_AXIS_Y );
	DrawAxis( S3D_AXIS_Z );

	s3dSimpleShader->DisableAttribute( 0 );
	s3dSimpleShader->EndShaderProgram();
}

CS3DVec3f CS3DTransformedObject::GetPivot()
{
	return Pivot;
}

void CS3DTransformedObject::SetPivot( CS3DVec3f pivot )
{
	Pivot = pivot;
}

void CS3DTransformedObject::SetPosition( CS3DVec3f pos )
{
	Position = pos;
}

void CS3DTransformedObject::SetPosition( S3D_ENUM axis, float value )
{
	Position[ axis ] = value;
}

void CS3DTransformedObject::AddPosition( S3D_ENUM axis, float value )
{
	Position[ axis ] += value;
}

CS3DVec3f CS3DTransformedObject::GetPosition()
{
	return Position;
}

CS3DMat16f *CS3DTransformedObject::GetRotation()
{
	return &rotMat;
}

const CS3DMat16f *CS3DTransformedObject::GetM()
{
	return &modelMat;
}

void CS3DTransformedObject::BindScene( CS3DScene *scene )
{
	CurScene = scene;
}

void CS3DTransformedObject::DrawSceneWalls()
{
	if ( CurScene )
		CurScene->DrawWalls();
}

bool CS3DTransformedObject::IsMoving()
{
	return Movement.Activated;
}

void CS3DTransformedObject::SetNormalMaxLength( float value )
{
	NormalMaxLength = value;
}

void CS3DTransformedObject::StartMovement( CS3DVec3f Where, float mt, float rt, bool NonStop, float delay )
{
	if ( CurScene )
	{
		float pt[2];
		if ( !CurScene->Hover( Where[0], Where[2], pt, NormalMaxLength ) )
		{
			if ( pt[0] == (float)(unsigned)~0
			  && pt[1] == (float)(unsigned)~0 ) return;
			Where[0] = pt[0];
			Where[2] = pt[1];
		}
	}
	if ( Position != Where )
	{
		EndMovement();
		Movement.NonStop = NonStop;
		Movement.Where = new CS3DVec3f();
		Movement.Whence = new CS3DVec3f();
		Movement.Entrepot = new CS3DVec3f();
		Movement.tMovTime = mt;
		Movement.tRotTime = rt;
		Movement.tDelay = delay;
		if ( CurScene )
		{
			float x, y;
			CurScene->SetEnds( Position[0], Position[2], Where[0], Where[2] );
			CurScene->FindPath( x, y );
			*Movement.Entrepot = CS3DVec3f( x, Where[1], y );
		}
		else *Movement.Entrepot = Where;
		*Movement.Where = Where;
		*Movement.Whence = Position;

		CS3DVec3f d1 = *Movement.Entrepot - *Movement.Whence;
		float dist = d1.magnitude();
		Movement.tMovInterval = (unsigned int)(Movement.tMovTime * dist);
		if ( Movement.tMovInterval == 0 )
		{
			Movement.tMovInterval = 1;
		}

		Movement.Activated = true;
		Movement.DstAngle = (float)(atan2( -d1[2], d1[0] ) / S3D_PI * 180.0);
		float X = rotMat.getRow(0)[0]/*cos1*/ + rotMat.getRow(2)[2]/*cos2*/ ;
		float Z = rotMat.getRow(2)[0]/*sin2*/ - rotMat.getRow(0)[2]/*sin1*/ ;
		Movement.CurAngle = atan2f( -X, Z ) / S3D_PI * 180.0f;
		Movement.SrcAngle = Movement.CurAngle;

		if ( S3D_ABS( Movement.DstAngle - Movement.SrcAngle ) >
			 S3D_ABS( Movement.DstAngle - Movement.SrcAngle - 360 )) Movement.SrcAngle += 360;
		if ( S3D_ABS( Movement.DstAngle - Movement.SrcAngle ) > 
			 S3D_ABS( Movement.DstAngle - Movement.SrcAngle + 360 ) ) Movement.SrcAngle -= 360;
		
		float d2 = Movement.DstAngle - Movement.SrcAngle;
		dist = sqrtf( d2 * d2 ) / 360.0f;
		Movement.tRotInterval = (unsigned int)(Movement.tRotTime * dist);
		if ( Movement.tRotInterval == 0 )
			Movement.tRotInterval = 1;
	}
}

void CS3DTransformedObject::EndMovement()
{
	if ( Movement.Where )
	{
		delete Movement.Where;
		Movement.Where = 0;
	}
	if ( Movement.Whence )
	{
		delete Movement.Whence;
		Movement.Whence = 0;
	}
	if ( Movement.Entrepot )
	{
		delete Movement.Entrepot;
		Movement.Entrepot = 0;
	}
	memset( &Movement, 0, sizeof( S3D_Movement ) );
}

void CS3DTransformedObject::Move()
{
	if ( Movement.Activated )
	{
		if ( Movement.tDelay )
		{
			if ( !Movement.tDelayStarted ) Movement.tDelayStarted = s3dCurTime();
			float progress = (float)( s3dCurTime() - Movement.tDelayStarted ) / Movement.tDelay;
			if ( progress < 1.0f )
			{
				return;
			}
			Movement.tDelay = 0;
		}
		if ( Movement.DstAngle != Movement.CurAngle )
		{
			if ( !Movement.tRotStarted ) Movement.tRotStarted = s3dCurTime();
			float progress = (float)( s3dCurTime() - Movement.tRotStarted ) / Movement.tRotInterval;
			if ( progress > 1.0f ) progress = 1.0f;

			float angle = S3D_MIX( Movement.SrcAngle, Movement.DstAngle, progress );
			rotMat.rotate( angle - Movement.CurAngle, S3D_AXIS_Y );
			Movement.CurAngle = angle;
		}
		if ( ( Movement.DstAngle == Movement.CurAngle || Movement.NonStop ) && Movement.tMovInterval )
		{
			if ( !Movement.tMovStarted ) Movement.tMovStarted = s3dCurTime();
			float progress = (float)( s3dCurTime() - Movement.tMovStarted ) / (float)Movement.tMovInterval;
			if ( progress > 1.0f ) progress = 1.0f;
						
			Position = S3D_MIX( (*Movement.Whence), (*Movement.Entrepot), progress );

			//char msg[0x100] = {0};
			//sprintf( msg, "%.05f, [%.05f,%.05f,%.05f]\n", progress, Position.x, Position.y, Position.z );
			//s3dIOController->PrintLog( msg );

			if ( progress == 1.0f )
			{
				if ( *Movement.Where == *Movement.Entrepot )
				{
					EndMovement();
				}
				else
				{
					StartMovement( *Movement.Where, Movement.tMovTime, Movement.tRotTime, true, 0 );
				}
			}
		}
	}
	ApplyTransforms();
}