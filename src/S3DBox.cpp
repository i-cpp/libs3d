#include <S3D.h>

CS3DBox::CS3DBox()
{
	Reset();
	color[3] = 1.0f;
	fillColor = S3D_Color( 1.f, .3f, .3f, .1f );
	fillStyle = false;
}

CS3DBox::~CS3DBox()
{
}

bool CS3DBox::IsInit()
{
	return init;
}

void CS3DBox::Reset()
{
	bbox.min = CS3DVec3f(  S3D_INFINITY,  S3D_INFINITY,  S3D_INFINITY );
	bbox.max = CS3DVec3f( -S3D_INFINITY, -S3D_INFINITY, -S3D_INFINITY );
	init = false;
}

void CS3DBox::Set( S3D_BoundingBox &other )
{
	Set( other.min );
	Set( other.max );
}

void CS3DBox::Set( CS3DVec3f &v )
{
	if ( bbox.min[0] > v[0] ) bbox.min[0] = v[0];
	if ( bbox.min[1] > v[1] ) bbox.min[1] = v[1];
	if ( bbox.min[2] > v[2] ) bbox.min[2] = v[2];

	if ( bbox.max[0] < v[0] ) bbox.max[0] = v[0];
	if ( bbox.max[1] < v[1] ) bbox.max[1] = v[1];
	if ( bbox.max[2] < v[2] ) bbox.max[2] = v[2];

	init = true;
}

bool CS3DBox::CollisionPoint( CS3DVec3f &p )
{
	bool result = ( p[0] <= bbox.max[0] && p[0] >= bbox.min[0] &&
				    p[1] <= bbox.max[1] && p[1] >= bbox.min[1] &&
				    p[2] <= bbox.max[2] && p[2] >= bbox.min[2] );
	return result;
}

bool CS3DBox::CollisionBBox( S3D_BoundingBox &other_bbox )
{
    if ( bbox.max[0] < other_bbox.min[0] || bbox.min[0] > other_bbox.max[0] ) return false;
    if ( bbox.max[1] < other_bbox.min[1] || bbox.min[1] > other_bbox.max[1] ) return false;
    if ( bbox.max[2] < other_bbox.min[2] || bbox.min[2] > other_bbox.max[2] ) return false;
    return true;
}

bool CS3DBox::CollisionRay( S3D_Ray &ray )
{
	CS3DVec3f tmin = ( bbox.min - ray.origin ) / ray.direct;
	CS3DVec3f tmax = ( bbox.max - ray.origin ) / ray.direct;
	//tmin.normalize();
	//tmax.normalize();
	CS3DVec3f real_min = tmin.minimize( tmax );
	CS3DVec3f real_max = tmin.maximize( tmax );
	float minmax = min( real_max[1],min( real_max[0], real_max[2] ) );
	float maxmin = max( real_min[1],max( real_min[0], real_min[2] ) );
	return minmax >= maxmin;
}

S3D_BoundingBox CS3DBox::GetBBoxTransformed()
{
	S3D_BoundingBox out;
	out.max = mvpMat * bbox.max;
	out.min = mvpMat * bbox.min;
	return out;
}

void CS3DBox::SetSubTransforms( CS3DMat16f mat )
{
	subTransfMat = mat;
}

#if ( S3D_USE_LIGTHING )
#if ( S3D_USE_SHADOWS )
void CS3DBox::DrawShadow( const CS3DMat16f &lmvMat, const CS3DMat16f &lpMat )
{
	if ( !fillStyle ) return;
	ApplyTransforms();
	CS3DMat16f mvMat = modelMat * lmvMat;

	CS3DShader *shader = s3dShadow->depthTextureShader;
	CS3DMat16f mvpMat = mvMat * lpMat;
	shader->SetUniformMatrixf( "mvpMat", 4, (float*)&mvpMat );
	int Animated = 0;
	shader->SetUniformi( "Animated", 1, &Animated );
	shader->EnableAttribute( 0 );

	CS3DVec3f verts[4];

	verts[0] = CS3DVec3f( bbox.min[0], bbox.min[1], bbox.min[2] );
	verts[1] = CS3DVec3f( bbox.max[0], bbox.min[1], bbox.min[2] );
	verts[2] = CS3DVec3f( bbox.min[0], bbox.max[1], bbox.min[2] );
	verts[3] = CS3DVec3f( bbox.max[0], bbox.max[1], bbox.min[2] );
	shader->SetAttribPointerf( 0, 3, (float*)&verts );
	S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );

	verts[0] = CS3DVec3f( bbox.min[0], bbox.min[1], bbox.max[2] );
	verts[1] = CS3DVec3f( bbox.max[0], bbox.min[1], bbox.max[2] );
	verts[2] = CS3DVec3f( bbox.min[0], bbox.max[1], bbox.max[2] );
	verts[3] = CS3DVec3f( bbox.max[0], bbox.max[1], bbox.max[2] );
	shader->SetAttribPointerf( 0, 3, (float*)&verts );
	S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );

	verts[0] = CS3DVec3f( bbox.min[0], bbox.max[1], bbox.min[2] );
	verts[1] = CS3DVec3f( bbox.max[0], bbox.max[1], bbox.min[2] );
	verts[2] = CS3DVec3f( bbox.min[0], bbox.max[1], bbox.max[2] );
	verts[3] = CS3DVec3f( bbox.max[0], bbox.max[1], bbox.max[2] );
	shader->SetAttribPointerf( 0, 3, (float*)&verts );
	S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );

	verts[0] = CS3DVec3f( bbox.min[0], bbox.min[1], bbox.min[2] );
	verts[1] = CS3DVec3f( bbox.max[0], bbox.min[1], bbox.min[2] );
	verts[2] = CS3DVec3f( bbox.min[0], bbox.min[1], bbox.max[2] );
	verts[3] = CS3DVec3f( bbox.max[0], bbox.min[1], bbox.max[2] );
	shader->SetAttribPointerf( 0, 3, (float*)&verts );
	S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );

	verts[0] = CS3DVec3f( bbox.max[0], bbox.min[1], bbox.min[2] );
	verts[1] = CS3DVec3f( bbox.max[0], bbox.max[1], bbox.min[2] );
	verts[2] = CS3DVec3f( bbox.max[0], bbox.min[1], bbox.max[2] );
	verts[3] = CS3DVec3f( bbox.max[0], bbox.max[1], bbox.max[2] );
	shader->SetAttribPointerf( 0, 3, (float*)&verts );
	S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );

	verts[0] = CS3DVec3f( bbox.min[0], bbox.min[1], bbox.min[2] );
	verts[1] = CS3DVec3f( bbox.min[0], bbox.max[1], bbox.min[2] );
	verts[2] = CS3DVec3f( bbox.min[0], bbox.min[1], bbox.max[2] );
	verts[3] = CS3DVec3f( bbox.min[0], bbox.max[1], bbox.max[2] );
	shader->SetAttribPointerf( 0, 3, (float*)&verts );
	S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );

	shader->DisableAttribute( 0 );
}
#endif
#endif

void CS3DBox::Draw()
{
	if ( !s3dSimpleShader ) return;
	
	const CS3DMat16f &camMat = s3dCamera->Get();
	bool lookUpdateTransforms = false;
#if ( S3D_USE_SHADOWS )
	lookUpdateTransforms = s3dShadow && s3dShadow->enabled;
#endif
	if ( !lookUpdateTransforms || !fillStyle )
	{
		ApplyTransforms();
	}
	modelMat = subTransfMat * modelMat;
	CS3DMat16f mvMat = modelMat * camMat;
	mvpMat = mvMat * s3dProjection;

	if ( fillStyle && s3dColoredMeshShader )
	{
		s3dColoredMeshShader->BeginShaderProgram();
		s3dColoredMeshShader->EnableAttributes();

		S3D_Material lightProduct;
		memset( &lightProduct, 0, sizeof( S3D_Material ) );
		lightProduct.ambient = s3dLight->GetLight( 0 )->ambient * fillColor;
		lightProduct.diffuse = s3dLight->GetLight( 0 )->diffuse * fillColor;
		s3dColoredMeshShader->SetUniformf( "lightProductAmbient", 4, (float*)&lightProduct.ambient );
		s3dColoredMeshShader->SetUniformf( "lightProductDiffuse", 4, (float*)&lightProduct.diffuse );

		s3dColoredMeshShader->SetUniformMatrixf( "mvpMat", 4, (float*)&mvpMat );
#if ( S3D_USE_LIGTHING )
		CS3DVec3f lightPosition = camMat * s3dLight->GetLight( 0 )->pos;
		s3dColoredMeshShader->SetUniformf( "mvLightPos", 3, (float*)&lightPosition );
#if ( S3D_USE_SHADOWS )
		s3dColoredMeshShader->SetUniformMatrixf( "lMat", 4, (float*)&s3dLight->GetLight( 0 )->mat );
#endif
#endif
#if ( S3D_USE_LIGTHING )
		s3dColoredMeshShader->SetUniformMatrixf( "mvMat", 4, (float*)&mvMat );
		float nMat[9];
		mvMat.getNormalMat( nMat );
		s3dColoredMeshShader->SetUniformMatrixf( "nMat", 3, nMat );
#endif
		
#if ( S3D_USE_LIGTHING )
#if ( S3D_USE_SHADOWS )
		int use = 0;
		if ( s3dShadow && s3dShadow->enabled )
		{
#if ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
			S3D_OpenGLOp::EnableTexture( s3dShadow->soft ? s3dShadow->colorTexture : s3dShadow->depthTexture, 0 );
#else
			S3D_OpenGLOp::EnableTexture( s3dShadow->depthTexture, 0 );
#endif
			use = 1;
		}
		s3dColoredMeshShader->SetUniformi( "useShadow", 1, &use );
		s3dColoredMeshShader->SetSampler( "shadowMap", 0 );
#endif
#endif

		CS3DVec3f verts[4];
		CS3DVec3f norms[4];

		verts[0] = CS3DVec3f( bbox.min[0], bbox.min[1], bbox.min[2] );
		verts[1] = CS3DVec3f( bbox.max[0], bbox.min[1], bbox.min[2] );
		verts[2] = CS3DVec3f( bbox.min[0], bbox.max[1], bbox.min[2] );
		verts[3] = CS3DVec3f( bbox.max[0], bbox.max[1], bbox.min[2] );
		norms[0] = CS3DVec3f(0, 0, -1);
		norms[3] = norms[2] = norms[1] = norms[0];
		s3dColoredMeshShader->SetAttribPointerf( 1, 3, (float*)&norms );
		s3dColoredMeshShader->SetAttribPointerf( 0, 3, (float*)&verts );
		S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );

		verts[0] = CS3DVec3f( bbox.min[0], bbox.min[1], bbox.max[2] );
		verts[1] = CS3DVec3f( bbox.max[0], bbox.min[1], bbox.max[2] );
		verts[2] = CS3DVec3f( bbox.min[0], bbox.max[1], bbox.max[2] );
		verts[3] = CS3DVec3f( bbox.max[0], bbox.max[1], bbox.max[2] );
		norms[0] = CS3DVec3f(0, 0, 1);
		norms[3] = norms[2] = norms[1] = norms[0];
		s3dColoredMeshShader->SetAttribPointerf( 1, 3, (float*)&norms );
		s3dColoredMeshShader->SetAttribPointerf( 0, 3, (float*)&verts );
		S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );

		verts[0] = CS3DVec3f( bbox.min[0], bbox.max[1], bbox.min[2] );
		verts[1] = CS3DVec3f( bbox.max[0], bbox.max[1], bbox.min[2] );
		verts[2] = CS3DVec3f( bbox.min[0], bbox.max[1], bbox.max[2] );
		verts[3] = CS3DVec3f( bbox.max[0], bbox.max[1], bbox.max[2] );
		norms[0] = CS3DVec3f(0, 1, 0);
		norms[3] = norms[2] = norms[1] = norms[0];
		s3dColoredMeshShader->SetAttribPointerf( 1, 3, (float*)&norms );
		s3dColoredMeshShader->SetAttribPointerf( 0, 3, (float*)&verts );
		S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );

		verts[0] = CS3DVec3f( bbox.min[0], bbox.min[1], bbox.min[2] );
		verts[1] = CS3DVec3f( bbox.max[0], bbox.min[1], bbox.min[2] );
		verts[2] = CS3DVec3f( bbox.min[0], bbox.min[1], bbox.max[2] );
		verts[3] = CS3DVec3f( bbox.max[0], bbox.min[1], bbox.max[2] );
		norms[0] = CS3DVec3f(0, -1, 0);
		norms[3] = norms[2] = norms[1] = norms[0];
		s3dColoredMeshShader->SetAttribPointerf( 1, 3, (float*)&norms );
		s3dColoredMeshShader->SetAttribPointerf( 0, 3, (float*)&verts );
		S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );

		verts[0] = CS3DVec3f( bbox.max[0], bbox.min[1], bbox.min[2] );
		verts[1] = CS3DVec3f( bbox.max[0], bbox.max[1], bbox.min[2] );
		verts[2] = CS3DVec3f( bbox.max[0], bbox.min[1], bbox.max[2] );
		verts[3] = CS3DVec3f( bbox.max[0], bbox.max[1], bbox.max[2] );
		norms[0] = CS3DVec3f(1, 0, 0);
		norms[3] = norms[2] = norms[1] = norms[0];
		s3dColoredMeshShader->SetAttribPointerf( 1, 3, (float*)&norms );
		s3dColoredMeshShader->SetAttribPointerf( 0, 3, (float*)&verts );
		S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );

		verts[0] = CS3DVec3f( bbox.min[0], bbox.min[1], bbox.min[2] );
		verts[1] = CS3DVec3f( bbox.min[0], bbox.max[1], bbox.min[2] );
		verts[2] = CS3DVec3f( bbox.min[0], bbox.min[1], bbox.max[2] );
		verts[3] = CS3DVec3f( bbox.min[0], bbox.max[1], bbox.max[2] );
		norms[0] = CS3DVec3f(-1, 0, 0);
		norms[3] = norms[2] = norms[1] = norms[0];
		s3dColoredMeshShader->SetAttribPointerf( 1, 3, (float*)&norms );
		s3dColoredMeshShader->SetAttribPointerf( 0, 3, (float*)&verts );
		S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );

		s3dColoredMeshShader->DisableAttributes();
		s3dColoredMeshShader->EndShaderProgram();
#if ( S3D_USE_SHADOWS )
		if ( s3dShadow )
		{
			S3D_OpenGLOp::DisableTexture( 0 );
		}
#endif
	}
	else
	{
		s3dSimpleShader->BeginShaderProgram();
		s3dSimpleShader->SetUniformf( "u_color", 4, (float*)&color );
		s3dSimpleShader->SetUniformMatrixf( "mvpMat", 4, (float*)&mvpMat );
		s3dSimpleShader->EnableAttribute( 0 );
		float verts0[] = {
			bbox.max[0], bbox.max[1], bbox.min[2],
			bbox.min[0], bbox.max[1], bbox.min[2],
			bbox.min[0], bbox.min[1], bbox.min[2],
			bbox.max[0], bbox.min[1], bbox.min[2],
		};
		s3dSimpleShader->SetAttribPointerf( 0, 3, verts0 );
		S3D_OpenGLOp::DrawPrimitives( S3D_LINE_LOOP, 0, 4 );
		float verts1[] = {
			bbox.max[0], bbox.min[1], bbox.max[2],
			bbox.max[0], bbox.max[1], bbox.max[2],
			bbox.min[0], bbox.max[1], bbox.max[2],
			bbox.min[0], bbox.min[1], bbox.max[2],
		};
		s3dSimpleShader->SetAttribPointerf( 0, 3, verts1 );
		S3D_OpenGLOp::DrawPrimitives( S3D_LINE_LOOP, 0, 4 );
		float verts2[] = {
			bbox.max[0], bbox.max[1], bbox.min[2],
			bbox.max[0], bbox.max[1], bbox.max[2],
			bbox.min[0], bbox.max[1], bbox.max[2],
			bbox.min[0], bbox.max[1], bbox.min[2],
		};
		s3dSimpleShader->SetAttribPointerf( 0, 3, verts2 );
		S3D_OpenGLOp::DrawPrimitives( S3D_LINE_LOOP, 0, 4 );
		float verts3[] = {
			bbox.max[0], bbox.min[1], bbox.max[2],
			bbox.min[0], bbox.min[1], bbox.max[2],
			bbox.min[0], bbox.min[1], bbox.min[2],
			bbox.max[0], bbox.min[1], bbox.min[2],
		};
		s3dSimpleShader->SetAttribPointerf( 0, 3, verts3 );
		S3D_OpenGLOp::DrawPrimitives( S3D_LINE_LOOP, 0, 4 );
		s3dSimpleShader->DisableAttribute( 0 );
		s3dSimpleShader->EndShaderProgram();
	}
}

void S3D_BBList::Create( int Level, S3D_Body *body, S3D_Index *faces, int numFaces, bool dynamic )
{
	if ( !body ) return;
	// calc parent bounding box:
	int id;
	if ( faces )
	{
		for ( int face = 0; face < numFaces; ++face )
		{
			id = face * 3;
			parent.Set( body[faces[id]    ].vertex );
			parent.Set( body[faces[id + 1]].vertex );
			parent.Set( body[faces[id + 2]].vertex );
		}
	}
	else
	{
		for ( int face = 0; face < numFaces; ++face )
		{
			id = face * 3;
			parent.Set( body[id    ].vertex );
			parent.Set( body[id + 1].vertex );
			parent.Set( body[id + 2].vertex );
		}
	}

	numNodes = dynamic ? Level : Level * Level * Level;
	nodes = new S3D_BBNode*[ numNodes ];
	memset( nodes, 0, sizeof( S3D_BBNode* ) * numNodes );

	if ( dynamic ) return;

	// calc children:
	S3D_BoundingBox parent_bb = parent.GetBBox();
	CS3DVec3f size = parent.Size();
	CS3DVec3f cell_size = size / (float)Level;
	CS3DVec3f inv_cell_size( 
		1.f / cell_size[0], 
		1.f / cell_size[1], 
		1.f / cell_size[2] 
	);
	int OverlapBoxes[3][2];
	S3D_BoundingBox TriBbox;
	S3D_ENUM axis;
	int x, y, z, yz, xyz, *cur;
	for ( int face = 0; face < numFaces; ++face )
	{
		CS3DBox tri_bb;
		if ( faces )
		{
			tri_bb.Set( body[faces[face * 3    ]].vertex );
			tri_bb.Set( body[faces[face * 3 + 1]].vertex );
			tri_bb.Set( body[faces[face * 3 + 2]].vertex );
		}
		else
		{
			tri_bb.Set( body[face * 3    ].vertex );
			tri_bb.Set( body[face * 3 + 1].vertex );
			tri_bb.Set( body[face * 3 + 2].vertex );
		}
		TriBbox = tri_bb.GetBBox();
		for ( axis = S3D_AXIS_X; axis <= S3D_AXIS_Z; ++axis )
		{
			cur = OverlapBoxes[axis];
			cur[0] = (int)((TriBbox.min[axis] - parent_bb.min[axis]) * inv_cell_size[axis]);
			cur[0] = max( min( cur[0], Level - 1 ), 0 );

			cur[1] = (int)((TriBbox.max[axis] - parent_bb.min[axis]) * inv_cell_size[axis]);
			cur[1] = max( min( cur[1], Level - 1 ), 0 );
		}
		for ( z = OverlapBoxes[S3D_AXIS_Z][0]; z <= OverlapBoxes[S3D_AXIS_Z][1]; ++z )
		{
			for ( y = OverlapBoxes[S3D_AXIS_Y][0]; y <= OverlapBoxes[S3D_AXIS_Y][1]; ++y )
			{
				yz = y + z * Level;
				for ( x = OverlapBoxes[S3D_AXIS_X][0]; x <= OverlapBoxes[S3D_AXIS_X][1]; ++x )
				{
					xyz = x + yz * Level;
					if ( nodes[xyz] == 0x0 )
					{
						nodes[xyz] = new S3D_BBNode;
						nodes[xyz]->IDs = new S3D_Indices;
						CS3DVec3f vxyz( (float)x, (float)y, (float)z );
						CS3DVec3f v;
						v = parent_bb.min + cell_size * vxyz;
						nodes[xyz]->bbox.Set( v );
						v = parent_bb.min + cell_size * vxyz + cell_size;
						nodes[xyz]->bbox.Set( v );
					}
					nodes[xyz]->IDs->push_back( face );
				}
			}
		}
	}
}

void S3D_BBList::Delete()
{
	if ( nodes )
	{
		for ( unsigned i = 0; i < numNodes; ++i )
		{
			if ( nodes[i] )
			{
				if ( nodes[i]->IDs ) delete nodes[i]->IDs;
				delete nodes[i];
			}
		}
		delete [] nodes;
	}
}
