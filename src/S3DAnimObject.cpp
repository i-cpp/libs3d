#include <S3D.h>

unsigned int (*s3dCurTime)() = 0;
#define S3D_USED_LERP_TO 10

void s3dSetTimeFunc( unsigned int (*func)() )
{
	s3dCurTime = func;
}

void S3D_TransformOp::to_matrix( CS3DMat16f &m, const S3D_Transform &t )
{
	S3D_QuatOp::to_matrix( (float*)&m, t.rotation );
	memcpy( &m[12], &t.position, sizeof( float ) * 3 );
}

void S3D_TransformOp::from_matrix( S3D_Transform &t, const CS3DMat16f &m )
{
	t.rotation = S3D_QuatOp::from_matrix( (const float*)&m );
	memcpy( &t.position, (const float *)&m + 12, sizeof( float ) * 3 );
}

CS3DAnimObject::CS3DAnimObject()
: tBlendStarted ( 0 )
, Skeleton( 0 )
, Transformed( 0 )
, tBlendTime( 500 )
{
	memset( &CurFrame, 0, sizeof( S3D_BlendedObject ) );
	memset( &NextAnimFrame, 0, sizeof( S3D_BlendedObject ) );
	memset( &CurAnimation, 0, sizeof( S3D_BlendedObject ) );
}

CS3DAnimObject::~CS3DAnimObject()
{
	if ( Transformed ) delete [] Transformed;
	for ( unsigned int animID = 0; animID < Animations.size(); animID++ )
	{
		S3D_Animation *anim = Animations[ animID ];
		if ( anim->Shader ) delete anim->Shader;
		if ( anim->Type == SkinAnimation && Skeleton )
		{
			if ( anim->Transforms )
			{
				for ( unsigned b = 0; b < Skeleton->numBones; b++ )
					delete [] anim->Transforms[b];
				delete [] anim->Transforms;
			}
			if ( anim->BaseTransforms )
			{
				for ( unsigned b = 0; b < Skeleton->numBones; b++ )
					delete [] anim->BaseTransforms[b];
				delete [] anim->BaseTransforms;
			}
		}
		delete anim;
	}
	if ( Skeleton ) delete Skeleton;
}

void CS3DAnimObject::BoneTransform( unsigned int animID, S3D_Bone *bone, S3D_Transform *transform, int frame )
{
	if ( !Skeleton ) return;
	S3D_Animation *anim = Animations[ animID ];
	S3D_Quat *q1 = &anim->Transforms[bone->index][frame].rotation;
	S3D_Quat *q2 = &transform->rotation;
	CS3DVec3f *p1 = &anim->Transforms[bone->index][frame].position;
	CS3DVec3f *p2 = &transform->position;

	anim->Transforms[bone->index][frame].rotation = S3D_QuatOp::mult( *q2, *q1 );
	anim->Transforms[bone->index][frame].position = *p2 + S3D_QuatOp::rotate( *p1, *q2 );

	// find children
	for ( unsigned i = 0; i < Skeleton->numBones; i++ )
	{
		if ( Skeleton->bones[i].parent == bone )
		{
			BoneTransform( animID, &Skeleton->bones[i], transform, frame );
		}
	}
}

void CS3DAnimObject::AddTransformToBoneByName( const char *boneName, CS3DMat16f &transforms, bool substr )
{
	if ( !Skeleton || Animations.empty() ) return;
	S3D_Bone *bone = Skeleton->GetBoneByName( boneName, substr );
	if ( bone )
	{
		AddTransformToBoneByID( bone->index, transforms );
	}
}

void CS3DAnimObject::ClearBoneTransforms()
{
	auxBoneTransforms.clear();
}

bool CS3DAnimObject::GetBoneTransformByID( S3D_BoneIndex boneID, CS3DMat16f &transforms )
{
	unsigned count = auxBoneTransforms.size();
	for ( unsigned i = 0; i < count; i++ )
	{
		if ( auxBoneTransforms[i].boneID == boneID )
		{
			transforms = auxBoneTransforms[i].mat;
			return true;
		}
	}
	return false;
}

void CS3DAnimObject::AddTransformToBoneByID( S3D_BoneIndex boneID, CS3DMat16f &transforms )
{
	S3D_AuxBoneTransform auxBoneTransform = { boneID, transforms };
	auxBoneTransforms.push_back( auxBoneTransform );
}

void CS3DAnimObject::ApplyTransformToBoneByID( unsigned int animID, 
	S3D_BlendedObject &blendedObject, S3D_BoneIndex boneID, CS3DMat16f &transforms )
{
	S3D_Transform transform;
	CS3DMat16f m;
	CS3DVec3f pivot;
	int frames[2] = { blendedObject.curID, blendedObject.nextID };
	for ( int i = 0; i < 2; i++ )
	{
		int frame = frames[i];
		m.identity();
		S3D_Transform *t = &Animations[ animID ]->Transforms[boneID][frame];
		pivot = t->position + S3D_QuatOp::rotate( Skeleton->bones[boneID].joint, t->rotation );
		m.translate( pivot );
		m = transforms * m;
		m.translate( -pivot );
		transform.position = CS3DVec3f( m[12], m[13], m[14] );
		transform.rotation = S3D_QuatOp::from_matrix( (float*)&m );
		transform.rotation.normalize();
		BoneTransform( animID, &Skeleton->bones[boneID], &transform, frame );
	}
}

void CS3DAnimObject::GetCurrentTransform( S3D_BoneIndex boneID, S3D_Transform &transform )
{
	S3D_Animation *anim = Animations[ CurAnimation.curID ];
	if ( anim->frameRate >= S3D_USED_LERP_TO || tBlendStarted )
	{
		transform.rotation = anim->Transforms[boneID][CurFrame.curID].rotation;
		transform.position = anim->Transforms[boneID][CurFrame.curID].position;
	}
	else
	{
		transform.rotation = S3D_QuatOp::slerp( anim->Transforms[boneID][CurFrame.curID ].rotation, 
												anim->Transforms[boneID][CurFrame.nextID].rotation, CurFrame.time );
		transform.position = S3D_MIX( anim->Transforms[boneID][CurFrame.curID ].position, 
									  anim->Transforms[boneID][CurFrame.nextID].position, CurFrame.time );
	}
}

void CS3DAnimObject::DrawSkeleton( CS3DMat16f &modelViewMat, bool Absolute, CS3DBox &box )
{
	if ( s3dSimpleShader && Skeleton )
	{
		s3dSimpleShader->BeginShaderProgram();
		s3dSimpleShader->EnableAttribute( 0 );

		CS3DMat16f mvpMat = modelViewMat * s3dProjection;
		s3dSimpleShader->SetUniformMatrixf( "mvpMat", 4, (float*)&mvpMat );
		
		float colorRGB[4];
		S3D_Transform t;
		
		const float j_sz = 0.0025f * box.Size().magnitude();
		//nCurFrame = 0;
		for ( unsigned i = 0; i < Skeleton->numBones; i++ )
		{
			S3D_Bone *b = &Skeleton->bones[i];
			if ( b->parent )
			{
				CS3DVec3f v1 = b->joint;
				CS3DVec3f v2 = b->parent->joint;
				if ( !Absolute )
				{
					GetCurrentTransform( b->index, t );
					v1 = t.position + S3D_QuatOp::rotate( v1, t.rotation );
					GetCurrentTransform( b->parent->index, t );
					v2 = t.position + S3D_QuatOp::rotate( v2, t.rotation );
				}
				
				float verts[] = { v1.x, v1.y, v1.z, v2.x, v2.y, v2.z };
				s3dSimpleShader->SetAttribPointerf( 0, 3, verts );
				
				colorRGB[0] = 1; colorRGB[1] = 1; colorRGB[2] = 1; colorRGB[3] = 1;
				s3dSimpleShader->SetUniformf( "u_color", 4, colorRGB );

				// draw bone
				S3D_OpenGLOp::DrawPrimitives( S3D_LINES, 0, 2 );

				colorRGB[0] = 1; colorRGB[1] = 1; colorRGB[2] = 0; colorRGB[3] = 1;
				
				//if ( !strcmp( b->name, "Head_L_Mouth" ) )
				//	colorRGB[1] = 1;
				s3dSimpleShader->SetUniformf( "u_color", 4, colorRGB );

				float point_verts[] = { 
					v1.x - j_sz, v1.y, v1.z - j_sz, 
					v1.x + j_sz, v1.y, v1.z - j_sz, 
					v1.x - j_sz, v1.y, v1.z + j_sz, 
					v1.x + j_sz, v1.y, v1.z + j_sz, 
				};
				s3dSimpleShader->SetAttribPointerf( 0, 3, point_verts );
				// draw joint
				S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );
			}
		}
		s3dSimpleShader->DisableAttribute( 0 );
		s3dSimpleShader->EndShaderProgram();
	}
}

bool CS3DAnimObject::LoadAnimation( const char *filename, S3D_Header &Header, unsigned int mixedWithAnimID )
{
	bool Result = false;
	void *io = s3dIOController->OpenFile( filename );
	if ( io )
	{
		S3D_Animation *anim = new S3D_Animation();
		// read header
		char Type = (char)s3dIOController->ReadByte();
		anim->Type = (S3D_AnimType)Type;
		unsigned numFrames = (unsigned)s3dIOController->ReadInt();
		anim->frameRate = (unsigned)s3dIOController->ReadInt();

		bool mixed = false;
		if ( mixedWithAnimID < Animations.size() )
		{
			mixed = true;
			anim->numFrames = numFrames;
			//anim->numFrames = max( Animations[mixedWithAnimID]->numFrames, numFrames );
		}
		else
		{
			anim->numFrames = numFrames;
		}
		// fill playlist
		//anim->PlayList.push_back( 0 );
		for ( unsigned i = 0; i < anim->numFrames; i++ )
			anim->PlayList.push_back( i );

		if ( Type == SkinAnimation && Skeleton )
		{
			// read transforms
			if ( !Transformed ) Transformed = new S3D_Body[ Header.numFaces * 3 ];

			anim->Transforms = new S3D_Transform*[ Skeleton->numBones ];
			anim->BaseTransforms = new S3D_Transform*[ Skeleton->numBones ];
			for ( unsigned b = 0; b < Skeleton->numBones; b++ )
			{
				anim->Transforms[b] = new S3D_Transform[ anim->numFrames ];
				anim->BaseTransforms[b] = new S3D_Transform[ anim->numFrames ];
				for ( unsigned i = 0; i < numFrames; i++ )
				{
					s3dIOController->ReadBlock( (unsigned char *)&anim->Transforms[b][i], sizeof( S3D_Transform ) );
					anim->BaseTransforms[b][i] = anim->Transforms[b][i];
				}
			}
			Result = true;
		}
		//int AnimSize = s3dIOController->GetCurPos();
		if ( Result )
		{
			if ( mixed )
			{
				MixWith( anim, Animations[mixedWithAnimID] );
			}
			Animations.push_back( anim );
		}
		s3dIOController->CloseFile( io );
	}
	if ( !Result )
	{
		char Err[0x100] = { 0 };
		strcpy( Err, "Can not load animation: " );
		strcat( Err, filename ); strcat( Err, ".\n" );
		s3dIOController->PrintLog( Err );
	}
	return Result;
}

void CS3DAnimObject::MixWith( S3D_Animation *dst, S3D_Animation *src )
{
	S3D_Transform t1, t2/*, t3*/;
	if ( src->numFrames == dst->numFrames )
	{
		for ( unsigned i = 0; i < Skeleton->numBones; i++ )
		{
			for ( unsigned j = 0; j < dst->numFrames; j++ )
			{
				t1 = src->BaseTransforms[i][j];
				t2 = dst->BaseTransforms[i][j];
				
				dst->Transforms[i][j].rotation = S3D_QuatOp::mult( t1.rotation, t2.rotation );
				dst->Transforms[i][j].position = t1.position + S3D_QuatOp::rotate( t2.position, t1.rotation );
				dst->BaseTransforms[i][j] = dst->Transforms[i][j];
				/*
				t3.rotation = S3D_Quat(0,0,0,1);
				t3.position = S3D_Vertex(0,0,0);
				src->Transforms[i][j] = t3;
				src->BaseTransforms[i][j] = src->Transforms[i][j];
				//*/
			}
		}
	}
}

void CS3DAnimObject::ChangeFrameRate( unsigned int animID, int frameRate )
{
	if ( animID >= Animations.size() ) return;
	Animations[animID]->frameRate = frameRate;
}

void s3dIntToStr( char *out, int number )
{
	int i = 0, sign;
    if ( number < 0 ) number = -number;
    do out[ i++ ] = number % 10 + '0';
    while ( ( number /= 10 ) > 0 );
	sign = number;
    if ( sign < 0 ) out[ i++ ] = '-';
    out[i] = '\0';
	// reverse string
	int len = (int)strlen( out ), j;
    for ( i = 0, j = len - 1; i < j; i++, j-- )
    {
        char c = out[i];
        out[i] = out[j];
        out[j] = c;
    }
}


void CS3DAnimObject::SetTransformsToSkinShader( CS3DShader *shader )
{
	if ( !shader || !Skeleton ) return;
	char uniformName[0x10];
	char boneIdName[0x10];
	S3D_Transform t;
	for ( unsigned i = 0; i < Skeleton->numBones; i++ )
	{
		GetCurrentTransform( i, t );
		memset( boneIdName, 0, sizeof( char ) * 0x10 );
		s3dIntToStr( boneIdName, i );

		memset( uniformName, 0, sizeof( char ) * 0x10 );
		strcpy( uniformName, "position[" );
		strcat( uniformName, boneIdName );
		uniformName[ strlen( uniformName ) ] = ']';
		shader->SetUniformf( uniformName, 3, (float*)&t.position );

		memset( uniformName, 0, sizeof( char ) * 0x10 );
		strcpy( uniformName, "rotation[" );
		strcat( uniformName, boneIdName );
		uniformName[ strlen( uniformName ) ] = ']';
		shader->SetUniformf( uniformName, 4, (float*)&t.rotation );
	}
}

void CS3DAnimObject::CalcTransformMatrices()
{
	S3D_Transform t;
	for ( unsigned i = 0; i < Skeleton->numBones; i++ )
	{
		GetCurrentTransform( i, t );
		S3D_TransformOp::to_matrix( Skeleton->bones[i].transfMat, t );
		//S3D_QuatOp::to_matrix( (float*)&Skeleton->bones[i].transfMat, t.rotation );
		//memcpy( &Skeleton->bones[i].transfMat[12], (float*)&t.position, sizeof( float ) * 3 );
	}
}

unsigned int CS3DAnimObject::AnimationCount()
{
	return Animations.size();
}

bool CS3DAnimObject::SetAnimation( unsigned int id, bool smooth )
{
	if ( id >= Animations.size() ) return false;
	if ( !smooth )
		CurAnimation.curID = id;
	CurAnimation.nextID = id;
	return true;
}

void CS3DAnimObject::BuildPlaylist( S3D_PlayList &new_list )
{
	if ( Animations.empty() ) return;
	S3D_Animation *anim = Animations[CurAnimation.curID];
	anim->PlayList = new_list;
	for ( unsigned int i = 0; i < anim->PlayList.size(); i++ )
	{
		if ( anim->PlayList[i] >= anim->numFrames )
			anim->PlayList[i] = anim->numFrames - 1;
		anim->PlayList[i] = anim->PlayList[i]; 
	}
}

void CS3DAnimObject::CalcFrameID( unsigned int animID, S3D_BlendedObject &frame )
{
	S3D_Animation *anim = Animations[animID];
	float NumFrames = (float)anim->PlayList.size();
	if ( !frame.tStarted ) frame.tStarted = s3dCurTime();
	float TimeProgress = ( (float)( s3dCurTime() - frame.tStarted ) / 1000.0f );
	TimeProgress *= ( anim->frameRate / NumFrames );
	if ( TimeProgress >= 1.0f )
	{
		frame.tStarted = 0;
		TimeProgress = 0.f;
	}

	int id = (int)( TimeProgress * NumFrames );
	int next_id = id + 1;
	if ( next_id >= NumFrames ) next_id = 0;

	frame.curID = anim->PlayList[ id ];
	frame.time = ( TimeProgress - (float)frame.curID / NumFrames ) * NumFrames;
	frame.nextID = anim->PlayList[ next_id ];
}

void CS3DAnimObject::ApplyAuxTransforms()
{
	for ( unsigned int i = 0; i < auxBoneTransforms.size(); i++ )
	{
		ApplyTransformToBoneByID( CurAnimation.curID, CurFrame, 
			auxBoneTransforms[i].boneID, auxBoneTransforms[i].mat );
	}
}

void CS3DAnimObject::ResetTransforms()
{
	if ( !Skeleton ) return;
	int size = Animations[CurAnimation.curID]->numFrames * sizeof( S3D_Transform );
	for ( unsigned i = 0; i < Skeleton->numBones; i++ )
		memcpy( Animations[CurAnimation.curID]->Transforms[i], Animations[CurAnimation.curID]->BaseTransforms[i], size );
}

void CS3DAnimObject::UpdateAnimation()
{
	CalcFrameID( CurAnimation.curID, CurFrame );
	ResetTransforms();
	if ( CurAnimation.curID != CurAnimation.nextID )
	{
		if ( Skeleton )
		{
			// blend animation progress:
			if ( !tBlendStarted ) tBlendStarted = s3dCurTime();
			CurAnimation.time = (float)( s3dCurTime() - tBlendStarted ) / tBlendTime;
			if ( CurAnimation.time >= 1.0f ) 
			{
				// end blending:
				tBlendStarted = 0;
				CurAnimation.time = 0;
				CurAnimation.curID = CurAnimation.nextID;
				CurFrame.tStarted = NextAnimFrame.tStarted;
				NextAnimFrame.tStarted = 0;
				CalcFrameID( CurAnimation.curID, CurFrame );
			}
			else
			{
				CalcFrameID( CurAnimation.nextID, NextAnimFrame );
				
				S3D_Transform transform;
				S3D_Animation *anim = Animations[ CurAnimation.curID ];
				S3D_Animation *nextAnim = tBlendStarted ? Animations[ CurAnimation.nextID ] : 0;
				for ( unsigned boneID = 0; boneID < Skeleton->numBones; boneID++ )
				{
					if ( anim->frameRate >= S3D_USED_LERP_TO )
					{
						transform.rotation = anim->Transforms[boneID][CurFrame.curID].rotation;
						transform.position = anim->Transforms[boneID][CurFrame.curID].position;
						if ( nextAnim )
						{
							S3D_Transform nextAnimTransf = nextAnim->Transforms[boneID][NextAnimFrame.curID];
							MixTransformWith( nextAnimTransf, boneID, transform );
						}
					}
					else
					{
						transform.rotation = S3D_QuatOp::slerp( anim->Transforms[boneID][CurFrame.curID ].rotation, 
																anim->Transforms[boneID][CurFrame.nextID].rotation, CurFrame.time );
						transform.position = S3D_MIX( anim->Transforms[boneID][CurFrame.curID ].position, 
													  anim->Transforms[boneID][CurFrame.nextID].position, CurFrame.time );
						if ( nextAnim )
						{
							S3D_Transform nextAnimTransf;
							nextAnimTransf.rotation = S3D_QuatOp::slerp( nextAnim->Transforms[boneID][NextAnimFrame.curID ].rotation, 
																		 nextAnim->Transforms[boneID][NextAnimFrame.nextID].rotation, NextAnimFrame.time );
							nextAnimTransf.position = S3D_MIX( nextAnim->Transforms[boneID][NextAnimFrame.curID ].position, 
															   nextAnim->Transforms[boneID][NextAnimFrame.nextID].position, NextAnimFrame.time );
							MixTransformWith( nextAnimTransf, boneID, transform );
						}
					}
					anim->Transforms[boneID][CurFrame.curID ] = transform;
				}
			}
		}
		else
			CurAnimation.curID = CurAnimation.nextID;
	}
	ApplyAuxTransforms();
}

void CS3DAnimObject::MixTransformWith( S3D_Transform t, S3D_BoneIndex boneID, S3D_Transform &result )
{
	result.position += S3D_QuatOp::rotate( Skeleton->bones[boneID].joint, result.rotation );
	
	t.position = t.position + S3D_QuatOp::rotate( Skeleton->bones[boneID].joint, t.rotation );
	result.rotation = S3D_QuatOp::slerp( result.rotation, t.rotation, CurAnimation.time );
	result.position = S3D_MIX( result.position, t.position, CurAnimation.time );

	S3D_Vertex joint = -Skeleton->bones[boneID].joint;
	result.position += S3D_QuatOp::rotate( joint, result.rotation );
}