#include <S3D.h>
#include <S3DPlatform.h>
#include <S3DShaders.h>

#if ( !S3D_OGLES_2_0 )
	PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = 0;
	PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = 0;
	PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = 0;
	PFNGLCREATESHADERPROC glCreateShader = 0;
	PFNGLCREATEPROGRAMPROC glCreateProgram = 0;
	PFNGLATTACHSHADERPROC glAttachShader = 0;
	PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation = 0;
	PFNGLLINKPROGRAMPROC glLinkProgram = 0;
	PFNGLVALIDATEPROGRAMPROC glValidateProgram = 0;
	PFNGLGETPROGRAMIVPROC glGetProgramiv = 0;
	PFNGLGETSHADERIVPROC glGetShaderiv = 0;
	PFNGLSHADERSOURCEPROC glShaderSource = 0;
	PFNGLCOMPILESHADERPROC glCompileShader = 0;
	PFNGLUSEPROGRAMPROC glUseProgram = 0;
	PFNGLISPROGRAMPROC glIsProgram = 0;
	PFNGLISSHADERPROC glIsShader = 0;
	PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation = 0;
	PFNGLGETUNIFORMFVPROC glGetUniformfv = 0;
	PFNGLUNIFORM1IPROC glUniform1i = 0;
	PFNGLUNIFORM1FVPROC glUniform1fv = 0;
	PFNGLUNIFORM2FVPROC glUniform2fv = 0;
	PFNGLUNIFORM3FVPROC glUniform3fv = 0;
	PFNGLUNIFORM4FVPROC glUniform4fv = 0;
	PFNGLUNIFORM1IVPROC glUniform1iv = 0;
	PFNGLUNIFORM2IVPROC glUniform2iv = 0;
	PFNGLUNIFORM3IVPROC glUniform3iv = 0;
	PFNGLUNIFORM4IVPROC glUniform4iv = 0;
	PFNGLUNIFORMMATRIX3FVPROC glUniformMatrix3fv = 0;
	PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv = 0;
	PFNGLGETATTRIBLOCATIONPROC glGetAttribLocation = 0;
	PFNGLVERTEXATTRIB1FPROC glVertexAttrib1f = 0;
	PFNGLVERTEXATTRIB2FVPROC glVertexAttrib2fv = 0;
	PFNGLVERTEXATTRIB3FVPROC glVertexAttrib3fv = 0;
	PFNGLVERTEXATTRIB4FVPROC glVertexAttrib4fv = 0;
	PFNGLDETACHSHADERPROC glDetachShader = 0;
	PFNGLDELETESHADERPROC glDeleteShader = 0;
	PFNGLDELETEPROGRAMPROC glDeleteProgram = 0;
	PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = 0;
	PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = 0;
#endif

S3D_Shader::S3D_Shader()
: h ( 0 ),
type( 0 ),
name( S3D_UNDEFINED )
{
}

S3D_Shader::~S3D_Shader()
{
	if ( glIsShader( h ) )
		glDeleteShader( h );
}

CS3DShader::CS3DShader()
: program( 0 )
{
	for ( int i = 0; i < S3D_MAX_ATTRIBS; i++ )
	{
		Attribs[i] = 0;
	}
	for ( int i = 0; i < S3D_MAX_SAMPLERS; i++ )
	{
		Samplers[i] = 0;
	}

	vertShader = 0;
	fragShader = 0;

#if ( !S3D_OGLES_2_0 )
	S3D_GET_FUNC( PFNGLENABLEVERTEXATTRIBARRAYPROC, glEnableVertexAttribArray, "glEnableVertexAttribArray" )
	S3D_GET_FUNC( PFNGLDISABLEVERTEXATTRIBARRAYPROC, glDisableVertexAttribArray, "glDisableVertexAttribArray" )
	S3D_GET_FUNC( PFNGLVERTEXATTRIBPOINTERPROC, glVertexAttribPointer, "glVertexAttribPointer" )
	S3D_GET_FUNC( PFNGLCREATESHADERPROC, glCreateShader, "glCreateShader" )
	S3D_GET_FUNC( PFNGLCREATEPROGRAMPROC, glCreateProgram, "glCreateProgram" )
	S3D_GET_FUNC( PFNGLATTACHSHADERPROC, glAttachShader, "glAttachShader" )
	S3D_GET_FUNC( PFNGLBINDATTRIBLOCATIONPROC, glBindAttribLocation, "glBindAttribLocation" )
	S3D_GET_FUNC( PFNGLLINKPROGRAMPROC, glLinkProgram, "glLinkProgram" )
	S3D_GET_FUNC( PFNGLVALIDATEPROGRAMPROC, glValidateProgram, "glValidateProgram" )
	S3D_GET_FUNC( PFNGLGETPROGRAMIVPROC, glGetProgramiv, "glGetProgramiv" )
	S3D_GET_FUNC( PFNGLGETSHADERIVPROC, glGetShaderiv, "glGetShaderiv" )
	S3D_GET_FUNC( PFNGLSHADERSOURCEPROC, glShaderSource, "glShaderSource" )
	S3D_GET_FUNC( PFNGLCOMPILESHADERPROC, glCompileShader, "glCompileShader" )
	S3D_GET_FUNC( PFNGLUSEPROGRAMPROC, glUseProgram, "glUseProgram" )
	S3D_GET_FUNC( PFNGLISPROGRAMPROC, glIsProgram, "glIsProgram" )
	S3D_GET_FUNC( PFNGLISSHADERPROC, glIsShader, "glIsShader" )
	S3D_GET_FUNC( PFNGLGETUNIFORMLOCATIONPROC, glGetUniformLocation, "glGetUniformLocation" )
    S3D_GET_FUNC( PFNGLGETUNIFORMFVPROC, glGetUniformfv, "glGetUniformfv" )
	S3D_GET_FUNC( PFNGLUNIFORM1IPROC, glUniform1i, "glUniform1i" )
	S3D_GET_FUNC( PFNGLUNIFORM1FVPROC, glUniform1fv, "glUniform1fv" )
	S3D_GET_FUNC( PFNGLUNIFORM2FVPROC, glUniform2fv, "glUniform2fv" )
	S3D_GET_FUNC( PFNGLUNIFORM3FVPROC, glUniform3fv, "glUniform3fv" )
	S3D_GET_FUNC( PFNGLUNIFORM4FVPROC, glUniform4fv, "glUniform4fv" )
	S3D_GET_FUNC( PFNGLUNIFORM1IVPROC, glUniform1iv, "glUniform1iv" )
	S3D_GET_FUNC( PFNGLUNIFORM2IVPROC, glUniform2iv, "glUniform2iv" )
	S3D_GET_FUNC( PFNGLUNIFORM3IVPROC, glUniform3iv, "glUniform3iv" )
	S3D_GET_FUNC( PFNGLUNIFORM4IVPROC, glUniform4iv, "glUniform4iv" )
	S3D_GET_FUNC( PFNGLUNIFORMMATRIX3FVPROC, glUniformMatrix3fv, "glUniformMatrix3fv" )
	S3D_GET_FUNC( PFNGLUNIFORMMATRIX4FVPROC, glUniformMatrix4fv, "glUniformMatrix4fv" )
	S3D_GET_FUNC( PFNGLGETATTRIBLOCATIONPROC, glGetAttribLocation, "glGetAttribLocation" )
	S3D_GET_FUNC( PFNGLVERTEXATTRIB1FPROC, glVertexAttrib1f, "glVertexAttrib1f" )
	S3D_GET_FUNC( PFNGLVERTEXATTRIB2FVPROC, glVertexAttrib2fv, "glVertexAttrib2fv" )
	S3D_GET_FUNC( PFNGLVERTEXATTRIB3FVPROC, glVertexAttrib3fv, "glVertexAttrib3fv" )
	S3D_GET_FUNC( PFNGLVERTEXATTRIB4FVPROC, glVertexAttrib4fv, "glVertexAttrib4fv" )
	S3D_GET_FUNC( PFNGLDETACHSHADERPROC, glDetachShader, "glDetachShader" )
	S3D_GET_FUNC( PFNGLDELETESHADERPROC, glDeleteShader, "glDeleteShader" )
	S3D_GET_FUNC( PFNGLDELETEPROGRAMPROC, glDeleteProgram, "glDeleteProgram" )
	S3D_GET_FUNC( PFNGLGETSHADERINFOLOGPROC, glGetShaderInfoLog, "glGetShaderInfoLog" )
	S3D_GET_FUNC( PFNGLGETPROGRAMINFOLOGPROC, glGetProgramInfoLog, "glGetProgramInfoLog" )
#endif
}

CS3DShader::~CS3DShader()
{
	int i;
	for ( i = 0; i < S3D_MAX_ATTRIBS; i++ )
	{
		if ( Attribs[i] ) free( Attribs[i] );
	}
	for ( i = 0; i < S3D_MAX_SAMPLERS; i++ )
	{
		if ( Samplers[i] ) free( Samplers[i] );
	}
	if ( program )
	{
		if ( glIsProgram( program ) )
		{
			if ( fragShader && glIsShader( fragShader->h ) )
			{
				glDetachShader( program, fragShader->h );
			}
			if ( vertShader && glIsShader( vertShader->h ) )
			{
				glDetachShader( program, vertShader->h );
			}
			glDeleteProgram( program );
		}
	}
}

const char *CS3DShader::GetShaderSource( S3D_ENUM shaderName )
{
	const char *source = 0;
	switch ( shaderName )
	{
		case S3D_VERTEX_SHADER_SIMPLE:				{ source = vshSimple; } break;
		case S3D_VERTEX_SHADER_FUR:					{ source = vshFur; } break;
		case S3D_VERTEX_SHADER_MESH:				{ source = vshMesh; } break;
		case S3D_VERTEX_SHADER_COLORED_MESH:		{ source = vshColoredMesh; } break;
		case S3D_VERTEX_SHADER_SKIN:				{ source = vshSkin; } break;
		case S3D_VERTEX_SHADER_SPRITE:				{ source = vshSprite; } break;
		case S3D_FRAGMENT_SHADER_ORENNAYARPHONG:	{ source = fshOrenNayarPhong; } break;
		case S3D_FRAGMENT_SHADER_LAMBERT:			{ source = fshLambert; } break;
		case S3D_FRAGMENT_SHADER_COLORED_LAMBERT:	{ source = fshColoredLambert; } break;
		case S3D_FRAGMENT_SHADER_LAMBERTPHONG:		{ source = fshLambertPhong; } break;
		case S3D_FRAGMENT_SHADER_SIMPLE:			{ source = fshSimple; } break;
		case S3D_FRAGMENT_SHADER_FUR:				{ source = fshFur; } break;
#if ( S3D_USE_SHADOWS )
		case S3D_VERTEX_SHADER_DEPTH_TEXTURE:		{ source = vshDepthTexture; } break;
		case S3D_FRAGMENT_SHADER_DEPTH_TEXTURE:		{ source = fshDepthTexture; } break;
#endif
		case S3D_FRAGMENT_SHADER_SPRITE:			{ source = fshSprite; } break;
		case S3D_FRAGMENT_SHADER_GAUSS_BLUR:		{ source = fshGaussBlur; } break;
		default: { s3dIOController->PrintLog( "Can not found a suitable shader.\n" ); }
	}
	return source;
}

S3D_ENUM CS3DShader::GetShaderName( unsigned int type )
{
	switch ( type )
	{
	case S3D_VERTEX_SHADER:
		return vertShader->name;
	case S3D_FRAGMENT_SHADER:
		return fragShader->name;
	}
	return S3D_UNDEFINED;
}

bool CS3DShader::Load ( S3D_ENUM vsh, S3D_ENUM fsh )
{
	if ( !s3dLoadedShaders ) return false;

	vertShader = new S3D_Shader;
	vertShader->type = GL_VERTEX_SHADER;
	vertShader->name = vsh;

	fragShader = new S3D_Shader;
	fragShader->type = GL_FRAGMENT_SHADER;
	fragShader->name = fsh;

	const char *source = GetShaderSource( vertShader->name );
	if ( !source )
	{
		return false;
	}
	int size = sizeof( char ) * strlen( source );
	int cur_pos = 0;

	int cur_attr = 0;
	const int max_len = 0x100;
	const char *cur_source = source;
	while ( cur_pos < size )
	{
		int i;
		for ( i = 0; i < max_len; i++ )
		{
			if ( cur_source[i] == 0 ) break;
			if ( cur_source[i] == '\r' || cur_source[i] == '\n' )
			{
				if ( cur_source[i + 1] == '\n' ) i++;
				i++;
				break;
			}
		}
		cur_pos += i;
		if ( cur_source[0] == 'a'
		  && cur_source[1] == 't'
		  && cur_source[2] == 't'
		  && cur_source[3] == 'r'
		  && cur_source[4] == 'i'
		  && cur_source[5] == 'b'
		  && cur_source[6] == 'u'
		  && cur_source[7] == 't'
		  && cur_source[8] == 'e'
		  && S3D_ISSPASE( cur_source[9] ) )
		{
			bool attrib_finded = false;
			bool attrib_type_finded = false;
			char arrtib_name[0x100] = { 0 };
			int cur = 0;
			for ( int i = 10; i < max_len; i++ )
			{
				if ( !attrib_type_finded )
				{
					if ( S3D_ISSPASE( cur_source[i] ) ) continue;
					else attrib_type_finded = true;
				}
				else
				{
					if ( !attrib_finded )
					{
						if ( S3D_ISSPASE( cur_source[i] ) ) attrib_finded = true;
						else continue;
					}
					else
					{
						if ( ( S3D_ISSPASE( cur_source[i] ) || cur_source[i] == ';' ) && *arrtib_name != 0 ) break;
						else if ( S3D_ISSPASE( cur_source[i] ) ) continue;
						else arrtib_name[cur++] = cur_source[i];
					}
				}
			}
			bool bSkip = false;
#if ( !S3D_USE_LIGTHING )
			bSkip = !strcmp( arrtib_name, "normal" ) || 
					!strcmp( arrtib_name, "tangent" );
#else
#if ( !S3D_BUMPMAPPING )
			bSkip = !strcmp( arrtib_name, "tangent" );
#endif
#endif
			if ( !bSkip )
			{
				int attr_len = strlen( arrtib_name );
				Attribs[ cur_attr ] = (char*)malloc( attr_len + 1 );
				memcpy( Attribs[ cur_attr ], arrtib_name, attr_len );
				Attribs[ cur_attr ][ attr_len ] = 0;
				cur_attr++;
			}
		}
		cur_source = &source[ cur_pos ];
	}
	source = fragShader->name != S3D_UNDEFINED ? GetShaderSource( fragShader->name ) : 0;
	if ( source )
	{
		size = sizeof( char ) * strlen( source );
		cur_pos = 0;

		int cur_sampler = 0;
		cur_source = source;
		while ( cur_pos < size )
		{
			int i;
			for ( i = 0; i < max_len; i++ )
			{
				if ( cur_source[i] == 0 ) break;
				if ( cur_source[i] == '\r' || cur_source[i] == '\n' )
				{
					if ( cur_source[i + 1] == '\n' ) i++;
					i++;
					break;
				}
			}
			cur_pos += i;
			if ( cur_source[0] == 'u'
			  && cur_source[1] == 'n'
			  && cur_source[2] == 'i'
			  && cur_source[3] == 'f'
			  && cur_source[4] == 'o'
			  && cur_source[5] == 'r'
			  && cur_source[6] == 'm'
			  && S3D_ISSPASE( cur_source[7] ) )
			{
				bool sampler_finded = false;
				bool sampler_type_finded = false;
				char sampler_name[0x100] = { 0 };
				int cur = 0;
				for ( int i = 8; i < max_len; i++ )
				{
					if ( !sampler_type_finded )
					{
						if ( S3D_ISSPASE( cur_source[i] ) ) continue;
						else
						{
							if ( cur_source[i + 0] == 's'
							  && cur_source[i + 1] == 'a'
							  && cur_source[i + 2] == 'm'
							  && cur_source[i + 3] == 'p'
							  && cur_source[i + 4] == 'l'
							  && cur_source[i + 5] == 'e'
							  && cur_source[i + 6] == 'r' )
							{
								sampler_type_finded = true;
							}
							else break;
						}
					}
					else
					{
						if ( !sampler_finded )
						{
							if ( S3D_ISSPASE( cur_source[i] ) ) sampler_finded = true;
							else continue;
						}
						else
						{
							if ( ( S3D_ISSPASE( cur_source[i] ) || cur_source[i] == ';' ) && *sampler_name != 0 ) break;
							else if ( S3D_ISSPASE( cur_source[i] ) ) continue;
							else sampler_name[cur++] = cur_source[i];
						}
					}
				}
				if ( sampler_type_finded )
				{
					int sampler_len = strlen( sampler_name );
					Samplers[ cur_sampler ] = (char*)malloc( sampler_len + 1 );
					memcpy( Samplers[ cur_sampler ], sampler_name, sampler_len );
					Samplers[ cur_sampler ][ sampler_len ] = 0;
					cur_sampler++;
				}
			}
			cur_source = &source[ cur_pos ];
		}
	}
	return Init();
}

bool CS3DShader::GetShader( S3D_Shader **shader )
{
	bool result = false;
	S3D_ENUM name = (*shader)->name;
	if ( (unsigned)name != S3D_UNDEFINED )
	{
		int i;
		int count = s3dLoadedShaders->size();
		for ( i = 0; i < count; i++ )
		{
			if ( (*s3dLoadedShaders)[ i ]->name == name )
			{
				delete *shader;
				*shader = (*s3dLoadedShaders)[ i ];
				result = true;
				break;
			}
		}
		if ( i == count )
		{
			result = Load( *shader );
			if ( result ) s3dLoadedShaders->push_back( *shader );
		}
	}
	else
	{
		delete *shader;
		*shader = 0;
		result = true;
	}
	return result;
}

bool CS3DShader::Init()
{
	if ( GetShader( &vertShader ) )
	{
		if ( GetShader( &fragShader ) )
		{
			if ( !vertShader && !fragShader ) return false;

			program = glCreateProgram();
			if ( vertShader && glIsShader( vertShader->h ) ) glAttachShader( program, vertShader->h );
			if ( fragShader && glIsShader( fragShader->h ) ) glAttachShader( program, fragShader->h );

			for ( int i = 0; i < S3D_MAX_ATTRIBS; i++ )
				if ( Attribs[i] ) glBindAttribLocation( program, i, Attribs[i] );

			GLint status = 0;
			glLinkProgram( program );
			glGetProgramiv( program, GL_LINK_STATUS, &status );

			if ( status == 1 )
			{
				{
					BeginShaderProgram();
					for ( int i = S3D_MAX_SAMPLERS - 1; i >= 0; --i )
					{
						if ( Samplers[i] )
						{
							bool cubeMap = strstr( Samplers[i], "reflection" ) != 0;
							unsigned texId = S3D_OpenGLOp::GetBlankTexture( cubeMap ? 1 : 0 );
							S3D_OpenGLOp::EnableTexture( texId, i, cubeMap );
							SetSampler( Samplers[i], i );
							S3D_OpenGLOp::DisableTexture( -1, cubeMap );
						}
					}
					EndShaderProgram();
					S3D_OpenGLOp::DisableTexture( 0, true );
					S3D_OpenGLOp::DisableTexture( 0, false );
				}
#ifndef S3D_IPHONE_PLATFORM
				status = 0;
				glValidateProgram( program );
				glGetProgramiv( program, GL_VALIDATE_STATUS, &status );
#endif
				if ( status == 1 )
				{
					return true;
				}
			}

			char *buffer = (char *)calloc( 0x1000, sizeof( char ) );
			GLsizei length = 0;
			glGetProgramInfoLog( program, 0x1000, &length, buffer );
			s3dIOController->PrintLog( buffer );
			free( buffer );
		}
	}
	return false;
}

bool CS3DShader::Load( S3D_Shader *shader )
{
	const char *source = GetShaderSource( shader->name );
	if ( source )
	{
	    shader->h = glCreateShader( shader->type );
		int size = sizeof( char ) * strlen( source );
		glShaderSource( shader->h, 1, (const char **)&source, &size );
		glCompileShader( shader->h );

		GLint status = 0;
		glGetShaderiv( shader->h, GL_COMPILE_STATUS, &status );

		if ( status == 1 )
			return true;

		char *buffer = (char *)calloc( 0x1000, sizeof( char ) );
		GLsizei length = 0;
		glGetShaderInfoLog( shader->h, 0x1000, &length, buffer );
		s3dIOController->PrintLog( buffer );
		free( buffer );
	}
	return false;
}

void CS3DShader::BeginShaderProgram()
{
	glUseProgram( program );
}

void CS3DShader::EndShaderProgram()
{
	glUseProgram( 0 );
}

void CS3DShader::EnableAttributes()
{
	for ( int i = S3D_MAX_ATTRIBS - 1; i >= 0; i-- )
	{
		if ( Attribs[i] )
		{
			glEnableVertexAttribArray( i );
		}
	}
}

void CS3DShader::DisableAttributes()
{
	for ( int i = S3D_MAX_ATTRIBS - 1; i >= 0; i-- )
	{
		if ( Attribs[i] )
		{
			glDisableVertexAttribArray( i );
		}
	}
}

void CS3DShader::EnableAttribute( int id )
{
	glEnableVertexAttribArray( id );
}

void CS3DShader::DisableAttribute( int id )
{
	glDisableVertexAttribArray( id );
}


bool CS3DShader::SetSampler( const char *name, int value )
{
	int id = glGetUniformLocation( program, name );
	if ( id != -1 )
	{
		glUniform1i( id, value );
		return true;
	}
	return false;
}

bool CS3DShader::GetUniformf( const char *name, float *value )
{
	int id = glGetUniformLocation( program, name );
	if ( id != -1 )
	{
		glGetUniformfv( program, id, value );
		return true;
	}
	return false;
}

bool CS3DShader::SetUniformMatrixf( const char *name, int size, float *value )
{
		int id = glGetUniformLocation( program, name );
		if ( id != -1 )
		{
			switch ( size )
			{
			case 3:
				glUniformMatrix3fv( id, 1, 0, value );
				break;
			case 4:
				glUniformMatrix4fv( id, 1, 0, value );
				break;
			}
			return true;
		}
		return false;
}

bool CS3DShader::SetUniformf( const char *name, int size, float *value )
{
	int id = glGetUniformLocation( program, name );
	if ( id != -1 )
	{
		switch ( size )
		{
		case 1:
			glUniform1fv( id, 1, value );
			break;
		case 2:
			glUniform2fv( id, 1, value );
			break;
		case 3:
			glUniform3fv( id, 1, value );
			break;
		case 4:
			glUniform4fv( id, 1, value );
			break;
		}
		return true;
	}
	return false;
}

bool CS3DShader::SetUniformi( const char *name, int size, int *value )
{
	int id = glGetUniformLocation( program, name );
	if ( id != -1 )
	{
		switch ( size )
		{
		case 1:
			glUniform1iv( id, 1, value );
			break;
		case 2:
			glUniform2iv( id, 1, value );
			break;
		case 3:
			glUniform3iv( id, 1, value );
			break;
		case 4:
			glUniform4iv( id, 1, value );
			break;
		}
		return true;
	}
	return false;
}

bool CS3DShader::SetAttribf( const char *name, int size, float *value )
{
	int id = glGetAttribLocation( program, name );
	if ( id != -1 )
	{
		switch ( size )
		{
		case 1:
			glVertexAttrib1f( id, *value );
			break;
		case 2:
			glVertexAttrib2fv( id, value );
			break;
		case 3:
			glVertexAttrib3fv( id, value );
			break;
		case 4:
			glVertexAttrib4fv( id, value );
			break;
		}
		return true;
	}
	return false;
}

void CS3DShader::SetAttribf( unsigned int id, int size, float *value )
{
	switch ( size )
	{
	case 1:
		glVertexAttrib1f( id, *value );
		break;
	case 2:
		glVertexAttrib2fv( id, value );
		break;
	case 3:
		glVertexAttrib3fv( id, value );
		break;
	case 4:
		glVertexAttrib4fv( id, value );
		break;
	}
}

void CS3DShader::SetAttribPointerf( unsigned int id, int size, void *offset, int stride )
{
	glVertexAttribPointer( id, size, GL_FLOAT, 0, stride, offset );
}
