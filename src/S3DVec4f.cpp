#include <S3D.h>

CS3DVec4f::CS3DVec4f()
{
    v[0] = 0;
    v[1] = 0;
    v[2] = 0;
    v[3] = 0;
}

CS3DVec4f::CS3DVec4f( float x, float y, float z, float w )
{
    v[0] = x;
    v[1] = y;
    v[2] = z;
    v[3] = w;
}

CS3DVec4f::CS3DVec4f( float *xyz, float w )
{
    v[0] = xyz[0];
    v[1] = xyz[1];
    v[2] = xyz[2];
    v[3] = w;
}

float &CS3DVec4f::operator[]( int index )
{
    return v[index];
}

float CS3DVec4f::operator[]( int index ) const
{
    return v[index];
}

CS3DVec4f CS3DVec4f::operator+( float scale ) const
{
    return CS3DVec4f( v[0] + scale, v[1] + scale,
					  v[2] + scale, v[3] + scale );
}

CS3DVec4f CS3DVec4f::operator-( float scale ) const
{
    return CS3DVec4f( v[0] - scale, v[1] - scale,
					  v[2] - scale, v[3] - scale );
}

CS3DVec4f CS3DVec4f::operator*( float scale ) const
{
    return CS3DVec4f( v[0] * scale, v[1] * scale,
					  v[2] * scale, v[3] * scale );
}

CS3DVec4f CS3DVec4f::operator/( float scale ) const
{
    return CS3DVec4f( v[0] / scale, v[1] / scale,
					  v[2] / scale, v[3] / scale );
}

CS3DVec4f CS3DVec4f::operator+( const CS3DVec4f &other ) const
{
    return CS3DVec4f( v[0] + other.v[0], v[1] + other.v[1],
					  v[2] + other.v[2], v[3] + other.v[3] );
}

CS3DVec4f CS3DVec4f::operator-( const CS3DVec4f &other ) const
{
    return CS3DVec4f( v[0] - other.v[0], v[1] - other.v[1],
					  v[2] - other.v[2], v[3] - other.v[3] );
}

CS3DVec4f CS3DVec4f::operator*( const CS3DVec4f &other ) const
{
    return CS3DVec4f( v[0] * other.v[0], v[1] * other.v[1],
					  v[2] * other.v[2], v[3] * other.v[3] );
}

CS3DVec4f CS3DVec4f::operator/( const CS3DVec4f &other ) const
{
    return CS3DVec4f( v[0] / other.v[0], v[1] / other.v[1],
					  v[2] / other.v[2], v[3] / other.v[3] );
}

CS3DVec4f CS3DVec4f::operator-() const
{
    return CS3DVec4f( -v[0], -v[1], -v[2], -v[3] );
}

bool CS3DVec4f::operator!() const
{
    return ( v[0] == 0 && v[1] == 0
          && v[2] == 0 && v[3] == 0 );
}

bool CS3DVec4f::operator==( const CS3DVec4f &other ) const
{
    return ( v[0] == other.v[0]
          && v[1] == other.v[1]
          && v[2] == other.v[2]
          && v[3] == other.v[3] );
}

bool CS3DVec4f::operator!=( const CS3DVec4f &other ) const
{
    return ( v[0] != other.v[0]
          || v[1] != other.v[1]
          || v[2] != other.v[2]
          || v[3] != other.v[3] );
}

const CS3DVec4f &CS3DVec4f::operator+=( float scale )
{
    v[0] += scale;
    v[1] += scale;
    v[2] += scale;
    v[3] += scale;
    return *this;
}

const CS3DVec4f &CS3DVec4f::operator-=( float scale )
{
    v[0] -= scale;
    v[1] -= scale;
    v[2] -= scale;
    v[3] -= scale;
    return *this;
}

const CS3DVec4f &CS3DVec4f::operator*=( float scale )
{
    v[0] *= scale;
    v[1] *= scale;
    v[2] *= scale;
    v[3] *= scale;
    return *this;
}

const CS3DVec4f &CS3DVec4f::operator/=( float scale )
{
    v[0] /= scale;
    v[1] /= scale;
    v[2] /= scale;
    v[3] /= scale;
    return *this;
}

const CS3DVec4f &CS3DVec4f::operator+=( const CS3DVec4f &other )
{
    v[0] += other.v[0];
    v[1] += other.v[1];
    v[2] += other.v[0];
    v[3] += other.v[1];
    return *this;
}

const CS3DVec4f &CS3DVec4f::operator-=( const CS3DVec4f &other )
{
    v[0] -= other.v[0];
    v[1] -= other.v[1];
    v[2] -= other.v[2];
    v[3] -= other.v[3];
    return *this;
}

const CS3DVec4f &CS3DVec4f::operator*=( const CS3DVec4f &other )
{
    v[0] *= other.v[0];
    v[1] *= other.v[1];
    v[2] *= other.v[2];
    v[3] *= other.v[3];
    return *this;
}

const CS3DVec4f &CS3DVec4f::operator/=( const CS3DVec4f &other )
{
    v[0] /= other.v[0];
    v[1] /= other.v[1];
    v[2] /= other.v[2];
    v[3] /= other.v[3];
    return *this;
}

void CS3DVec4f::set( float *values )
{
	if ( !values ) return;
	v[0] = values[0];
    v[1] = values[1];
    v[2] = values[2];
    v[3] = values[3];
}

CS3DVec3f CS3DVec4f::xyz()
{
	return CS3DVec3f( x, y, z );
}

float CS3DVec4f::dot( const CS3DVec4f &other ) const
{
    return v[0] * other.v[0] + v[1] * other.v[1] + v[2] * other.v[2] + v[3] * other.v[3];
}

void CS3DVec4f::normalize()
{
	float m = magnitude();
	if ( m == 0.f ) return;
	m = 1.f / m; 
	v[0] *= m;
	v[1] *= m;
	v[2] *= m;
	v[3] *= m;
}

float CS3DVec4f::magnitude() const
{
    return sqrtf( v[0] * v[0] + v[1] * v[1] + v[2] * v[2] + v[3] * v[3] );
}

float CS3DVec4f::magnitudeSquared() const
{
    return v[0] * v[0] + v[1] * v[1] + v[2] * v[2] + v[3] * v[3];
}

CS3DVec4f CS3DVec4f::maximize( const CS3DVec4f &other )
{
    CS3DVec4f result = *this;
    if ( other.v[0] > v[0] ) result[0] = other.v[0];
    if ( other.v[1] > v[1] ) result[1] = other.v[1];
    if ( other.v[2] > v[2] ) result[2] = other.v[2];
    if ( other.v[3] > v[3] ) result[3] = other.v[3];
    return result;
}

CS3DVec4f CS3DVec4f::minimize( const CS3DVec4f &other )
{
    CS3DVec4f result = *this;
    if ( other.v[0] < v[0] ) result[0] = other.v[0];
    if ( other.v[1] < v[1] ) result[1] = other.v[1];
    if ( other.v[2] < v[2] ) result[2] = other.v[2];
    if ( other.v[3] < v[3] ) result[3] = other.v[3];
    return result;
}

void CS3DVec4f::calculatePlane( CS3DVec3f v1, CS3DVec3f v2, CS3DVec3f v3 )
{
	v[0] = v1.y*(v2.z-v3.z) + v2.y*(v3.z-v1.z) + v3.y*(v1.z-v2.z);
	v[1] = v1.z*(v2.x-v3.x) + v2.z*(v3.x-v1.x) + v3.z*(v1.x-v2.x);
	v[2] = v1.x*(v2.y-v3.y) + v2.x*(v3.y-v1.y) + v3.x*(v1.y-v2.y);
	v[3] = -( v1.x*(v2.y*v3.z - v3.y*v2.z) +
			  v2.x*(v3.y*v1.z - v1.y*v3.z) +
			  v3.x*(v1.y*v2.z - v2.y*v1.z) );
}

S3D_Quat S3D_QuatOp::mult( S3D_Quat &q1, S3D_Quat &q2 )
{
	S3D_Quat q;
	q.w = (q2.w * q1.w) - (q2.x * q1.x) - (q2.y * q1.y) - (q2.z * q1.z);
	q.x = (q2.w * q1.x) + (q2.x * q1.w) + (q2.y * q1.z) - (q2.z * q1.y);
	q.y = (q2.w * q1.y) + (q2.y * q1.w) + (q2.z * q1.x) - (q2.x * q1.z);
	q.z = (q2.w * q1.z) + (q2.z * q1.w) + (q2.x * q1.y) - (q2.y * q1.x);
	return q;
}

S3D_Quat S3D_QuatOp::slerp( S3D_Quat q1, S3D_Quat q2, float blend )
{
	float cosOmega = q1.dot( q2 );
	if ( cosOmega < 0.0f )
	{
		cosOmega = -cosOmega;
		q2 = -q2;
	}
	float scalar1, scalar2;
	if ( 1.0f - cosOmega > S3D_EPS )
	{
		float omega = acosf( cosOmega );
		float invSinOmega = 1.0f / sinf( omega );
		scalar1 = sinf( ( 1.0f - blend ) * omega ) * invSinOmega;
		scalar2 = sinf( blend * omega ) * invSinOmega;
	}
	else
	{
		scalar1 = 1.0f - blend;
		scalar2 = blend;
	}
	return q1 * scalar1 + q2 * scalar2;
}

S3D_Quat S3D_QuatOp::lerp( S3D_Quat q1, S3D_Quat q2, float blend )
{
	if ( q1.dot( q2 ) < 0.0f ) q2 = -q2;
	return S3D_MIX( q1, q2, blend );
}

void S3D_QuatOp::to_matrix( float m[16], const S3D_Quat &q )
{
	float wx, wy, wz, xx, yy, yz, xy, xz, zz, x2, y2, z2;

	x2 = q.x + q.x;
	y2 = q.y + q.y;
	z2 = q.z + q.z;

	xx = q.x * x2;  xy = q.x * y2;  xz = q.x * z2;
	yy = q.y * y2;  yz = q.y * z2;  zz = q.z * z2;
	wx = q.w * x2;  wy = q.w * y2;  wz = q.w * z2;

	m[0] = 1 - (yy + zz);  m[4] = xy + wz;        m[8] = xz - wy;
	m[1] = xy - wz;        m[5] = 1 - (xx + zz);  m[9] = yz + wx;
	m[2] = xz + wy;        m[6] = yz - wx;        m[10] = 1 - (xx + yy);
	
	m[3] = 0;
	m[7] = 0;
	m[11] = 0;
	m[12] = 0;
	m[13] = 0;
	m[14] = 0;
	m[15] = 1;
}

S3D_Quat S3D_QuatOp::from_matrix( const float matrix[16] )
{
	float m[3][3];
    m[ 0 ][ 0 ] = matrix[ 0 ]; m[ 1 ][ 0 ] = matrix[ 4 ]; m[ 2 ][ 0 ] = matrix[ 8 ];
    m[ 0 ][ 1 ] = matrix[ 1 ]; m[ 1 ][ 1 ] = matrix[ 5 ]; m[ 2 ][ 1 ] = matrix[ 9 ];
    m[ 0 ][ 2 ] = matrix[ 2 ]; m[ 1 ][ 2 ] = matrix[ 6 ]; m[ 2 ][ 2 ] = matrix[ 10 ];

	S3D_Quat q;
	float diagonal = 1.0f + m[0][0] + m[1][1] + m[2][2];
	if ( diagonal > 0.0f )
	{
		// get scale from diagonal
		const float scale = 0.5f / sqrtf( diagonal );
		q[0] = ( m[2][1] - m[1][2] ) * scale;
		q[1] = ( m[0][2] - m[2][0] ) * scale;
		q[2] = ( m[1][0] - m[0][1] ) * scale;
		q[3] = 0.25f / scale;
	}
	else 
	{
		if( ( m[0][0] > m[1][1] ) && ( m[0][0] > m[2][2] ) )
		{
			// 1st element of diag is greatest value
			// find scale according to 1st element, and double it
			const float scale = sqrtf( 1.0f + m[0][0] - m[1][1] - m[2][2] ) * 2.0f;
			q[0] = 0.25f * scale;
			q[1] = ( m[0][1] + m[1][0] ) / scale;
			q[2] = ( m[2][0] + m[0][2] ) / scale;
			q[3] = ( m[2][1] - m[1][2] ) / scale;
		}
		else if ( m[1][1] > m[2][2] )
		{
			// 2nd element of diag is greatest value
			// find scale according to 2nd element, and double it
			const float scale = sqrtf( 1.0f + m[1][1] - m[0][0] - m[2][2] ) * 2.0f;
			q[0] = ( m[0][1] + m[1][0] ) / scale;
			q[1] = 0.25f * scale;
			q[2] = ( m[1][2] + m[2][1] ) / scale;
			q[3] = ( m[0][2] - m[2][0] ) / scale;
		}
		else
		{
			// 3rd element of diag is greatest value
			// find scale according to 3rd element, and double it
			const float scale = sqrtf( 1.0f + m[2][2] - m[0][0] - m[1][1] ) * 2.0f;
			q[0] = ( m[0][2] + m[2][0] ) / scale;
			q[1] = ( m[1][2] + m[2][1] ) / scale;
			q[2] = 0.25f * scale;
			q[3] = ( m[1][0] - m[0][1] ) / scale;
		}
	}
	return q;
}

CS3DVec3f S3D_QuatOp::rotate( CS3DVec3f &p, S3D_Quat &q )
{
	CS3DVec3f v = p * q.w + ( p ^ q.xyz() );
	return v * q.w + q.xyz() * p.dot( q.xyz() ) - ( q.xyz() ^ v );
}

S3D_Quat S3D_QuatOp::from_angle( float angle, S3D_ENUM axis )
{
	CS3DVec3f v;
	v[axis] = 1;
	S3D_Quat q;
	float half = -angle * S3D_PI / 360.f;
	float s = sinf( half );
	q.x = v.x * s;
	q.y = v.y * s;
	q.z = v.z * s;
	q.w = cosf( half );
	q.normalize();
	return q;
}
