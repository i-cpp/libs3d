#include <S3D.h>

S3D_MatIDs::S3D_MatIDs()
: IDs( 0 ), count( 0 ), buffID( 0 )
{}

S3D_MatIDs::~S3D_MatIDs()
{
	if ( IDs ) delete [] IDs;
	if ( s3dBuffer && buffID )
		s3dBuffer->FreeBuffer( buffID, S3D_IBO );
}

CS3DMaterial::CS3DMaterial()
{
	memset( &Materials, 0, sizeof( S3D_Material ) * S3D_MAX_MATERIALS );
}

CS3DMaterial::~CS3DMaterial()
{
}

unsigned int CS3DMaterial::MaterialTexture( int matID, S3D_MaterialTexture mt )
{
	return matID < S3D_MAX_MATERIALS ? Materials[matID].ts[mt] : 0;
}

void CS3DMaterial::MaterialTexture( int matID, S3D_MaterialTexture mt, unsigned int texID )
{
	if ( matID < 0 )
	{
		for ( int i = 0; i < S3D_MAX_MATERIALS; i++ )
		{
			Materials[i].ts[mt] = texID;
		}
	}
	else if ( matID < S3D_MAX_MATERIALS )
		Materials[matID].ts[mt] = texID;
}

void CS3DMaterial::SetDiffuseLevel( int matID, float level )
{
	if ( matID < 0 )
	{
		for ( int i = 0; i < S3D_MAX_MATERIALS; i++ )
		{
			Materials[i].diffuseLevel = level;
		}
	}
	else if ( matID < S3D_MAX_MATERIALS )
	{
		Materials[matID].diffuseLevel = level;
	}
}

void CS3DMaterial::LoadMaterialFromMesh( const char *filename, unsigned int numFaces )
{
	char Path[0x100];
	strcpy( Path, filename );
	int len = strlen( Path );
	for ( int i = len - 1; i >= 0; i-- )
	{
		if ( !strchr( "/\\", Path[i] ) ) Path[i] = 0;
		else break;
	}
	unsigned char numMat = s3dIOController->ReadByte();
	for ( int j = 0; j < numMat; j++ )
	{
		S3D_Material mat;
		memset( &mat, 0, sizeof( S3D_Material ) );
		mat.enabled = true;
		mat.diffuseLevel = 1;
		s3dIOController->ReadBlock( (unsigned char *)&mat.ambient[0], 16 * sizeof( float ) );
		mat.shininess = (float)s3dIOController->ReadShort();
		if ( mat.shininess == 0.0f )
			memset( &mat.specular, 0, sizeof( CS3DVec4f ) );
		else
		{
			if ( mat.specular.xyz() == CS3DVec3f(0.f) )
				mat.shininess = 1;
		}
		mat.shininess = max( 1, mat.shininess );
		// read mat names:
		short nameLen;
		int numMatTextures = S3D_MAX_MATERIAL_TEXTURES - 1;
#if ( !S3D_USE_LIGTHING )
		numMatTextures = 3;
#else
#if ( !S3D_BUMPMAPPING )
		numMatTextures++;
#endif
#if ( !S3D_USE_CUBEMAPS )
		numMatTextures++;
#endif
#endif
		typedef char* string;
		string *name = new string[numMatTextures];
		memset( name, 0, sizeof( string ) * numMatTextures );
		for ( int m = 0; m < numMatTextures; m++ )
		{
			nameLen = s3dIOController->ReadShort();
			if ( nameLen )
			{
				name[m] = (char*)malloc( nameLen + 1 );
				s3dIOController->ReadBlock( (unsigned char *)&name[m][0], nameLen + 1 );
				if ( s3dLoadTexture && name[m][0] )
				{
					char PathWithName[0x100] = { 0 };
					strcpy( PathWithName, Path );
					strcat( PathWithName, name[m] );
					strcat( PathWithName, S3D_TEXTURE_FILE_FORMAT );
					bool bLoad = true;
#if ( !S3D_USE_LIGTHING )
					if ( m != S3D_DiffuseMap ) bLoad = false;
#else
#if ( !S3D_BUMPMAPPING )
					if ( m == 2 ) bLoad = false;
#endif
#endif
					if ( bLoad )
						mat.ts[m] = s3dLoadTexture( PathWithName, 0, true );
				}
				free( name[m] );
				name[m] = 0;
#if ( S3D_BUMPMAPPING )
				if ( m == S3D_NormalMap )
					mat.bumpAmount = s3dIOController->ReadFloat();
#else
				if ( m == 2 ) s3dIOController->ReadFloat();
#endif
			}
			if ( !mat.ts[m] )
			{
				 // set as default
				if ( m == S3D_DiffuseMap )
				{
					mat.ts[m] = S3D_OpenGLOp::GetBlankTexture();
				}
#if ( S3D_USE_LIGTHING )
				else if ( m == S3D_SpecularMap )
				{
					mat.ts[m] = S3D_OpenGLOp::GetBlankTexture();
				}
#if ( S3D_BUMPMAPPING )
				else if ( m == S3D_NormalMap )
				{
					mat.ts[m] = S3D_OpenGLOp::GetBlankTexture(2);
					mat.bumpAmount = 0;
				}
#endif
#endif
			}
		}
		delete [] name;
		PushMaterial( &mat );
	}
	if ( numMat > 1 )
	{
		MatIDs = new S3D_MatIDs[numMat];
		unsigned int num_ids = numFaces * 3;

		S3D_Index *ids = new S3D_Index[num_ids];
		unsigned int *counters = new unsigned int[numMat];
		memset( counters, 0, sizeof( unsigned int ) * numMat );

		int cur_id = 0;
		for( unsigned int i = 0; i < numFaces; i++ )
		{
			S3D_Index id = (S3D_Index)s3dIOController->ReadByte();
			ids[cur_id + 0] = id;
			ids[cur_id + 1] = id;
			ids[cur_id + 2] = id;
			MatIDs[ids[cur_id]].count += 3;
			cur_id += 3;
		}
		for ( unsigned int i = 0; i < numMat; i++ )
		{
			MatIDs[i].IDs = new S3D_Index[MatIDs[i].count];
		}
		for( unsigned int i = 0; i < num_ids; i++ )
		{
			MatIDs[ids[i]].IDs[counters[ids[i]]] = i;
			counters[ids[i]]++;
		}
		delete [] ids;
		delete [] counters;
	}
	else
	{
		if ( numMat == 0 )
		{
			// set default material:
			S3D_Material mat;
			memset( &mat, 0, sizeof( S3D_Material ) );
			mat.enabled = true;
			mat.diffuseLevel = 1;
			mat.ambient = CS3DVec4f( 0.5f, 0.5f, 0.5f, 1.0f );
			mat.diffuse = CS3DVec4f( 0.8f, 0.8f, 0.8f, 1.0f );
			mat.specular = CS3DVec4f( 1.0f, 1.0f, 1.0f, 1.0f );
			mat.shininess = 32.0f;
			unsigned char pixel[3] = { 255, 255, 255 };
			S3D_OpenGLOp::CreateTexture( mat.ts[ S3D_DiffuseMap ], pixel, 1, 1, 3 );
#if ( S3D_USE_LIGTHING )
			mat.ts[ S3D_SpecularMap ] = mat.ts[ S3D_DiffuseMap ];
#if ( S3D_BUMPMAPPING )
			pixel[0] = 127; pixel[1] = 127; pixel[2] = 255;
			S3D_OpenGLOp::CreateTexture( mat.ts[ S3D_NormalMap ], pixel, 1, 1, 3 );
#endif
#endif
            S3D_OpenGLOp::DisableTexture();
			PushMaterial( &mat );
		}
		MatIDs = new S3D_MatIDs[1];
		MatIDs[0].count = numFaces * 3;
		MatIDs[0].IDs = new S3D_Index[MatIDs[0].count];
		for( unsigned int i = 0; i < MatIDs[0].count; i++ )
			MatIDs[0].IDs[i] = i;
	}
}

void CS3DMaterial::PushMaterial( S3D_Material *material )
{
	if ( material )
	{
		for ( int i = 0; i < S3D_MAX_MATERIALS; i++ )
		{
			if ( !Materials[i].enabled )
			{
				Materials[i] = *material;
				break;
			}
		}
	}
}

int CS3DMaterial::NumMaterials()
{
	int count = 0;
	for ( int i = 0; i < S3D_MAX_MATERIALS; i++ )
	{
		if ( Materials[i].enabled )
			count++;
	}
	return count;
}

void CS3DMaterial::EnableMaterial( int matID, CS3DShader *shader, bool useTextures )
{
	if ( matID >= S3D_MAX_MATERIALS || !Materials[matID].enabled ) return;

	if ( shader )
	{
#if ( S3D_USE_LIGTHING )
		S3D_Material lightProduct;
		memset( &lightProduct, 0, sizeof( S3D_Material ) );
        S3D_Light *light = s3dLight->GetLight( 0 );
		lightProduct.ambient = light->ambient * Materials[matID].ambient;
		lightProduct.diffuse = light->diffuse * Materials[matID].diffuse * Materials[matID].diffuseLevel;
		lightProduct.specular = light->specular * Materials[matID].specular;
		
		shader->SetUniformf( "lightProductAmbient", 4, (float*)&lightProduct.ambient );
		shader->SetUniformf( "lightProductDiffuse", 4, (float*)&lightProduct.diffuse );
		shader->SetUniformf( "lightProductSpecular", 4, (float*)&lightProduct.specular );
		shader->SetUniformf( "materialEmission", 4, (float*)&Materials[matID].emission );
		shader->SetUniformf( "shininess", 1, &Materials[matID].shininess );
#if ( S3D_BUMPMAPPING )
		shader->SetUniformf( "bumpAmount", 1, &Materials[matID].bumpAmount );
#endif
#endif
		int sampler = 0;
		if ( useTextures )
		{
			const char *texNames[S3D_MAX_MATERIAL_TEXTURES] = { 
				"diffuseMap", 
#if ( S3D_USE_LIGTHING )
				"specularMap", 
#if ( S3D_BUMPMAPPING )
				"normalMap",
#endif
#endif
#if ( S3D_USE_CUBEMAPS )
				"reflectionMap",
#endif
			};
			float reflectible = 0;
			for ( sampler = 0; sampler < S3D_MAX_MATERIAL_TEXTURES; sampler++ )
			{
                bool cubeMap = false;
#if ( S3D_USE_CUBEMAPS )
                cubeMap = sampler == S3D_ReflectionMap;
#endif
				if ( S3D_OpenGLOp::EnableTexture( Materials[matID].ts[ sampler ], sampler, cubeMap ) )
					reflectible = cubeMap ? 1.f : 0.f;
				shader->SetSampler( texNames[ sampler ], sampler );
			}
			shader->SetUniformf( "reflectible", 1, &reflectible );
#if ( S3D_USE_LIGTHING )
#if ( S3D_USE_SHADOWS )
			int use = 0;
			if ( s3dShadow && s3dShadow->enabled )
			{
#if ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
				S3D_OpenGLOp::EnableTexture( s3dShadow->soft ? s3dShadow->colorTexture : s3dShadow->depthTexture, sampler );
#else
				S3D_OpenGLOp::EnableTexture( s3dShadow->depthTexture, sampler );
#endif
				use = 1;
			}
			shader->SetUniformi( "useShadow", 1, &use );
			shader->SetSampler( "shadowMap", sampler );
#endif
			if ( shader->GetShaderName( S3D_FRAGMENT_SHADER ) == S3D_FRAGMENT_SHADER_ORENNAYARPHONG )
			{
				sampler = S3D_MAX_MATERIAL_TEXTURES;
#if ( S3D_USE_SHADOWS )
				sampler++;
#endif
				float roughness = 0.9f;
				float r2 = roughness * roughness;
				float A  = 1.0f - ( 0.5f * ( r2 / ( r2 + 0.33f ) ) );
				float B  = 0.45f * ( r2 / ( r2 + 0.09f ) );
				shader->SetUniformf( "A", 1, &A );
				shader->SetUniformf( "B", 1, &B );

				s3dSetLookupMap( sampler );
				shader->SetSampler( "lookupMap", sampler );
			}
#endif
		}
	}
#if ( !S3D_OGLES_2_0 )
	else
	{
		S3D_Material *mat = &Materials[matID];
		S3D_OpenGLOp::SetMaterial( S3D_FRONT, mat );
		if ( !S3D_OpenGLOp::EnableTexture( mat->ts[0] ) )
			S3D_OpenGLOp::DisableTexture();
	}
#endif
}

void CS3DMaterial::DisableMaterial( int matID, CS3DShader *shader, bool useTextures )
{
	if ( !useTextures ) return;
	if ( matID < S3D_MAX_MATERIALS && Materials[matID].enabled )
	{
		if ( shader )
		{
			int sampler = 0;
			for ( sampler = 0; sampler < S3D_MAX_MATERIAL_TEXTURES; sampler++ )
			{
                bool cubeMap = false;
#if ( S3D_USE_CUBEMAPS )
                cubeMap = sampler == S3D_ReflectionMap;
#endif
				S3D_OpenGLOp::DisableTexture( sampler, cubeMap );
			}
#if ( S3D_USE_SHADOWS )
			if ( s3dShadow )
			{
				S3D_OpenGLOp::DisableTexture( sampler );
			}
			sampler++;
#endif
#if ( S3D_USE_LIGTHING )
			if ( shader->GetShaderName( S3D_FRAGMENT_SHADER ) == S3D_FRAGMENT_SHADER_ORENNAYARPHONG )
			{
				S3D_OpenGLOp::DisableTexture( sampler );
			}
#endif
		}
		S3D_OpenGLOp::DisableTexture( 0 );
	}
}