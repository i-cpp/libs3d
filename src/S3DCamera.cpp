#include <S3D.h>

CS3DCamera::CS3DCamera()
: groundLevel( 0 )
, Type( S3D_FREE_CAMERA )
{
}

CS3DCamera::~CS3DCamera()
{
}

void CS3DCamera::GroundLevel( float value )
{
	groundLevel = value;
}

float CS3DCamera::GroundLevel()
{
	return groundLevel;
}

S3D_CameraType CS3DCamera::GetType()
{
	return Type;
}

void CS3DCamera::Reset( S3D_CameraType type )
{
	Type = type;
	worldMat.identity();
	Rotation = CS3DVec3f( 0, 0, 0 );
	eyePosition = CS3DVec3f( 0, 0, 0 );
	Target = CS3DVec3f( 0, groundLevel, 0 );
}

void CS3DCamera::SetPosition( S3D_ENUM axis, float value )
{
	eyePosition[ axis ] = value;
}

void CS3DCamera::AddPosition( S3D_ENUM axis, float value )
{
	eyePosition[ axis ] += value;
}

void CS3DCamera::SetPosition( CS3DVec3f vec )
{
	eyePosition = vec;
}

void CS3DCamera::AddPosition( CS3DVec3f vec )
{
	eyePosition += vec;
}

void CS3DCamera::SetRotation( S3D_ENUM axis, float value )
{
	Rotation[ axis ] = value;
}

void CS3DCamera::AddRotation( S3D_ENUM axis, float value )
{
	Rotation[ axis ] += value;
}

CS3DMat16f CS3DCamera::GetRotationMat()
{
	CS3DMat16f rotMat;
	if ( Type == S3D_FREE_CAMERA )
	{
		//rotMat.rotate( Rotation.z, S3D_AXIS_Z );
		//rotMat.rotate( Rotation.y, S3D_AXIS_Y );
		//rotMat.rotate( Rotation.x, S3D_AXIS_X );
		rotMat.rotate( Rotation.x, S3D_AXIS_X );
		rotMat.rotate( Rotation.y, S3D_AXIS_Y );
		rotMat.rotate( Rotation.z, S3D_AXIS_Z );
	}
	else if ( Type == S3D_TARGET_CAMERA )
	{
		CS3DVec3f localTarget = Target - eyePosition;
		CS3DVec3f up( 0, 1, 0 );
		rotMat.lookAt( CS3DVec3f(), localTarget, up );
	}
	return rotMat;
}

void CS3DCamera::SetTarget( CS3DVec3f target )
{
	Target = target;
}

void CS3DCamera::Update()
{
	worldMat = GetRotationMat();
	worldMat.translate( -eyePosition );
}
