#ifndef _S3D_PHYSICS_H
#define _S3D_PHYSICS_H

/*** UNDER CONSTRUCTION ***/

#if ( S3D_USE_PHYSICS )

struct S3D_PhysicalObject {
	S3D_PhysicalObject() : handle(0) {}

	void *handle;
	CS3DMat16f mat;
	float mass;

};

void s3dInitPhysics( CS3DVec3f gravity );
void s3dSimulationPhysics();
void s3dFreePhysics();

void s3dSetAsPlayer( S3D_PhysicalObject &obj, float radius, float height, float stepHeight, float jumpSpeed, float walkVelocity, float rotationStep );
void s3dControlCharacter( int l, int r, int f, int b, int j );
void s3dAddRigidBox( S3D_PhysicalObject &obj, const CS3DVec3f &size );
void s3dUpdatePhysicalObject( S3D_PhysicalObject &obj );
void s3dProceedToTransform( S3D_PhysicalObject &obj );
CS3DVec3f s3dGetAabbSize( S3D_PhysicalObject &obj );

void s3dSetVelocity( S3D_PhysicalObject &obj, const CS3DVec3f &linear, 
	const CS3DVec3f &angular = CS3DVec3f() );
CS3DVec3f s3dGetLinearVelocity( S3D_PhysicalObject &obj );
CS3DVec3f s3dGetLinearVelocity( S3D_PhysicalObject &obj );

#endif

#endif // _S3D_PHYSICS_H
