#ifndef _S3D_CAMERA_H
#define _S3D_CAMERA_H

enum S3D_CameraType { S3D_TARGET_CAMERA, S3D_FREE_CAMERA };

class CS3DCamera
{
public:
	CS3DCamera();
	~CS3DCamera();

	void Reset( S3D_CameraType type );
	S3D_CameraType GetType();

	void SetPosition( CS3DVec3f vec );
	void AddPosition( CS3DVec3f vec );
	void SetPosition( S3D_ENUM axis, float value );
	void AddPosition( S3D_ENUM axis, float value );
	CS3DVec3f GetPosition() { return eyePosition; }

	// for Free Camera type:
	void SetRotation( S3D_ENUM axis, float value );
	void AddRotation( S3D_ENUM axis, float value );
	CS3DVec3f GetRotation() { return Rotation; }
	CS3DMat16f GetRotationMat();

	// for Target Camera type:
	void SetTarget( CS3DVec3f target );
	CS3DVec3f GetTarget() { return Target; }

	void Update();
	const CS3DMat16f &Get() { return worldMat; }

	void GroundLevel( float value );
	float GroundLevel();

private:
	CS3DMat16f worldMat;
	CS3DVec3f Rotation;
	CS3DVec3f eyePosition;
	CS3DVec3f Target;
	float groundLevel;
	S3D_CameraType Type;

};

#endif // _S3D_CAMERA_H
