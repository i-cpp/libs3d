#ifndef _S3D_MATERIAL_H
#define _S3D_MATERIAL_H

enum S3D_MaterialTexture {
	S3D_DiffuseMap,
#if ( S3D_USE_LIGTHING )
	S3D_SpecularMap,
#if ( S3D_BUMPMAPPING )
	S3D_NormalMap,
#endif
#endif
#if ( S3D_USE_CUBEMAPS )
	S3D_ReflectionMap,
#endif
    
	S3D_MAX_MATERIAL_TEXTURES
};

struct S3D_Material {
	unsigned int ts[S3D_MAX_MATERIAL_TEXTURES];
	S3D_Color ambient;
	S3D_Color diffuse;
	S3D_Color specular;
	S3D_Color emission;
	float shininess;
#if ( S3D_BUMPMAPPING )
	float bumpAmount;
#endif
	float diffuseLevel;
	bool enabled;
};

typedef unsigned short S3D_Index;
//typedef unsigned int S3D_Index;
typedef std::vector< S3D_Index > S3D_Indices;

#define S3D_MAX_TRIANGLES ((S3D_Index)~0 / 3)

struct S3D_MatIDs {
	S3D_MatIDs();
	~S3D_MatIDs();
	S3D_Index *IDs;
	unsigned int count;
	unsigned int buffID;
};

class CS3DMaterial
{
public:
	CS3DMaterial();
	~CS3DMaterial();

	void MaterialTexture( int matID, S3D_MaterialTexture mt, unsigned int texID );
	unsigned int MaterialTexture( int matID, S3D_MaterialTexture mt );

protected:
	void LoadMaterialFromMesh( const char *filename, unsigned int numFaces );
	void PushMaterial( S3D_Material *material );
	void EnableMaterial( int matID, CS3DShader *shader, bool useTextures = true );
	void DisableMaterial( int matID, CS3DShader *shader, bool useTextures = true );
	int NumMaterials();
	void SetDiffuseLevel( int matID, float level );

	S3D_MatIDs *MatIDs;

private:
	S3D_Material Materials[S3D_MAX_MATERIALS];

};

#endif // _S3D_MATERIAL_H
