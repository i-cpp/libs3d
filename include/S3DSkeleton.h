#ifndef _S3D_SKELETON_H
#define _S3D_SKELETON_H

typedef float S3D_Weight;
typedef short S3D_BoneIndex;

struct S3D_Bone {
	S3D_Bone() : parent( 0 ), name( 0 ) {}
	~S3D_Bone()
	{
		if ( name ) free( name );
	}
	short index;
	char *name;
	S3D_Bone *parent;
	S3D_Vertex joint;
	S3D_Quat orient;
	CS3DMat16f transfMat;
};

struct S3D_Skeleton {
	S3D_Skeleton();
	~S3D_Skeleton();

	bool LoadSkeleton( const char *filename );
	CS3DVec3f GetTransformedVertex( int vertID, const S3D_Vertex &base );
#if ( S3D_USE_LIGTHING )
	CS3DVec3f GetTransformedNormal( int vertID, const S3D_Normal &base );
#endif
	S3D_Bone *GetBoneByName( const char *boneName, bool substr = false );

	unsigned int numBones;
	unsigned int numWeights;

	S3D_Weight **weights;
	S3D_BoneIndex **indices;
	S3D_Bone *bones;

	unsigned int weightBuffer;
	unsigned int indexBuffer;
};

#endif // _S3D_SKELETON_H