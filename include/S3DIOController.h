#ifndef _S3D_BREADER_H
#define _S3D_BREADER_H

class CS3DIOController
{
public:
	CS3DIOController();
	~CS3DIOController();

	void SetIOFunctions( void *(*open_func) ( const char *filename ),
						 bool  (*read_func) ( void* io, void *data, unsigned int size ),
						 void  (*close_func)( void* io ),
						 void  (*log_func) ( const char *msg ),
						 void  (*delete_func) ( const char *filename ) );

	void *OpenFile( const char *filename );
	void CloseFile( void* io );
	void PrintLog( const char *msg );

	void DeleteFile( const char *filename );

	unsigned char ReadByte();
	void ReadBlock( unsigned char *buffer, int size );
	int ReadInt();
	short ReadShort();
	unsigned short ReadUShort();
	float ReadFloat();
	CS3DVec3f ReadVec3f();
	S3D_Quat ReadQuat();
	bool Eof() { return eof; }
	int GetCurPos() { return pos; }

private:
	void *(*Open)( const char *filename );
	bool  (*Read)( void* io, void *data, unsigned int size );
	void  (*Close)( void* io );
	void  (*Log)( const char *msg );
	void  (*Delete)( const char *filename );

	int ToInt( const char* bytes );
	short ToShort( const char* bytes );
	unsigned short ToUShort( const char* bytes );
	float ToFloat( const char* bytes );

	void * IOs[ S3D_MAX_IO ];
	bool eof;
	int pos;

};

#endif // _S3D_BREADER_H
