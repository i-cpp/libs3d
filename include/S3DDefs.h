#ifndef _S3D_DEFS_H
#define _S3D_DEFS_H

// Platform definitions
#if defined( IW_SDK ) // Marmalade SDK
#	define S3D_MARMALADE_SDK
#	define S3D_OGLES_2_0 1
#else // X11 Platform
#	if defined( __linux__ )
#		define S3D_X11_PLATFORM
#		define S3D_OGLES_2_0 0
#	else
#		if defined( ANDROID ) // Android Platform
#			define S3D_ANDROID_PLATFORM
#			define S3D_OGLES_2_0 1
#		else
#			if defined( _WIN32 ) // Windows Platform
#				define S3D_WINDOWS_PLATFORM
#				define S3D_OGLES_2_0 0
#			else
#               if defined( __APPLE__ ) // Apple Platform
#                   include <TargetConditionals.h>
#                   if defined( __IPHONE__ ) || ( defined( TARGET_OS_IPHONE ) && TARGET_OS_IPHONE ) || ( defined( TARGET_IPHONE_SIMULATOR ) && TARGET_IPHONE_SIMULATOR )
#                       define S3D_IPHONE_PLATFORM
#                       define S3D_OGLES_2_0 1
#                   elif defined( TARGET_OS_MAC )
#                       define S3D_MAC_PLATFORM
#                       define S3D_OGLES_2_0 0
#                   endif
#				endif
#			endif
#		endif
#	endif
#endif

#define S3D_VERSION		"1.0.0"

#define S3D_UNDEFINED (unsigned)(~0)

#define S3D_USE_PHYSICS 0
#define S3D_USE_SHADERS 1
#define S3D_USE_BUFFERS 1
#if ( !S3D_OGLES_2_0 )
#	define S3D_USE_STDIO 1
#else
#	define S3D_USE_STDIO 0
#endif

#define S3D_INFINITY 1E9
#define S3D_EPS 0.001f
#define S3D_BONE_AFFECT_LIMITED 4
#define S3D_MAX_BONES "90"

#define S3D_USE_LIGTHING 1
#define S3D_MAX_LIGHTS 1

#if ( S3D_USE_LIGTHING )
#	define S3D_USE_SHADOWS 1
#	define S3D_USE_SPECULAR 1
#	define S3D_BUMPMAPPING 1
#	define S3D_USE_CUBEMAPS 1
#endif
#define S3D_MAX_MATERIALS 10
#define S3D_TEXTURE_FILE_FORMAT ".png"

#define S3D_DEPTH_TEXTURE (-1)
#define S3D_FLOAT_TEXTURE (-2)

#define S3D_MAX_IO 10
#define S3D_LOG_FILE "s3d.log"

#define S3D_ENUM	int

#define S3D_VENDOR			0x1F00
#define S3D_RENDERER		0x1F01
#define S3D_OGL_VERSION		0x1F02
#define S3D_OGL_EXTENSIONS	0x1F03

#define S3D_AXIS_X	0
#define S3D_AXIS_Y	1
#define S3D_AXIS_Z	2

#define S3D_MAX_SAMPLERS 8
#define S3D_MAX_ATTRIBS 8
#define S3D_VERTEX_SHADER 0x8B31
#define S3D_FRAGMENT_SHADER 0x8B30

#define S3D_VERTEX_SHADER_SIMPLE			0x001
#define S3D_VERTEX_SHADER_COLORED_MESH		0x002
#define S3D_VERTEX_SHADER_MESH				0x003
#define S3D_VERTEX_SHADER_SKIN				0x004
#if ( S3D_USE_SHADOWS )
#define S3D_VERTEX_SHADER_DEPTH_TEXTURE		0x005
#endif
#define S3D_VERTEX_SHADER_FUR				0x006
#define S3D_VERTEX_SHADER_SPRITE			0x007

#define S3D_FRAGMENT_SHADER_SIMPLE			0x101
#define S3D_FRAGMENT_SHADER_COLORED_LAMBERT	0x102
#define S3D_FRAGMENT_SHADER_LAMBERT			0x103
#define S3D_FRAGMENT_SHADER_LAMBERTPHONG	0x104
#define S3D_FRAGMENT_SHADER_ORENNAYARPHONG	0x105
#define S3D_FRAGMENT_SHADER_FUR				0x106
#define S3D_FRAGMENT_SHADER_DEPTH_TEXTURE	0x107
#define S3D_FRAGMENT_SHADER_SPRITE			0x108
#define S3D_FRAGMENT_SHADER_GAUSS_BLUR		0x10A

#define S3D_HARD_SHADOW 0x0000
#define S3D_SOFT_SHADOW_VSM 0x0001

#define S3D_SHADOW_ALGORITHM S3D_SOFT_SHADOW_VSM

#define S3D_PI 3.1415926535897932384626433832795f

#define S3D_FOVY 25.0f
#define S3D_NEAR 10.0f
#define S3D_FAR 100000.0f

#define S3D_MIX( x1, x2, t ) ( x1 * ( 1 - t ) + x2 * t )
#define S3D_ISSPASE( ch ) ( ch == 0x20 || ( ch >= 0x09 && ch <= 0x0d ) )
#define S3D_ABS( val ) ( (val) < 0 ? -(val) : (val) )

#define S3D_GET_FUNC( type, func, name ) \
	if ( !func && s3dGetProcAddress ) func = (type)s3dGetProcAddress( name ); \
	if ( !func )\
	{\
		s3dIOController->PrintLog( "Error: Can not load function " );\
		s3dIOController->PrintLog( name );\
		s3dIOController->PrintLog( "!\n" );\
	}

#define S3D_POINTS			0x0000
#define S3D_LINES			0x0001
#define S3D_LINE_LOOP		0x0002
#define S3D_LINE_STRIP		0x0003
#define S3D_TRIANGLES		0x0004
#define S3D_TRIANGLE_STRIP	0x0005

#define S3D_FRONT	0x0404
#define S3D_BACK	0x0405

#ifdef S3D_WINDOWS_PLATFORM
#	define S3D_SLASH '\\'
#else
#	define S3D_SLASH '/'
#endif

#define S3D_RAND( a, b ) ( ( ( b - a ) * ( (float)rand() / RAND_MAX ) ) + a )

#endif // _S3D_DEFS_H
