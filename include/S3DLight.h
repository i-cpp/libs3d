#ifndef _S3D_LIGTH_H
#define _S3D_LIGTH_H

struct S3D_Light {
	bool enabled;
	S3D_Color ambient;
	S3D_Color diffuse;
	S3D_Color specular;
	CS3DVec3f pos;
	CS3DMat16f mat;

};

class CS3DLight
{
public:
	CS3DLight();
	~CS3DLight();

	void EnableLighting();
	void DisableLighting();
	void LoadLight( const char *filename );
	void SetLight( S3D_Light *light, unsigned int id = 0 );
	S3D_Light *GetLight( int id );
	int NumLights();

private:
	S3D_Light LightSources[ S3D_MAX_LIGHTS ];

};

#endif // _S3D_LIGTH_H
