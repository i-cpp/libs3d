#ifndef _S3D_MAT16_H
#define _S3D_MAT16_H

class CS3DMat16f
{
public:
    CS3DMat16f();

    float &operator[]( int );
    float operator[]( int ) const;
    CS3DMat16f operator*( const CS3DMat16f &other ) const;
    CS3DVec3f operator*( const CS3DVec3f &v ) const;
    CS3DVec4f operator*( const CS3DVec4f &v ) const;
	struct S3D_BoundingBox operator*( const struct S3D_BoundingBox &bb ) const;

 	void set( const float matrix[16] );
	void setRow( int row, float *v, int rank );
	float *getRow( int row );

	void simpleInverse(); /* if this is a simple matrix which only contains a translation and a rotation */
	void transpose();
	bool inverse();

	void identity();
	void translate( const CS3DVec3f &v );
	void rotate( float angle, S3D_ENUM axis );
	void scale( const CS3DVec3f &v );

	CS3DVec3f multVecWithoutTranslate( const CS3DVec3f in );

	void getNormalMat( float matrix[9] ); /*if this model view matrix*/

	void lookAt( CS3DVec3f eye, CS3DVec3f target, CS3DVec3f up );
	void frustum( float left, float right, float bottom, float top, float nearZ, float farZ );
	void ortho( float left, float right, float bottom, float top, float nearZ, float farZ );
	CS3DVec3f eulerXYZ();
    CS3DVec3f translation();

private:
	float m[16];
};

#endif // _S3D_MAT16_H
