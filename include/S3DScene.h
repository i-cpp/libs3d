#ifndef _S3D_SCENE_H
#define _S3D_SCENE_H

class CS3DScene
{
public:
	CS3DScene();
	~CS3DScene();

	bool IsInit();
	void Init( float **polys, int size[], int num_polys );
	void SetEnds( float x1, float y1, float x2, float y2 );
	void FindPath( float &x, float &y );
	void DrawWalls();
	bool Hover( float x, float y, float *pt = 0, float max_len = 1.0f );

private:
	void ProcessCorner( float *p0, float *p1, float *p );
	void ProcessPair( int i, int j );
	bool IsClearLine( float x1, float y1, float x2, float y2 );

	struct SceneDef *Scene;
	bool bInit;

};

#endif // _S3D_SCENE_H
