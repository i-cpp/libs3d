#ifndef _S3D_VECTOR3_H
#define _S3D_VECTOR3_H

class CS3DVec3f
{
public:
    CS3DVec3f();
    CS3DVec3f( float scalar );
    CS3DVec3f( float x, float y, float z );
	void set( float *values );

    float &operator[]( int index );
    float operator[]( int index ) const;

    CS3DVec3f operator+( float scalar ) const;
    CS3DVec3f operator-( float scalar ) const;
    CS3DVec3f operator*( float scalar ) const;
    CS3DVec3f operator/( float scalar ) const;

    CS3DVec3f operator+( const CS3DVec3f &other ) const;
    CS3DVec3f operator-( const CS3DVec3f &other ) const;
    CS3DVec3f operator*( const CS3DVec3f &other ) const;
    CS3DVec3f operator/( const CS3DVec3f &other ) const;
    CS3DVec3f operator^( const CS3DVec3f &other ) const;

    CS3DVec3f operator-() const;

    bool operator==( const CS3DVec3f &other ) const;
    bool operator!=( const CS3DVec3f &other ) const;
    bool operator!() const;

    const CS3DVec3f &operator+=( float scalar );
    const CS3DVec3f &operator-=( float scalar );
    const CS3DVec3f &operator*=( float scalar );
    const CS3DVec3f &operator/=( float scalar );

    const CS3DVec3f &operator+=( const CS3DVec3f &other );
    const CS3DVec3f &operator-=( const CS3DVec3f &other );
    const CS3DVec3f &operator*=( const CS3DVec3f &other );
    const CS3DVec3f &operator/=( const CS3DVec3f &other );

    float magnitude() const;
    float magnitudeSquared() const;
    float dot( const CS3DVec3f &other ) const;
    void normalize();
    CS3DVec3f maximize( const CS3DVec3f &other );
    CS3DVec3f minimize( const CS3DVec3f &other );

	union {
		float v[3];
		struct { float x; float y; float z; };
	};
};

#endif // _S3D_VECTOR3_H
