#ifndef _S3D_ANIMOBJ3D_H
#define _S3D_ANIMOBJ3D_H

enum S3D_AnimType {
	SkinAnimation = 0x53
};

typedef std::vector< unsigned int > S3D_PlayList;

struct S3D_Transform {
	CS3DVec3f position;
	S3D_Quat rotation;
};

namespace S3D_TransformOp {
	void to_matrix( CS3DMat16f &m, const S3D_Transform &t );
	void from_matrix( S3D_Transform &t, const CS3DMat16f &m );
};

struct S3D_Animation {
	S3D_Animation() : Shader( 0 ) {}

	S3D_AnimType Type;
	unsigned int numFrames;
	unsigned int frameRate;

	S3D_PlayList PlayList;
	CS3DShader *Shader;

//	skeleton animation spec.:
	S3D_Transform **Transforms;
	S3D_Transform **BaseTransforms;
};

class CS3DAnimObject
{
public:
	CS3DAnimObject();
	virtual ~CS3DAnimObject();
	
	bool SetAnimation( unsigned int id, bool smooth = true );
	unsigned int AnimationCount();
	unsigned int AnimationId() { return CurAnimation.curID; }

	void BuildPlaylist( S3D_PlayList &new_list );
	int CurrentFrame() { return CurFrame.curID; }
	int NumFrames() { return Animations[ CurAnimation.curID ]->numFrames; }
	void ChangeFrameRate( unsigned int animID, int frameRate );
	
	// skeleton animation spec. ->
	void ClearBoneTransforms();
	bool GetBoneTransformByID( S3D_BoneIndex boneID, CS3DMat16f &transforms );
	void AddTransformToBoneByID( S3D_BoneIndex boneID, CS3DMat16f &transforms );
	void AddTransformToBoneByName( const char *boneName, CS3DMat16f &transforms, bool substr = false );
	void GetCurrentTransform( S3D_BoneIndex boneID, S3D_Transform &transform );
	// skeleton animation spec. <-

protected:
	bool LoadAnimation( const char *filename, S3D_Header &Header, unsigned int mixedWithAnimID );
	void UpdateAnimation();

	struct S3D_BlendedObject { 
		int curID, nextID; 
		unsigned int tStarted;
		float time;
	};
	S3D_BlendedObject CurFrame, CurAnimation;

	//struct S3D_CurrentAnimation { int ID; S3D_Animation *anim; };
	//S3D_CurrentAnimation CurAnimation;

	std::vector< S3D_Animation * > Animations;

	// skeleton animation spec. ->
	void MixWith( S3D_Animation *dst, S3D_Animation *src );
	void CalcTransformMatrices();
	void DrawSkeleton( CS3DMat16f &modelViewMat, bool Absolute, CS3DBox &box );
	void SetTransformsToSkinShader( CS3DShader *shader );

	S3D_Skeleton *Skeleton;
	S3D_Body *Transformed;
	// skeleton animation spec. <-

private:
	// skeleton animation spec. ->
	void ResetTransforms();
	void ApplyAuxTransforms();
	void ApplyTransformToBoneByID( unsigned int animID, S3D_BlendedObject &blendedObject, S3D_BoneIndex boneID, CS3DMat16f &transforms );
	void BoneTransform( unsigned int animID, S3D_Bone *bone, S3D_Transform *transform, int frame );
	void MixTransformWith( S3D_Transform t, S3D_BoneIndex boneID, S3D_Transform &result );

	struct S3D_AuxBoneTransform { S3D_BoneIndex boneID; CS3DMat16f mat; };
	std::vector< S3D_AuxBoneTransform > auxBoneTransforms;
	// skeleton animation spec. <-

	void CalcFrameID( unsigned int animID, S3D_BlendedObject &frame );

	S3D_BlendedObject NextAnimFrame;

	unsigned int tBlendStarted;
	unsigned int tBlendTime;

};

#endif // _S3D_ANIMOBJ3D_H
