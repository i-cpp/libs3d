#ifndef _S3D_MESH_H
#define _S3D_MESH_H

struct S3D_TexCoord { float S; float T; };

typedef CS3DVec3f S3D_Vertex;
#if ( S3D_USE_LIGTHING )
	typedef CS3DVec3f S3D_Normal;
#	if ( S3D_BUMPMAPPING )
		typedef CS3DVec4f S3D_Tangent;
#	endif
#endif

struct S3D_Header {
	unsigned int numVerts;
	unsigned int numTVerts;
	unsigned int numFaces;
};

struct S3D_Body {
	S3D_Vertex vertex;
	S3D_TexCoord tcoord;
#if ( S3D_USE_LIGTHING )
	S3D_Normal normal;
#if ( S3D_BUMPMAPPING )
	S3D_Tangent tangent;
#endif
#endif
};

class CS3DBox;
class CS3DMesh : public CS3DMaterial,
				 public CS3DTransformedObject
{
public:
	CS3DMesh();
	virtual ~CS3DMesh();
	virtual unsigned int TrianglesCount() = 0;
	void GetBody( S3D_Body **body, S3D_Header &header );
	int LoadMesh( const char *filename );
	void DrawMesh();
	void GetOriginalBox( CS3DBox &box );

protected:

	S3D_Header Header;
	S3D_Index *Faces;
	
	bool Textured;
	S3D_Body *Body;
	unsigned int Buffer;
	std::vector< unsigned int > VertexArray;
	unsigned int MeshSize;
	CS3DShader *MeshShader;
	CS3DBox *originalBox;
	struct S3D_BBList *BBList;
	unsigned int BBLevel;
	unsigned int TriangleType;

private:
#if ( S3D_USE_LIGTHING )
#if ( S3D_BUMPMAPPING )
	void CalculateTangents();
#endif
#endif
	void BuildBuffers();
};

#endif // _S3D_MESH_H