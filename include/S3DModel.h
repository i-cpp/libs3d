#ifndef _S3D_MODEL_H
#define _S3D_MODEL_H

#include <S3DDefs.h>
#include <S3DBuffer.h>
#include <S3DShader.h>
#include <S3DVec3f.h>
#include <S3DVec4f.h>
#include <S3DMat16f.h>
#include <S3DCamera.h>
#include <S3DLight.h>
#include <S3DMaterial.h>
#include <S3DIOController.h>
#include <S3DGaussianBlur.h>
#include <S3DShadow.h>
#include <S3DScene.h>
#include <S3DTransformedObject.h>
#include <S3DMesh.h>
#include <S3DBox.h>
#include <S3DSkeleton.h>
#include <S3DAnimObject.h>
#include <S3DPhysics.h>
#include <S3DFur.h>

class CS3DModel : public CS3DMesh, 
				  public CS3DAnimObject
{
public:

#if ( S3D_USE_LIGTHING )
	friend class CS3DFur;
	friend class CS3DWhiskers;
#endif

	CS3DModel();
	~CS3DModel();

	void Load( const char *filename );
	void Draw();
#if ( S3D_USE_SHADOWS )
	void DrawShadow( const CS3DMat16f &lmvMat, const CS3DMat16f &lpMat );
#endif

	bool CollisionModel( CS3DModel *otherModel );
	S3D_BBList *GetBBList();
	S3D_BoundingBox GetBBox();
	S3D_BoundingBox GetBBoxTransformed();
	bool Hover( int x, int y, S3D_TexCoord *tc = 0 );
	void BackFace( bool is_discarded );
	const CS3DMat16f *GetMVP() { return &mvpMat; }
	const CS3DMat16f *GetMV() { return &modelViewMat; }
	unsigned int TrianglesCount();
	S3D_Skeleton *GetSkeleton();
	const char *GetName();
	const char *GetSelectedBoneName();
	bool Animated();

	void DrawSkeleton();
	void DrawBBoxes( bool parent = false );

private:
	void DrawAnimation();
	void ParseFilename( const char *filename );

	bool DisableBackFace;
	CS3DMat16f modelViewMat;
	CS3DMat16f mvpMat;
	char *Name, *Path;
	const char *SelectedBoneName;
	unsigned int reflectTexture;

#if ( S3D_USE_LIGTHING )
	CS3DFur *Fur;
#endif

};

#endif // _S3D_MODEL_H
