#ifndef _S3D_BBOX_H
#define _S3D_BBOX_H

struct S3D_BoundingBox {
    CS3DVec3f max;
    CS3DVec3f min; 
};

struct S3D_Ray {
    CS3DVec3f origin;
    CS3DVec3f direct; 
};

class CS3DBox : public CS3DTransformedObject
{
public:
	CS3DBox();
	~CS3DBox();
	
	bool IsInit();
	void Reset();
	void Set( CS3DVec3f & );
	void Set( S3D_BoundingBox & );
	void SetColor( S3D_Color new_color ) { color = new_color; }
	void SetFillColor( S3D_Color new_color ) { fillColor = new_color; }
	void SetFillStyle( bool fill ) { fillStyle = fill; }

	bool CollisionPoint( CS3DVec3f & );
	bool CollisionBBox( S3D_BoundingBox &other_bbox );
	bool CollisionRay( S3D_Ray &ray );

	void Draw();
#if ( S3D_USE_LIGTHING )
#if ( S3D_USE_SHADOWS )
	void DrawShadow( const CS3DMat16f &lmvMat, const CS3DMat16f &lpMat );
#endif
#endif

	void SetSubTransforms( CS3DMat16f mat );
	S3D_BoundingBox GetBBoxTransformed();
	S3D_BoundingBox GetBBox() { return bbox; }

	CS3DVec3f Size() { return bbox.max - bbox.min; }
	CS3DVec3f Center() { return bbox.min + ( bbox.max - bbox.min ) * 0.5f; }
	
private:
	S3D_BoundingBox bbox;
	S3D_Color color;
	CS3DMat16f mvpMat;
	CS3DMat16f subTransfMat;
	bool init;

	bool fillStyle;
	S3D_Color fillColor;
};

struct S3D_BBNode {
	S3D_BBNode() : IDs( 0 ) {}
	CS3DBox bbox;
	S3D_Indices *IDs;
};

struct S3D_BBList {
	S3D_BBList() : nodes( 0 ), numNodes( 0 ) {}
	~S3D_BBList() { Delete(); }
	S3D_BBNode** nodes;
	unsigned numNodes;
	CS3DBox parent;
	void Create( int Level, S3D_Body *body, S3D_Index *faces, int numFaces, bool dynamic = false );
	void Delete();
};

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#endif // _S3D_BBOX_H
