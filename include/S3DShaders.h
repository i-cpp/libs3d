//////////////////////////////////////////////////////////////////////////////
// Vertex definitions ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

#define S3D_DEF_MAX_BONES \
	"#define MAX_BONES " \
	S3D_MAX_BONES \
	"\n"

#if ( S3D_USE_SHADOWS )
#	define S3D_SHADOW_VSHHEADER \
	"uniform mat4 lMat;\n" \
	"varying vec2 shadowTexCoord;\n" \
	"varying float distToLight;\n"
#	define S3D_SHADOW_VSHBODY \
	"	vec4 smcoord = lMat * vec4( mvPos, 1.0 );\n" \
	"	float invW = 1.0 / smcoord.w;\n" \
	"	shadowTexCoord = smcoord.xy * invW;\n" \
	"	distToLight = smcoord.z * invW;\n" \
	"	//shadowTexCoord = smcoord.xy;\n" \
	"	//distToLight = smcoord.z;\n"
#else
#	define S3D_SHADOW_VSHHEADER ""
#	define S3D_SHADOW_VSHBODY ""
#endif

#if ( S3D_BUMPMAPPING )
#	define S3D_BUMPMAPPING_VSHHEADER \
		"attribute vec4 tangent;\n" \
		"varying vec3 t;\n" \
		"varying vec3 b;\n"
#	define S3D_BUMPMAPPING_VSHBODY0 \
		"	vec3 mTangent = vec3( 0.0, 0.0, 0.0 );\n"
#	define S3D_BUMPMAPPING_VSHBODY1( cur ) \
		"		mTangent += weights" cur " * quatRotate( tangent.xyz, rotation[ boneID ] );\n"
#	define S3D_BUMPMAPPING_VSHBODY0_1 \
		"	t = normalize( nMat * tangent.xyz );\n" \
		"	b = cross( n, t ) * -tangent.w;\n"
#	define S3D_BUMPMAPPING_VSHBODY2 \
		"	t = normalize( nMat * mTangent );\n" \
		"	b = cross( n, t ) * -tangent.w;\n"
#else
#	define S3D_BUMPMAPPING_VSHHEADER ""
#	define S3D_BUMPMAPPING_VSHBODY0 ""
#	define S3D_BUMPMAPPING_VSHBODY1( cur ) ""
#	define S3D_BUMPMAPPING_VSHBODY0_1 ""
#	define S3D_BUMPMAPPING_VSHBODY2 ""
#endif

#if ( S3D_USE_LIGTHING )
#	define S3D_LIGTHING_VSHHEADER \
	"attribute vec3 normal;\n" \
	"uniform mat4 mvMat;\n" \
	"uniform mat3 nMat;\n" \
	"varying vec3 mvPos;\n" \
	"varying vec3 n; // Normal\n"

#	define S3D_LIGTHING_VSHBODY0 \
	"	vec3 mNormal = vec3( 0.0, 0.0, 0.0 );\n"
#	define S3D_LIGTHING_VSHBODY1( cur ) \
	"		mNormal += weights" cur " * quatRotate( normal, rotation[ boneID ] );\n"
#	define S3D_LIGTHING_VSHBODY2 \
	"	mvPos = vec3( mvMat * vec4( mPos, 1.0 ) );\n" \
	"	n = normalize( nMat * mNormal );\n"
#	define S3D_LIGTHING_VSHBODY0_1 \
	"	mvPos = vec3( mvMat * vec4( vertex, 1.0 ) );\n" \
	"	n = normalize( nMat * normal );\n"
#else
#	define S3D_LIGTHING_VSHHEADER ""
#	define S3D_LIGTHING_VSHBODY0 ""
#	define S3D_LIGTHING_VSHBODY1( cur ) ""
#	define S3D_LIGTHING_VSHBODY2 ""
#	define S3D_LIGTHING_VSHBODY0_1 ""
#endif

#define S3D_SKIN_ANIMATION_VSHHEADER \
	"attribute vec4 weights;\n" \
	"attribute vec4 indices;\n" \
	"uniform vec3 position[ MAX_BONES ];\n" \
	"uniform vec4 rotation[ MAX_BONES ];\n"

#define S3D_SKIN_ANIMATION_APPLYTRANSFORM( cur ) \
	"	if ( weights" cur " > 0.001 )\n" \
	"	{\n" \
	"		boneID = int( indices" cur " );\n" \
	"		mPos	 += weights" cur " * ( quatRotate( vertex, rotation[ boneID ] ) + position[ boneID ] );\n" \
			S3D_LIGTHING_VSHBODY1( cur ) \
			S3D_BUMPMAPPING_VSHBODY1( cur ) \
	"	}\n"
#define S3D_SKIN_ANIMATION_APPLYTRANSFORM_NO_BUMP( cur ) \
	"	if ( weights" cur " > 0.001 )\n" \
	"	{\n" \
	"		boneID = int( indices" cur " );\n" \
	"		mPos	 += weights" cur " * ( quatRotate( vertex, rotation[ boneID ] ) + position[ boneID ] );\n" \
			S3D_LIGTHING_VSHBODY1( cur ) \
	"	}\n"
#define S3D_SKIN_ANIMATION_APPLYTRANSFORM_NO_LIGHT( cur ) \
	"	if ( weights" cur " > 0.001 )\n" \
	"	{\n" \
	"		boneID = int( indices" cur " );\n" \
	"		mPos	 += weights" cur " * ( quatRotate( vertex, rotation[ boneID ] ) + position[ boneID ] );\n" \
	"	}\n"

//////////////////////////////////////////////////////////////////////////////
// Fragment definitions //////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

#define S3D_PRECISION_FLOAT \
"#ifdef GL_ES\n" \
"precision highp float;\n" \
"#endif\n"

#if ( S3D_USE_SHADOWS )
#	define S3D_DEBUG_SHADOW 0
#	if ( S3D_SHADOW_ALGORITHM == S3D_HARD_SHADOW )
#		define S3D_SHADOW_FSHHEADER \
			"uniform sampler2D shadowMap;\n" \
			"uniform int useShadow;\n" \
			"varying vec2 shadowTexCoord;\n" \
			"varying float distToLight;\n" \
			"float shadow2DCompare( in vec2 uv, in float compare )\n" \
			"{\n" \
			"    return step( compare, texture2D( shadowMap, uv ).r );\n" \
			"}\n" \
			"float shadow2DLerp( in vec2 uv, in float compare, in float size, in float texelSize )\n" \
			"{\n" \
			"    vec2 f = fract( uv * size + 0.5 );\n" \
			"    vec2 centroidUV = floor( uv * size + 0.5) * texelSize;\n" \
			"    float lb = shadow2DCompare( centroidUV, compare );\n" \
			"    float lt = shadow2DCompare( centroidUV + vec2(0.0, texelSize), compare );\n" \
			"    float rb = shadow2DCompare( centroidUV + vec2(texelSize, 0.0), compare );\n" \
			"    float rt = shadow2DCompare( centroidUV + vec2(texelSize, texelSize), compare );\n" \
			"    return mix( mix( lb, lt, f.y ), mix( rb, rt, f.y ), f.x );\n" \
			"}\n"
#	elif ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
#		define S3D_SHADOW_FSHHEADER \
			"uniform sampler2D shadowMap;\n" \
            "uniform int useShadow;\n" \
			"varying vec2 shadowTexCoord;\n" \
			"varying float distToLight;\n" \
			"vec2 texture2DLerp( in vec2 uv )\n" \
			"{\n" \
			"	 float size = 2048.0;\n" \
			"	 float texelSize = 0.00048828125;\n" \
			"    vec2 f = fract( uv * size + 0.5 );\n" \
			"    vec2 centroidUV = floor( uv * size + 0.5) * texelSize;\n" \
			"    vec2 lb = texture2D( shadowMap, centroidUV ).xy;\n" \
			"    vec2 lt = texture2D( shadowMap, centroidUV + vec2(0.0, texelSize) ).xy;\n" \
			"    vec2 rb = texture2D( shadowMap, centroidUV + vec2(texelSize, 0.0) ).xy;\n" \
			"    vec2 rt = texture2D( shadowMap, centroidUV + vec2(texelSize, texelSize) ).xy;\n" \
			"    return mix( mix( lb, lt, f.y ), mix( rb, rt, f.y ), f.x );\n" \
			"}\n" \
			"float linstep( in float low, in float high, in float v )\n" \
			"{\n" \
			"    return clamp((v - low) / (high - low), 0.0, 1.0);\n" \
			"}\n" \
			"float VSM( in vec2 uv, in float compare )\n" \
			"{\n" \
			"    float p = 0.0;\n" \
			"    vec2 moments = texture2D( shadowMap, uv ).xy;\n" \
			"    //vec2 moments = texture2DLerp( uv );\n" \
			"    if ( compare <= moments.x )\n" \
			"        p = 1.0;\n" \
			"    float variance = moments.y - moments.x * moments.x;\n" \
			"    variance = max( variance, 0.00001 );\n" \
			"    float d = compare - moments.x;\n" \
			"    float p_max = variance / ( variance + d * d );\n" \
 			"    p_max = linstep( 0.3, 1.0, p_max );\n" \
			"    return max( p, p_max );\n" \
			"}\n"
#	endif
#	if ( S3D_SHADOW_ALGORITHM == S3D_HARD_SHADOW )
#		define S3D_SHADOW_FSHBODY \
			"	float shadowFactor = useShadow != 0 ? shadow2DLerp( shadowTexCoord, distToLight - 0.001, 2048.0, 0.00048828125 ) : 1.0;\n" \
			"	sumColor.xyz *= shadowFactor;\n"
#	elif ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
#		define S3D_SHADOW_FSHBODY \
			"	float shadowFactor = useShadow != 0 ? VSM( shadowTexCoord, distToLight/* - 0.00002*/ ) : 1.0;\n" \
			"	sumColor.xyz *= shadowFactor;\n"
#	endif
#else
#	define S3D_SHADOW_FSHHEADER ""
#	define S3D_SHADOW_FSHBODY ""
#endif


#define S3D_NO_BUMP_FSHBODY \
"	nn = normalize( n );\n" \
"	nl = normalize( mvLightPos - mvPos );\n" \
"	nv = normalize( -mvPos );\n"

#if ( S3D_BUMPMAPPING )
#	define S3D_BUMPMAPPING_FSHHEADER \
	"uniform sampler2D normalMap;\n" \
	"uniform float bumpAmount;\n" \
	"varying vec3 t; // Tangent\n" \
	"varying vec3 b; // Binormal\n"
#	define S3D_BUMPMAPPING_FSHBODY \
	"	if ( bumpAmount == 0.0 )\n" \
	"	{\n" \
	"		nn = normalize( n );\n" \
	"		nl = normalize( mvLightPos - mvPos );\n" \
	"		nv = normalize( -mvPos );\n" \
	"	} else {\n" \
	"		nn = texture2D( normalMap, TexCoord ).rgb * 2.0 - 1.0;\n" \
	"		nn.xy *= bumpAmount;\n" \
	"		nn = normalize( nn );\n" \
	"		mat3 tbn = mat3( vec3(t.x, b.x, n.x), vec3(t.y, b.y, n.y), vec3(t.z, b.z, n.z) );\n" \
	"		nl = normalize( tbn * ( mvLightPos - mvPos ) );\n" \
	"		nv = normalize( tbn * -mvPos );\n" \
	"	}\n"
#else
#	define S3D_BUMPMAPPING_FSHHEADER ""
#	define S3D_BUMPMAPPING_FSHBODY S3D_NO_BUMP_FSHBODY
#endif

#if ( S3D_USE_CUBEMAPS )
#	define S3D_CUBEMAP_VSHHEADER \
		"uniform mat3 mNormalMat;\n" \
		"uniform mat3 mMat;\n" \
		"uniform vec3 wsCamPos;\n" \
		"varying vec3 ReflCoord;\n"
#	define S3D_CUBEMAP_VSHBODY \
		"	ReflCoord = reflect( normalize(mMat * vertex - wsCamPos), normalize(mNormalMat * normal) );\n"
#	define S3D_SKIN_ANIMATION_CUBEMAP_VSHBODY \
		"	ReflCoord = reflect( normalize(mMat * mPos - wsCamPos), normalize(mNormalMat * mNormal) );\n"
#	define S3D_CUBEMAP_FSHHEADER \
		"uniform samplerCube reflectionMap;\n" \
		"uniform float reflectible;\n" \
		"varying vec3 ReflCoord;\n"
#	define S3D_CUBEMAP_FSHBODY \
		"	vec4 reflection = reflectible > 0.001 ? textureCube( reflectionMap, ReflCoord ) : vec4( 1.0 );\n"
#else
#	define S3D_CUBEMAP_VSHHEADER ""
#	define S3D_CUBEMAP_VSHBODY ""
#	define S3D_SKIN_ANIMATION_CUBEMAP_VSHBODY ""
#	define S3D_CUBEMAP_FSHHEADER ""
#	define S3D_CUBEMAP_FSHBODY "	vec4 reflection = vec4(1.0, 1.0, 1.0, 1.0);\n"
#endif

#if ( S3D_USE_LIGTHING )
#		define S3D_FSHHEADER \
		"uniform vec4 lightProductAmbient;\n" \
		"uniform vec4 materialEmission;\n" \
		"uniform vec3 mvLightPos;\n" \
		"varying vec3 mvPos;\n" \
		"varying vec3 n; // Normal\n"

#		define S3D_DIFFUSE_FSHHEADER \
		"uniform vec4 lightProductDiffuse;\n"
#		define S3D_DIFFUSE_FSHBODY \
		"	sumColor.xyz += lightProductDiffuse.xyz * texel.xyz * max( dot( nn, nl ), 0.0 ) * reflection.xyz;\n"

#		define S3D_ORENNAYAR_FSHHEADER \
		"uniform sampler2D lookupMap;\n" \
		"uniform float A;\n" \
		"uniform float B;\n"
#		define S3D_ORENNAYAR_FSHBODY \
		"	vec4 diffColor = vec4( 0.0, 0.0, 0.0, 0.0 );\n" \
		"	float dotNL = dot( nn, nl );\n" \
		"	float dotNV = dot( nn, nv );\n" \
		"	if( dotNL > 0.0 )\n" \
		"	{\n" \
		"		vec3 lProj = normalize( nl - nn * dotNL );\n" \
		"		vec3 vProj = normalize( nv - nn * dotNV );\n" \
		"		float C = max( dot( lProj, vProj ), 0.0 ) * texture2D( lookupMap, vec2( dotNL, dotNV ) ).x;\n" \
		"		diffColor = dotNL * lightProductDiffuse * ( A + B * C ) * reflection;\n" \
		"	}\n" \
		"	diffColor.xyz += pow( 1.0 - max( dotNV, 0.0 ), 4.0 ); // fresnel factor\n" \
		"	sumColor.xyz += diffColor.xyz * texel.xyz;\n"

#	if ( S3D_USE_SPECULAR ) 
#		define S3D_SPECULAR_FSHHEADER \
		"uniform sampler2D specularMap;\n" \
		"uniform vec4 lightProductSpecular;\n" \
		"uniform float shininess;\n"
#		define S3D_SPECULAR_FSHBODY \
		"	vec4 specColor = lightProductSpecular * texture2D( specularMap, TexCoord ) * reflection;\n" \
		"	vec3 r = reflect( -nl, nn );\n" \
		"	specColor.xyz *= pow( max( dot( nv, r ), 0.0 ), shininess );\n" \
		"	sumColor.xyz += specColor.xyz;\n"
#	else
#		define S3D_SPECULAR_FSHHEADER ""
#		define S3D_SPECULAR_FSHBODY ""
#	endif
#endif

#define S3D_FINALLY_FSHBODY \
	"	sumColor += materialEmission + lightProductAmbient * texel * reflection;\n" \
	"	gl_FragColor = sumColor;\n"

//////////////////////////////////////////////////////////////////////////////
// Vertex shaders ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

const char *vshSimple =
"attribute vec3 vertex;\n"
"uniform mat4 mvpMat;\n"
"\n"
"void main (void)\n"
"{\n"
"	gl_Position = mvpMat * vec4( vertex, 1.0 );\n"
"}\n";

const char *vshSprite =
"attribute vec3 vertex;\n"
"attribute vec2 texcoord;\n"
"uniform mat4 mvpMat;\n"
"varying vec2 TexCoord;\n"
"\n"
"void main (void)\n"
"{\n"
"	TexCoord = texcoord;\n"
"	gl_Position = mvpMat * vec4( vertex, 1.0 );\n"
"}\n";

const char *vshFur =
S3D_DEF_MAX_BONES
"attribute vec3 vertex;\n"
S3D_LIGTHING_VSHHEADER
"attribute vec2 texcoord1;\n"
"attribute vec2 texcoord2;\n"
S3D_SKIN_ANIMATION_VSHHEADER
"uniform mat4 mvpMat;\n"
"uniform float staticFactor;\n"
"varying vec2 TexCoord1;\n"
"varying vec2 TexCoord2;\n"
"\n"
"vec3 quatRotate( in vec3 p, in vec4 q )\n" \
"{\n" \
"	vec3 qxyz = q.xyz;\n" \
"	vec3 rv = p * q.w + cross( p, qxyz );\n" \
"	rv = q.w * rv + qxyz * dot( p, qxyz ) - cross( qxyz, rv );\n" \
"	return rv;\n" \
"}\n"
"\n"
"void main (void)\n"
"{\n"
"	TexCoord1 = texcoord1;\n"
"	TexCoord2 = texcoord2;\n"
"	vec3 mPos = vertex * staticFactor;\n"
#if ( S3D_USE_LIGTHING )
"	vec3 mNormal = normal * staticFactor;\n"
#endif
"	if ( staticFactor == 0.0 )\n"
"	{\n"
"		int boneID;\n"
		S3D_SKIN_ANIMATION_APPLYTRANSFORM_NO_BUMP( ".x" )
		S3D_SKIN_ANIMATION_APPLYTRANSFORM_NO_BUMP( ".y" )
		S3D_SKIN_ANIMATION_APPLYTRANSFORM_NO_BUMP( ".z" )
		S3D_SKIN_ANIMATION_APPLYTRANSFORM_NO_BUMP( ".w" )
"	}\n"
	S3D_LIGTHING_VSHBODY2
"	gl_Position = mvpMat * vec4( mPos, 1.0 );\n"
"}\n";

const char *vshMesh =
"attribute vec3 vertex;\n"
S3D_LIGTHING_VSHHEADER
S3D_BUMPMAPPING_VSHHEADER
S3D_SHADOW_VSHHEADER
S3D_CUBEMAP_VSHHEADER
"attribute vec2 texcoord;\n"
"uniform mat4 mvpMat;\n"
"varying vec2 TexCoord;\n"
"\n"
"void main (void)\n"
"{\n"
"	TexCoord = texcoord;\n"
	S3D_LIGTHING_VSHBODY0_1
	S3D_BUMPMAPPING_VSHBODY0_1
	S3D_SHADOW_VSHBODY
	S3D_CUBEMAP_VSHBODY
"	gl_Position = mvpMat * vec4( vertex, 1.0 );\n"
"}\n";

const char *vshColoredMesh =
"attribute vec3 vertex;\n"
S3D_LIGTHING_VSHHEADER
S3D_SHADOW_VSHHEADER
"uniform mat4 mvpMat;\n"
"\n"
"void main (void)\n"
"{\n"
	S3D_LIGTHING_VSHBODY0_1
	S3D_SHADOW_VSHBODY
"	gl_Position = mvpMat * vec4( vertex, 1.0 );\n"
"}\n";

const char *vshSkin =
S3D_DEF_MAX_BONES
"attribute vec3 vertex;\n"
S3D_LIGTHING_VSHHEADER
S3D_BUMPMAPPING_VSHHEADER
S3D_SHADOW_VSHHEADER
S3D_CUBEMAP_VSHHEADER
"attribute vec2 texcoord;\n"
S3D_SKIN_ANIMATION_VSHHEADER
"uniform mat4 mvpMat;\n"
"varying vec2 TexCoord;\n"
"\n"
"vec3 quatRotate( in vec3 p, in vec4 q )\n" \
"{\n" \
"	vec3 qxyz = q.xyz;\n" \
"	vec3 rv = p * q.w + cross( p, qxyz );\n" \
"	rv = q.w * rv + qxyz * dot( p, qxyz ) - cross( qxyz, rv );\n" \
"	return rv;\n" \
"}\n"
"\n"
"void main (void)\n"
"{\n"
"	TexCoord = texcoord;\n"
"	vec3 mPos = vec3( 0.0, 0.0, 0.0 );\n"
	S3D_LIGTHING_VSHBODY0
	S3D_BUMPMAPPING_VSHBODY0
"	int boneID;\n"
	S3D_SKIN_ANIMATION_APPLYTRANSFORM( ".x" )
	S3D_SKIN_ANIMATION_APPLYTRANSFORM( ".y" )
	S3D_SKIN_ANIMATION_APPLYTRANSFORM( ".z" )
	S3D_SKIN_ANIMATION_APPLYTRANSFORM( ".w" )
	S3D_LIGTHING_VSHBODY2
	S3D_BUMPMAPPING_VSHBODY2
	S3D_SHADOW_VSHBODY
	S3D_SKIN_ANIMATION_CUBEMAP_VSHBODY
"	gl_Position = mvpMat * vec4( mPos, 1.0 );\n"
"}\n";

#if ( S3D_USE_LIGTHING )
#if ( S3D_USE_SHADOWS )
const char *vshDepthTexture =
S3D_DEF_MAX_BONES
"attribute vec3 vertex;\n"
S3D_SKIN_ANIMATION_VSHHEADER
"uniform mat4 mvpMat;\n"
"uniform int Animated;\n"
#if ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
"varying float depth;\n"
#endif
"\n"
"vec3 quatRotate( in vec3 p, in vec4 q )\n" \
"{\n" \
"	vec3 qxyz = q.xyz;\n" \
"	vec3 rv = p * q.w + cross( p, qxyz );\n" \
"	rv = q.w * rv + qxyz * dot( p, qxyz ) - cross( qxyz, rv );\n" \
"	return rv;\n" \
"}\n"
"\n"
"void main(void)\n"
"{\n"
"	vec3 mPos = vec3( 0.0, 0.0, 0.0 );\n"
"	if ( Animated == 1 )\n"
"	{\n"
"		int boneID;\n"
		S3D_SKIN_ANIMATION_APPLYTRANSFORM_NO_LIGHT( ".x" )
		S3D_SKIN_ANIMATION_APPLYTRANSFORM_NO_LIGHT( ".y" )
		S3D_SKIN_ANIMATION_APPLYTRANSFORM_NO_LIGHT( ".z" )
		S3D_SKIN_ANIMATION_APPLYTRANSFORM_NO_LIGHT( ".w" )
"	}\n"
"	else mPos = vertex;\n"
"	gl_Position = mvpMat * vec4( mPos, 1.0 );\n"
#if ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
"	//depth = gl_Position.z / gl_Position.w * 0.5 + 0.5;\n"
"	depth = gl_Position.z * 0.5 + 0.5;\n"
#endif
"}\n";
#endif
#endif

//////////////////////////////////////////////////////////////////////////////
// Fragment shaders //////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

const char *fshOrenNayarPhong =
S3D_PRECISION_FLOAT
"uniform sampler2D diffuseMap;\n"
"varying vec2 TexCoord;\n"
#if ( S3D_USE_LIGTHING )
S3D_BUMPMAPPING_FSHHEADER
S3D_DIFFUSE_FSHHEADER
S3D_ORENNAYAR_FSHHEADER
S3D_SPECULAR_FSHHEADER
S3D_SHADOW_FSHHEADER
S3D_CUBEMAP_FSHHEADER
S3D_FSHHEADER
#endif
"\n"
"void main (void)\n"
"{\n"
"	vec4 texel = texture2D( diffuseMap, TexCoord );\n"
#if ( S3D_USE_LIGTHING )
"	vec3 nn, nv, nl;\n"
"	vec4 sumColor = vec4( 0.0, 0.0, 0.0, 0.0 );\n"
	S3D_BUMPMAPPING_FSHBODY
	S3D_CUBEMAP_FSHBODY
	S3D_ORENNAYAR_FSHBODY
	S3D_SPECULAR_FSHBODY
	S3D_SHADOW_FSHBODY
	S3D_FINALLY_FSHBODY
#if ( S3D_DEBUG_SHADOW )
"	gl_FragColor = vec4( vec3( shadowFactor ), 1.0 );\n"
#endif
#else
"	gl_FragColor = texel;\n"
#endif
"}\n";

const char *fshLambert =
S3D_PRECISION_FLOAT
"uniform sampler2D diffuseMap;\n"
"varying vec2 TexCoord;\n"
#if ( S3D_USE_LIGTHING )
S3D_BUMPMAPPING_FSHHEADER
S3D_DIFFUSE_FSHHEADER
S3D_SHADOW_FSHHEADER
S3D_CUBEMAP_FSHHEADER
S3D_FSHHEADER
#endif
"\n"
"void main (void)\n"
"{\n"
"	vec4 texel = texture2D( diffuseMap, TexCoord );\n"
#if ( S3D_USE_LIGTHING )
"	vec3 nn, nv, nl;\n"
"	vec4 sumColor = vec4( 0.0, 0.0, 0.0, 0.0 );\n"
	S3D_BUMPMAPPING_FSHBODY
	S3D_CUBEMAP_FSHBODY
	S3D_DIFFUSE_FSHBODY
	S3D_SHADOW_FSHBODY
	S3D_FINALLY_FSHBODY
#if ( S3D_DEBUG_SHADOW )
"	gl_FragColor = vec4( vec3( shadowFactor ), 1.0 );\n"
#endif
#else
"	gl_FragColor = texel;\n"
#endif
"}\n";

const char *fshColoredLambert =
S3D_PRECISION_FLOAT
#if ( S3D_USE_LIGTHING )
S3D_SHADOW_FSHHEADER
S3D_FSHHEADER
"uniform vec4 lightProductDiffuse;\n"
#endif
"\n"
"void main (void)\n"
"{\n"
#if ( S3D_USE_LIGTHING )
"	vec3 nn, nl;\n"
"	nn = normalize( n );\n" \
"	nl = normalize( mvLightPos - mvPos );\n" \
"	vec4 sumColor = vec4( lightProductDiffuse.rgb * max( dot( nn, nl ), 0.0 ), lightProductDiffuse.a );\n"
	S3D_SHADOW_FSHBODY
"	gl_FragColor = lightProductAmbient + sumColor;\n"
#if ( S3D_DEBUG_SHADOW )
"	gl_FragColor = vec4( vec3( shadowFactor ), 1.0 );\n"
#endif
#else
"	gl_FragColor = u_color;\n"
#endif
"}\n";

const char *fshLambertPhong =
S3D_PRECISION_FLOAT
"uniform sampler2D diffuseMap;\n"
"varying vec2 TexCoord;\n"
#if ( S3D_USE_LIGTHING )
S3D_BUMPMAPPING_FSHHEADER
S3D_DIFFUSE_FSHHEADER
S3D_SPECULAR_FSHHEADER
S3D_SHADOW_FSHHEADER
S3D_CUBEMAP_FSHHEADER
S3D_FSHHEADER
#endif
"\n"
"void main (void)\n"
"{\n"
"	vec4 texel = texture2D( diffuseMap, TexCoord );\n"
#if ( S3D_USE_LIGTHING )
"	vec3 nn, nv, nl;\n"
"	vec4 sumColor = vec4( 0.0, 0.0, 0.0, 0.0 );\n"
	S3D_BUMPMAPPING_FSHBODY
	S3D_CUBEMAP_FSHBODY
	S3D_DIFFUSE_FSHBODY
	S3D_SPECULAR_FSHBODY
	S3D_SHADOW_FSHBODY
	S3D_FINALLY_FSHBODY
//"	gl_FragColor = reflection;\n"
#if ( S3D_DEBUG_SHADOW )
"	gl_FragColor = vec4( vec3( shadowFactor ), 1.0 );\n"
#endif
#else
"	gl_FragColor = texel;\n"
#endif
"}\n";

const char *fshSimple =
S3D_PRECISION_FLOAT
"uniform vec4 u_color;\n"
"void main (void)\n"
"{\n"
"	gl_FragColor = u_color;\n"
"}\n";

const char *fshSprite =
S3D_PRECISION_FLOAT
"uniform sampler2D textute;\n"
"varying vec2 TexCoord;\n"
"void main (void)\n"
"{\n"
"	gl_FragColor = texture2D( textute, TexCoord );\n"
"}\n";

const char *fshDepthTexture =
S3D_PRECISION_FLOAT
"varying float depth;\n"
"void main (void) {\n"
"	vec2 moments;\n"
"	moments.x = depth;\n"
"	moments.y = moments.x * moments.x;\n"
#if ( !S3D_OGLES_2_0 )
"	//float dx = dFdx(moments.x);\n"
"	//float dy = dFdy(moments.x);\n"
"	//moments.y += 0.25 * (dx * dx + dy * dy);\n"
#endif
"	gl_FragColor = vec4(moments.x, moments.y, 1.0, 1.0);\n"
"}\n";

const char *fshFur =
S3D_PRECISION_FLOAT
"uniform sampler2D furMap;\n"
"uniform sampler2D diffuseMap;\n"
"uniform sampler2D maskMap;\n"
"uniform float shadow;\n"
"uniform float edges;\n"
"varying vec2 TexCoord1;\n"
"varying vec2 TexCoord2;\n"
#if ( S3D_USE_LIGTHING )
"uniform vec4 lightProductDiffuse;\n"
"uniform vec3 mvLightPos;\n"
"varying vec3 mvPos;\n"
"varying vec3 n; // Normal\n"
#endif
"\n"
"void main (void)\n"
"{\n"
"	vec4 color = vec4( 1.0 );\n"
"	float mask = texture2D( maskMap, TexCoord2 ).x;\n"
"	if ( mask != 0.0 )\n"
"	{\n"
"		vec4 texel = texture2D( furMap, TexCoord1 ) * texture2D( diffuseMap, TexCoord2 );\n"
#if ( S3D_USE_LIGTHING )
"       color = texel * ( 0.6 + max( dot( normalize( n ), normalize( mvLightPos - mvPos ) ), 0.0 ) );\n"
#else
"		color = texel;\n"
#endif
"		color.rgb *= ( 1.0 - TexCoord1.t ) * floor( 0.6 + edges ) + shadow;\n"
"		color.a *= 1.0 - edges;\n"
"	} else discard;\n"
"	gl_FragColor = color;\n"
"}\n";

const char *fshGaussBlur =
S3D_PRECISION_FLOAT
"uniform float weights[7];\n"
"uniform int numSamples;\n"
"uniform sampler2D texture;\n"
"uniform vec2 dx;\n"
"varying vec2 TexCoord;\n"
"void main (void)\n"
"{\n"
"	vec2 sdx = dx;\n"
"	vec4 origin = texture2D( texture, TexCoord );\n"
"	vec4 sum = origin * weights[0];\n"
"	for ( int i = 1; i < numSamples; i++ )\n"
"	{\n"
"		sum += ( texture2D( texture, TexCoord + sdx ) + texture2D( texture, TexCoord - sdx ) ) * weights[i];\n"
"		sdx += dx;\n"
"	}\n"
"	gl_FragColor = sum;\n"
"}\n";
