#ifndef _S3DPLATFORM_h_
#define _S3DPLATFORM_h_

// OpenGL Platform definitions
#if defined( S3D_IPHONE_PLATFORM )
#	include <OpenGLES/ES2/gl.h>
#	include <OpenGLES/ES2/glext.h>
#else
#	if defined( S3D_MAC_PLATFORM )
#		include <OpenGL/gl.h>
#		include <OpenGL/glext.h>
#	else
#		if defined ( S3D_X11_PLATFORM )
#			include <GL/gl.h>
#			include <GL/glx.h>
#			include <GL/glext.h>
#		else
#			if defined( S3D_ANDROID_PLATFORM )
#				include <GLES2/gl2.h>
#				include <GLES2/gl2ext.h>
#			else
#				if defined( S3D_WINDOWS_PLATFORM )
#					include <GL/gl.h>
#					include <GL/glext.h>
#				else
#					if defined( S3D_MARMALADE_SDK )
#						include <GLES2/gl2.h>
#						include <GLES2/gl2ext.h>
#					endif // S3D_MARMALADE_SDK
#				endif // S3D_WINDOWS_PLATFORM
#			endif // S3D_ANDROID_PLATFORM
#		endif // S3D_X11_PLATFORM
#	endif // S3D_MAC_PLATFORM
#endif // S3D_IPHONE_PLATFORM

#endif //_S3DPLATFORM_h_