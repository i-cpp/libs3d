#ifndef _S3D_SHADOW_H
#define _S3D_SHADOW_H

#if ( S3D_USE_SHADOWS )

class S3D_ShadowMap
{
public:
	S3D_ShadowMap();
	~S3D_ShadowMap();

	bool Create();
	void Draw( class CS3DTransformedObject **shadow_casters, unsigned int count );
	unsigned int GetSize();

	bool enabled;
	bool soft;
	CS3DShader *depthTextureShader;
	unsigned int colorTexture, depthTexture;
    float fovy, nearFactor, farFactor;

private:
	unsigned int blurTexture;
	unsigned int depthFrameBuffer, blurFrameBuffer;
	unsigned int size;
};

#endif

#endif // _S3D_SHADOW_H
