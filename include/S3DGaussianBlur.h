#ifndef _S3D_GAUSSBLUR_H
#define _S3D_GAUSSBLUR_H

class S3D_GaussianBlur
{
public:
	S3D_GaussianBlur();
	~S3D_GaussianBlur();

	bool Create( unsigned defaultPresentId );
	void SetBlurPresent( unsigned id ); // [0 -- no blur, 6 -- max blur]
	void Draw( unsigned v_fbo, unsigned v_tex, unsigned h_fbo, unsigned h_tex, unsigned size, CS3DMat16f &mvpMat );

	CS3DShader *shader;

private:
	unsigned blurPresent;
	unsigned kernelSize;
	float *weights;
};

#endif // _S3D_GAUSSBLUR_H
