#ifndef _S3D_FUR_H
#define _S3D_FUR_H

#if ( S3D_USE_LIGTHING )

class CS3DWhiskers
{
public:
	CS3DWhiskers( class CS3DModel *m, const char* filename );
	~CS3DWhiskers();
	void Draw();

private:
	S3D_Vertex AnimateVertex( S3D_Vertex in, S3D_Index index );
	S3D_Vertex CalcSpline( S3D_Vertex &start, S3D_Vertex &end, float t, 
		S3D_Vertex *ctrl_1 = 0, S3D_Vertex *ctrl_2 = 0 );

	float Lenght;
	S3D_Indices Indices;
	class CS3DModel *BindedModel;
	S3D_Color Color;
	S3D_Vertex *Geometry;
	S3D_Vertex *Transformed;
	unsigned int NumVerts;
	short NumParts;

};

#define S3D_MAX_FURSHELLS 16

class CS3DFur
{
public:
	CS3DFur( class CS3DModel *m, const char* filename );
	~CS3DFur();
	
	bool Initialized() { return bInit; }
	void Draw();
	unsigned int TrianglesCount();

private:
	bool LoadSettings( const char*filename );
	void GenShellTextures();
	void GenEdges();
	void DrawEdges();
	void GenShells();
	void DrawShells();
	void BeginFurShader( CS3DShader *shader );
	void SetAttributes( CS3DShader *shader, unsigned int buffID, unsigned int furTexID );
	void EndFurShader( CS3DShader *shader );

	unsigned int EdgeTexture;
	unsigned int MaskTexture;
	unsigned int NumShells;
	unsigned int *ShellTextures;
	unsigned int ShellTextureSize;
	unsigned int NumTriengles;

	float Lenght;
	float Scale;
	float EdgeLenght;
	unsigned int Density;
	const float ShadowCoef;

	class CS3DModel *BindedModel;
	bool bInit;

	struct S3D_FurBody {
		S3D_Vertex vertex;
		S3D_Normal normal;
		S3D_TexCoord tcoord1;
		S3D_TexCoord tcoord2;
		CS3DVec4f weights;
		CS3DVec4f indices;
	};
	unsigned int EdgeBuffer;
	unsigned int ShellBuffers[S3D_MAX_FURSHELLS];

	struct S3D_Image *MaskImg;
	CS3DWhiskers *Whiskers;

};

#endif

#endif // _S3D_FUR_H
