#ifndef _S3D_VECTOR4_H
#define _S3D_VECTOR4_H

class CS3DVec4f
{
public:
    CS3DVec4f();
    CS3DVec4f( float x, float y, float z, float w );
    CS3DVec4f( float *xyz, float w );
	CS3DVec3f xyz();
	void set( float *values );

    float &operator[]( int );
    float operator[]( int ) const;

    CS3DVec4f operator+( float ) const;
    CS3DVec4f operator-( float ) const;
    CS3DVec4f operator*( float ) const;
    CS3DVec4f operator/( float ) const;

    CS3DVec4f operator+( const CS3DVec4f & ) const;
    CS3DVec4f operator-( const CS3DVec4f & ) const;
    CS3DVec4f operator*( const CS3DVec4f & ) const;
    CS3DVec4f operator/( const CS3DVec4f & ) const;

    CS3DVec4f operator-() const;

    bool operator==( const CS3DVec4f & ) const;
    bool operator!=( const CS3DVec4f & ) const;
    bool operator!() const;

    const CS3DVec4f &operator+=( float );
    const CS3DVec4f &operator-=( float );
    const CS3DVec4f &operator*=( float );
    const CS3DVec4f &operator/=( float );

    const CS3DVec4f &operator+=( const CS3DVec4f & );
    const CS3DVec4f &operator-=( const CS3DVec4f & );
    const CS3DVec4f &operator*=( const CS3DVec4f & );
    const CS3DVec4f &operator/=( const CS3DVec4f & );

    float magnitude() const;
    float magnitudeSquared() const;
    float dot( const CS3DVec4f &other ) const;
    void normalize();
    CS3DVec4f maximize( const CS3DVec4f &other );
    CS3DVec4f minimize( const CS3DVec4f &other );
	void calculatePlane( CS3DVec3f v1, CS3DVec3f v2, CS3DVec3f v3 );

    union {
		float v[4];
		struct { float x; float y; float z; float w; };
		struct { float r; float g; float b; float a; };
	};
};

typedef CS3DVec4f S3D_Quat;
typedef CS3DVec4f S3D_Color;

namespace S3D_QuatOp {
	S3D_Quat mult( S3D_Quat &q1, S3D_Quat &q2 );
	S3D_Quat lerp( S3D_Quat q1, S3D_Quat q2, float blend );
	S3D_Quat slerp( S3D_Quat q1, S3D_Quat q2, float blend );
	void to_matrix( float m[16], const S3D_Quat &q );
	S3D_Quat from_matrix( const float m[16] );
	S3D_Quat from_angle( float angle, S3D_ENUM axis );
	CS3DVec3f rotate( CS3DVec3f &p, S3D_Quat &q );
};

#endif // _S3D_VECTOR4_H
