#ifndef _S3D_MOVEMENT_H
#define _S3D_MOVEMENT_H

class CS3DTransformedObject
{
public:
	CS3DTransformedObject();
	~CS3DTransformedObject();
	
	void StartMovement( CS3DVec3f Where, float mt, float rt, bool NonStop = false, float delay = 0.f );
	void EndMovement();
	bool IsMoving();
	void BindScene( CS3DScene *scene );
	CS3DVec3f GetPivot();
	void SetPivot( CS3DVec3f pivot );
	void SetPosition( CS3DVec3f pos );
	void SetPosition( S3D_ENUM axis, float value );
	void AddPosition( S3D_ENUM axis, float value );
	CS3DVec3f GetPosition();
	CS3DMat16f *GetRotation();
	const CS3DMat16f *GetM();
	void ApplyTransforms();
	void DrawPivot();
	void DrawSceneWalls();
	void SetTransforms( CS3DTransformedObject &other );
	virtual S3D_BoundingBox GetBBox() = 0;
	virtual S3D_BoundingBox GetBBoxTransformed() = 0;

#if ( S3D_USE_LIGTHING )
#if ( S3D_USE_SHADOWS )
	virtual void DrawShadow( const CS3DMat16f &lmvMat, const CS3DMat16f &lpMat ) = 0;
#endif
#endif

protected:
	void Move();
	void SetNormalMaxLength( float value );
	void DrawAxis( S3D_ENUM axis );

	CS3DScene *CurScene;
	CS3DVec3f Position;
	CS3DVec3f Pivot;
	CS3DMat16f rotMat;
	CS3DMat16f modelMat;
	CS3DMat16f lmvpMat;

private:
	struct S3D_Movement {
		bool NonStop;
		float tMovTime;
		unsigned int tMovStarted;
		unsigned int tMovInterval;
		CS3DVec3f *Where;
		CS3DVec3f *Whence;
		CS3DVec3f *Entrepot;

		float tRotTime;
		unsigned int tRotStarted;
		unsigned int tRotInterval;
		float SrcAngle;
		float DstAngle;
		float CurAngle;

		float tDelay;
		unsigned int tDelayStarted;

		bool Activated;
	};
	S3D_Movement Movement;
	float NormalMaxLength;

};

#endif // _S3D_MOVEMENT_H
