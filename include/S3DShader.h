#ifndef _S3D_SHADER_H
#define _S3D_SHADER_H

struct S3D_Shader
{
	S3D_Shader();
	~S3D_Shader();
	unsigned int h;
	unsigned int type;
	S3D_ENUM name;
};

class CS3DShader
{
public:
	CS3DShader();
	~CS3DShader();
	
	bool Load( S3D_ENUM vsh, S3D_ENUM fsh );

	void BeginShaderProgram();
	void EndShaderProgram();

	void EnableAttributes();
	void DisableAttributes();
	void EnableAttribute( int id );
	void DisableAttribute( int id );

	bool SetSampler( const char *, int );
	bool GetUniformf( const char *, float * );
	bool SetUniformi( const char *, int, int * );
	bool SetUniformf( const char *, int, float * );
	bool SetUniformMatrixf( const char *, int, float * );
	bool SetAttribf( const char *, int, float * );
	void SetAttribf( unsigned int, int, float * );
	void SetAttribPointerf( unsigned int, int, void *, int = 0 );
	S3D_ENUM GetShaderName( unsigned int type );

private:
	bool Init();
	bool GetShader( S3D_Shader **shader );
	bool Load( S3D_Shader *shader );
	const char *GetShaderSource( S3D_ENUM shaderName );

	unsigned int program;
	S3D_Shader *vertShader;
	S3D_Shader *fragShader;
	bool bSupported;
	char *Samplers[ S3D_MAX_SAMPLERS ];
	char *Attribs[ S3D_MAX_ATTRIBS ];

};

typedef std::vector< S3D_Shader* > S3D_Shaders;


#endif // _S3D_SHADER_H
