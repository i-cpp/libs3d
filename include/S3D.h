#ifndef _S3D_h_
#define _S3D_h_

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <vector>

#include <S3DModel.h>

// --------------------------------------------------------------------------------------
// initialization functions
// --------------------------------------------------------------------------------------
bool s3dInit();
void s3dFree();
void s3dSetIOFunctions( void *(*open_func) ( const char * ), bool  (*read_func) ( void *, void *, unsigned int ),
	void  (*close_func)( void * ), void  (*log_func) ( const char * ), void  (*delete_func) ( const char * ) );
struct S3D_Image { S3D_Image() : data(0) {} unsigned char *data; int width, height, bpp; };
void s3dSetLoadTextureFunc( unsigned int (*func)( const char *, S3D_Image *, bool ) );
void s3dSetLoadTextureFunc( unsigned int (*func)( unsigned char *, int, int, int, bool, bool ) );
void s3dSetGetProcAddressFunc( void* (*func)( const char * ) );
void s3dSetTimeFunc( unsigned int (*func)() );
// --------------------------------------------------------------------------------------
// other functions
// --------------------------------------------------------------------------------------
void s3dPerspective( float fovy, float zNear, float zFar );
void s3dOrtho( float zNear, float zFar );
bool s3dProject( CS3DVec3f *obj, CS3DVec3f *win, const float modelMat[16] );
bool s3dUnProject( CS3DVec3f *win, CS3DVec3f *obj, const float invmvpMat[16] );
CS3DVec3f s3dUnProjectGround( int srcX, int srcY );
void s3dSetViewport( int x, int y, int width, int height );

void s3dIntToStr( char *out, int number );
bool s3dInsidePoly( float poly[], int num_verts, float x, float y );
const char *s3dGetInfo( S3D_ENUM id );
void s3dSetLookupMap( int activeID );
unsigned int s3dGetTexMemAlloted();
// --------------------------------------------------------------------------------------
// OpenGL functions
// --------------------------------------------------------------------------------------
namespace S3D_OpenGLOp {
#if ( !S3D_OGLES_2_0 )
	void BindMesh( S3D_Body *body, bool useNormals, bool useTexCoords );
	void UnbindMesh();

	void EnableLighting( S3D_Light *LightSources );
	void DisableLighting( S3D_Light *LightSources );
	void SetMaterial( unsigned int face, S3D_Material *mat );
	
	void SetMatrices( const float ModelViewMat[16] );
#endif

	void DrawPrimitives( unsigned int mode, int first, int count );
	void DrawPrimitives( unsigned int mode, const S3D_Index *IDs, int count );

	void CreateTexture( unsigned int &texID, void *data, int w, int h, int bpp, bool linear = false, S3D_ENUM type = 0, bool genMipMaps = false );
	void DeleteTextures( int count, const unsigned int *ids );
	void Clamp( bool s, bool t );
	bool EnableTexture( unsigned int texID, int activeID = -1, bool cubeMap = false );
	void DisableTexture( int activeID = -1, bool cubeMap = false );
	unsigned CreateCubeMap( const char *format );
	unsigned GetBlankTexture( S3D_ENUM type = 0 );

	bool ExtensionSupported( const char *extension );

	void EnableDepthMask();
	void DisableDepthMask();

	void EnableBackFace();
	void DisableBackFace();

	void ClearFrameBuffer();
	void EnableShadowBuffer( int size );
	void DisableShadowBuffer();
    
    int GetError();
	
};

// --------------------------------------------------------------------------------------
// extern global vars
// --------------------------------------------------------------------------------------
extern CS3DBuffer *s3dBuffer;
extern CS3DIOController *s3dIOController;
extern CS3DCamera *s3dCamera;
#if ( S3D_USE_LIGTHING )
	extern CS3DLight *s3dLight;
#endif
extern S3D_Shaders *s3dLoadedShaders;
extern CS3DShader *s3dSimpleShader;
extern CS3DShader *s3dColoredMeshShader;
extern CS3DShader *s3dSpriteShader;
extern CS3DShader *s3dFurShader;
#if ( S3D_USE_SHADOWS )
	extern S3D_ShadowMap *s3dShadow;
#endif
extern S3D_GaussianBlur *s3dGaussianBlur;
extern CS3DMat16f s3dProjection;

extern unsigned int (*s3dCurTime)();
extern unsigned int (*s3dLoadTexture)( const char *filename, S3D_Image *img, bool );
extern unsigned int (*s3dLoadTextureFromData)( unsigned char *data, 
	int width, int height, int bpp, bool linearFilter, bool genMipMaps );
extern void* (*s3dGetProcAddress)( const char *name );

#endif //_S3D_h_
