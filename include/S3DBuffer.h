#ifndef _S3D_BUFF_H
#define _S3D_BUFF_H

#define S3D_VBO 0x100
#define S3D_IBO 0x101
#if ( !S3D_OGLES_2_0 )
#define S3D_VAO 0x102
#endif
#define S3D_FBO 0x103

struct S3D_VideoMemChunk {
	S3D_VideoMemChunk() : id(0), size(0) {}
	S3D_VideoMemChunk( unsigned int _id, unsigned int _sz )
	{
		id = _id; size = _sz;
	}
	unsigned int id;
	unsigned int size;
};
typedef std::vector< S3D_VideoMemChunk > S3D_TexMemChunks;

class CS3DBuffer
{
public:
	CS3DBuffer();
	~CS3DBuffer();
	
	bool Supported( unsigned int = S3D_VBO );
	void BindBuffer( unsigned int &, unsigned int = S3D_VBO );
	void UnbindBuffer( unsigned int = S3D_VBO );
	void FreeBuffer( unsigned int &, unsigned int = S3D_VBO );
	bool SetBufferData( unsigned int &id, void *data, int len,
		int offset = 0, unsigned int sizeElem = sizeof( float ), unsigned int = S3D_VBO );
	unsigned int MemAlloted();

};

#endif // _S3D_BUFF_H
