global ModelExporter

struct sSkeleton
(
	skin,
	bones = #()
)

fn CheckObject obj = 
(
	if obj != undefined then
	(
		if ( (classof obj == Editable_Mesh) or (classof obj == Editable_Poly) or (classof obj == PolyMeshObject) ) then
		(
			return "ok"
		)
	)
	return "fail"
)

-----------------------------------------------------
-- write Mesh
-----------------------------------------------------
fn WriteMesh filename obj = 
(
	local mesh = snapshotAsMesh obj
	-----------------------------------------------------
	-- create mesh-file
	-----------------------------------------------------
	f = fopen filename "wb"
	-----------------------------------------------------
	-- write  header
	-----------------------------------------------------
	if ModelExporter.chkUseTC.state == true then
		numTVerts = mesh.numTVerts
	else numTVerts = 0
	
	writelong f mesh.numVerts	-- write vertexes number
	writelong f numTVerts -- write textures coords number
	writelong f mesh.numFaces -- write triangles number
	-----------------------------------------------------
	-- write geometry
	-----------------------------------------------------
	for i = 1 to mesh.numVerts do at time AnimationRange.start
	(
		-- write vertex
		local v = in coordsys world (GetVert mesh i)
		writefloat f v.x
		writefloat f v.y
		writefloat f v.z
	)
	-----------------------------------------------------
	-- write fases
	-----------------------------------------------------
	for i = 1 to mesh.numFaces do
	(
		--write fase
		writeshort f (((getFace mesh i).x) - 1) #unsigned
		writeshort f (((getFace mesh i).y) - 1) #unsigned
		writeshort f (((getFace mesh i).z) - 1) #unsigned
	)
	-----------------------------------------------------
	-- write texture vertexes and tvfases
	-----------------------------------------------------			
	if numTVerts != 0 then
	(
		for i = 1 to mesh.numTVerts do
		(
			writefloat f ((getTVert mesh i).x)
			writefloat f (1 - (getTVert mesh i).y) 
		)
		for i = 1 to mesh.numFaces do
		(
			--write tvfase
			writeshort f (((getTVFace mesh i).x) - 1) #unsigned
			writeshort f (((getTVFace mesh i).y) - 1) #unsigned
			writeshort f (((getTVFace mesh i).z) - 1) #unsigned
		)
	)
	-----------------------------------------------------
	--write normals
	-----------------------------------------------------
	for i = 1 to mesh.numFaces do
	(
		normals = #();
		if (getFaceSmoothGroup mesh i) == 0 then
		(
			normals[1] = getFaceNormal mesh i
			normals[2] = getFaceNormal mesh i
			normals[3] = getFaceNormal mesh i
		)
		else normals = meshop.getFaceRNormals mesh i
		
		writefloat f normals[1].x
		writefloat f normals[1].y
		writefloat f normals[1].z
		
		writefloat f normals[2].x
		writefloat f normals[2].y
		writefloat f normals[2].z
		
		writefloat f normals[3].x
		writefloat f normals[3].y
		writefloat f normals[3].z
	)
	-----------------------------------------------------
		-- write  material
	-----------------------------------------------------
	if obj.material != undefined then
	(
		local materials = #()
		if classof obj.material == StandardMaterial then
		(
			append materials obj.material
		)
		else if classof obj.material == MultiMaterial then
		(
			for i = 1 to obj.material.numsubs do
			(
				append materials obj.material[i]
			)
		)
		
		writebyte f materials.count
		for mat in materials do 
		(
			-----------------------------------------------------
			-- write  ambient
			-----------------------------------------------------
			writefloat f (mat.ambient.r / 255.0)
			writefloat f (mat.ambient.g / 255.0)
			writefloat f (mat.ambient.b / 255.0)
			writefloat f (mat.ambient.a / 255.0)
			-----------------------------------------------------
			-- write  diffuse
			-----------------------------------------------------
			writefloat f (mat.diffuse.r / 255.0)
			writefloat f (mat.diffuse.g / 255.0)
			writefloat f (mat.diffuse.b / 255.0)
			writefloat f (mat.diffuse.a / 255.0)
			-----------------------------------------------------
			-- write  specular
			-----------------------------------------------------
			local specFactor
			local max_specLevel = 100.0
			if mat.specularLevel > max_specLevel then specFactor = 1.0
			else specFactor = mat.specularLevel / max_specLevel
			
			local specular = ((mat.specular / 255.0) * specFactor) as point3
			writefloat f specular.x
			writefloat f specular.y
			writefloat f specular.z
			writefloat f (mat.specular.a / 255.0)
			-----------------------------------------------------
			-- write  emission
			-----------------------------------------------------
			local emission = undefined
			if mat.useSelfIllumColor == true then
				emission = mat.SelfIllumColor
			else
				emission = mat.diffuse * ( mat.selfIllumAmount / 100.0 )
			writefloat f (emission.r / 255.0)
			writefloat f (emission.g / 255.0)
			writefloat f (emission.b / 255.0)
			writefloat f (emission.a / 255.0)
			-----------------------------------------------------
			-- write  shininess
			-----------------------------------------------------
			local shininess = ( ( mat.Glossiness / 100.0 ) * 128.0 ) as Integer 
			writeshort f shininess
			
			--local mapChannelTwo = false
			local mapName = ""
			if mat.diffuseMapEnable == true and classof mat.diffuseMap == BitmapTexture then
			(
				mapName = getFilenameFile mat.diffuseMap.filename
				writeshort f mapName.count
				writestring f mapName
				--if mat.diffuseMap.coords.mapChannel == 2 then
				--	mapChannelTwo = true
			)
			else writeshort f 0
			if mat.specularMapEnable == true and classof mat.specularMap == BitmapTexture  then
			(
				mapName = getFilenameFile mat.specularMap.filename
				writeshort f mapName.count
				writestring f mapName
			)
			else writeshort f 0
			if mat.bumpMapEnable == true and classof mat.bumpMap == BitmapTexture  then
			(
				mapName = getFilenameFile mat.bumpMap.filename
				writeshort f mapName.count
				writestring f mapName
				writefloat f (mat.bumpMapAmount / 100.0)
			)
			else if mat.bumpMapEnable == true and classof mat.bumpMap == Normal_Bump  then
			(
				mapName = getFilenameFile mat.bumpMap.normal.filename
				writeshort f mapName.count
				writestring f mapName
				writefloat f (mat.bumpMapAmount / 100.0)
			)
			else writeshort f 0
		)

		if materials.count > 1 then
		(
			for i = 1 to mesh.numFaces do
			(
				writebyte f ((getFaceMatID mesh i) - 1)
			)
		)
		/*
		local otherChannel = 2
		if ModelExporter.chkUseTC.state == true and mapChannelTwo == true then
		(
			writebyte f 1
			-----------------------------------------------------
			-- write texture coords for other map
			-----------------------------------------------------
			local numTVerts = meshop.getNumMapVerts mesh otherChannel
			for i = 1 to numTVerts do
			(
				
				writefloat f ((meshop.getMapVert mesh otherChannel i).x)
				writefloat f (1 - (meshop.getMapVert mesh otherChannel i).y) 
			)
			local numFaces = meshop.getNumMapFaces mesh otherChannel
			for i = 1 to numFaces do
			(
				writeshort f (((meshop.getMapFace mesh otherChannel i).x) - 1) #unsigned
				writeshort f (((meshop.getMapFace mesh otherChannel i).y) - 1) #unsigned
				writeshort f (((meshop.getMapFace mesh otherChannel i).z) - 1) #unsigned
			)
		)
		else writebyte f 0
		*/
	)
	else writebyte f 0
	fclose f
	print ("OK! Object \"" + filename + "\" was saved")
	return "ok"
)

-----------------------------------------------------
-- write Animation
-----------------------------------------------------
fn WriteVertAnim filename obj first_frame last_frame =
(
	f = fopen filename "wb"
	-----------------------------------------------------
	-- write header
	-----------------------------------------------------
	writebyte f 0x4d -- animation type Vertex Animation
	writelong f (last_frame - first_frame + 1) -- frames number
	writelong f frameRate -- write fps
	-----------------------------------------------------
	-- write frames
	-----------------------------------------------------
	local mesh
	for j = first_frame to last_frame do 
	(
		mesh = at time j snapShotAsMesh obj
		for i = 1 to mesh.numVerts do at time j
		(
			-- write vertex
			local v = in coordsys world (GetVert mesh i)
			writefloat f v.x
			writefloat f v.y
			writefloat f v.z
			-- write normal
			local vn = in coordsys world (GetNormal mesh i)
			writefloat f vn.x
			writefloat f vn.y
			writefloat f vn.z
		)
	)
	fclose f
	print ("OK! Animation \"" + filename + "\" was saved")
	return "ok"
)

fn GetBones obj =
(
	-----------------------------------------------------
	-- find Skin
	-----------------------------------------------------
	skin = undefined
	mesh = undefined
	if ( canConvertTo obj TriMeshGeometry ) then
	(
		for i = 1 to obj.modifiers.count do
		(
			if obj.modifiers[i].Name == "Skin" then
			(
				mesh = obj
				skin = obj.modifiers[i]
			)
		)
	)
	else
	(
		print ("WARNING! " + obj.name + " is not a valid object for export")
		return undefined
	)
	if skin == undefined then
	(
		print ("ERROR! Skin is not found in object " + obj.name)
		return undefined
	)
	-----------------------------------------------------
	-- select Skin
	-----------------------------------------------------
	max modify mode
	modPanel.setCurrentObject skin
	-----------------------------------------------------
	-- check numVerts
	-----------------------------------------------------
	if mesh.numVerts != skinOps.GetNumberVertices skin then
	(
		fclose f
		print ("ERROR! The number vertices in object is not equal to the number vertices in Skin")
		return undefined
	)
	-----------------------------------------------------
	-- find Bones
	-----------------------------------------------------
	skeleton = sSkeleton()
	skeleton.skin = skin
	/*bones = for o in objects where (refs.dependencyLoopTest skin o) collect o
	local numBonesOfTheSkin = skinOps.GetNumberBones skin
	for i = 1 to numBonesOfTheSkin do
	(
		for bone in bones do
		(
			local bone_name = skinOps.GetBoneName skin i 0
			if bone_name == bone.Name then
			(
				append skeleton.bones bone
				continue
			)
		)
	)*/
	skeleton.bones = for o in objects where (refs.dependencyLoopTest skin o) collect o

	return skeleton
)

fn WriteSkeleton filename obj =
(
	-- fill struct MySkeleton
	skeleton = GetBones obj
	if skeleton == undefined then return "fail"
	
	f = fopen filename "wb"
	-----------------------------------------------------
	-- write header
	-----------------------------------------------------
	writelong f skeleton.bones.count -- write bones number
	vertex_count = skinOps.GetNumberVertices skeleton.skin
	writelong f vertex_count -- write verts number
	-----------------------------------------------------
	-- write weights
	-----------------------------------------------------
	for i = 1 to vertex_count do
	(
		local weight_count = skinOps.getVertexWeightCount skeleton.skin i
		if weight_count > 4 then
		(
			fclose f
			print ("ERROR! Bone Affect Limited should be no more than 4")
			return "fail"
		)
		writelong f weight_count
		for j = 1 to weight_count do
		(
			local bone_name = skinOps.GetBoneName skeleton.skin (skinOps.getVertexWeightBoneID skeleton.skin i j) 0
			local bone_id = undefined
			for id = 1 to skeleton.bones.count do
			(
				if skeleton.bones[id].Name == bone_name then
					bone_id = (id - 1)
			)
			writeshort f  bone_id
			writefloat f (skinOps.getVertexWeight skeleton.skin i j)
		)
	)
	-----------------------------------------------------
	-- write bones
	-----------------------------------------------------
	for bone in skeleton.bones do --at time AnimationRange.start
	(
		-- write name
		writeshort f bone.Name.count
		writestring f bone.Name
		-- write parent name
		local parent_id
		if bone.Parent != undefined then
		(
			for id = 1 to skeleton.bones.count do
			(
				if bone.Parent.Name == skeleton.bones[id].Name then
				(
					parent_id = id - 1
					break
				)
			)
		)
		if parent_id == undefined then
		(
			parent_id = -1
		)
		writeshort f parent_id
		-- write joint
		local boneBindTM = skinUtils.GetBoneBindTM obj bone
		--local boneJoint = bone.objectTransform.position
		local boneJoint = boneBindTM.position
		writefloat f boneJoint.x
		writefloat f boneJoint.y
		writefloat f boneJoint.z
		-- write orient
		--local boneOrient = bone.objectTransform.rotation
		local boneOrient = boneBindTM.rotation
		writefloat f boneOrient.x
		writefloat f boneOrient.y
		writefloat f boneOrient.z
		writefloat f boneOrient.w
	)
	fclose f
	max utility mode
	print ("OK! Skeleton \"" + filename + "\" was saved")
	return "ok"
)

fn WriteSkinAnim filename obj first_frame last_frame =
(
	-- fill struct MySkeleton
	skeleton = GetBones obj
	if skeleton == undefined then return "fail"
	
	f = fopen filename "wb"
	-----------------------------------------------------
	-- write header
	-----------------------------------------------------
	writebyte f 0x53 -- animation type Skin Animation
	writelong f (last_frame - first_frame + 1) -- write frames number
	writelong f frameRate -- write fps
	-----------------------------------------------------
	-- write transformations
	-----------------------------------------------------
	--local numBonesOfTheSkin = skinOps.GetNumberBones skeleton.skin
	for bone in skeleton.bones do
	(
		m0 = skinUtils.GetBoneBindTM obj bone
		m0 = inverse m0
		for frame = first_frame to last_frame do at time frame
		(
			-- get transformation
			m = m0 * bone.objectTransform
			-- write position
			writefloat f m.position.x
			writefloat f m.position.y
			writefloat f m.position.z
			-- write quaternion
			writefloat f m.rotation.x
			writefloat f m.rotation.y
			writefloat f m.rotation.z
			writefloat f m.rotation.w
		)
	)
	fclose f
	max utility mode
	print ("OK! Animation \"" + filename + "\" was saved")
	return "ok"
)

utility ModelExporter "Model Exporter"
(
	radiobuttons radHowExp labels:#( "Export Selected Object", "Export All Objects" )
	group  "Mesh" 
	(
		button btnExportMesh "Export Mesh"
		checkbox chkUseTC "With Texture Coords" checked: true
	)
	group  "Skeleton" 
	(
		button btnExportSkeleton "Export Skeleton"
	)
	group  "Animation" 
	(
		radiobuttons radUseAnim labels:#( "Active Time Segment", "Custom Time Segment" )
		spinner sprAnimStart range:[ 0, 1000, 0 ] type:#integer fieldwidth:40 across:2
		spinner sprAnimEnd "to" range:[ 0, 1000, 100] type:#integer fieldwidth:40
		radiobuttons radAnimType labels:#( "Skin Animation", "Vertex Animation" )
		edittext txtAnimPrefix "Name prefix:" fieldWidth:137 labelOnTop:true
		button btnExportAnim "Export Animation"
	)
	
	on btnExportMesh pressed do
	(
		if radHowExp.state == 1 then
		(
			filename = GetSaveFileName caption:"Save Mesh" filename: $.name types:"Mesh (*.msh)|*.msh|"
			if filename != undefined then
			(
				actionMan.executeAction 0 "40472" -- Open the Listener
				res = CheckObject $
				if res == "ok" then WriteMesh filename $
				else  print ("WARNING! " + $.name + " is not a valid object for export")
			)
		)
		else
		(
			path = getSavePath "Save Meshes"
			if path != undefined then
			(
				actionMan.executeAction 0 "40472" -- Open the Listener
				for obj in objects do
				(
					res = CheckObject obj
					if res == "ok" then
					(
						filename = path + "\\" + obj.name + ".msh"
						WriteMesh filename obj
					)
					else  print ("WARNING! " + obj.name + " is not a valid object for export")
				)
			)
		)
	)
	
	on btnExportSkeleton pressed do
	(
		if radHowExp.state == 1 then
		(
			filename = GetSaveFileName caption:"Save Skeleton" filename: $.name types:"Skeleton (*.skn)|*.skn|"
			if filename != undefined then
			(
				actionMan.executeAction 0 "40472" -- Open the Listener
				res = CheckObject $
				if res == "ok" then WriteSkeleton filename $
				else  print ("WARNING! " + $.name + " is not a valid object for export")
			)
		)
		else
		(
			path = getSavePath "Save Skeletons"
			if path != undefined then
			(
				actionMan.executeAction 0 "40472" -- Open the Listener
				for obj in objects do
				(
					res = CheckObject obj
					if res == "ok" then
					(
						filename = path + "\\" + obj.name + ".skn"
						WriteSkeleton filename obj
					)
					else  print ("WARNING! " + obj.name + " is not a valid object for export")
				)
			)
		)
	)
	
	on btnExportAnim pressed do
	(
		-----------------------------------------------------
		-- get animation range
		-----------------------------------------------------
		if ModelExporter.radUseAnim.state == 1 then 
		(
			first_frame = AnimationRange.start
			last_frame = AnimationRange.end
		)
		else
		(
			first_frame = ModelExporter.sprAnimStart.value
			last_frame = ModelExporter.sprAnimEnd.value
		)
		if radHowExp.state == 1 then
		(
			filename = GetSaveFileName caption:"Save Animation" filename: (txtAnimPrefix.text + "_" + $.name) types:"Animation (*.anm)|*.anm|"
			if filename != undefined then
			(
				actionMan.executeAction 0 "40472" -- Open the Listener
				res = CheckObject $
				if res == "ok" then
				(
					if radAnimType.state == 1 then WriteSkinAnim filename $ first_frame last_frame
					else WriteVertAnim filename $ first_frame last_frame
				)
				else  print ($.name + " is not a valid object for export")
			)
		)
		else
		(
			path = getSavePath "Save Animations"
			if path != undefined then 
			(
				actionMan.executeAction 0 "40472" -- Open the Listener
				for obj in objects do
				(
					res = CheckObject obj
					if res == "ok" then
					(
						filename = path + "\\" + txtAnimPrefix.text + "_" + obj.name + ".anm"
						if radAnimType.state == 1 then WriteSkinAnim filename obj first_frame last_frame
						else WriteVertAnim filename obj first_frame last_frame
					)
					else  print ("WARNING! " + obj.name + " is not a valid object for export")
				)
			)
		)
	)
)
